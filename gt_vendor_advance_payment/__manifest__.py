# -*- coding: utf-8 -*-
{
    'name': "gt_vendor_advance_payment",

    'summary': """
        vendor advance payment in purchase module""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Globalteckz",
    'website': "http://www.globalteckz.com",


    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','purchase'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'wizard/purchase_make_invoice_advance_view.xml',
        'views/views.xml',
        
    ],
  
}