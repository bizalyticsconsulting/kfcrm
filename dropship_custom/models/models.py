# -*- coding: utf-8 -*-

from odoo import models, fields, api

class DropshipCustom(models.Model):
    _name = 'dropship.custom'
    _description='Dropship Custom'

    dropship_id = fields.Many2one('res.partner')
    vendor=fields.Many2one("res.partner","Vendor",domain=[("supplier","=",True)])

    ship_via = fields.Many2one('delivery.carrier','Ship Via')
    terms = fields.Many2one('account.payment.term', string="Terms", size=100)
    freight_terms = fields.Many2one('account.payment.term','Freight Terms')
    account_no = fields.Char(string="Account #")
    note = fields.Char(string="Note")

class DropshipCustomShipVia(models.Model):

    _name = 'dropship.custom.shipvia'

    name = fields.Char("Name")


class ResPartner(models.Model):
    _inherit = "res.partner"

    dropship_ids = fields.One2many('dropship.custom','dropship_id','Dropship')

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    ship_via = fields.Many2one('delivery.carrier','Ship Via')
    account_no = fields.Char("Account No",compute="get_detail")
    terms = fields.Many2one('account.payment.term',string="Terms", size=100)
    freight_terms = fields.Many2one('account.payment.term','Freight Terms')
    note = fields.Char(string="Note", size=200)

    @api.multi
    def get_detail(self):
        cus_dropship = self.env['dropship.custom']
        dropship_line_detail = ""
        for line in self:
            vendor = line.product_id.seller_ids and line.product_id.seller_ids[0].name
            if vendor:
                dropship_line = cus_dropship.search([('dropship_id', '=', line.order_id.partner_id.id),('vendor', '=', vendor.id)])
                if len(dropship_line) > 0:
                    dropship_line_detail = dropship_line[0]

                if dropship_line_detail:
                    line.account_no     = dropship_line_detail.account_no
                    line.ship_via       = dropship_line_detail.ship_via.id
                    line.terms          = dropship_line_detail.terms.id
                    line.freight_terms  = dropship_line_detail.freight_terms.id
                    line.note           = dropship_line_detail.note

                    line.order_id.x_ship_via       = dropship_line_detail.ship_via.id
                    line.order_id.x_freight_terms  = dropship_line_detail.freight_terms.id
                    line.order_id.x_client_account = dropship_line_detail.account_no
                    line.order_id.x_showroom_terms = dropship_line_detail.terms.id


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    ship_via = fields.Many2one('delivery.carrier','Ship Via', compute="get_detail")
    account_no = fields.Char("Account No", compute="get_detail")
    terms = fields.Many2one('account.payment.term',string="Terms", compute="get_detail")
    freight_terms = fields.Many2one('account.payment.term','Freight Terms', compute="get_detail")
    note = fields.Char(string="Note", compute="get_detail")

    @api.multi
    def get_detail(self):
        # pro_obj = self.env['procurement.order']
        for line in self:
            # p_order = pro_obj.sudo().search([('purchase_line_id', '=', line.id)])
            print(line.sale_line_id,line.sale_order_id)
            if line.sale_line_id:
                    line.account_no = line.sale_line_id.account_no
                    line.ship_via = line.sale_line_id.ship_via.id
                    line.terms = line.sale_line_id.terms.id
                    line.freight_terms = line.sale_line_id.freight_terms.id
                    line.note = line.sale_line_id.note
