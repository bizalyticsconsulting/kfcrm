{
    'name': 'Hide Any Report From Any User or Any Group',
    'version': '1.0',
    'author': 'FreelancerApps',
    'category': 'Tools',
    'depends': ['base'],
    'summary': 'Hide Any Report From Any User or Any Group and its user',
    'description': """
Hide Any Report From Any User or Any Group
==========================================
* Go to setting --> Users --> Users --> Report To Hide --> Select any report that you want to hide for this user. --> Just save and refresh page.
* Go to setting --> Users --> Groups --> Report To Hide --> Select any report that you want to hide for this group. --> Just save and refresh page.
* Go to setting --> Technical --> Actions --> Reports --> Select Report --> Hide From Users --> Select User that you want to hide this report.
* Go to setting --> Technical --> Actions --> Reports --> Select Report --> Hide From Groups --> Select Group that you want to hide this report.


Key features:
-------------
* Easy To Use
* User Wise Hide Report Setting
* Group Wise Hide Report Setting
""",
    'data': ['view/hide_report_view.xml'],
    'images': ['static/description/hide_any_report_banner.png'],
    'price': 4.99,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'installable': True,
    'auto_install': False,
}
