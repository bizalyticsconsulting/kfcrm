# -*- coding: utf-8 -*-
from odoo import fields, models


class Users(models.Model):
    _inherit = 'res.users'

    report_ids = fields.Many2many('ir.actions.report', 'user_report_rel', 'user_id', 'report_id', 'Report To Hide',
                                  help='Select Report To Hide From This User')


class ResGroups(models.Model):
    _inherit = 'res.groups'

    report_ids = fields.Many2many('ir.actions.report', 'group_report_rel', 'group_id', 'report_id', 'Report To Hide',
                                  help='Select Report To Hide From This User')


class IrActionsReport(models.Model):
    _inherit = 'ir.actions.report'

    hide_user_ids = fields.Many2many('res.users', 'user_report_rel', 'report_id', 'user_id', string='Hide From Users')
    hide_group_ids = fields.Many2many('res.groups', 'group_report_rel', 'report_id', 'group_id', string='Hide From Groups')
