# -*- coding: utf-8 -*-
###############################################################################
#
#   Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#   Copyright (C) 2016-today Geminate Consultancy Services (<http://geminatecs.com>).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

{
    'name': 'Archive Connector',
    'summary': 'Archive Purchase and Sales related Documents',
    'author': 'Geminate Consultancy Services',
    'company': 'Geminate Consultancy Services',
    'website': 'https://www.geminatecs.com/',
    'category': 'Purchase',
    'version': '12.0.0.1',
    'license': 'Other proprietary',
    'depends': ['purchase','sale','stock','account'
        
    ],
    'description': """ 
            Archive Connector
     """,
    'data': [
        'security/ir.model.access.csv',
        'wizard/stock_picking_archive_data.xml',
        'wizard/stock_picking_unarchive_data.xml',
        'wizard/account_vendor_archive.xml',
        'wizard/account_vendor_unarchive.xml',
        'wizard/account_payment_archive.xml',
        'wizard/account_payment_unarchive.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application' : True,
    "images": ['static/description/connector.png'],
    'price': 9.99,
    'currency': 'EUR'
}
