# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
import pymssql
from odoo.exceptions import UserError

class ImportData(models.TransientModel):

    _name = 'import.data.wizard'
    
    config_ids=fields.Many2many('config.config','config_wizard_rel','config_id','wizard_id',string='Studio Instance')
    sales_categories = fields.Boolean('Sales Categories')
    vendors = fields.Boolean('Vendors')
    product_purchased = fields.Boolean('Products')
    employees = fields.Boolean('Employees')
    sales_purchase_tax_locations = fields.Boolean('Sales Purchase Tax Locations')
    ship_via_delivery_carrier = fields.Boolean('Ship Via delivery carrier')
    terms = fields.Boolean('Terms')
    reorder_rule = fields.Boolean('Create Reorder Rules')
    customers = fields.Boolean('Customers')
    address_contacts = fields.Boolean('Address Contacts')
    confirm_sales_orders = fields.Boolean('Confirmed Sales Orders')
    quotes = fields.Boolean('Quotes')
    rfq = fields.Boolean('RFQ')
    purchase_orders = fields.Boolean('Purchase Orders')
    salesperson2 = fields.Boolean('Update Salesperson 2')
    update_ship_to = fields.Boolean('Update Ship To')
    po_notes = fields.Boolean('Update Notes')
    import_showroom_customers_data = fields.Boolean('Import Customers with Showroom Prefix')
    import_showroom_address_contacts = fields.Boolean('Import Address Contacts WSP')
    product_manufactured = fields.Boolean('Products Manufactured')
    locations = fields.Boolean('Locations')
    dropship_custom = fields.Boolean('DropShip Custom')
    dropship_with_customers = fields.Boolean('DropShip Custom with Showroom Prefix')


    @api.one
    def import_datas(self):
        partner_obj = self.env['res.partner']
        sale_categ_obj = self.env['product.category']
        prod_temp_obj = self.env['product.template']
        config_id = self.env['config.config'].search([('type','=','import')],limit=1)
        stock_loc_obj = self.env['stock.location']
        user_obj = self.env['res.users']
        partner_obj = self.env['res.partner']
        delivery_obj = self.env['delivery.carrier']
        tax_obj = self.env['account.tax']
        payment_terms_obj = self.env['account.payment.term']
        sale_order_obj = self.env['sale.order']
        puchase_obj = self.env['purchase.order']
        dropship_obj = self.env['dropship.custom']
        if not config_id:
            raise UserError(_("Please create studio instance with type import for Mssql database."))
        if config_id:
            db = pymssql.connect(host=config_id[0].server,
                                 user=config_id[0].user_name,
                                 password=config_id[0].password,
                                 database=config_id[0].db_name)
            cursor = db.cursor()
            if self.sales_categories:
                sale_categ_obj.import_sale_category(cursor)
            if self.vendors:
                partner_obj.import_vendors_data(cursor)
            if self.product_purchased:
                prod_temp_obj.import_product_purchased_data(cursor)
        #
            if self.employees:
                user_obj.import_users_data(cursor)
            if self.locations:
                stock_loc_obj.import_stock_location_data(cursor)
            if self.customers:
                partner_obj.import_customers_data(cursor)
            if self.ship_via_delivery_carrier:
                delivery_obj.import_delivery_data(cursor)
            if self.sales_purchase_tax_locations:
                tax_obj.import_tax_location(cursor)
            if self.terms:
                payment_terms_obj.import_payment_terms(cursor)
            if self.address_contacts:
                partner_obj.import_address_contacts(cursor)
            if self.product_manufactured:
                prod_temp_obj.import_product_manufactured_data(cursor)
            # if self.product_purchased:
            #     prod_temp_obj.with_context({'create_reordering_rule':self.reorder_rule}).import_product_purchased_data(cursor)
            if self.confirm_sales_orders:
                sale_order_obj.import_confirm_sale_orders(cursor)
            if self.quotes:
                sale_order_obj.import_quotes(cursor)
            if self.purchase_orders:
                puchase_obj.import_purchase_orders(cursor)
            if self.rfq:
                puchase_obj.import_rfq(cursor)
            if self.dropship_custom:
                dropship_obj.import_dropship_custom_data(cursor)
            if self.salesperson2:
                sale_order_obj.update_salesperson2(cursor)
            if self.update_ship_to:
                sale_order_obj.update_ship_to(cursor)
            if self.po_notes:
                sale_order_obj.po_notes(cursor)
            if self.import_showroom_customers_data:
                partner_obj.import_showroom_customers_data(cursor)
            if self.import_showroom_address_contacts:
                partner_obj.import_address_contacts_with_showroom(cursor)
            if self.dropship_with_customers:
                dropship_obj.import_dropship_custom_data_with_showroom(cursor)

        return True


    @api.one
    def update_datas(self):
        partner_obj = self.env['res.partner']
        sale_categ_obj = self.env['product.category']
        prod_temp_obj = self.env['product.template']
        if not self.config_ids:
            raise UserError(_("Please select atleast one studio instance for Update/Export."))
        for config in self.config_ids:
            db = pymssql.connect(host=config.server,
                                 user=config.user_name,
                                 password=config.password,
                                 database=config.db_name)
            cursor = db.cursor()
            if self.sales_categories:
                sale_categ_obj.update_sale_category(cursor,db)
            if self.vendors:
                partner_obj.update_vendors_data(cursor,db)
            if self.product_purchased:
                prod_temp_obj.update_product_purchased_data(cursor,db)
        return True
    
    
    @api.one
    def export_datas(self):
        partner_obj = self.env['res.partner']
        sale_categ_obj = self.env['product.category']
        prod_temp_obj = self.env['product.template']
        if not self.config_ids:
            raise UserError(_("Please select atleast one studio instance for Update/Export."))
        for config in self.config_ids:
            db = pymssql.connect(host=config.server,
                                 user=config.user_name,
                                 password=config.password,
                                 database=config.db_name)
            cursor = db.cursor()
            if self.sales_categories:
                sale_categ_obj.export_sale_category(cursor,db,config)
            if self.vendors:
                partner_obj.export_vendors_data(cursor,db,config)
            if self.product_purchased:
                prod_temp_obj.export_product_purchased_data(cursor,db,config)

        return True