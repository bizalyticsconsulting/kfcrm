update product_template set description_purchase = FOO.product_description, description_sale = FOO.product_description
from
(
select product_tmpl_id, string_agg(product_catalog_details.desc, '<br />' ORDER BY product_catalog_details.sequence) as product_description from product_catalog_details
group by product_tmpl_id 
) AS FOO
where product_template.id = FOO.product_tmpl_id

delete from product_catalog_details;