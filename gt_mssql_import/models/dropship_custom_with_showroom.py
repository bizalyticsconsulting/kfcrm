# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
import logging
from odoo.tools.safe_eval import safe_eval
logger = logging.getLogger(__name__)


class ShipViaDeliveryWithShowroom(models.Model):
    _inherit = 'dropship.custom'

    @api.multi
    def import_dropship_custom_data_with_showroom(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']
        partner_obj = self.env['res.partner']

        seq_obj = self.env['ir.sequence']

        seq_ids = seq_obj.search([('code', '=', 'sale.order')], limit=1)
        showroom_prefix = ""
        if seq_ids:
            showroom_prefix = seq_ids[0].prefix

        # Query to get all Carriers from mssql
        conn.execute("SELECT * from [Address Accounts]")
        conn_data = conn.fetchall()
        logger.info("Total Number of Dropship Custom Ids getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                # Log if Description is not available
                customer_name = showroom_prefix + '-' + data[0]
                ir_model_dropship_customer_id = ir_model_data_obj.search(
                    [('name', '=', customer_name), ('model', '=', 'res.partner'), ('module', '=', 'ms_customer')])
                if ir_model_dropship_customer_id:
                    customer_id = partner_obj.browse(ir_model_dropship_customer_id[0].res_id)
                    if not customer_id:
                        log_vals = {'operation': 'dropship_custom',
                                    'message': "Customer Id not found for DropShip Custom  {}".format(data[0]),
                                    'date': datetime.now()}
                        log_obj.create(log_vals)
                        continue
                    else:
                        term = False
                        freight_term = False
                        if data[3]:
                            terms = self.env['account.payment.term'].search([('name','=',data[3])])
                            logger.debug("looking for term ..... %s" % data[3])
                            if terms:
                                term = terms[0].id
                            else:
                                term = self.env['account.payment.term'].create({'name': data[3]}).id
                        if data[4]:
                            freight_terms = self.env['account.payment.term'].search([('name', '=', data[4])])
                            logger.debug("looking for term ..... %s" % data[4])
                            if freight_terms:
                                freight_term = freight_terms[0].id
                            else:
                                freight_term = self.env['account.payment.term'].create({'name': data[4]}).id
                        dropship_vals = {
                            'terms': term,
                            'freight_terms': freight_term,
                            'account_no': data[5],
                            'note': data[6],
                        }

                        dropship_vals.update({'dropship_id': customer_id[0].id})
                        if data[2]:
                            ship_via_id = self.env['delivery.carrier'].search([('name', '=', data[2])])
                            if not ship_via_id:
                                products = self.env['product.product'].search([('name', '=', data[2])])
                                if products:
                                    product = products[0].id
                                else:
                                    product = self.env['product.product'].create({'name': data[2], 'type': 'service'}).id
                                ship_via_id = self.env['delivery.carrier'].create({'name': data[2],'product_id':product})
                                dropship_vals.update({'ship_via': ship_via_id.id})
                            else:
                                dropship_vals.update({'ship_via': ship_via_id[0].id})

                        if data[1]:
                            ir_model_dropship_vendor_id = ir_model_data_obj.search(
                                [('name', '=', data[1]), ('model', '=', 'res.partner'),('module','=','ms_vendor')])
                            if ir_model_dropship_vendor_id:
                                vendor_id = partner_obj.browse(ir_model_dropship_vendor_id[0].res_id)
                                dropship_vals.update({'vendor': vendor_id[0].id})

                        dropship_ids = self.search(
                            [('freight_terms', '=', freight_term), ('terms', '=', term),
                             ('dropship_id', '=', customer_id.id), ('note', '=', data[6]), ('account_no', '=', data[5])])
                        if dropship_ids:
                            logger.info("Dropship Ids Updated ----> {}".format(dropship_ids))
                            dropship_ids[0].write(dropship_vals)
                        else:
                            dropship_ids = self.create(dropship_vals)
                            logger.info("Dropship Ids Created ----> {}".format(dropship_ids))
                self.env.cr.commit()
        except Exception as e:
            log_vals = {'operation': 'add_contacts', 'message': e, 'date': datetime.now()}
            log_obj.create(log_vals)
        return True