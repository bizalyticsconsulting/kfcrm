
from odoo import fields, models

class LogManagement(models.Model):
    _name = "log.management"
    _order= 'create_date desc'
    
    operation = fields.Selection([('user','Employees'),('tax_location','Sales Purchase Tax Location'),('ship_via_del','Ship Via Delivery'),
            ('terms','Terms'),('sales_categ','Sales Categories'),('locations','Locations'),('vendors','Vendors'),('p_manufactured','Product Manufactured')
             ,('p_purchased','Product Purchased'),('add_contacts','Address Contacts'),('con_sale','Confirmed SaleOrder'),('customers','Customers'),
            ('quotes','Quotes'),('q_lineitem','Quote Line Item'),('sale_order_lineitem','Sale Order LineItem'),
            ('rfq', 'RFQ'), ('rfq_lineitem', 'RFQ Line Item'),('purchase_order', 'PurchaseOrder'),('pur_lineitem','Purchase Line Item'),('bom','BOM'), ('bom_line','BOM Lines'),
            ('cust_inv','Customer Invoices'),('ven_bill','Vendor Bills'),('dropship_custom','Dropship Custom')] ,string="Operation")
    message = fields.Char(string='Message')
    date = fields.Datetime('Date',default=fields.Date.context_today)


