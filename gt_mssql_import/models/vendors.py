# -*- coding: utf-8 -*-
from odoo import models,fields, api
from datetime import datetime
import logging
import pymssql
logger = logging.getLogger(__name__)


class ImportVendorData(models.Model):
    _inherit = 'res.partner'
    
#    Cron for update and export
    @api.model
    def cron_update_export_vendores(self):
        config_ids = self.env['config.config'].search([('type','=','export')])
        for config in config_ids:
            db = pymssql.connect(host=config.server,
                                 user=config.user_name,
                                 password=config.password,
                                 database=config.db_name)
            cursor = db.cursor()
            self.update_vendors_data(cursor,db)
#            db = pymssql.connect(host=config.server,
#                                 user=config.user_name,
#                                 password=config.password,
#                                 database=config.db_name)
#            cursor = db.cursor()
            self.export_vendors_data(cursor,db,config)
        
    
    partner_code=fields.Char('Partner Code',copy=False)

    def createVendor(self, vender_data):
        ir_model_data_obj = self.env['ir.model.data']
        country_obj = self.env['res.country']
        res_state_obj = self.env['res.country.state']
        user_obj = self.env['res.users']
        log_obj = self.env['log.management']
#        dropship_obj = self.env['dropship.custom']
        terms_obj = self.env['account.payment.term']
        carrier_obj = self.env['delivery.carrier']
        vender_ids = []

        for data in vender_data:
            
            ir_model_ids = ir_model_data_obj.search(
                [('name', '=', data[0]), ('model', '=', 'res.partner'), ('module', '=', 'ms_vendor')])
            vendor_val = {
                'partner_code':data[0],
                'supplier': True,
                'customer': False,
                'partner_type': data[1].lower(),
                'active': True if data[2] in ('1','Yes') else False,
                'street': data[4],
                'street2': data[5],
                'city': data[6],
                'zip': data[8],
                'phone': data[10],
                'fax': data[11],
                'email': data[12],
                'sidemark': data[16],
                'fob': data[21],
                'specifier': data[22],
                'company_type':'company',
                'commissions_percentage': data[23],
            }
#            Check if email is emplty it will continue
#            if not data[12]:
#                log_vals = {'operation': 'vendors',
#                        'message': 'Not getting email for vendor %s '%data[3], 'date': datetime.now()}
#                log_obj.create(log_vals)
#                continue
#            if isinstance(data[12], (str, unicode)):
            if isinstance(data[12], (str)) or data[12]==None:
                    # note: this removes the character and encodes back to string.
#                email = data[12].decode('ascii', 'ignore').encode('ascii')
                vendor_val.update({'email': data[12]})
            else:
                email = data[12].decode(encoding)
                vendor_val.update({'email': email})
                
            if data[3]:
                vendor_val.update({'name': data[3]})
            elif not data[3] or data[0]:
                vendor_val.update({'name': data[0]})

            if data[9] == "USA":
                country_id = country_obj.search([('code', '=', 'US')])
                if country_id:
                    vendor_val.update({'country_id': country_id.id})
                    state_id = res_state_obj.search(
                        ['|', ('name', '=', data[7]), ('code', '=', data[7]), ('country_id', '=', country_id.id)])
                    if state_id:
                        vendor_val.update({'state_id': state_id.id})
            elif data[9]:
                country_id = country_obj.search(['|', ('name', '=', data[9]), ('code', '=', data[9])])
                if country_id:
                    vendor_val.update({'country_id': country_id.id})
                    state_id = res_state_obj.search(
                        ['|', ('name', '=', data[7]), ('code', '=', data[7]), ('country_id', '=', country_id.id)])
                    if state_id:
                        vendor_val.update({'state_id': state_id.id})
            elif not data[9] and data[7]:
                country_id = country_obj.search([('code', '=', 'US')])
                if country_id:
                    vendor_val.update({'country_id': country_id.id})
                    state_id = res_state_obj.search(
                        ['|', ('name', '=', data[7]), ('code', '=', data[7]), ('country_id', '=', country_id.id)])
                    if state_id:
                        vendor_val.update({'state_id': state_id.id})

            if data[19]:
                terms_id = terms_obj.search([('name', '=', data[19])])
                if terms_id:
                    vendor_val.update({'client_terms': terms_id[0].id})
                else:
                    terms_id=terms_obj.create({'name':data[19]})
                    vendor_val.update({'client_terms': terms_id.id})

            if data[20]:
                terms_id = terms_obj.search([('name', '=', data[20])])
                if terms_id:
                    vendor_val.update({'client_freight_terms': terms_id[0].id})
                else:
                    terms_id=terms_obj.create({'name':data[20]})
                    vendor_val.update({'client_freight_terms': terms_id.id})

            if data[18]:
                carrier_id = carrier_obj.search([('name', '=', data[18])])
                if carrier_id:
                    vendor_val.update({'client_ship_via': carrier_id[0].id})
                else:
                    products = self.env['product.product'].search([('name', '=', data[18])])
                    if products:
                        product = products[0].id
                    else:
                        product = self.env['product.product'].create({'name': data[18], 'type': 'service'}).id
                    carrier_id=carrier_obj.create({'name':data[18], 'product_id':product})
                    vendor_val.update({'client_ship_via': carrier_id.id})
                    
            child_address_val = {}
#            if data[10] or data[11] or data[12] or data[14] or data[16]:
#                child_address_val = {
#                    'type': 'delivery',
#                    'street': data[10],
#                    'street2': data[11],
#                    'city': data[12],
#                    'zip': data[14],
#                    'name': data[16],
#                    'message_follower_ids': []
#                }
#                if data[9] == "USA" or data[9]:
#                    country_id = country_obj.search(
#                        ['|', ('name', '=', data[9]), '|', ('code', '=', data[9]), ('code', '=', 'US')])
#                    if country_id:
#                        child_address_val.update({'country_id': country_id.id})
#                        state_id = res_state_obj.search(
#                            ['|', ('name', '=', data[7]), ('code', '=', data[7]), ('country_id', '=', country_id.id)])
#                        if state_id:
#                            child_address_val.update({'state_id': state_id.id})
#                if not data[9] and data[7]:
#                    country_id = country_obj.search([('code', '=', 'US')])
#                    if country_id:
#                        child_address_val.update({'country_id': country_id.id})
#                        state_id = res_state_obj.search(
#                            ['|', ('name', '=', data[7]), ('code', '=', data[7]), ('country_id', '=', country_id.id)])
#                        if state_id:
#                            child_address_val.update({'state_id': state_id[0].id})
            if data[0]:
                ir_model_user_id = ir_model_data_obj.search([('name', '=', data[0]), ('model', '=', 'res.users')])
                if ir_model_user_id:
                    user_id = user_obj.browse(ir_model_user_id[0].res_id)
                    vendor_val.update({'user_id': user_id[0].id})
                
            if ir_model_ids:
                ir_model_id = ir_model_ids[0]
                vender_ids.append(ir_model_id.res_id)
                vendor_id = self.browse(ir_model_id.res_id)
                vendor_id.write(vendor_val)
                ship_id = self.search([('parent_id', '=', vendor_id.id)])
                if child_address_val:
                    for ship in ship_id:
                        if ship.type == 'delivery':
                            ship.write(child_address_val)
                else:
                    ship_id.unlink()
                logger.info("Vendor available with name %s and Id %s " % (vendor_val.get('name'), vendor_id.id))
            else:
                vendor_id = self.with_context(tracking_disable=False).create(vendor_val)
                vender_ids.append(vendor_id.id)
                if child_address_val:
                    child_address_val.update({'parent_id': vendor_id.id})
                    self.with_context(tracking_disable=False).create(child_address_val)
                ir_model_data_obj.create(
                    {'name': data[0], 'res_id': vendor_id.id, 'model': 'res.partner', 'module': 'ms_vendor'})
                logger.info("Vendor created with name %s and Id %s " % (vendor_val.get('name'), vendor_id.id))
#            if data[18] in ['','Ground','Fed Ex Ground','Sold from Floor','Will Call','Truck','DHL','DHL Ground','IDL','Best Way','FedEx','FedEx 3rd Day','FedEx Priority','Marina Packing','Regal Delivery','UPS Express','UPS Overnight','UPS 2nd Day','UPS Ground','Will Call Pick Up']:
#                if data[18] or data[20] or data[19]:
#                    drophip_vals = {
#                        'ship_via': data[18],
#                        'freight_terms': data[20],
#                        'terms': data[19],
#                        'dropship_id': vendor_id.id
#                    }
#                    dropship_ids = dropship_obj.search(
#                        [('ship_via', '=', data[18]), ('freight_terms', '=', data[20]), ('terms', '=', data[19]),
#                         ('dropship_id', '=', vendor_id.id)])
#                    if dropship_ids:
#                        dropship_ids.write(drophip_vals)
#                    else:
#                        dropship_ids.create(drophip_vals)
#            else:
#                log_vals = {'operation': 'vendors',
#                        'message': 'Not getting Shipvia for vendor %s value %s'%(vendor_id.name,data[18]), 'date': datetime.now()}
#                log_obj.create(log_vals)
            log_vals = {'operation': 'vendors',
                        'message': 'Imported vendor %s '%(vendor_id.name), 'date': datetime.now()}
            log_obj.create(log_vals)
            self.env.cr.commit()
        return vender_ids

    @api.multi
    def import_vendors_data(self, conn):
        log_obj = self.env['log.management']

        # Query to get all Venders
        vendor_list = '''SELECT 
                        [Address ID] as ref,
                        Type as partner_type,
                        Active as active,
                        Name as name, 
                        Address as street, 
                        [Address 2] as street2,
                        City as city,
                        State as state_id,
                        Zip as zip, 
                        Country as country_id, 
                        Phone as phone, 
                        Fax as fax,
                        Email as email,
                        [Code 1] as tag1,
                        [Code 2] as tag2,
                        [Tax Percent] as taxable_percentage,
                        [Client Sidemark] as sidemark,
                        Salesperson as user_id,
                        [Ship Via] as client_ship_via,
                        Terms as client_terms,
                        [Freight Terms] as client_freight_terms,
                        FOB as fob,
                        Specifier as specifier,
                        [Commission Percent] as commissions_percentage
                        FROM
                        Address WHERE Type = 'Vendor';'''
        conn.execute(vendor_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of Venders getting ---- %s" % (len(conn_data)))
        try:
            self.env['res.partner'].createVendor(conn_data)
        except Exception as e:
            log_vals = {'operation': 'vendors',
                        'message': e, 'date': datetime.now()}
            log_obj.create(log_vals)
        return True

    @api.multi
    def update_vendors_data(self,conn,db):
        log_obj = self.env['log.management']
        
        # Query to get all Venders
        vendor_list = '''SELECT 
                        [Address ID] as ref,
                        Type as partner_type,
                        Active as active,
                        Name as name, 
                        Address as street, 
                        [Address 2] as street2,
                        City as city,
                        State as state_id,
                        Zip as zip, 
                        Country as country_id, 
                        Phone as phone, 
                        
                        Email as email,
                        [Code 1] as tag1,
                        [Code 2] as tag2,
                        [Tax Percent] as taxable_percentage,
                        [Client Sidemark] as sidemark,
                        Salesperson as user_id,
                        [Ship Via] as client_ship_via,
                        Terms as client_terms,
                        [Freight Terms] as client_freight_terms,
                        FOB as fob,
                        Specifier as specifier
                        
                        FROM
                        Address WHERE Type = 'Vendor';'''
        conn.execute(vendor_list)
        conn_data = conn.fetchall()
        
        update_vendor_ids=[code[0] for code in conn_data]
        partner_ids=self.env['res.partner'].search([('supplier','=',True),('partner_code','!=',False),('partner_code','in',update_vendor_ids)])
#       Prepare the query
        query='''UPDATE Address SET 
                        Active=%s,
                        Name=%s, 
                        Address=%s, 
                        [Address 2]=%s,
                        City=%s,
                        State=%s,
                        Zip=%s, 
                        Country=%s, 
                        Phone=%s, 
                        
                        Email=%s,
                        [Code 1]=%s,
                        [Code 2]=%s,
                        [Tax Percent]=%s,
                        [Client Sidemark]=%s,
                        Salesperson=%s,
                        [Ship Via]=%s,
                        Terms=%s,
                        [Freight Terms]=%s,
                        FOB=%s,
                        Specifier=%s
                        
                        WHERE [Address ID]=%s;'''
        try:
            for partner in partner_ids:
                data=('Yes' if partner.active else 'No',
                partner.name,
                partner.street or None,
                partner.street2 or None,
                partner.city or None,
                partner.state_id.code if partner.state_id else None,
                partner.zip or None,
                partner.country_id.code if partner.country_id else None,
                partner.phone or None,
#                partner.fax or None,
                partner.email or None,
                '',
                '',
                0.0,
                partner.sidemark or None,
                partner.user_id.name if partner.user_id else None,
                partner.client_ship_via.name if partner.client_ship_via else None,
                partner.client_terms.name if partner.client_terms else None,
                partner.client_freight_terms.name if partner.client_freight_terms else None,
                partner.fob or None,
                partner.specifier or '',
#                partner.commissions_percentage,
                partner.partner_code,
                )
                conn.execute(query,data)
    #           commit the changes made
                db.commit()
                logger.info("Updated the Vendor ---- %s" % partner.name)
                log_vals = {'operation': 'vendors',
                        'message': 'Updated vendor %s '%(partner.name), 'date': datetime.now()}
                log_obj.create(log_vals)
        except Exception as e:
            log_vals = {'operation': 'vendors',
                        'message':'Update: '+ str(e), 'date':datetime.now() }
            log_obj.create(log_vals)
#       Will close all the connections
#        finally:
#            conn.close()
#            db.close()
            
        return True
    
    @api.multi
    def export_vendors_data(self,conn,db,config):
        log_obj = self.env['log.management']
        ir_model_data_obj = self.env['ir.model.data']
        # Query to get all Venders
        vendor_list = '''SELECT 
                        [Address ID] as ref,
                        Type as partner_type,
                        Active as active,
                        Name as name, 
                        Address as street, 
                        [Address 2] as street2,
                        City as city,
                        State as state_id,
                        Zip as zip, 
                        Country as country_id, 
                        Phone as phone, 
                        
                        Email as email,
                        [Code 1] as tag1,
                        [Code 2] as tag2,
                        [Tax Percent] as taxable_percentage,
                        [Client Sidemark] as sidemark,
                        Salesperson as user_id,
                        [Ship Via] as client_ship_via,
                        Terms as client_terms,
                        [Freight Terms] as client_freight_terms,
                        FOB as fob,
                        Specifier as specifier
                        
                        FROM
                        Address WHERE Type = 'Vendor';'''
        conn.execute(vendor_list)
        conn_data = conn.fetchall()
        
        export_vendor_ids=[code[0] for code in conn_data]
        partner_ids=self.env['res.partner'].search([('supplier','=',True),('partner_code','!=',False),('partner_code','not in',export_vendor_ids)])
#       Prepare the query
        query='''INSERT INTO Address( 
                        Active,
                        Type,
                        Name, 
                        Address, 
                        [Address 2],
                        City,
                        State,
                        Zip, 
                        Country, 
                        Phone, 
                        
                        Email,
                        [Code 1],
                        [Code 2],
                        [Tax Percent],
                        [Client Sidemark],
                        Salesperson,
                        [Ship Via],
                        Terms,
                        [Freight Terms],
                        FOB,
                        Specifier,
                        
                        [Address ID])
                        VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'''
        try:
            for partner in partner_ids:
                ir_model_id=ir_model_data_obj.search([('name','=',partner.partner_code),('model','=','res.partner'),('module','=',config.db_name)])
                if ir_model_id:
                    continue
                data=('Yes' if partner.active else 'No',
                'vendor',
                partner.name,
                partner.street or None,
                partner.street2 or None,
                partner.city or None,
                partner.state_id.code if partner.state_id else None,
                partner.zip or None,
                partner.country_id.code if partner.country_id else None,
                partner.phone or None,
               
                partner.email or None,
                '',
                '',
                0.0,
                partner.sidemark or None,
                partner.user_id.name if partner.user_id else None,
                partner.client_ship_via.name if partner.client_ship_via else None,
                partner.client_terms.name if partner.client_terms else None,
                partner.client_freight_terms.name if partner.client_freight_terms else None,
                partner.fob or None,
                partner.specifier or None,
#                partner.commissions_percentage,
                partner.partner_code,
                )
                conn.execute(query,data)
    #           commit the changes made
                db.commit()
                ir_model_data_obj.create(
                        {'name': partner.partner_code, 'res_id': partner.id, 'model': 'res.partner','module':config.db_name})
                logger.info("Exported Vendor With Name ---- %s" % partner.name)
                log_vals = {'operation': 'vendors',
                        'message': 'Exported vendor %s '%(partner.name), 'date': datetime.now()}
                log_obj.create(log_vals)
        except Exception as e:
            log_vals = {'operation': 'vendors',
                        'message':'Export: '+ str(e), 'date':datetime.now() }
            log_obj.create(log_vals)
#       Will close all the connections
#        finally:
#            conn.close()
#            db.close()
            
        return True
    
   