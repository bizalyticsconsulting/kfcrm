# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
import re
import logging
logger = logging.getLogger(__name__)


class ImportAddressContacts(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def import_address_contacts(self,conn):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']
        partner_object = self.env['res.partner']

        # Query to get all address
        address_contact_list = "SELECT * FROM [Address Contact] order by [Address ID]"
        conn.execute(address_contact_list)
        conn_data = conn.fetchall()
        logger.info("Total Number of Address getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                logger.debug("Processing customer .... %s", data[0])
                if not (data[4] or data[6] or data[2]):
                    log_vals = {
                        'operation': 'add_contacts',
                        'message': "Contact Name or Email or phone not found for MSSQL ID {}".format(data[0]),
                        'date': datetime.now()
                    }
                    log_obj.create(log_vals)
                    logger.debug("Skipping contact for ...... %s", data)
                else:
                    customer_address_val = {'type':'contact'}

                    if isinstance(data[0], str):
                        address_name = data[0]
                        # address_name = re.sub(r'<.*?>', '', address_name.encode('utf-8'))
                    elif data[0] != None:
                        address_name = data[0].decode('ascii', 'ignore').encode('ascii')
                        # address_name = re.sub(r'<.*?>', '', address_name.encode('utf-8'))


                    ir_model_data_id = ir_model_data_obj.search([('name','=',address_name),('model','=','res.partner')])
                    logger.debug("Found customer record .... %s ... %s", address_name, ir_model_data_id)
                    if ir_model_data_id:
                        address_id = self.browse(ir_model_data_id[0].res_id)
                        customer_address_val.update({'parent_id':address_id.id})

                        name  = ""
                        email = ""
                        phone = ""

                        if data[4] and data[5]:
                            if isinstance(data[4], str) and isinstance(data[5], str):
                                phone = data[4] + "[" +  data[5] + "]"
                                customer_address_val.update({'phone': phone})
                        elif data[4] and not data[5]:
                            if isinstance(data[4], str):
                                phone = data[4]
                                customer_address_val.update({'phone': phone})

                        if data[2]:
                            if isinstance(data[2], str):
                                name = data[2]
                                customer_address_val.update({'name': name})

                        if data[3]:
                            if isinstance(data[3], str):
                                function = data[3]
                                customer_address_val.update({'function': function})

                        if data[6]:
                            if isinstance(data[6], str):
                                email = data[6]
                                customer_address_val.update({'email': email})

                        if data[7]:
                            if isinstance(data[7], str):
                                comment = data[7]
                                customer_address_val.update({'comment': comment})

                        contact_item_no = False
                        if data[1]:
                            contact_item_no = data[1]
                            customer_address_val.update({'contact_item_no': contact_item_no})

                        logger.debug("Trying to find contact .... %s .... %s .... %s .... %s", address_id.id, name, email, phone)
                        if name and email and phone:
                            partner_ids = partner_object.search(
                                [('parent_id', '=', address_id.id), ('email', '=', email),
                                 ('phone', '=', phone), ('name','=', name), ('type', '=', 'contact')], limit = 1)
                        elif email and name:
                            partner_ids = partner_object.search(
                                [('parent_id', '=', address_id.id), ('name', '=', name), ('email', '=', email),('type', '=', 'contact')], limit = 1)
                        elif name and phone:
                            partner_ids = partner_object.search(
                                [('parent_id', '=', address_id.id), ('name', '=', name), ('phone', '=', phone),('type', '=', 'contact')], limit = 1)
                        elif phone and email:
                            partner_ids = partner_object.search(
                                [('parent_id', '=', address_id.id), ('email', '=', email),('phone', '=', phone),('type', '=', 'contact')], limit = 1)
                        elif name:
                            partner_ids = partner_object.search(
                                [('parent_id', '=', address_id.id), ('name', '=', name), ('type', '=', 'contact')], limit = 1)
                        elif email:
                            partner_ids = partner_object.search(
                                [('parent_id', '=', address_id.id), ('email', '=', email), ('type', '=', 'contact')], limit = 1)
                        elif phone:
                            partner_ids = partner_object.search(
                                [('parent_id', '=', address_id.id), ('phone', '=', phone), ('type', '=', 'contact')], limit = 1)

                        if not customer_address_val.get('name', False):
                            customer_address_val.update({'name': customer_address_val.get('email', False) or customer_address_val.get('phone', False)})

                        if not partner_ids:
                            logger.debug("Creating record ..... %s", data)
                            address_id = partner_object.create(customer_address_val)
                            logger.info("Address Created ---- %s" % (address_id))
                            logger.debug("Address Created ..... %s", name)
                        else:
                            logger.debug("UPDATING record ..... %s", data)
                            self.write(customer_address_val)
                            logger.debug("Address updated ..... %s", name)
                            logger.info("Address available ---- %s" % (partner_ids[0].id))
                    else:
                        log_vals = {
                            'operation': 'add_contacts',
                            'message': "Parent not found for Contact ID {}".format(address_name),
                            'date': datetime.now()
                        }
                        log_obj.create(log_vals)
                    self.env.cr.commit()
        except Exception as e:
            log_vals = {'operation': 'add_contacts',
                        'message': e, 'date': datetime.now()}
            log_obj.create(log_vals)

        return True
