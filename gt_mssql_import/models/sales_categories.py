# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
import logging
import pymssql
logger = logging.getLogger(__name__)


class SalesCategories(models.Model):
    _inherit = 'product.category'
    
    #    Cron for update and export
    @api.model
    def cron_update_export_categories(self):
        config_ids = self.env['config.config'].search([('type','=','export')])
        for config in config_ids:
            db = pymssql.connect(host=config.server,
                                 user=config.user_name,
                                 password=config.password,
                                 database=config.db_name)
            cursor = db.cursor()
            self.update_sale_category(cursor,db)
#            db = pymssql.connect(host=config.server,
#                                 user=config.user_name,
#                                 password=config.password,
#                                 database=config.db_name)
#            cursor = db.cursor()
            self.export_sale_category(cursor,db,config)
    
    category_code=fields.Char('Category Code')

    @api.multi
    def import_sale_category(self,conn):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']

        # Query to get all Sales Category
        category_list = "SELECT [Sales Code]as id,[Description]as name FROM dbo.[Sales Codes]"
        conn.execute(category_list)
        conn_data = conn.fetchall()

        logger.info("Total Number of Category getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                # Check for name which is require
                if not data[1]:
                    log_vals = {'message': "The name not found of category  having MSSQL{} id".format(data[0]),
                                'operation': 'sales_categ', 'date': datetime.now()}
                    log_obj.create(log_vals)
                else:
                    ir_model_ids = ir_model_data_obj.search([('name', '=', data[0]),('model', '=', 'product.category')])
                    categ_val = {'name': data[1],'category_code':data[0]}
                    if ir_model_ids:
                        sale_categ_id = self.browse(ir_model_ids[0].res_id)
                        sale_categ_id.write(categ_val)
                        logger.info("Category is available with name %s and Id %s " % (data[1], sale_categ_id.id))
                    else:
                        sale_categ_id = self.create(categ_val)
                        ir_model_data_obj.create(
                            {'name': data[0], 'res_id': sale_categ_id.id, 'model': 'product.category'})
                        logger.info("Category is created with name %s and Id %s " % (data[1], sale_categ_id.id))
                    self.env.cr.commit()
                    log_vals = {'operation': 'sales_categ',
                        'message': 'Imported sales category : '+data[1], 'date':datetime.now() }
                    log_obj.create(log_vals)
        except Exception as e:
            log_vals = {'operation': 'sales_categ',
                        'message': e, 'date':datetime.now() }
            log_obj.create(log_vals)
        return True
    
    
    @api.multi
    def update_sale_category(self,conn,db):
        
        log_obj = self.env['log.management']
        
        category_list = "SELECT [Sales Code]as id,[Description]as name FROM dbo.[Sales Codes]"
        conn.execute(category_list)
        conn_data = conn.fetchall()
#        Need to Update Category ids 
        update_categ_ids=[code[0] for code in conn_data]
        
        product_categ_id=self.env['product.category'].search([('category_code','!=',False),('category_code','in',update_categ_ids)])
#       Prepare the query
        query='''UPDATE dbo.[Sales Codes] SET [Description]=%s WHERE [Sales Code]=%s ;'''
        try:
            for categ_id in product_categ_id:
                data=(categ_id.name,categ_id.category_code)
                conn.execute(query,data)
    #           commit the changes made
                db.commit()
                logger.info("Updated the Category ---- %s" % categ_id.name)
                log_vals = {'operation': 'sales_categ',
                        'message': 'Updated sales category : '+categ_id.name, 'date':datetime.now() }
                log_obj.create(log_vals)
        except Exception as e:
            log_vals = {'operation': 'sales_categ',
                        'message':'Update: '+ str(e), 'date':datetime.now() }
            log_obj.create(log_vals)
#       Will close all the connections
#        finally:
#            conn.close()
#            db.close()
            
        return True
    
    
    @api.multi
    def export_sale_category(self,conn,db,config):
        log_obj = self.env['log.management']
        ir_model_data_obj = self.env['ir.model.data']
        
        category_list = "SELECT [Sales Code]as id,[Description]as name FROM dbo.[Sales Codes]"
        conn.execute(category_list)
        conn_data = conn.fetchall()
    #        Exported Category ids 
        export_categ_ids=[code[0] for code in conn_data]

        product_categ_id=self.env['product.category'].search([('category_code','!=',False),('category_code','not in',export_categ_ids)])

#       Prepare the query
        if product_categ_id:
            query='''INSERT INTO dbo.[Sales Codes]([Description],[Sales Code]) VALUES(%s,%s);'''
            try:
                for categ_id in product_categ_id:
                    ir_model_id=ir_model_data_obj.search([('name','=',categ_id.category_code),('model','=','product.category'),('module','=',config.db_name)])
                    if ir_model_id:
                        continue
#                   Prepare valuse
                    data=(categ_id.name,categ_id.category_code)
#                   Execute query
                    conn.execute(query,data)
#                   Commit the changes
                    db.commit()
#                   Create ir.model.data so next time it will not create
                    ir_model_data_obj.create(
                        {'name': categ_id.category_code, 'res_id': categ_id.id, 'model': 'product.category','module':config.db_name})
                    logger.info("Category is created with name %s and Id %s " % (categ_id.name, categ_id.category_code))
                    log_vals = {'operation': 'sales_categ',
                        'message': 'Exported sales category : '+categ_id.name, 'date':datetime.now() }
                    log_obj.create(log_vals)
            except Exception as e:
                log_vals = {'operation': 'sales_categ',
                            'message':'Export: '+ str(e), 'date':datetime.now() }
                log_obj.create(log_vals)
#           Will close all the connections
#            finally:
#                conn.close()
#                db.close()
            
        return True