# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
import logging
from bs4 import BeautifulSoup
import re
import pymssql
logger = logging.getLogger(__name__)


class ImportProductPurchasedData(models.Model):
    _inherit = 'product.template'
    
    def remove_tags(self,text):
        result=''
        if text:
            result= re.sub(r'<p>&nbsp;</p>','', text)
            return result
        else:
            result
    
    
    @api.multi
    def button_update(self):
        config_ids = self.env['config.config'].search([('type','=','export')])
        for config in config_ids:
            db = pymssql.connect(host=config.server,
                                 user=config.user_name,
                                 password=config.password,
                                 database=config.db_name)
            cursor = db.cursor()
            self.update_product_purchased_data(cursor,db)
        
    @api.multi
    def button_export(self):
        config_ids = self.env['config.config'].search([('type','=','export')])
        for config in config_ids:
            db = pymssql.connect(host=config.server,
                                user=config.user_name,
                                password=config.password,
                                database=config.db_name)
            cursor = db.cursor()
            self.export_product_purchased_data(cursor,db,config)
        
    #    Cron for update and export
    @api.model
    def cron_update_export_products(self):
        config_ids = self.env['config.config'].search([('type','=','export')])
        for config in config_ids:
            db = pymssql.connect(host=config.server,
                                 user=config.user_name,
                                 password=config.password,
                                 database=config.db_name)
            cursor = db.cursor()
            self.update_product_purchased_data(cursor,db)
#            db = pymssql.connect(host=config.server,
#                                 user=config.user_name,
#                                 password=config.password,
#                                 database=config.db_name)
#            cursor = db.cursor()
            self.export_product_purchased_data(cursor,db,config)
    
    product_default_code=fields.Char('Default Code')
    default_code_option=fields.Char('Default Code Option')

    @api.multi
    def import_product_purchased_data(self,conn):
        ir_model_data_obj = self.env['ir.model.data']
        uom_obj = self.env['uom.uom']
        categ_obj = self.env['product.category']
        route_obj = self.env['stock.location.route']
        log_obj = self.env['log.management']
        uom_categ_obj = self.env['uom.category']
        supplier_info_obj = self.env['product.supplierinfo']
        reordering_obj = self.env['stock.warehouse.orderpoint']
        
        # Query to get all Catalog Details Products
        product_catalog_detail_list ='''SELECT
                                [Vendor] as vendor,
                                [Product #] as product_default_code,
                                [Product Option] as product_default_code_option,
                                [Line #] as sequence,
                                Code as product_code,
                                Description as description,
                                CONCAT([Vendor],'-',[Product #],'-',[Product Option]) as new_default_code
                                FROM
                                [Catalog Detail];'''

        conn.execute(product_catalog_detail_list)
        catalog_detail = conn.fetchall()
        logger.info("Total Number of Products Catalog details getting ---- %s" % (len(catalog_detail)))
        
        catalog_detail_list=[]
#        Prepare key,value pair for product detail
        for rec in catalog_detail:
            catalog_detail_vals={'key':rec[6],'sequence':rec[3],'product_code':rec[4],'description':rec[5]}
            catalog_detail_list.append(catalog_detail_vals)
            
        product_purchased_list ='''SELECT
                        Vendor as vendor,
                        [Product #] as default_code,
                        [Product Option] as default_code_option,
                        Description as description_sale,
                        [Sales Code] as categ_id,
                        [User Code 1] as tag_1,
                        [User Code 2] as tag_2,
                        [Purchase Cost] as standard_price,
                        [Selling Cost] as list_price,
                        Unit as uom_id,
                        Active as active,
                        CONCAT([Vendor],'-',[Product #],'-',[Product Option])    as new_default_code, 
                        [Client Description] as product_description
                        FROM
                        Catalog;'''
        
        conn.execute(product_purchased_list)
        conn_data = conn.fetchall()

        logger.info("Total Number of Purchase Products getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                detail_list=[]
                product_vals = {
                    'default_code':data[11],
                    'sale_ok':True,
                    'purchase_ok':True,
    #                    'flag':False,
                    'active':True if data[10] in ('1','Yes') else False,
                    'product_default_code':data[1],
                    'default_code_option':data[2],
                    'type':'product',
                    'purchase_method':'purchase',
                    'description_sale': data[12],
                    'description_purchase': data[12]
                }
    #               It will prepare the product details list
                for rec in catalog_detail_list:
                    if rec.get('key')==data[11]:
                        detail_vals={
                            'sequence':rec.get('sequence'),
                            'code':rec.get('product_code'),
                            'desc':rec.get('description')
                        }
                        detail_list.append((0,0,detail_vals))
    #                It will create one2many for product details    
                product_vals.update({'pro_catal_detail_ids':detail_list})
    #                product_vals.update({'pro_catal_detail_ids': data[11] or ' '})
                if not(data[3]):
                    product_vals.update({'name': data[11] or ' '})
                else:
                    product_vals.update({'name': data[3]})

                if data[8]:
                    product_vals.update({'list_price':float(data[8])})
                if data[7]:
                    product_vals.update({'standard_price':float(data[7])})
                if not data[9]:
                    uom_ids = uom_obj.search([('name', 'ilike', 'Each'), ('category_id.name','=','Unit')])
                    if uom_ids:
                        uom_id = uom_ids[0]
                    else:
                        uom_categ_ids = uom_categ_obj.search([('name', '=', 'Unit')])
                        if not uom_categ_ids:
                            uom_categ_id = uom_categ_obj.create({
                                'name': 'Unit'
                            })
                        else:
                            uom_categ_id = uom_categ_ids[0]
                        uom_id = uom_obj.create({'name': 'Each', 'category_id': uom_categ_id[0].id,'uom_type':'smaller'})
                    product_vals.update({'uom_id': uom_id.id, 'uom_po_id': uom_id.id})
                else:
                    # Add uom and Purchase UOM
                    uom_ids = uom_obj.search([('name', 'ilike', data[9].strip()), ('category_id.name','=', 'Unit')])
                    if uom_ids:
                        uom_id = uom_ids[0]
                    else:
                        uom_categ_ids = uom_categ_obj.search([('name', '=', 'Unit')])
                        if not uom_categ_ids:
                            uom_categ_id = uom_categ_obj.create({
                                'name': 'Unit'
                            })
                        else:
                            uom_categ_id = uom_categ_ids[0]
                        uom_id = uom_obj.create({'name': data[9].strip(), 'category_id': uom_categ_id[0].id,'uom_type':'smaller'})
                    product_vals.update({'uom_id': uom_id.id, 'uom_po_id': uom_id.id})



                if data[4]:
                    categ_id = categ_obj.search([('category_code', '=', data[4])],limit=1)
                    if categ_id:
    #                        categ_id = categ_obj.browse(ir_model_categ_id.res_id)
                        product_vals.update({'categ_id':categ_id.id})
                    else:
                        log_vals = {
                            'operation': 'p_purchased',
                            'message': 'Category is not geting in category for product name {}'.format(
                                product_vals.get('name')),
                            'date': datetime.now()
                        }
                        log_obj.create(log_vals)
                else:
                    product_vals.update({'categ_id':categ_obj.search([('id', '=', 1)],limit=1).id})

                ir_model_ids = ir_model_data_obj.search([('name', '=', data[11]), ('model', '=', 'product.template')])
                if ir_model_ids:
                    prod_purchase_id = self.browse(ir_model_ids[0].res_id)
                    prod_purchase_id.pro_catal_detail_ids.unlink()
                    prod_purchase_id.write(product_vals)
                    logger.debug("Description is .... %s", data[12])
                    logger.info("Purchase Product is update with name %s and Id %s " % (
                    product_vals.get('name'), prod_purchase_id.id))
                else:
                    prod_purchase_id = self.create(product_vals)
                    ir_model_data_obj.create({
                        'name': data[11],
                        'res_id': prod_purchase_id.id,
                        'model': 'product.template',
                    })
                    logger.info("Purchase Product created with name %s and Id %s " % (
                     product_vals.get('name'), prod_purchase_id.id))

                name=prod_purchase_id.name

                if isinstance(name, str):
                    name=prod_purchase_id.name
                else:
                    name = name.decode(encoding)
                log_vals = {
                    'operation': 'p_purchased',
                    'message': 'Product Imported for  product name {}'.format(name),
                    'date': datetime.now()
                }
                log_obj.create(log_vals)
                if data[0]:
                    ir_model_vender_ids = ir_model_data_obj.search(
                        [('name', '=', data[0]), ('model', '=', 'res.partner')])
                    if not ir_model_vender_ids:
    #                        name=prod_purchase_id.name
    #                        if isinstance(name, str):
    #                            name = name.decode('ascii', 'ignore').encode('ascii')
    #                        elif isinstance(name, unicode):
    #                            name = name.encode('ascii', 'ignore')
                        log_vals = {
                            'operation': 'p_purchased',
                            'message': 'Vender is not geting in ir model data for product name {}'.format(name),
                            'date': datetime.now()
                        }
                        log_obj.create(log_vals)
                    else:
                        svals = {
                            'name': ir_model_vender_ids[0].res_id,
                            'product_tmpl_id':  prod_purchase_id.id,
                            'min_qty': 1,
                            'product_name': prod_purchase_id.name,
                        }
                        if data[8]:
                            svals.update({'price':float(data[8])})
                        s_info_ids = supplier_info_obj.search([('name', '=', ir_model_vender_ids[0].res_id), ('product_tmpl_id', '=', prod_purchase_id.id)])
                        if not s_info_ids:
                            supplier_info_obj.create(svals)
                if self.env.context.get('create_reordering_rule') == True:
                    reorder_ids = reordering_obj.search([('product_id.product_tmpl_id', '=', prod_purchase_id.id)])
                    if not reorder_ids:
                        reordering_obj.create({
                            'product_id': prod_purchase_id.id,
                            'name': prod_purchase_id.name,
                            'product_min_qty': 1,
                            'product_max_qty': 1
                        })
                        route_id = route_obj.search([('name', '=', 'Buy')])
                        prod_purchase_id.write({'route_ids': [(6, 0, route_id[0].ids)]})
                else:
                    reorder_ids = reordering_obj.search([('product_id.product_tmpl_id', '=', prod_purchase_id.id)])
                    reorder_ids.unlink()
                    route_ids = route_obj.search([('name', 'in', ('Buy','Make To Order'))])
                    prod_purchase_id.write({'route_ids': [(6, 0, route_ids.ids)]})

                self.env.cr.commit()
        except Exception as e:
            log_vals = {
                'operation': 'p_purchased',
                'message': e,
                'date': datetime.now()
            }
            log_obj.create(log_vals)
        return True
    
    
    @api.multi
    def update_product_purchased_data(self,conn,db):
#        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']
        
        if self._context.get('update')==True:
            select_data=(self.product_default_code,
                    self.seller_ids[0].name.partner_code if self.seller_ids else '',
                    self.default_code_option)
            # Query to get all Purchase Products
            product_purchased_list ='''SELECT
                            Vendor as vendor,
                            [Product #] as default_code,
                            [Product Option] as default_code_option,
                            Description as description_sale,
                            [Sales Code] as categ_id,
                            [User Code 1] as tag_1,
                            [User Code 2] as tag_2,
                            [Purchase Cost] as standard_price,
                            [Selling Cost] as list_price,
                            Unit as uom_id,
                            Active as active,
                            [Client Description] as product_description
                            FROM
                            Catalog WHERE [Product #]=%s and Vendor=%s and [Product Option]=%s; '''
            conn.execute(product_purchased_list,select_data)
            conn_data = conn.fetchall()
        else:
            # Query to get all Purchase Products
            product_purchased_list ='''SELECT
                            Vendor as vendor,
                            [Product #] as default_code,
                            [Product Option] as default_code_option,
                            Description as description_sale,
                            [Sales Code] as categ_id,
                            [User Code 1] as tag_1,
                            [User Code 2] as tag_2,
                            [Purchase Cost] as standard_price,
                            [Selling Cost] as list_price,
                            Unit as uom_id,
                            Active as active,
                            [Client Description] as product_description
                            FROM
                            Catalog;'''

            conn.execute(product_purchased_list)
            conn_data = conn.fetchall()
        
        update_categ_ids=[code[1] for code in conn_data]
        if self._context.get('update')==True:
            product_template_id=self.env['product.template'].search([('id','=',self.id),('product_default_code','in',update_categ_ids)])
        else:
            product_template_id=self.env['product.template'].search([('product_default_code','!=',False),('product_default_code','in',update_categ_ids)])
#       Prepare the query
        query='''UPDATE Catalog SET 
                        Vendor=%s,
                        [Product Option]=%s,
                        Description=%s,
                        [Purchase Cost]=%s,
                        [Selling Cost]=%s,
                        Unit=%s,
                        [Client Description]=%s,
                        [Vendor Description]=%s,
                        Active=%s
                        WHERE [Product #]=%s and Vendor=%s and [Product Option]=%s;'''
        try:
            for product in product_template_id:
                data=(product.seller_ids[0].name.partner_code if product.seller_ids else '',
#                product.product_default_code,
                product.default_code_option,
                product.name,
                product.standard_price,
                product.list_price,
                product.uom_id.name,
                self.remove_tags(product.description_sale),
                self.remove_tags(product.description_purchase),
                'Yes' if product.active else None,
                product.product_default_code,
                product.seller_ids[0].name.partner_code if product.seller_ids else '',
                product.default_code_option)
                conn.execute(query,data)
                if product.pro_catal_detail_ids:
#                    self.update_product_catalog_data(conn,db,product.pro_catal_detail_ids)
                    product.update_product_catalog_data(conn,db,product.pro_catal_detail_ids)
    #           commit the changes made
                db.commit()
                logger.info("Updated the Product ---- %s" % product.name)
                log_vals = {
                    'operation': 'p_purchased',
                    'message': 'Updated for  product name {}'.format(product.name),
                    'date': datetime.now()
                }
                log_obj.create(log_vals)
        except Exception as e:
            log_vals = {'operation': 'p_purchased',
                        'message':'Update: '+ str(e), 'date':datetime.now() }
            log_obj.create(log_vals)
#       Will close all the connections
#        finally:
#            conn.close()
#            db.close()
            
        return True
    
    @api.multi
    def export_product_purchased_data(self,conn,db,config):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']
        
        if self._context.get('export')==True:
            select_data=(self.product_default_code,
                    self.seller_ids[0].name.partner_code if self.seller_ids else '',
                    self.default_code_option)
            # Query to get all Purchase Products
            product_purchased_list ='''SELECT
                            Vendor as vendor,
                            [Product #] as default_code,
                            [Product Option] as default_code_option,
                            Description as description_sale,
                            [Sales Code] as categ_id,
                            [User Code 1] as tag_1,
                            [User Code 2] as tag_2,
                            [Purchase Cost] as standard_price,
                            [Selling Cost] as list_price,
                            Unit as uom_id,
                            Active as active
                            FROM
                            Catalog WHERE [Product #]=%s and Vendor=%s and [Product Option]=%s; '''

            conn.execute(product_purchased_list,select_data)
            conn_data = conn.fetchall()
        else:
            # Query to get all Purchase Products
            product_purchased_list ='''SELECT
                            Vendor as vendor,
                            [Product #] as default_code,
                            [Product Option] as default_code_option,
                            Description as description_sale,
                            [Sales Code] as categ_id,
                            [User Code 1] as tag_1,
                            [User Code 2] as tag_2,
                            [Purchase Cost] as standard_price,
                            [Selling Cost] as list_price,
                            Unit as uom_id,
                            Active as active
                            FROM
                            Catalog;'''

            conn.execute(product_purchased_list)
            conn_data = conn.fetchall()

        export_categ_ids=[code[1] for code in conn_data]
        if self._context.get('export')==True:
            product_template_id=self.env['product.template'].search([('id','=',self.id),('product_default_code','not in',export_categ_ids)])
        else:
            product_template_id=self.env['product.template'].search([('product_default_code','!=',False),('product_default_code','not in',export_categ_ids)])

#       Prepare the query
        query='''INSERT INTO Catalog(
                    Vendor,
                    [Product #],
                    [Product Option],
                    Description,
                    [Sales Code],
                    [User Code 1],
                    [User Code 2],
                    [Purchase Cost],
                    [Selling Cost],
                    Unit,
                    [Client Description],
                    [Vendor Description],
                    Active) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'''
        try:
            for product in product_template_id:
                ir_model_id=ir_model_data_obj.search([('name','=',product.product_default_code),('model','=','product.template'),('module','=',config.db_name)])
                if ir_model_id:
                    continue
                data=(product.seller_ids[0].name.partner_code if product.seller_ids else '',
                product.product_default_code,
                product.default_code_option,
                product.name,
                '',
                '',
                '',
                product.standard_price,
                product.list_price,
                product.uom_id.name,
                self.remove_tags(product.description_sale),
                self.remove_tags(product.description_purchase),
                'Yes' if product.active else None)
                conn.execute(query,data)
                if product.pro_catal_detail_ids:
#                    self.export_product_catalog_data(conn,db,product.pro_catal_detail_ids)
                    product.export_product_catalog_data(conn,db,product.pro_catal_detail_ids)
    #           commit the changes made
                db.commit()
                ir_model_data_obj.create(
                        {'name': product.product_default_code, 'res_id': product.id, 'model': 'product.template','module':config.db_name})
                logger.info("Product is created with name %s and Id %s " % (product.product_default_code, product.id))
                log_vals = {
                    'operation': 'p_purchased',
                    'message': 'Exported for  product code {}'.format(product.product_default_code),
                    'date': datetime.now()
                }
                log_obj.create(log_vals)
        except Exception as e:
            log_vals = {'operation': 'p_purchased',
                        'message':'Export: '+ str(e), 'date':datetime.now() }
            log_obj.create(log_vals)
#       Will close all the connections
#        finally:
#            conn.close()
#            db.close()
        return True
    