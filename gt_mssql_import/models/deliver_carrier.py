# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
import logging
from odoo.tools.safe_eval import safe_eval
logger = logging.getLogger(__name__)


class ShipViaDelivery(models.Model):
    _inherit = 'delivery.carrier'

    @api.multi
    def import_delivery_data(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        log_obj = self.env['log.management']
        product_obj = self.env['product.product']

        # Query to get all Carriers from mssql
        conn.execute("SELECT [PO Ship Via]as ID,[PO Ship Via Description]as description FROM [PO Ship Via]")
        conn_data = conn.fetchall()
        logger.info("Total Number of Carriers getting ---- %s" % (len(conn_data)))
        try:
            for data in conn_data:
                # Log if Description is not available
                if not data[1] and data[1] == None:
                    log_vals = {'operation': 'ship_via_del',
                                'message': "Description is not getting for ID: " + str(data[1]), 'date': datetime.now()}
                    log_obj.create(log_vals)
                    continue

                # Search for record in model data
                ir_model_ids = ir_model_data_obj.search(
                    [('name', '=', data[0]), ('model', '=', 'delivery.carrier'), ('module', '=', 'delivery')])
                carrier_val = {'name': data[1],}

                product_id = product_obj.search([('name','=',str(data[1])),('type','=','service'),('sale_ok','=',False),('purchase_ok','=',False)])
                if not product_id:
                    product_id = product_obj.create({'name':str(data[1]),
                                                     'type':'service',
                                                     'sale_ok':False,
                                                     'purchase_ok':False
                                                     })
                carrier_val.update({
                    'product_id':product_id[0].id
                })

                if ir_model_ids:
                    res_id = self.browse(ir_model_ids.res_id)
                    res_id.write(carrier_val)
                    logger.info("Update Carrier---: with Name %s and ID %s" % (data[1], res_id.id))
                else:
                    carrier_id = self.create(carrier_val)
                    logger.info("Created Carrier---: with Name %s and ID %s" % (data[1], carrier_id.id))

                    ir_model_data_obj.create(
                        {'name': data[0], 'res_id': carrier_id.id, 'model': 'delivery.carrier', 'module': 'delivery'})
                self.env.cr.commit()
        except Exception as e:
            log_vals = {'operation': 'ship_via_del',
                        'message': e, 'date': datetime.now()}
            log_obj.create(log_vals)
        return True


    def get_price_from_picking(self, total, weight, volume, quantity):
        price = 0.0
        criteria_found = False
        price_dict = {'price': total, 'volume': volume, 'weight': weight, 'wv': volume * weight, 'quantity': quantity}
        for line in self.price_rule_ids:
            test = safe_eval(line.variable + line.operator + str(line.max_value), price_dict)
            if test:
                price = line.list_base_price + line.list_price * price_dict[line.variable_factor]
                criteria_found = True
                break
        # if not criteria_found:
        #     raise UserError(
        #         _("Selected product in the delivery method doesn't fulfill any of the delivery carrier(s) criteria."))

        return price


