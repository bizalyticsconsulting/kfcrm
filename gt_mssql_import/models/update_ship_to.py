# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from datetime import datetime
import logging
import re
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


class SalesOrderConfirmed(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def update_ship_to(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        log_obj = self.env['log.management']
        res_state_obj = self.env['res.country.state']
        order_obj = self.env['sale.order']
        msg_obj = self.env['mail.message']
        terms_obj = self.env['account.payment.term']
        carrier_obj = self.env['delivery.carrier']
        seq_obj = self.env['ir.sequence']

        # Get Lines
        confirm_sale_order_list = "SELECT \
                                p.[PO #] as ID,\
                                p.[Ship To]as ship_to, \
                                p.[Ship To Name]as ship_name, \
                                p.[Ship To Address]as shipping_address_1, \
                                p.[Ship To Address2]as shipping_address_2, \
                                p.[Ship To City]as shipping_city,  \
                                p.[Ship To State]as shipping_state, \
                                p.[Ship To Zip]as shipping_zip, \
                                p.[Ship To Attention]as shipping_attn_to, \
                                p.[Ship To Phone]as shipping_phone \
                                FROM    dbo.PO p \
                                WHERE i.[PO #] IS NOT NULL AND p.[PO Date] >= '01/01/2016 00:00:00'  \
                                ORDER BY[PO Date] ASC"
        conn.execute(confirm_sale_order_list)
        conn_data = conn.fetchall()
        logger.warning("Total Number of Confirm Sale Orders getting are ---- %s", len(conn_data))
        # try:
        sale_records = []
        for data in conn_data:
            # try:
            sale_order_val = {}
            partner_ship_vals = {}
            write_rec = False
            partner_found = False
            logger.debug("Working on id .... %s....", data[0])
            if data[0] in sale_records:
                continue
            sale_records.append(data[0])

            seq_ids = seq_obj.search([('code', '=', 'sale.order')])
            if seq_ids:
                ir_model_ids = ir_model_data_obj.search(
                    [('name', '=', seq_ids[0].prefix + "-" + (str(data[0]).strip()).rjust(seq_ids[0].padding, "0")), ('model', '=', 'sale.order'), ('module', '=', 'confirm_sale_order')])
                if ir_model_ids:
                    sale_records.append(data[0])
                    if data[1]: ## Ship To res.partner exists
                        ir_model_partner_ship_id = ir_model_data_obj.search(
                            [('name', '=', data[1]), ('model', '=', 'res.partner')], limit=1)
                        if ir_model_partner_ship_id:
                            partner_ship_id = partner_obj.browse(ir_model_partner_ship_id[0].res_id)
                            if partner_ship_id and partner_ship_id.id in partner_obj.ids:
                                partner_ship_vals = {'parent_id': partner_ship_id.id, 'type': 'delivery'}
                                partner_found = True

                    if data[2]: ## Ship To Name exists
                        partner_ship_vals.update({'name': data[2]})

                    if data[6]:
                        state_id = res_state_obj.search(
                            ['|', ('name', '=', data[17]), ('code', '=', data[6])])
                        if state_id:
                            partner_ship_vals.update({'state_id': state_id[0].id})

                    if data[3]:
                        partner_ship_vals.update({'street': data[3]})

                    if data[4]:
                        partner_ship_vals.update({'street2': data[4]})

                    if data[5]:
                        partner_ship_vals.update({'city': data[5]})

                    if data[9]:
                        partner_ship_vals.update({'phone': data[9]})

                    if data[7]:
                        partner_ship_vals.update({'zip': data[7]})

                    if partner_found:
                        shipping_address_ids = partner_obj.search(
                            [('street', '=', data[3]), ('street2', '=', data[4]),
                             ('city', '=', data[5]), ('zip', '=', data[7]), ('state_id', '=', state_id[0].id)
                             ('parent_id', '=', partner_ship_id.id), ('type', '=', 'delivery')], limit=1)
                        if shipping_address_ids:
                            ## partner_obj.write(partner_ship_vals)
                            sale_order_val.update({'partner_shipping_id': shipping_address_ids[0].id})
                            write_rec = True
                        else:
                            new_shipping_id = partner_obj.create(partner_ship_vals)
                            if new_shipping_id:
                                sale_order_val.update({'partner_shipping_id': new_shipping_id.id})
                            logger.warning("CREATED SHIPPING ID ..... %s", new_shipping_id)
                            write_rec = True
                    else:
                        # Create a new record for address...
                        logger.warning("Will create new address as ship to was null")


                    if write_rec:
                        confirm_sale_id = self.browse(ir_model_ids[0].res_id)
                        confirm_sale_id.write(sale_order_val)

                        logger.warning("Sale Order is available with Id {} ".format(confirm_sale_id))
                        self.env.cr.commit()
        return True
