# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from datetime import datetime
import logging
import re
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


class SalesOrderConfirmed(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def update_salesperson2(self, conn):
        ir_model_data_obj = self.env['ir.model.data']
        partner_obj = self.env['res.partner']
        log_obj = self.env['log.management']
        res_state_obj = self.env['res.country.state']
        order_obj = self.env['sale.order']
        msg_obj = self.env['mail.message']
        terms_obj = self.env['account.payment.term']
        carrier_obj = self.env['delivery.carrier']
        seq_obj = self.env['ir.sequence']

        # Get Lines
        confirm_sale_order_list = "SELECT  \
                                p.[PO #] as ID,\
                                p.[Salesperson 2]as salesperson_2 \
                                FROM    dbo.PO p \
                                WHERE p.[PO Date] >= '01/01/2016 00:00:00' and p.[Salesperson 2] is not NULL  \
                                ORDER BY[PO Date] ASC"

        confirm_sale_order_list = "SELECT p.[Proposal #] as ID, p.[Salesperson 2] as salesperson_2 from Proposal p \
                                   WHERE p.[Proposal Date] >= '06/01/2020 00:00:00' and p.[Salesperson 2] is not NULL \
                                   ORDER BY [Proposal Date] ASC"

        conn.execute(confirm_sale_order_list)
        conn_data = conn.fetchall()

        sale_records = []
        for data in conn_data:
            # try:
            sale_order_val = {}
            logger.debug("Working on id .... %s....", data[0])
            if data[0] in sale_records:
                continue
            sale_records.append(data[0])

            seq_ids = seq_obj.search([('code', '=', 'sale.order')])
            if seq_ids:
                ir_model_ids = ir_model_data_obj.search(
                    [('name', '=', seq_ids[0].prefix + "-" + (str(data[0]).strip()).rjust(seq_ids[0].padding, "0")), ('model', '=', 'sale.order')])
                if ir_model_ids:
                    sale_records.append(data[0])
                    ir_model_partner_id = ir_model_data_obj.search(
                        [('name', '=', data[1]), ('model', '=', 'res.users')], limit = 1)
                    if ir_model_partner_id:
                        sale_order_val.update({'sit_salesperson2': ir_model_partner_id[0].res_id})
                        logger.warning("Updating ... %s with .... %s", data[1], ir_model_partner_id[0].res_id)

                        confirm_sale_id = self.browse(ir_model_ids[0].res_id)
                        confirm_sale_id.write(sale_order_val)
                    logger.warning("Sale Order is available with Id {} ".format(confirm_sale_id))

                    self.env.cr.commit()

        return True

