# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': "GT Mail Attachment Report Extension",
    'summary': """
        Adding attachment on the object by sending this one""",
    'author': "Globalteckz",
    'website': "https://www.globalteckz.com/",
    'category': 'Social Network',
    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'depends': [
        'mail_attach_existing_attachment','showroom_fields',
    ],
    'data': [
        'views/mail_compose_message_view.xml',
        'views/sale_view.xml',
        #'report/report.xml',
        'views/mail_template.xml',
    ],
    'installable': True,
}
