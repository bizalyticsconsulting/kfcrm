# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api


class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.model
    def default_get(self, fields_list):
        res = super(MailComposeMessage, self).default_get(fields_list)
        if res.get('res_id') and res.get('model') in ['sale.order','purchase.order']:
            order_id = self.env[res.get('model')].browse(res.get('res_id'))
            if order_id:
                res['order_partner_id'] = order_id.partner_id.id
        return res

    order_partner_id = fields.Integer('Order Partner')
    object_partner_attachment_ids = fields.Many2many(
        comodel_name='ir.attachment',
        relation='mail_partner_ir_attachments_object_rel',
        column1='wizard_id', column2='attachment_id', string='Attachments',
        domain= "[('res_model', '=', 'res.partner'), ('res_id', '=',order_partner_id)]"
        )

    @api.multi
    def get_mail_values(self, res_ids):
        res = super(MailComposeMessage, self).get_mail_values(res_ids)
        if self.object_partner_attachment_ids.ids and self.model and len(res_ids) == 1:
            res[res_ids[0]].setdefault('attachment_ids', []).extend(
                self.object_partner_attachment_ids.ids)
        return res
