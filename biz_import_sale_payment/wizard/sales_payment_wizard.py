# -*- coding: utf-8 -*-

from odoo import fields, models, tools, api, _
from odoo.exceptions import UserError, ValidationError
import base64
import xlrd
import urllib.request, urllib.error
from datetime import datetime
import logging
import pandas as pd
import mimetypes

_logger = logging.getLogger(__name__)


excel_csv_format = [
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
]

class SalesPaymentImportWizard(models.TransientModel):
    _name = 'sales.payment.import.wizard'
    _description = 'Sales Payment Import Wizard'


    data_file = fields.Binary('Upload Excel File', required=True, attachment=False)
    filename = fields.Char('File Name', required=True)


    @api.multi
    def action_import_sales_payment(self):
        """ This method will import excel sheet using Pandas with Sale Order Payments """
        LogManagement = self.env['log.management']
        SaleOrder = self.env['sale.order']
        not_imported_so_list = []
        ctx = self.env.context.copy()
        # Validate files format...only allow Excel sheet format files
        mimetype = None
        if mimetype is None and self.filename:
            mimetype = mimetypes.guess_type(self.filename)[0]
            if not mimetype in excel_csv_format:
                raise UserError('Please upload only excel sheet (.xls, .xlsx) format files !')
        try:
            excel_file_data = pd.read_excel(io=base64.decodestring(self.data_file))
            mandatory_columns = ['Payment_Number', 'SalesOrder', 'TotalPayments', 'order_date', 'PO', 'PO #', 'SellingCost', 'FreightCost', 'CratingCost', 'Installation', 'OtherCost', 'SalesTax', 'SalesDiscount', 'TotalAmount']
            # mandatory_columns = ['SalesOrder', 'TotalPayments', 'order_date', 'PO', 'PO #', 'SellingCost', 'FreightCost', 'CratingCost', 'Installation', 'OtherCost', 'SalesTax', 'SalesDiscount', 'Unnamed: 12']
        except Exception as e:
            _logger.exception('File not get imported !')
            raise UserError(
                _('File %r not imported due to a malformed file.'
                  '\n\nTechnical Details:\n%s') % \
                (self.filename, tools.ustr(e))
            )
        # Validate 13 columns with name in excel sheet # If column not contains mandatory field...raise alert
        if len(excel_file_data.columns) < 13:
            raise UserError(_("Please upload an excel sheet with the minimum 14 mandatory columns !"))
        if any([i for i in mandatory_columns if i not in excel_file_data.columns]):
            raise UserError(_("Please upload an excel sheet with the mandatory columns !\n\n"
                              "Mandatory columns are: \n%s" %(', '.join(map(str, mandatory_columns)))))
        excel_data_df = pd.DataFrame(data=excel_file_data)

        try:
            log_vals = {'operation': 'sales_payment'}
            _logger.info("Total SalesOrder given in sheet: %s", len(excel_data_df['SalesOrder'].tolist()))
            sale_order_id_list = SaleOrder.search([('name', 'in', excel_data_df['SalesOrder'].tolist())])
            _logger.info("Total SalesOrder available in System: %s", len(sale_order_id_list))
            row_count = 1

            for index, pdata in excel_data_df.iterrows():
                try:
                    _logger.info("Import row count: %s", row_count)
                    _logger.info("Import Process for  SalesOrder: %s", pdata['SalesOrder'])
                    create_log_bool = False
                    sale_order_id = SaleOrder.search([('name', '=', pdata['SalesOrder'])])
                    # sale_order_id = sale_order_id_list.filtered(lambda x: x.name == pdata['SalesOrder'])
                    # _logger.info(f"Sale Order available in system: {sale_order_id}")
                    _logger.info("Sale Order available in system: %s" % str(sale_order_id))
                    # _logger.info("Sale Order available in system: %s", sale_order_id)
                    if sale_order_id:
                        if sale_order_id.state in ['sale', 'done'] and sale_order_id.amount_total >  0:
                            row_pdata_check_reference = str(pdata['SalesOrder']) + '/' + str(int(pdata['Payment_Number']))
                            payment_check_references = []
                            if len(sale_order_id.invoice_ids) > 0:
                                payment_ids = sale_order_id.invoice_ids.mapped('payment_ids').filtered(lambda x: x.state == 'posted')
                                total_paid_payment_amount = 0

                                if len(payment_ids) > 0:
                                    # payment_amount = sum([i.amount for i in payment_ids])
                                    total_paid_payment_amount = sum([i.amount for i in payment_ids for mvl in i.move_line_ids if
                                                          not mvl.move_id.reverse_entry_id and mvl.amount_residual > 0])
                                    payment_check_references = [str(i.new_check_number) for i in payment_ids]
                                due_balance_amount = sale_order_id.amount_total - total_paid_payment_amount
                                if due_balance_amount >= pdata['TotalPayments']:
                                    # If check reference is already done payments..Do not again apply payment for the same.
                                    if pdata['TotalPayments'] > 0 and row_pdata_check_reference not in payment_check_references:
                                        _logger.debug("Applying payment to %s", str(pdata['SalesOrder']))
                                        _logger.debug("Applying payments now ..... %s", pdata['TotalPayments'])
                                        sale_order_id.action_import_invoice_payment(pdata['TotalPayments'], pdata['order_date'], row_pdata_check_reference)
                                elif pdata['TotalPayments'] > due_balance_amount:
                                    if ((due_balance_amount == pdata['TotalAmount']) and ((pdata['TotalPayments'] - pdata['TotalAmount']) > 0) and (pdata['TotalPayments'] - pdata['TotalAmount']) < 1):
                                        if pdata['TotalPayments'] > 0 and row_pdata_check_reference not in payment_check_references:
                                            sale_order_id.action_import_invoice_payment(pdata['TotalAmount'], pdata['order_date'], row_pdata_check_reference)
                                else:
                                    log_vals['message'] = "No Payment is get imported for Sale Order(%s) because importing payment is greater than the balance due amount !" % str((pdata['SalesOrder']))
                                    # log_vals['message'] = "No Payment is get imported for Sale Order {} because importing payment is greater than the balance due amount !".format(pdata['SalesOrder'])
                                    # log_vals['date'] = datetime.now()
                                    create_log_bool = True
                                    not_imported_so_list.append(pdata['SalesOrder'])

                            elif len(sale_order_id.invoice_ids) == 0:
                                # Create Invoice
                                sale_order_id.action_invoice_create()
                                if len(sale_order_id.invoice_ids) > 0:
                                    for invoice in sale_order_id.invoice_ids:
                                        # Validate Invoices
                                        invoice.action_invoice_open()
                                    if pdata['TotalPayments'] == pdata['TotalAmount']:
                                        if sale_order_id.amount_total >= pdata['TotalPayments']:
                                            if pdata['TotalPayments'] > 0 and row_pdata_check_reference not in payment_check_references:
                                                sale_order_id.action_import_invoice_payment(pdata['TotalPayments'],  pdata['order_date'], row_pdata_check_reference)
                                    elif ((pdata['TotalPayments'] > pdata['TotalAmount']) and ((pdata['TotalPayments'] - pdata['TotalAmount']) > 0) and ((pdata['TotalPayments'] - pdata['TotalAmount']) < 1)):
                                        # Due to Freight being imported as a integer and not a decimal... :-(
                                        if sale_order_id.amount_total == pdata['TotalAmount']:
                                            if pdata['TotalAmount'] > 0 and row_pdata_check_reference not in payment_check_references:
                                                sale_order_id.action_import_invoice_payment(pdata['TotalAmount'],pdata['order_date'], row_pdata_check_reference)
                                    else:
                                        log_vals['message'] = "No Payment is get imported for Sale Order(%s) because importing payment is greater than the balance due amount !" % str((
                                        pdata['SalesOrder']))
                                        # log_vals['message'] = "No Payment is get imported for Sale Order {} because importing payment is greater than the balance due amount !".format(pdata['SalesOrder'])
                                        # log_vals['date'] = datetime.now()
                                        create_log_bool = True
                                        not_imported_so_list.append(pdata['SalesOrder'])
                            else:
                                log_vals['message'] = "No Payment is get imported for Sale Order(%s) because Invoice is not get created !" % str((pdata['SalesOrder']))
                                # log_vals['message'] = "No Payment is get imported for Sale Order {} because Invoice is not get created !".format(pdata['SalesOrder'])
                                # log_vals['date'] = datetime.now()
                                create_log_bool = True
                                not_imported_so_list.append(pdata['SalesOrder'])
                        else:
                            log_vals['message'] = "Sale Order(%s) is not 'Confirmed' yet. So that 'Payment' is not get imported !" % str((pdata['SalesOrder']))
                            # log_vals['message'] = "Sale Order {} is not 'Confirmed' yet. So that 'Payment' is not get imported !".format(pdata['SalesOrder'])
                            # log_vals['date'] = datetime.now()
                            create_log_bool = True
                            not_imported_so_list.append(pdata['SalesOrder'])
                    else:
                        log_vals['message'] = 'No Sale Order record found for %s ' % str((pdata['SalesOrder']))
                        # log_vals['message'] = "No Sale Order record found for {}".format(pdata['SalesOrder'])
                        # log_vals['date'] = datetime.now()
                        create_log_bool = True
                        not_imported_so_list.append(pdata['SalesOrder'])

                    if create_log_bool:
                        log_create_id = LogManagement.create(log_vals)
                        # _logger.info(f"Sale Payment import log created : {log_create_id}")
                        _logger.info("Sale Payment import log created : %s" % str(log_create_id))
                    row_count += 1
                    # self.env.cr.commit()

                except Exception as e:
                    _logger.info("Exception inside For Loop : %s", e)
                    log_vals = {'operation': 'sales_payment',
                                'message': 'Sales(%s) payment import is Failed. (%s) ' % (str(pdata['SalesOrder']), e),
                               }
                    row_count += 1
                    log_create_id = LogManagement.create(log_vals)
                    _logger.info("Sale Payment import log created : %s", log_create_id)
                    not_imported_so_list.append(pdata['SalesOrder'])
                self.env.cr.commit()

        except Exception as e:
            _logger.info("Exception: %s .... ", e)
            log_vals = {'operation': 'sales_payment',
                        'message': "Sales payment import is Failed. {}".format(e),
                       }
            LogManagement.create(log_vals)
            self.env.cr.commit()

        # Popup view to return message
        pop_view = self.env.ref('sh_message.sh_message_wizard')
        pop_view_id = pop_view and pop_view.id or False
        pop_context = dict(self._context or {})
        if len(not_imported_so_list) > 0:
            pop_context['message'] = "File imported successfully. \n But %s SaleOrder is not get imported in system.\n Please check Log Management to track the Errors with the SaleOrder !" %(len(not_imported_so_list))
        else:
            pop_context['message'] = "File imported with all SaleOrder Payment in the system successfully."
        return {
                'name': 'Import Results',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'sh.message.wizard',
                'views': [(pop_view.id, 'form')],
                'view_id': pop_view_id,
                'target': 'new',
                'context': pop_context,
        }
