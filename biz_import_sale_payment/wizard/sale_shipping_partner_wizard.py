# -*- coding: utf-8 -*-

from odoo import fields, models, tools, api, _
from odoo.exceptions import UserError, ValidationError
import base64
import xlrd
import urllib.request, urllib.error
from datetime import datetime
import logging
import pandas as pd
import mimetypes
from pprint import pprint

_logger = logging.getLogger(__name__)


excel_csv_format = [
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
]


class SaleShippingPartnerImportWizard(models.TransientModel):
    _name = 'sale.shipping.partner.import.wizard'
    _description = 'Sale Shipping Partner Import Wizard'


    data_file = fields.Binary('Upload Excel File', required=True, attachment=False)
    filename = fields.Char('File Name', required=True)


    @api.multi
    def action_import_sale_shipping_partner(self):
        """ This method will import excel sheet using Pandas with Sale Order Shipping Partners """
        SaleOrder = self.env['sale.order']
        IrModelData = self.env['ir.model.data']
        ResPartner = self.env['res.partner']
        ResState = self.env['res.country.state']
        IrSequence = self.env['ir.sequence']
        not_imported_so_list = []
        ctx = self.env.context.copy()

        # Validate files format...only allow Excel sheet format files
        mimetype = None
        if mimetype is None and self.filename:
            mimetype = mimetypes.guess_type(self.filename)[0]
            if not mimetype in excel_csv_format:
                raise UserError('Please upload only excel sheet (.xls, .xlsx) format files !')
        try:
            excel_file_data = pd.read_excel(io=base64.decodestring(self.data_file))
            mandatory_columns = ['ID', 'ship_to', 'ship_name', 'shipping_address_1', 'shipping_address_2', 'shipping_city', 'shipping_state', 'shipping_zip', 'shipping_attn_to', 'shipping_phone', 'shipping_mobile']
        except Exception as e:
            _logger.exception('File not get imported !')
            raise UserError(
                _('File %r not imported due to a malformed file.'
                  '\n\nTechnical Details:\n%s') % \
                (self.filename, tools.ustr(e))
            )
        # Validate 13 columns with name in excel sheet # If column not contains mandatory field...raise alert
        if len(excel_file_data.columns) < 11:
            raise UserError(_("Please upload an excel sheet with the minimum 14 mandatory columns !"))
        if any([i for i in mandatory_columns if i not in excel_file_data.columns]):
            raise UserError(_("Please upload an excel sheet with the mandatory columns !\n\n"
                              "Mandatory columns are: \n%s" %(', '.join(map(str, mandatory_columns)))))
        excel_data_df = pd.DataFrame(data=excel_file_data)

        sheet_so_sequence = 'SF'    # Need to change with respect to the Sheet name
        sale_records = []
        so_seq_ids = IrSequence.search([('code', '=', 'sale.order')])
        for index, pdata in excel_data_df.iterrows():
            _logger.warning("Sheet Columns Data %s: ", pdata)
            if not pdata['ID']:
                continue
            if pdata['ID'] in sale_records:
                continue

            sale_order_val = {}
            partner_ship_vals = {'type': 'delivery', 'partner_type': 'ship to'}
            write_rec = False
            partner_found = False
            sheet_ship_name = ''
            create_shipping_address = False
            create_shipping_address_null = False

            sale_records.append(pdata['ID'])

            so_sequence_number = sheet_so_sequence + '-' + (str(pdata['ID']).strip()).rjust(so_seq_ids[0].padding, "0") #str(pdata['ID']).strip()
            _logger.warning("Sale Order Sequence Number: %s", so_sequence_number)
            ir_model_ids = IrModelData.search([('name', '=', so_sequence_number), ('model', '=', 'sale.order'), ('module', '=', 'confirm_sale_order')])
            _logger.warning("Ir Model Data: %s", ir_model_ids)
            if ir_model_ids:
                confirm_sale_id = SaleOrder.browse(ir_model_ids.res_id)
                _logger.warning("Sale Order is available with Id {} ".format(confirm_sale_id))
                sale_records.append(pdata['ID'])
                # If sheet--.columns i.e. ship_to(B), ship_name(C) and shipping_attn_to(H) are Blank..Then skip those SaleOrders
                # if not pdata['ship_to'] and not pdata['ship_name'] and not pdata['shipping_attn_to']:
                if pd.isna(pdata['ship_to']) and pd.isna(pdata['ship_name']) and pd.isna(pdata['shipping_attn_to']):
                    continue
                if (not pd.isna(pdata['ship_to']) and not pdata['ship_to'].strip()) and (not pd.isna(pdata['ship_name']) and not pdata['ship_name'].strip()) \
                        and (pd.isna(pdata['shipping_attn_to']) and pdata['shipping_attn_to'].strip()):
                    continue

                shipping_address_domain = [('type', '=', 'delivery')]
                # Prepare a dict values for either update or Create Partner Shipping in SO
                if not pd.isna(pdata['ship_name']):
                    partner_ship_vals.update({'name': str(pdata['ship_name']).strip()})
                    sheet_ship_name = pdata['ship_name']
                if not pd.isna(pdata['shipping_address_1']):
                    partner_ship_vals.update({'street': str(pdata['shipping_address_1']).strip()})
                    shipping_address_domain.append(('street', '=', str(pdata['shipping_address_1']).strip()))
                if not pd.isna(pdata['shipping_address_2']):
                    partner_ship_vals.update({'street2': str(pdata['shipping_address_2']).strip()})
                    shipping_address_domain.append(('street2', '=', pdata['shipping_address_2']))
                if not pd.isna(pdata['shipping_city']):
                    partner_ship_vals.update({'city': str(pdata['shipping_city']).strip()})
                    shipping_address_domain.append(('city', '=', str(pdata['shipping_city']).strip()))

                us_country_id = self.env['res.country'].search([('name', '=', 'United States')], limit=1)

                if not pd.isna(pdata['shipping_state']) and pdata['shipping_state'].strip():
                    state_id = ResState.search([('code', '=', pdata['shipping_state']), ('country_id', '=', us_country_id.id)], limit=1) # '|', ('name', '=', pdata[17]),
                    if len(state_id) > 0:
                        partner_ship_vals.update({'state_id': state_id.id})
                        shipping_address_domain.append(('state_id', '=', state_id.id))

                if not pd.isna(pdata['shipping_phone']):
                    partner_ship_vals.update({'phone': pdata['shipping_phone']})
                if not pd.isna(pdata['shipping_mobile']):
                    partner_ship_vals.update({'mobile': pdata['shipping_mobile']})
                if not pd.isna(pdata['shipping_zip']):
                    partner_ship_vals.update({'zip': pdata['shipping_zip']})
                    shipping_address_domain.append(('zip', '=', pdata['shipping_zip']))

                if not pd.isna(pdata['shipping_attn_to']):
                        sale_order_val.update({'x_attention_ship_to': str(pdata['shipping_attn_to']).strip()})
                else:
                    if sheet_ship_name:
                        sale_order_val.update({'x_attention_ship_to': sheet_ship_name})

                # If ship_to partner exist in System then we can check for their addresses should match..If yes Process for the same to partner_shipping_id..Else Create new with same Address and parent_id
                if not pd.isna(pdata['ship_to']) and pdata['ship_to'].strip(): #pdata['ship_to'] != 'nan':
                    ir_model_partner_ship_id = IrModelData.search(
                        [('name', '=', pdata['ship_to']), ('model', '=', 'res.partner')], limit=1)
                    _logger.warning("ir_model_partner_ship_id: %s", ir_model_partner_ship_id)
                    if ir_model_partner_ship_id:
                        partner_ship_id = ResPartner.browse(ir_model_partner_ship_id.res_id)
                        if len(partner_ship_id) > 0: # and partner_ship_id.id in partner_obj.ids:
                            partner_ship_vals.update({'parent_id': partner_ship_id.id})
                            partner_found = True
                            shipping_address_domain.append(('parent_id', '=', partner_ship_id.id))

                if partner_found:
                    shipping_address_ids = ResPartner.search(shipping_address_domain, limit=1)
                    _logger.warning("shipping_address_ids: %s.", shipping_address_ids)
                    if shipping_address_ids:
                        sale_order_val.update({'partner_shipping_id': shipping_address_ids.id})
                        write_rec = True
                    else:
                        create_shipping_address = True
                else:
                    # Create a new record for address...
                    _logger.warning("Sheet column 'ship_to' is NULL, we will create new shipping address.")
                    create_shipping_address = True
                    create_shipping_address_null = True

                if create_shipping_address:
                    _logger.warning("partner_ship_vals: %s", partner_ship_vals)
                    if create_shipping_address_null:
                        shipping_address_domain.append(('name', '=', pdata['ship_name']))
                        # If ship_to Null..Check for Ship_name in Partner system avaialble..if yes use that one else create new.
                        shipping_address_null_id = ResPartner.search(shipping_address_domain, limit=1)
                        if shipping_address_null_id:
                            sale_order_val.update({'partner_shipping_id': shipping_address_null_id.id})
                            write_rec = True
                        else:
                            new_shipping_id = ResPartner.create(partner_ship_vals)
                            _logger.warning("New Created Shipping: %s", new_shipping_id)
                            if len(new_shipping_id) > 0:
                                sale_order_val.update({'partner_shipping_id': new_shipping_id.id})
                                write_rec = True
                    else:
                        new_shipping_id = ResPartner.create(partner_ship_vals)
                        _logger.warning("New Created Shipping: %s", new_shipping_id)
                        if len(new_shipping_id) > 0:
                            sale_order_val.update({'partner_shipping_id': new_shipping_id.id})
                            write_rec = True

                if write_rec:
                    confirm_sale_id.write(sale_order_val)
                    self.env.cr.commit()
            else:
                not_imported_so_list.append(pdata['ID'])
        _logger.warning("Sheet Sequence Number not found in IrModelData: %s: %s", len(not_imported_so_list), not_imported_so_list)

        # Popup view to return message
        pop_view = self.env.ref('sh_message.sh_message_wizard')
        pop_view_id = pop_view and pop_view.id or False
        pop_context = dict(self._context or {})
        if len(not_imported_so_list) > 0:
            pop_context['message'] = "File imported successfully. \n But %s SaleOrder ID is not get imported for shipping address.!" %(len(not_imported_so_list))
        else:
            pop_context['message'] = "File imported in the system successfully."
        return {
                'name': 'Import Results',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'sh.message.wizard',
                'views': [(pop_view.id, 'form')],
                'view_id': pop_view_id,
                'target': 'new',
                'context': pop_context,
        }
