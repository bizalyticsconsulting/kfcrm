# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = "sale.order"


    def action_import_invoice_payment(self, invoice_amount, payment_date, row_pdata_check_reference):
        """ This method will create Account Payment using Invoice Amount and Payment Date given in Excel Sheet """
        if invoice_amount <= 0:
            log_vals = {'operation': 'sales_payment',
                        'message': "No Payment is get imported for Sale Order(%s) because importing payment amount is less than zero !" % (self.name),
                        'date': datetime.now()
                       }
            self.env['log.management'].create(log_vals)
            return False
        if invoice_amount > 0:
            for single_invoice in self.invoice_ids:
                invoice_id = single_invoice
                if invoice_id.state != 'paid':
                    invoice_id = invoice_id.with_context(active_id=self.id, active_ids=self.ids)
                    method = invoice_id.journal_id.inbound_payment_method_ids
                    # checks for Payments and create a dict full of values of either cash or bank to be sent for invoice
                    payment_values = {'amount': invoice_amount,
                                      'journal_id': self.ks_journal_id.id,
                                      'currency_id': invoice_id.currency_id.id,
                                      'payment_date': payment_date,
                                      'payment_method_id': method[0].id or False,
                                      'payment_type': "inbound",
                                      # recieve money, seller paid and waiting for customer to pay
                                      'invoice_ids': [(6, 0, invoice_id.ids)],
                                      'partner_type': 'customer',
                                      'partner_id': self.partner_id.id,
                                      'communication': self.ks_sale_communication if self.ks_sale_communication else invoice_id.number,
                                      'branch_id': self.branch_id.id or False,
                                      'new_check_number': row_pdata_check_reference,
                                    }
                    # Creating invoice of Bank only & can be called multiple times
                    _logger.info(_(f"Payment values: {payment_values}"))
                    account_payment_id = self.env['account.payment'].create(payment_values)
                    _logger.info(_(f"Payment has been created: {account_payment_id}"))

                    # Below is the part of KS Payment as expected..
                    account_payment_id._onchange_amount()
                    account_payment_id._compute_payment_difference()
                    if account_payment_id.payment_difference > 0:
                        account_payment_id.payment_difference_handling = "open"
                    ks_payment_id = account_payment_id.with_context(active_id=invoice_id.id, active_ids=invoice_id.ids)
                    ks_payment_id.action_validate_invoice_payment()
                    _logger.info(_("Payment is Posted successfully !"))
                    # get the unpaid amount and show it to user
                    self.ks_residual = invoice_id.residual

        return True
