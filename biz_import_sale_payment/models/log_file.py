import time
from odoo import api, fields, models
from datetime import datetime


class LogManagement(models.Model):
    _inherit = "log.management"


    operation = fields.Selection([('user', 'Employees'),
                                  ('tax_location', 'Sales Purchase Tax Location'),
                                  ('ship_via_del', 'Ship Via Delivery'),
                                  ('terms', 'Terms'),
                                  ('sales_categ', 'Sales Categories'),
                                  ('locations', 'Locations'),
                                  ('vendors', 'Vendors'),
                                  ('p_manufactured', 'Product Manufactured'),
                                  ('p_purchased', 'Product Purchased'),
                                  ('add_contacts', 'Address Contacts'),
                                  ('con_sale', 'Confirmed SaleOrder'),
                                  ('customers', 'Customers'),
                                  ('quotes', 'Quotes'),
                                  ('q_lineitem', 'Quote Line Item'),
                                  ('sale_order_lineitem', 'Sale Order LineItem'),
                                  ('rfq', 'RFQ'),
                                  ('rfq_lineitem', 'RFQ Line Item'),
                                  ('purchase_order', 'PurchaseOrder'),
                                  ('pur_lineitem', 'Purchase Line Item'),
                                  ('bom', 'BOM'),
                                  ('bom_line', 'BOM Lines'),
                                  ('cust_inv', 'Customer Invoices'),
                                  ('ven_bill', 'Vendor Bills'),
                                  ('sales_payment', 'Sales Payment')], string="Operation")
    date = fields.Datetime('Date', default=lambda self: fields.datetime.now())