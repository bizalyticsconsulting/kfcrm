﻿

--Create View for calculate amount_total for Account Invoice

create view x_invoice_total as 
select ail.id, ail.invoice_id, (ail.price_subtotal_signed + ail.freight + ail.crating + ailinstall + other_charges) - discount_amount as total_amount 
from account_invoice_line ail, account_invoice ai
where  ai.id = ail.invoice_id


--Update Account Invoice using View amount_total

update account_invoice set amount_total = acc_sq.total_invoice_amount
from (select xit.invoice_id as invoice_id, sum(xit.total_amount) + sum(ai.amount_tax) as total_invoice_amount
	from 
	x_invoice_total xit,
	account_invoice ai
where xit.invoice_id = ai.id
group by xit.invoice_id
) as acc_sq
where id=acc_sq.invoice_id and id=28587




