# -*- coding: utf-8 -*-

{
    'name' : 'Import Sales Payment',
    'version' : '1.0',
    'author' : 'Biz',
    'category' : 'Extra Tools',
    'description' : """
""",
    "summary":"This Module can be used to import Sales Payment",
    'website': 'https://www.globalteckz.com',
    'depends' : ['sale', 'account', 'showroom_fields', 'gt_mssql_import', 'gt_studio_it_import_fields', 'gt_log_management', 'sh_message', 'ks_oneclick_sale_purchase'],
    'data': [
        'wizard/sales_payment_wizard.xml',
        'wizard/sale_shipping_partner_wizard_view.xml',
        'security/ir.model.access.csv'
    ],
    'qweb' : [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
