# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime

class StockPicking(models.Model):
    _inherit = "stock.picking"

    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
        ('3', 'Full Payment & Shipped Validation'),
    ], "When Paid", copy=False)
    
    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref):
        commission_journal = self.env['ir.values'].get_default('account.config.settings', 'commission_journal')
        account_obj = self.env['account.journal']
        invoice_obj = self.env['account.invoice']
        sale_obj = self.env['sale.order']
        account_id = account_obj.browse(commission_journal)
        sale_search = sale_obj.search([('name', '=', self.origin)], order="id desc", limit=1)
        prod_commissinable_amt = sum([i.price_subtotal for i in sale_search.order_line if i.product_id.flag])
        if self.env.context.get('delivery_order_contxt'):
            commission_pay = 'showroom_partner'
            comm_partner_id = sale_person.id
            sales_user = False
            commission_partner = comm_partner_id
        else:
            commission_pay = 'salesperson'
            comm_partner_id = sale_person.partner_id.id
            sales_user = sale_person.id
            commission_partner = False
        
        commission_vals = {
            'name': account_id.default_debit_account_id.name,
            'debit': commission_amt,
            'credit': 0.0,
            'account_id': account_id.default_debit_account_id.id,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_on': self.order_pay_state,
            'commission_partner': commission_partner,
            'sale_order_id': sale_search.id,
            'commission_pay': commission_pay,
            'commissionable_amount': prod_commissinable_amt,
            'commission_rate': sale_search.x_showroom.commissions_percentage,
            'total_commission': commission_amt,
            'sales_commission_rate': sale_search.x_showroom.commissions_percentage,
        }
        sale_person_vals = {
            'name': sale_person.name,
            'debit': 0.0,
            'credit': commission_amt,
            'account_id': sale_person.property_account_payable_id.id,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_on': self.order_pay_state,
            'commission_partner': commission_partner,
            'sale_order_id': sale_search.id,
            'commission_pay': commission_pay,
            'commissionable_amount': prod_commissinable_amt,
            'commission_rate': sale_search.x_showroom.commissions_percentage,
            'total_commission': commission_amt,
            'sales_commission_rate': sale_search.x_showroom.commissions_percentage,
        }
        now = datetime.now()
        vals = {
            'journal_id': account_id.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': journal_ref,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_partner': commission_partner,
        }
        move = self.env['account.move'].create(vals)
        # move.post()
        return move.id

    @api.multi
    def do_transfer(self):
        sale_obj = self.env['sale.order']
        ctx = self.env.context.copy()
        commission_type = self.env['ir.values'].get_default('sale.config.settings', 'commission_calculation')
        res = super(StockPicking, self).do_transfer()
        sale_id = sale_obj.search([('name', '=', self.origin)], order="id desc", limit=1)
        if self.picking_type_id and self.picking_type_id.code == 'outgoing':
            #Full Payment and Shipped Delivery Order     
            if self.order_pay_state == '3':
                #Check for the FUll PAYMENT against the SaleOrder and Full DeliveryOrder FUlly Shipped
                invoice_payments = sum([k.amount for j in sale_id.invoice_ids for k in j.payment_ids if k.state in ['posted','reconciled']])
                if sale_id.amount_total <= invoice_payments:
                    #Check for the Fully Shipped
                    not_done_state = any([i for i in sale_id.picking_ids if i.state != 'done'])
                    if not_done_state:
                        return res
                    line_prod_not_full_delvd = any([i for i in sale_id.order_line if i.product_id.type != 'service' if i.product_uom_qty > i.qty_delivered])
                    if line_prod_not_full_delvd:
                        return res
                    if sale_id.picking_ids and not line_prod_not_full_delvd and not sale_id.account_move_line_ids:
                        reference = '[' + sale_id.name + '] ' + self.name + ' Delivery Order'
                        if commission_type == '0':
                            for product_line in sale_id.order_line:
                                if product_line.product_id.prod_comm_per and product_line.product_id.prod_comm_per > 0:
                                    product_commission = (product_line.price_subtotal * product_line.product_id.prod_comm_per) / 100
                                    total_inv_commission += product_commission
                            reference = '[' + sale_id.name + '] ' + invoice_id.number + ' Payment'
                            self.commission_count_entries(total_inv_commission, sale_id.user_id, reference)
            
                        if commission_type == '1':
                            for product_line in sale_id.order_line:
                                if product_line.product_id.categ_id.prod_categ_comm_per and product_line.product_id.categ_id.prod_categ_comm_per > 0:
                                    product_commission = (product_line.price_subtotal * product_line.product_id.categ_id.prod_categ_comm_per) / 100
                                    total_inv_commission += product_commission
                            reference = '[' + sale_id.name + '] ' + invoice_id.number + ' Payment'
                            self.commission_count_entries(total_inv_commission, sale_id.user_id, reference)
            
                        if commission_type == '2':
                            if sale_id.team_id.sale_manage_comm and sale_id.team_id.sale_manage_comm > 0:
                                manager_commission = (sale_id.amount_untaxed * sale_id.team_id.sale_manage_comm) / 100
                                self.commission_count_entries(manager_commission, sale_id.team_id.user_id, reference)
                            if sale_id.team_id.sale_member_comm and sale_id.team_id.sale_member_comm > 0:
                                member_commission = (sale_id.amount_untaxed * sale_id.team_id.sale_member_comm) / 100
                                self.commission_count_entries(member_commission, sale_id.user_id, reference)
                        
                        #Showroom Partner Commission
                        if commission_type == '3':
                            ctx.update({'delivery_order_contxt': True})
                            total_order_commission = 0
                            for product_line in sale_id.order_line:
                                if sale_id.x_showroom and sale_id.x_showroom.commissions_percentage > 0 and product_line.product_id.flag: #self.x_showroom
                                    product_commission = (
                                                         product_line.price_subtotal * sale_id.x_showroom.commissions_percentage) / 100
                                    total_order_commission += product_commission
                                    # total_order_commission = (self.amount * sale_id.x_showroom.commissions_percentage) / 100
                            if sale_id.x_showroom:        
                                self.with_context(ctx).commission_count_entries(total_order_commission, sale_id.x_showroom, reference)
                            # sale_id.commission_generated = True
                            else:
                                raise UserError(_("Please select Showroom Partner in SaleOrder for the commission !"))
        return res
   
    @api.multi
    def _create_backorder(self, backorder_moves=[]):
        sale_obj = self.env['sale.order']
        res = super(StockPicking, self)._create_backorder(backorder_moves)
        if self.origin:
            sale_id = sale_obj.search([('name','=',self.origin)], order="id desc", limit=1)
            if sale_id:
                res.write({'order_pay_state': sale_id.order_pay_state})
        return res
    
class StockMove(models.Model):
    _inherit = "stock.move"
                        
    def _get_new_picking_values(self):
        """ Prepares a new picking for this move as it could not be assigned to
        another picking. This method is designed to be inherited. """
        res = super(StockMove, self)._get_new_picking_values()
        sale_order_srch = self.env['sale.order'].search([('name','=',self.origin)], order="id desc", limit=1)
        if sale_order_srch:
            order_pay_state = sale_order_srch.order_pay_state
            res.update({'order_pay_state': order_pay_state})
        return res