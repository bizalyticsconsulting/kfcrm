from odoo import models, fields, api, _,tools 
from datetime import date
import logging

_logger = logging.getLogger(__name__)

class special_instruction(models.Model):
   _name = "special.instruction"
   _description = "Special Instruction Vendor"
   _rec_name = "vendor"


   @api.depends('category')
   def _compute_vendor(self):
      print(self)
      for vendor in self:
         vendor.vendor = vendor.special_vendor_id

   vendor = fields.Many2one('res.partner', string="Vendor", domain=[('supplier', '=', 'True')], compute='_compute_vendor')
   category = fields.Many2one('product.category', string='Category')
   instruction = fields.Html(string='Instruction')
   special_vendor_id = fields.Many2one('res.partner',string='Special Vendor')

   @api.model
   def search(self, args, offset=0, limit=0, order=None, count=False):
      args = args or []
      x_supplier_id = x_prod_category_id = False
      if self.env.context.get('sale_special_instruction'):
         if self.env.context.get('x_supplier_id'):
            x_supplier_id = self.env.context.get('x_supplier_id')

         if self.env.context.get('x_prod_category_id'):
            x_prod_category_id = self.env.context.get('x_prod_category_id')

         x_categ_all_id = self.env['product.category'].search([('name', '=', 'All')], limit=1)
         x_categ_all_id = x_categ_all_id and x_categ_all_id.id or False
         # args += ['|', '&', ['vendor', 'in', [False, x_supplier_id]],
         #                    ['category', 'in', [False, x_categ_all_id, x_prod_category_id]],
         #                ['vendor', '=', False],
         #         ]
         args += ['|', '&', ['special_vendor_id', 'in', [False, x_supplier_id]],
                            ['category', 'in', [False, x_categ_all_id, x_prod_category_id]],
                        ['special_vendor_id', '=', False],
                  ]
      return super(special_instruction, self).search(args=args, offset=offset, limit=limit, order=order, count=count)


class partner_fields(models.Model):
   _inherit = 'res.partner'

   instruction_line = fields.One2many('special.instruction', 'special_vendor_id', string='Special Instruction')


class sale_order(models.Model):
   _inherit = 'sale.order'

   @api.multi
   def _load_default_insturction(self):
      speical_ids = []
      for line in self.order_line:
         for special in line.x_vendor.instruction_line:
            # print("specialspecial>>>>>>>>>>>>>",special)

            speical_ids.append(special.id)
      # special_line = self._compute_special_insturction()
      # print("special_line<<<<<<<<<<<<<<",speical_ids)
      if speical_ids:
         domain = [('id', 'in', speical_ids)]
         # print("domaindomaindomain",domain)
      else:
         domain = []
      # print("domain<<<<<<<<<<<<",domain)
      return domain
   #

   # @api.depends('instruction_list')
   # def _load_default_insturction(self):
   #    speical_ids = []
   #    for line in self.order_line:
   #       for special in line.x_vendor.instruction_line:
   #          print("specialspecial>>>>>>>>>>>>>",special)
   #          speical_ids.append(special.id)
   #    print("speical_ids<<<<<<<<<<<<<<<<<????????????",speical_ids)
   #    return speical_ids



   # domain = _load_default_insturction
   instruction_list = fields.Many2many('special.instruction', 'wiz_special_inst_id', 'special_id', 'special_id_rel',
                                       string='List Of Instruction', domain=_load_default_insturction)
   # compute = '_compute_instruction_list'
   sol_vendor_ids = fields.Many2many('res.partner', 'sol_vendor_rel', 'sol_vendor', 'vendor_id', string='Vendors')

   @api.onchange('order_line')
   @api.depends('order_line')
   def onchange_order_linefor_special(self):
      speical_ids = []
      domain = {}
      if self.order_line:
         self.x_supplier = self.order_line[0].x_vendor
      for line in self.order_line:
         for special in line.x_vendor.instruction_line:
            speical_ids.append(special.id)
            domain = {'instruction_list': [('id', 'in', speical_ids)]}
      return {'domain': domain}

   # 'value': {'instruction_list': speical_ids}



   @api.onchange('instruction_list')
   def special_instruction_value(self):
      special_instruction = ''
      p_id = []
      for line in self.instruction_list:
         p_id.append(line.id)
         if line.instruction:
            special_instruction += line.instruction + ' '
      if self.x_special_inst:
         self.x_special_inst += special_instruction
      else:
         self.x_special_inst= special_instruction


   @api.model
   def create(self, vals):
      res = super(sale_order, self).create(vals)
      if res.instruction_list:
         res.instruction_list = [(6,0,[])]
      return res

   @api.multi
   def write(self, vals):
      if vals.get('instruction_list'):
         vals.update({'instruction_list': [(6,0,[])]})
      res = super(sale_order, self).write(vals)
      return res



class sale_order_line(models.Model):
   _inherit = 'sale.order.line'

   # @api.multi
   # @api.onchange('x_vendor')
   # def x_vendor_id_change(self):
   #    # so = super(sale_order_line, self).product_id_change()
   #    vendors = []
   #    for line in self:
   #       line.order_id.sol_vendor_ids = (4, line.x_vendor.id)
   #       print("line.x_vendor.id",line.x_vendor.id)
   #       # # line.order_id['sol_vendor_ids'] = [(6, 0, [line.x_vendor.id])]
   #       # line.order_id.write({'sol_vendor_ids': (6, 0, [line.x_vendor.id])})
   #
   #       print("sososooooooooosososo",line.order_id.sol_vendor_ids)
   #    # return line






