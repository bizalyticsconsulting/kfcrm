from odoo import models, fields, api, _,tools 
from datetime import date
import logging
import re

_logger = logging.getLogger(__name__)


class WizardInstLine(models.TransientModel):
   _name = "wiz.inst.line"

   vendor = fields.Many2one('res.partner', string="Vendor",)
   category = fields.Many2one('product.category', string='Category')
   instruction = fields.Html(string='Instruction')
   select = fields.Boolean( string='Sr.No')
   wiz_special_inst_id = fields.Many2one('wiz.special.instruction', string='Wiz Special')


class special_instruction(models.TransientModel):
   _name = "wiz.special.instruction"


   # instruction_list = fields.Many2many('special.instruction', 'wiz_special_inst_id', 'special_id', 'special_id_rel', string='List Of Instruction')
   instruction_list = fields.One2many('wiz.inst.line', 'wiz_special_inst_id', string='List Of Instruction')

   @api.model
   def default_get(self, fields):
      res = super(special_instruction, self).default_get(fields)
      active_id = self._context.get('active_id')
      active_model = self._context.get('active_model')
      sale_id = self.env[active_model].browse(active_id)
      instruction_list = []
      for sale_line in sale_id.order_line:
         print("sale_line = []",sale_line)
         for special_inst in sale_line.x_vendor.instruction_line:
            instruction_list.append((0,0,{'vendor':special_inst.vendor.id,'category':special_inst.category.id,
                                          'instruction':special_inst.instruction}))
      res.update({'instruction_list':instruction_list})
      return res


   @api.multi
   def select_special_instruction(self):
      # special_line = self.instruction_list.filtered(lambda r: r.select == True)
      # print(special_line)
      active_id = self._context.get('active_id')
      active_model = self._context.get('active_model')
      sale_id = self.env[active_model].browse(active_id)
      special_instruction =''
      for line in self.instruction_list1:
         if line.instruction:
            special_instruction += line.instruction + ' '
      if sale_id.x_special_inst:
         sale_id.x_special_inst += special_instruction
      else:
         sale_id.write({'x_special_inst':special_instruction})


      # for inst_line in self.instruction_list:



