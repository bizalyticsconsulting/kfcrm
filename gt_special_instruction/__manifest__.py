# -*- coding: utf-8 -*-
{
    'name': "Special Instruction",

    'summary': """
	Module adds custom fields to Quotes and Sales Orders for the Showrooms. 
    """,

    'description': """
	The fields are 
        - Terms
        - Freight Terms
        - FOB
        - Attention —> Customer Contact
        - Client PO
        - Client Account
        - Sidemark
        - Mfg Order
        - Mfg Order Date
        - Vendor Invoice Number
        - Vendor Invoice Date
        - Special Instructions
        - CFA
        - Salesperson 1
        - Salesperson 2
        - Ship Date
        - Tracking Number
    """,

    'author': "BizAlytics Consulting LLC",
    'website': "http://www.bizalytics.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sales',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale'],

    # always loaded
    'data': [
         'security/ir.model.access.csv',
         # 'wizard/supplier_disc_wiz_view.xml',
         'instruction_fields_view.xml',

        
        
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
