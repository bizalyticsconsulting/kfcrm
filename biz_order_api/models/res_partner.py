from odoo import fields, models, api, _
from odoo.exceptions import UserError
from datetime import datetime
import requests
import json
DTF = '%Y-%m-%d %H:%M:%S'
DF = '%Y-%m-%d'

import logging

_logger = logging.getLogger(__name__)

class Partner(models.Model):
    _inherit = 'res.partner'

    import_orders = fields.Boolean(string='Import Orders', copy=False)
    get_token_url = fields.Char(string='Token URL', copy=False)
    get_order_url = fields.Char(string='Order URL', copy=False)
    get_single_order_url = fields.Char(string='Single Order URL', copy=False)
    postman_key = fields.Char(string='Postman Key', copy=False)
    showroom_mapping_line_ids = fields.One2many('showroom.mapping.line', 'partner_id', 'Showroom Mapping Lines')
    api_order_date_from = fields.Date(string='API Order Date From', default=fields.Date.today)
    api_order_date_to = fields.Date(string='API Order Date To', default=fields.Date.today)

    def create_so_from_api(self, multiple_orders_dict=False, single_order_dict=False, logger=False):
        showroom_mapping_line_obj = self.env['showroom.mapping.line']
        dropship_custom_obj = self.env['dropship.custom']
        product_obj = self.env['product.product']
        sale_order_obj = self.env['sale.order']
        api_logger_obj = self.env['api.logger']
        list_logger_vals = []

        ### Sale Order creating code.
        if multiple_orders_dict:
            final_order_list = multiple_orders_dict.get('orders', [])
        else:
            final_order_list = []
            final_order_list.append(single_order_dict.get('order', []))

        for order in final_order_list:
            create_sale_order = True
            x_mfg_order = order.get('orderNo', '')
            mfg_invoice = order.get('no', '')
            _logger.debug("Importing order number .... %s", x_mfg_order)
            _logger.debug("Importing invoice number .... %s", mfg_invoice)
            existing_order = sale_order_obj.search([('mfg_invoice', '=', mfg_invoice)], limit=1)
            if existing_order:
                _logger.debug("SKIPPING Order as it already exists in the system %s", mfg_invoice)
                list_logger_vals.append({'order_number': x_mfg_order,
                                       'customer_number': order.get('sellToCustomerNo', ''),
                                       'ssi_order_number': mfg_invoice,
                                       'state': 'false',
                                       'comment': "SSI Order Number '%s', Customer %s Number from API '%s', Order SSI Order number for Customer NOT Imported as Sales Order is already imported %s" % (
                                        mfg_invoice, order.get('sellToCustomerName', ''), order.get('sellToCustomerNo', ''), existing_order.name)})
                continue

            so_line_vals = []
            partner_id = dropship_custom_obj.search([('vendor', '=', self.id), ('account_no', '=', order.get('sellToCustomerNo', ''))]).mapped('dropship_id').filtered(lambda par: par.active and par.customer)
            if not partner_id:
                _logger.debug("SKIPPING Order as customer does not exist in the system %s", order.get('sellToCustomerName', ''))

                list_logger_vals.append({'order_number': x_mfg_order,
                                       'customer_number': order.get('sellToCustomerNo', ''),
                                       'ssi_order_number': mfg_invoice,
                                       'state': 'false',
                                       'comment': "SSI Order Number '%s', Customer %s Number from API '%s', Order SSI Order number for Customer NOT Imported as Customer %s is Not Found" % (
                                       mfg_invoice, order.get('sellToCustomerName', ''), order.get('sellToCustomerNo', ''), order.get('sellToCustomerNo', ''))})
                continue
            if len(partner_id) > 1:
                _logger.debug("SKIPPING Order as multiple customers exist in the system %s", order.get('sellToCustomerName', ''))

                list_logger_vals.append({'order_number': x_mfg_order,
                                       'customer_number': order.get('sellToCustomerNo', ''),
                                       'ssi_order_number': mfg_invoice,
                                       'state': 'false',
                                       'comment': "SSI Order Number '%s', Customer %s Number from API '%s', Order SSI Order number for Customer NOT Imported as multiple Customer %s are Found %s" % (
                                       mfg_invoice, order.get('sellToCustomerName', ''), order.get('sellToCustomerNo', ''), order.get('sellToCustomerName', ''), ', '.join(partner_id.mapped('name')))})
                continue
            showroom_mapping_line_id = showroom_mapping_line_obj.search([('partner_id', '=', self.id), ('showroom_code', '=', order.get('showroomCode', ''))], limit=1)
            so_vals = {
                'partner_id': partner_id.id,
                'partner_shipping_id': partner_id.id,
                'x_supp_production_status': 'SHIPPED',
                'branch_id': showroom_mapping_line_id.branch_id.id if showroom_mapping_line_id else False,
                'user_id': partner_id.user_id.id,
                'team_id': partner_id.team_id.id,
                'x_mfg_order': x_mfg_order,
                'confirmation_date': datetime.strptime(order.get('orderDate', '') + ' 14:00:00', DTF),
                'date_order': datetime.strptime(order.get('orderDate', '') + ' 14:00:00', DTF),
                'x_order_shipdate': order.get('shipmentDate', ''),
                'mfg_invoice': mfg_invoice,
                'x_tracking_num': order.get('trackingNo', ''),
                'x_client_po': order.get('poNum', ''),
            }
            for invoice_line in order.get('invoice_line', []):
                if invoice_line.get('quantity', 0) == 0:
                    continue
                internal_reference = self.partner_code + '-' + invoice_line.get('itemCategoryCode', '') + '-' + invoice_line.get('no', '') + '-' + invoice_line.get('colorCode', '')
                product_id = product_obj.search([('default_code', '=', internal_reference)], limit=1)
                if not product_id:
                    create_sale_order = False
                    _logger.debug("SKIPPING Order as product does not exist in the system %s", internal_reference)

                    list_logger_vals.append({'order_number': x_mfg_order,
                                           'customer_number': order.get('sellToCustomerNo', ''),
                                           'ssi_order_number': mfg_invoice,
                                           'state': 'false',
                                           'comment': "SSI Order Number '%s', Customer %s Number from API '%s', Order SSI Order number for Customer NOT Imported as Product %s is Not Found" % (
                                           mfg_invoice, order.get('sellToCustomerName', ''), order.get('sellToCustomerNo', ''), internal_reference)})
                    continue
                else:
                    _logger.debug("PRODUCT FOUND ... %s", internal_reference)
                    so_line_vals.append((0, 0, {'name': product_id.name,
                                                'product_id': product_id.id,
                                                'product_uom_qty': invoice_line.get('quantity', 0),
                                                'x_internal_cat1': product_id.categ_id.id,
                                                'product_uom': product_id.uom_id.id,
                                                'price_unit': invoice_line.get('unitPrice', 0)}))

                    if not so_vals.get("x_prod_category"):
                        so_vals.update({"x_prod_category": product_id.categ_id.id})

            if len(so_line_vals) == 0:
                create_sale_order = False
                _logger.debug("SO_LINE_VALS IS EMPTY!!!!")

            create_sale_order = False

            if create_sale_order and len(so_line_vals) > 0:
                so_vals['order_line'] = so_line_vals
                sale_order = sale_order_obj.create(so_vals)
                list_logger_vals.append({'order_number': x_mfg_order,
                                       'customer_number': order.get('sellToCustomerNo', ''),
                                       'ssi_order_number': mfg_invoice,
                                       'state': 'true',
                                       'comment': "SSI Order Number '%s', Customer %s Number from API '%s', Order SSI Order number for Customer %s has been imported as Order# %s" % (
                                        mfg_invoice, order.get('sellToCustomerName', ''), order.get('sellToCustomerNo', ''), sale_order.partner_id.name, sale_order.name)})
                sale_order.ks_confirm_oneclick_sale()
        for logger_vals in list_logger_vals:
            api_logger_obj.create(logger_vals)
        return list_logger_vals



    @api.multi
    def import_order_from_postman(self, api_order_date_from=False, api_order_date_to=False):
        ### API calling code and data return.
        token_url = self.get_token_url
        order_url = self.get_order_url

        ### If configuration will not founded then warning msgs will be raised.
        if not token_url:
            raise UserError(_("Please set the Token URL in the selected Vendor."))
        if not order_url:
            raise UserError(_("Please set the Order URL in the selected Vendor."))
        if not self.postman_key:
            raise UserError(_("Please set the Postman Key in the selected Vendor."))
        if api_order_date_from and api_order_date_to:
            order_url = order_url + "?fromDate=%s&toDate=%s" % (api_order_date_from, api_order_date_to)

        key_dict = json.loads(self.postman_key)
        response_dict = requests.post(token_url, json=key_dict)
        tocken_dict = json.loads(response_dict.text)
        access_token = tocken_dict.get('access_token')
        headers = {'Authorization': 'Bearer' +' ' + access_token, 'Content-Type': 'application/json'}
        order_response = requests.get(order_url, headers=headers)
        multiple_orders_dict = json.loads(order_response.text)
        return self.create_so_from_api(multiple_orders_dict=multiple_orders_dict)

    @api.multi
    def action_api_order_import_cron(self):
        # self.api_order_date_from = self.api_order_date_to
        # self.api_order_date_to = datetime.strftime(fields.datetime.now(), DF)
        vendors = self.search([('supplier', '=', True),('import_orders','=',True)])
        for vendor in vendors:
            api_logger_data_dict = vendor.import_order_from_postman(datetime.strftime(self.api_order_date_from, DF) if self.api_order_date_from else False, datetime.strftime(self.api_order_date_to, DF) if self.api_order_date_to else False)
            email_template = self.env.ref('biz_order_api.email_template_biz_order_api_report')
            if email_template:
                email_template.with_context(api_logger_data_dict=api_logger_data_dict).send_mail(email_template.report_template.id, force_send=True)

class ShowroomMappingLine(models.Model):
    _name = 'showroom.mapping.line'
    _description = 'Showroom Mapping Line'

    partner_id = fields.Many2one('res.partner', string='Customer')
    branch_id = fields.Many2one('res.branch', string='Showroom')
    showroom_code = fields.Char(string='Showroom Code')
