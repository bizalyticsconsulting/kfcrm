from odoo import fields, models, api, _


class APILogger(models.Model):
    _name = 'api.logger'
    _description = 'API Logger'
    _order = "name desc"

    name = fields.Char(string='Name', required=True, copy=False, readonly=True, default=lambda self: _('New'))
    order_number = fields.Char(string='Order Number', copy=False, readonly=True)
    ssi_order_number = fields.Char(string='SSI Order Number', copy=False, readonly=True)
    customer_number = fields.Char(string='Customer Number', readonly=True)
    state = fields.Selection([('true', 'Imported'), ('false', 'Failed')], string='Status', readonly=True, required=True, copy=False, default='false')
    comment = fields.Text('Comment', readonly=True)

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('api.logger') or _('New')
        return super(APILogger, self).create(vals)
