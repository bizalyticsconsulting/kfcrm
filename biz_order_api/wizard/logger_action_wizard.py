from odoo import models, api, fields, _
from odoo.exceptions import UserError
import requests
import json


class LoggerActionWizard(models.TransientModel):
    _name = 'logger.action.wizard'
    _description = 'Logger Action Wizard'

    partner_id = fields.Many2one('res.partner', string='Vendor', domain=[('supplier', '=', True),('import_orders','=',True)], required=True)

    @api.multi
    def action_reimport_order_from_api(self):
        logger_ids = self.env.context.get('active_ids',False)
        for logger in self.env['api.logger'].browse(logger_ids):
            token_url = self.partner_id.get_token_url

            ### If configuration will not founded then warning msgs will be raised.
            if not token_url:
                raise UserError(_("Please set the Token URL in the selected Vendor."))
            if not self.partner_id.get_single_order_url:
                raise UserError(_("Please set the Single Order URL in the selected Vendor."))
            if not self.partner_id.postman_key:
                raise UserError(_("Please set the Postman Key in the selected Vendor."))

            order_url = self.partner_id.get_single_order_url + logger.ssi_order_number
            key_dict = json.loads(self.partner_id.postman_key)
            response_dict = requests.post(token_url, json=key_dict)
            tocken_dict = json.loads(response_dict.text)
            access_token = tocken_dict.get('access_token')
            headers = {'Authorization': 'Bearer' + ' ' + access_token, 'Content-Type': 'application/json'}
            order_response = requests.get(order_url, headers=headers)
            single_order_dict = json.loads(order_response.text)
            self.partner_id.create_so_from_api(single_order_dict=single_order_dict, logger=logger)