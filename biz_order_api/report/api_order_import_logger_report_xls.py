from odoo import models


class APIOrderImportLogger(models.AbstractModel):
    _name = 'report.biz_order_api.api_order_import_logger_report_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, sales_invoice_report_wizard):
        header = workbook.add_format({'bold': True, 'font_size': 12,'bg_color': '#dee6ef'})

        filter_dates = self.env.context.get('api_logger_data_dict', False)
        sheet = workbook.add_worksheet('API Logger')
        sheet.set_column(0, 0, 15)
        sheet.set_column(1, 1, 15)
        sheet.set_column(2, 2, 15)
        sheet.set_column(3, 3, 15)
        sheet.set_column(4, 4, 15)
        sheet.set_column(5, 5, 300)

        sheet.write(0, 0, 'Logger No.', header)
        sheet.write(0, 1, 'Order No.', header)
        sheet.write(0, 2, 'Customer No.', header)
        sheet.write(0, 3, 'SSI Order No.', header)
        sheet.write(0, 4, 'Status', header)
        sheet.write(0, 5, 'Comment', header)

        row_index = 1
        for logger_dict in filter_dates:
            sheet.write(row_index, 0, logger_dict.get('name', ''))
            sheet.write(row_index, 1, logger_dict.get('order_number', ''))
            sheet.write(row_index, 2, logger_dict.get('customer_number', ''))
            sheet.write(row_index, 3, logger_dict.get('ssi_order_number', ''))
            sheet.write(row_index, 4, 'Imported' if logger_dict.get('state', '') == 'true' else 'Failed')
            sheet.write(row_index, 5, logger_dict.get('comment', ''))
            row_index += 1