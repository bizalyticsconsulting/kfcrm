# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import logging
import werkzeug.urls

_logger = logging.getLogger(__name__)


class ReceiveCommissionIncome(models.TransientModel):
    _inherit = "receive.commission.income"


    name = fields.Char("Name")
    batch_amount = fields.Float(string="Batch Amount", digits=(16, 3))
    batch_date = fields.Date("Batch Date", default=datetime.today().date())
    batch_remaining_amount = fields.Float(string="Batch Remaining Amount", digits=(16, 3), compute="_compute_batch_used_remaining_amount")
    used_batch_amount = fields.Float(string="Used Batch Amount", digits=(16, 3), compute="_compute_batch_used_remaining_amount")
    total_payment_amount = fields.Float("Total Payment Amount", digits=(16, 3), compute="_compute_batch_used_remaining_amount")
    process_overunder_payments = fields.Boolean("Process Over Payments?")
    refund_payment_check = fields.Boolean("Process Refund?")
    total_refund_amount = fields.Float("Total Refund Amount", digits=(16, 3), compute="_compute_batch_used_remaining_amount")
    comm_description = fields.Text("Note")

    commission_income_draft_id = fields.Many2one("receive.commission.income.draft", string="Commission Draft",
                                                 domain="[('partner_id', '=', partner_id), ('branch_id', '=', branch_id), ('state', '!=', 'posted')]")
    commission_draft_name = fields.Char('Comm. Draft Name')
    state = fields.Selection([('draft', 'Draft'), ('posted', 'Posted')], default="draft", string="State")

    draft_batch_amount = fields.Float(related="commission_income_draft_id.batch_amount", string="Draft Batch Amount", digits=(16, 3), store=True)
    draft_batch_remaining_amount = fields.Float(related="commission_income_draft_id.batch_remaining_amount", string="Draft Batch Remaining Amount", digits=(16, 3), store=True)
    draft_used_batch_amount = fields.Float(related="commission_income_draft_id.used_batch_amount", string="Draft Used Batch Amount", digits=(16, 3), store=True)
    payment_journal = fields.Many2one('account.journal', 'Payment Journal',
                                      domain="[('type', 'in', ['bank','cash'])]", required=False)


    @api.one
    @api.depends('batch_amount', 'receive_commission_line_ids.receive_check', 'receive_commission_line_ids.payment_amount', 'receive_commission_line_ids.refund_amount')
    def _compute_batch_used_remaining_amount(self):
        """
            This method will calculate,
            Case-1: If Batch Amount is more than 0--> The total amount used with the 'Receive Check' checked payment_amount.
            Case-2: If Batch Amount is 0--> Then revert the Commission Lines with 'Receive Check' Boolean False and used Payment Amount = 0.
        """
        self.total_refund_amount = 0.0
        self.total_payment_amount = 0.0
        self.used_batch_amount = 0.0
        self.batch_remaining_amount = 0.0
        # Batch Amount field has given (16, 3). So when user will pass 3 digit then its taking correct otherwise taking as '814.9200000000001'..So that round it.
        current_batch_amount = round(self.batch_amount, 3)
        if current_batch_amount > 0:
            if self.commission_income_draft_id and self.env.context.get('commission_draft_ctx'):
                self.total_payment_amount = self.commission_income_draft_id.total_payment_amount
                self.used_batch_amount = self.commission_income_draft_id.used_batch_amount
                self.batch_remaining_amount = self.commission_income_draft_id.batch_remaining_amount
            else:
                CommissionIncomeLine = self.env['receive.commission.income.line']
                commission_id_list = []
                batch_remaining_amount = current_batch_amount
                # Because of the onchanges automatically taking 2 lines. So that the calculations and alert is automatically raising.This will avoid that issue.
                for line in self.receive_commission_line_ids.filtered(lambda x: x.receive_check):
                    if line.og_acc_mov_line_id not in commission_id_list:
                        CommissionIncomeLine |= line
                        commission_id_list.append(line.og_acc_mov_line_id)
                if len(CommissionIncomeLine) > 0:
                    if self.refund_payment_check:
                        total_payment_amount_used = sum([i.refund_amount for i in CommissionIncomeLine.filtered(lambda x: x.refund_amount > 0)])
                        self.total_refund_amount = total_payment_amount_used
                    else:
                        total_payment_amount_used = sum([i.used_payment_amount for i in CommissionIncomeLine])

                    batch_remaining_amount = current_batch_amount - round(total_payment_amount_used, 3)  # self.batch_amount - total_payment_amount_used
                    self.total_payment_amount = sum([i.payment_amount for i in CommissionIncomeLine])
                    self.used_batch_amount = round(total_payment_amount_used, 3)
                self.batch_remaining_amount = round(batch_remaining_amount, 3)


    @api.onchange('branch_id', 'partner_id')
    def onchange_branch_id(self):
        # return_domain = {'sale_id': []}
        # sale_domain = [('state', 'in', ['sale', 'done'])]
        self.update({'sale_id': False, 'commission_income_draft_id': False, 'commission_draft_name': False})
        comm_name = ''
        if self.branch_id:
            # sale_domain.append(('branch_id', '=', self.branch_id.id))
            comm_name = self.branch_id.name
        if self.partner_id:
            # sale_domain.append(('x_supplier', '=', self.partner_id.id))
            if comm_name:
                comm_name = '[' + comm_name + ']' + self.partner_id.name
            else:
                comm_name = self.partner_id.name
        self.name = comm_name
        # return_domain['sale_id'] = sale_domain
        # return {'domain': return_domain}


    @api.onchange('batch_date')
    def onchange_batch_date(self):
        for comm_line in self.receive_commission_line_ids:
            comm_line.commission_receive_date = self.batch_date


    @api.onchange('branch_id', 'sale_id', 'partner_id', 'start_date', 'end_date') # 'payment_journal'
    def onchange_partner_id(self):
        self.receive_commission_line_ids = False

        # Comment out the code on 16.12.2021

#         # Already commented
#         # ctx = self.env.context.copy()
#         # if self.process_overunder_payments:
#         #     ctx.update({'checked_overunder': True})
#         # self = self.with_context(ctx)
#         #

        # receive_commission_list = []
        # search_domain = ['|',
        #                  ('account_id.user_type_id.name', '=', 'Receivable'),
        #                  ('account_id.internal_type', '=', 'receivable'),
        #                  ('journal_id.name', '=', 'Commission Income'),
        #                  ('branch_id', '=', self.branch_id.id),
        #                 ]
        #
        #                 # ['|', ('paid', '=', False)] # this domain condition is not working..so changed to com_pay_status
        #                 #       ('comm_balance_amount', '>', 0)
        #
        # if self.refund_payment_check:
        #   search_domain.append(('com_pay_status', 'in', ['pending', 'partial_refund']))
        #   search_domain.append(('credit', '>', 0))
        # else:
        #     search_domain.append(('com_pay_status', 'in', ['pending', 'partial_paid']))
        #     search_domain.append(('debit', '>', 0))
        #
        # #### we have to discuss for the Balance remaining Payments if any Partial Payments done.
        # ### In that case, 'paid' is True, 'com_pay_status' is 'Partial Paid'....27.03.2021
        #
        # if self.partner_id:
        #     search_domain.append(('partner_id', '=', self.partner_id.id))
        # if self.sale_id:
        #     search_domain.append(('sale_order_id', '=', self.sale_id.id))
        # if self.start_date:
        #     search_domain.append(('date', '>=', self.start_date))
        # if self.end_date:
        #     search_domain.append(('date', '<=', self.end_date))
        #
        # self.receive_commission_line_ids = False
        # if self.branch_id and self.partner_id: # and self.payment_journal:
        #     # 16.04.2021: Changed this for if any partial payment is done and still some payment is remaining then again able to pay
        #     # search_domain += [('paid', '=', False)]
        #     # ('comm_balance_amount', '>', 0)
        #
        #     acc_mov_line_ids = self.env['account.move.line'].search(search_domain)
        #     _logger.debug(f"Account Commission Lines Counts: {len(acc_mov_line_ids)}")
        #     # self.receive_commission_line_ids = False
        #
        #     for i in acc_mov_line_ids:
        #         if i.comm_balance_amount == 0:
        #             continue
        #
        #         ### Incase of negative commission...will take credit amount07.02.2021
        #         ### 17.07.2021: Incase 'Partial Payment' done for commission lines...so consider that and so only remaining amount
        #         if i.commissionable_amount < 0:
        #             comm_credit_amount = i.comm_balance_amount # i.credit  # 17.07.2021
        #             comm_debit_amount = 0.0
        #         else:
        #             comm_debit_amount = i.comm_balance_amount  # i.debit    # 16.04.2021
        #             comm_credit_amount = 0.0
        #
        #             ### Checking for the 'Refund'.if commissionable amount is not negative value..But commission record is 'refundable'..june2021..In case any issue in Flow..check this changes
        #             if i.debit > 0:
        #                 comm_debit_amount = i.comm_balance_amount # i.debit # 17.07.2021
        #                 comm_credit_amount = 0.0
        #
        #
        #
        #
        #             else:
        #                 comm_debit_amount = 0.0
        #                 comm_credit_amount = i.comm_balance_amount
        #             #------------------------------------------------
        #
        #         receive_check = False
        #         line_payment_amount = not self.process_overunder_payments and comm_debit_amount or 0.0
        #         line_used_payment_amount = 0.0
        #         if self.commission_income_draft_id:
        #             for draft_line in self.commission_income_draft_id.receive_commission_draft_line_ids:
        #                 if int(draft_line.og_acc_mov_line_id) == i.id:
        #                     receive_check = True
        #                     line_payment_amount = line_used_payment_amount = draft_line.payment_amount
        #                     # line_used_payment_amount = draft_line.payment_amount
        #
        #         record_sale_url = self._get_sale_url(i.sale_line_id.order_id)
        #         if record_sale_url and isinstance(record_sale_url, list):
        #             record_sale_url = record_sale_url[0]
        #
        #         receive_commission_list.append((0, 0, {'name': i.partner_id.name,
        #                                                'date': i.date,
        #                                                'journal_id': i.journal_id.id,
        #                                                'commissionable_amount': i.commissionable_amount,
        #                                                'commission_rate': i.commission_rate,
        #                                                'receive_comm_amt': (
        #                                                                            i.receive_comm_amt > 0) and i.receive_comm_amt or i.total_commission,
        #                                                'receive_comm_rate': (
        #                                                                             i.receive_comm_rate > 0) and i.receive_comm_rate or i.commission_rate,
        #
        #                                                'debit': comm_debit_amount, #i.debit,
        #                                                'credit': comm_credit_amount,
        #                                                'total_commission': i.total_commission,
        #                                                # 'wizard_id': wizard_id.id,
        #                                                'og_acc_mov_line_id': i.id,
        #                                                'sale_line_id': i.sale_line_id.id,
        #                                                'payment_amount': line_payment_amount, #not self.process_overunder_payments and comm_debit_amount or 0.0, #i.debit,
        #                                                'used_payment_amount': line_used_payment_amount,
        #                                                'refund_amount': comm_credit_amount,
        #                                                'receive_check': receive_check, #False,
        #                                                'sale_order_ref': i.sale_line_id and i.sale_line_id.order_id.id or False,
        #                                                'commission_receive_date': self.batch_date,
        #                                                'so_mfg_invoice': i.sale_line_id and i.sale_line_id.order_id.mfg_invoice or '',
        #                                                'so_mfg_order': i.sale_line_id and i.sale_line_id.order_id.x_mfg_order or '',
        #
        #                                                'sale_url': record_sale_url,
        #
        #                                                # 'over_payment': rec_over_payment,
        #                                                # 'balance_due': rec_balance_due,
        #
        #                                                }))
        #     self.update({'receive_commission_line_ids': receive_commission_list})


    @api.onchange('process_overunder_payments')
    def onchange_process_overunder_payments(self):
        for rec in self:
            rec.batch_amount = 0.0
            rec.receive_commission_line_ids = False
            rec.partner_id = False
            rec.refund_payment_check = False

            rec.commission_income_draft_id = False
            rec.commission_draft_name = False


    @api.onchange('refund_payment_check')
    def onchange_refund_payment_check(self):
        for rec in self:
            rec.batch_amount = 0.0
            rec.receive_commission_line_ids = False
            rec.partner_id = False
            rec.process_overunder_payments = False

            rec.commission_income_draft_id = False
            rec.commission_draft_name = False


    @api.multi
    def do_payment_receive_commission_income(self):
        """
            This method will do payment journal entries against the commission and maintain data on that linke, sda_id, layered_id,
            sale_line_id, parent_id etc.

            We can do payment Over/Under as we have given in the Payment Amount i.e. against the 'debit' original commission amount

            Also handled refund commission too with reverse entry. 09.02.202

            # 27.03.2021
            If Commission Lines field 'Receive Check' is checked  and 'Payment Amount > 0', then only process the same Commission Lines for Payments Entry.
        """
        if not self.payment_journal:
            raise UserError(_("Please provide 'Payment Journal' !"))
        if not self.pay_reference:
            raise UserError(_("Please provide 'Reference' !"))

        batch_amount_round = round(self.batch_amount, 3)
        income_commission_processed = False
        if self.refund_payment_check or self.process_overunder_payments:
            if self.refund_payment_check:
                receive_commission_lines_ids = self.receive_commission_line_ids.filtered(lambda x: x.receive_check and x.refund_amount > 0)
                total_move_amount = sum([i.refund_amount for i in receive_commission_lines_ids])
            else:
                receive_commission_lines_ids = self.receive_commission_line_ids.filtered(lambda x: x.receive_check)
                total_move_amount = sum([i.used_payment_amount for i in receive_commission_lines_ids])

            ### Validation for If Batch Amount Less Than the Move Lines Payment/Refund amount
            if round(total_move_amount, 3) > batch_amount_round:
                raise ValidationError(_("The Commission Lines 'Total Payment Amount' should be Less Than the 'Batch Amount' !\n"
                                        "Please check Commission Lines 'Payment Amount / Refund Amount' !"))

            _logger.info(f"Commission Lines Count: {len(receive_commission_lines_ids)}")
            if len(receive_commission_lines_ids) == 0:
                raise UserError(_("In commission lines, there is no lines is selected 'Receive' check box !\n"
                                  "Please select at least one commission lines to process !"))

        # If Normal Process then process the payment using the 'Draft' Commission lines.
        if (not self.refund_payment_check and not self.process_overunder_payments):
            # Call method to create the Commission Income Draft record
            self.save_commission_income_draft()
            _logger.info(_("The Commission Income 'Draft' is created: %s"), self.commission_income_draft_id)
            if self.commission_income_draft_id and len(self.commission_income_draft_id.receive_commission_draft_line_ids) > 0:
                # Validation for the Batch Amount and Commission Draft Lines Payment Amounts.
                receive_commission_draft_lines_ids = self.commission_income_draft_id.receive_commission_draft_line_ids.filtered(lambda x: x.receive_check)
                if len(receive_commission_draft_lines_ids) == 0:
                    raise UserError(_("In 'Draft' commission lines, there is no lines is selected as 'Receive' checked !\n"
                                      "Please select at least one 'Draft' commission lines to process !"))
                total_move_amount = sum([i.payment_amount for i in receive_commission_draft_lines_ids])
                income_batch_amount_round = round(self.commission_income_draft_id.batch_amount, 3)
                if round(total_move_amount, 3) > income_batch_amount_round:
                    raise ValidationError(
                        _("The 'Draft' Commission Lines 'Total Payment Amount' should be Less Than the 'Batch Amount' !\n"
                          "Please check 'Draft' Commissoin Lines 'Payment Amount' !"))
                if self.commission_income_draft_id.state == 'posted':
                    raise ValidationError(
                        _("The Commission 'Draft' is already 'Posted'.You can not process to post again !"))
                elif self.commission_income_draft_id.state == 'invalid':
                    raise ValidationError(
                        _("The Commission 'Draft' is in 'Invalid' state.You can not process to post Invalid Commission Draft !"))
                self.action_payment_receive_commission_draft(receive_commission_draft_lines_ids)
                self.commission_income_draft_id.state = 'posted'

                # If any 'draft' record is available for same vendor/showroom combination then check if any 'draft' commission lines is available which is posted
                # Then change state of those 'draft' as 'Invalid'..# Search for other same vendor / showroom combination
                draft_commission_line_ids = self.env['receive.commission.income.draft'].search(
                    [('partner_id', '=', self.partner_id.id), ('branch_id', '=', self.branch_id.id), ('state', '=', 'draft')])
                if len(draft_commission_line_ids) > 0:
                    processed_commission_draft_id = draft_commission_line_ids.mapped('receive_commission_draft_line_ids').filtered(
                        lambda x: x.og_acc_mov_line_id in receive_commission_draft_lines_ids.mapped('og_acc_mov_line_id')).mapped('income_draft_id')
                    if len(processed_commission_draft_id) > 0:
                        processed_commission_draft_id.write({'state': 'invalid'})
                income_commission_processed = True

        else:
            for comm_line in receive_commission_lines_ids:
                comm_line_id = self.env['account.move.line'].browse(int(comm_line.og_acc_mov_line_id))
                if not comm_line_id:
                    continue
                if comm_line_id.move_id.state == 'draft':
                    comm_line_id.move_id.action_post()

                now = datetime.now()

                ### Incase Over/under payment process, we dont have Payment Amount but we can have over/under payments for the adjustments.
                ### So we no need to creare Journal Entry in this case but create adjustments in the under/over payments models.

                ### 31.07.2021: In case of Over Payments we need to create 'Received Commissoin' i.e. Journal Entry..But from the Original Created Journal
                ### should get 'Reversal Entry for that adjusted amount' first..Then create Journal Entry for the same Amount..
                if not comm_line.wizard_id.process_overunder_payments:
                    if (comm_line_id.commissionable_amount >= 0) and (comm_line_id.commission_rate >= 0):
                        refund_check_case = False
                        if comm_line_id.debit > 0:
                            # total_amount = comm_line.receive_comm_amt
                            # total_amount = (comm_line_id.receive_comm_amt > 0) and comm_line_id.receive_comm_amt or comm_line_id.total_commission
                            total_amount = comm_line.payment_amount
                            if total_amount <= 0:
                                raise ValidationError(_("Please enter proper commission payment amount !"))
                            if total_amount <= 0:
                                continue
                            credit_payment_amount = total_amount
                            debit_payment_amount = 0.0
                            jr_name = comm_line_id.sale_order_id.name
                        else:
                            # If negative amount...payment journal should be in the reverse order
                            credit_payment_amount = 0.0
                            debit_payment_amount = comm_line.refund_amount
                            jr_name = 'Refund: ' + str(comm_line_id.sale_order_id.name)
                            refund_check_case = True

                    else:
                        # If negative amount...payment journal should be in the reverse order
                        credit_payment_amount = 0.0
                        debit_payment_amount = comm_line.refund_amount
                        jr_name = 'Refund: ' + str(comm_line_id.sale_order_id.name)
                        refund_check_case = True

                    comm_pay_reference = ''
                    if comm_line_id.ref and self.pay_reference:
                        comm_pay_reference = str(comm_line_id.ref) + ', ' + str(self.pay_reference)
                    elif self.pay_reference:
                        comm_pay_reference = self.pay_reference
                    elif comm_line_id.ref:
                        comm_pay_reference = comm_line_id.ref

                    acc_receivable_vals = {
                        'name': jr_name, #comm_line_id.sale_order_id.name,  # self.partner_id.name,
                        'credit': credit_payment_amount, #total_amount,
                        'debit': debit_payment_amount, #0.0,
                        'account_id': self.partner_id.property_account_receivable_id.id,
                        'partner_id': self.partner_id.id,
                        'pay_reference': self.pay_reference,

                        'sale_line_id': comm_line_id.sale_line_id.id,
                        'parent_commission_id': comm_line_id.id,
                        'sda_id': comm_line_id.sda_id and comm_line_id.sda_id.id or False,
                        'layered_sda_id': comm_line_id.layered_sda_id and comm_line_id.layered_sda_id.id or False,

                        'branch_id': comm_line_id.branch_id.id,
                        'ref': comm_pay_reference, # comm_line_id.ref,
                        'commission_partner': comm_line_id.commission_partner.id,
                        'product_id': comm_line_id.sale_line_id.product_id.id,
                        # 'sale_order_id': comm_line_id.sale_order_id.id,
                        # 'commission_user': commission_user,

                        'date': comm_line.commission_receive_date,
                        'comm_description': self.comm_description,
                    }
                    bank_cash_vals = {
                        'name': jr_name, #comm_line_id.sale_order_id.name,  # self.partner_id.name,
                        'credit': debit_payment_amount, #0.0,
                        'debit': credit_payment_amount, #total_amount,
                        'account_id': self.payment_journal.default_debit_account_id.id,
                        'partner_id': self.partner_id.id,
                        'pay_reference': self.pay_reference,

                        'sale_line_id': comm_line_id.sale_line_id.id,
                        'parent_commission_id': comm_line_id.id,
                        'sda_id': comm_line_id.sda_id and comm_line_id.sda_id.id or False,
                        'layered_sda_id': comm_line_id.layered_sda_id and comm_line_id.layered_sda_id.id or False,

                        'branch_id': comm_line_id.branch_id.id,
                        'ref': comm_pay_reference,
                        'commission_partner': comm_line_id.commission_partner.id,
                        'product_id': comm_line_id.sale_line_id.product_id.id,
                        # 'sale_order_id': comm_line_id.sale_order_id.id,
                        # 'commission_user': commission_user,
                        'date': comm_line.commission_receive_date,
                        'comm_description': self.comm_description,
                    }
                    vals = {
                        'journal_id': self.payment_journal.id,
                        'date': self.batch_date, #now.strftime('%Y-%m-%d'),
                        'state': 'draft',
                        'line_ids': [(0, 0, acc_receivable_vals), (0, 0, bank_cash_vals)],
                        'ref': self.pay_reference,
                        'commission_partner': comm_line_id.commission_partner.id,
                        'partner_id': comm_line_id.commission_partner.id,
                        # 'commission_user': commission_user,
                    }

                    move = self.env['account.move'].create(vals)

                    if (comm_line_id.commissionable_amount >= 0) and (comm_line_id.commission_rate >= 0):
                        _logger.info(
                            f"Commission payment is created i.e. : {move} for the commission line i.e. {comm_line_id}.")
                    else:
                        _logger.info(
                            f"Commission refund payment is created i.e. : {move} for the commission line i.e. {comm_line_id}.")

                    move.action_post()

                    # This payment status is only for the current Payment(Batch Payment) but not for the associated Commission Lines...Just for record
                    if self.refund_payment_check:
                        move.line_ids.write({'paid': True, 'com_pay_status': 'refund'})
                    else:
                        move.line_ids.write({'paid': True, 'com_pay_status': 'fully_paid'})

                    if (comm_line_id.commissionable_amount >= 0) and (comm_line_id.commission_rate >= 0):
                        comm_line_id.sale_order_id.message_post(body=_(
                            f"Income Commission payment({move.name}) is created for the Product commission: {comm_line_id.sale_line_id.product_id.display_name}."))

                        if comm_line_id.sda_id:
                            comm_line_id.sda_id.write({'commission_status': 'confirmed', 'commission_confirmed_date': now})
                        if comm_line_id.layered_sda_id:
                            comm_line_id.layered_sda_id.write(
                                {'commission_status': 'confirmed', 'commission_confirmed_date': now})

                        # Checking for the 'Refund'.if commissionable amount is not negatuve value..But commission record is 'refundable'..
                        # june2021..In case any issue in Flow..check this changes
                        if refund_check_case:
                            # 16.07.2021: We have to write code for Check Partial Refund and update the refunded amount in the Commission lines associated with the Sales..
                            # So user can understand how much is refunded and how much is remaining has to be refund (same like the normal one)-->refer: sale_commission_status_update
                            comm_line_id.move_id.line_ids.sudo().write({'paid': True})
                            comm_line_id.sale_order_id.message_post(body=_(
                                f"Income Commission Refund payment({move.name}) is created for the Product commission: {comm_line_id.sale_line_id.product_id.display_name}."))
                        # ------------------------------------------------

                        ### Code For Do entry in commission.under.over.payments Model..To Manage Commission Payment History and adjustment
                        comm_line.action_commission_payment_manage_entries(comm_line_id, move)

                        # This code will update the same commission line Total Paid amount...which is associated with the Sale Order
                        # Create scheduler To udpate the Commission Line Paid amount..Form Under/Over Payment Model(Stored all the payment details with commisison lines & Sale Order)
                        self.sale_commission_status_update(comm_line_id, comm_line.payment_amount, refund_check_case, comm_line.refund_amount)  # UPDATE COMMISSION LINES TOTAL PAID AND STATUS
                    else:
                        # 16.07.2021: We have to write code for Check Partial Refund and update the refunded amount in the Commission lines associated with the Sales..
                        # So user can understand how much is refunded and how much is remaining has to be refund (same like the normal one)-->refer: sale_commission_status_update
                        comm_line_id.move_id.line_ids.sudo().write({'paid': True})
                        comm_line_id.sale_order_id.message_post(body=_(
                            f"Income Commission Refund payment({move.name}) is created for the Product commission: {comm_line_id.sale_line_id.product_id.display_name}."))
                        self.sale_commission_status_update(comm_line_id, comm_line.payment_amount, refund_check_case, comm_line.refund_amount)
                    income_commission_processed = True
                else:
                    if comm_line.payment_amount > 0:
                        continue    # If payment amount is given then no need to process
                    if len(comm_line.commission_payment_ids) == 0 or not comm_line.commission_payment_ids:
                        continue    # If no under/over payments is selected then no need to process

                    ### Code For Do entry in commission.under.over.payments Model..To Manage Commission Payment History and adjustment
                    comm_line.action_commission_payment_manage_entries(comm_line_id)
                    income_commission_processed = True


        # Display Message with popup screen
        pop_context = dict(self._context or {})
        pop_view = self.env.ref('sh_message.sh_message_wizard')
        pop_view_id = pop_view and pop_view.id or False
        pop_context['message'] = "The Payment for Commission Income Lines is not 'Posted'.\nPlease check commission lines !"
        if income_commission_processed:
            self.state = 'posted'
            # Delete the Current Commission Post Wizard record and return action
            self.unlink()
            wizard_action = self.env.ref('gt_commission_income.receive_commission_income_action').read()[0]
            return wizard_action

            pop_context['message'] = "The Payment for Commission Income Lines is successfully 'Posted'."


        return {
            'name': 'Draft Commission Lines',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(pop_view.id, 'form')],
            'view_id': pop_view_id,
            'target': 'new',
            'context': pop_context,
        }


    @api.multi
    def sale_commission_status_update(self, comm_line_id, payment_amount, refund_check_case=False, refund_pay_amount=False):
        """
            This code will update the same commission line Total Paid amount...which is associated with the Sale Order
            Create scheduler To udpate the Commission Line Paid amount..Form Under/Over Payment Model(Stored all the payment details with commisison lines & Sale Order)

            IF commission paid is more than the balance amount...then also only show commission amount in paid.
            Other amount we can see/manage in under/over payment model.30.04.2021
            Commission 'Paid' only when full payment done
        """
        for mv_com_line in comm_line_id.move_id.line_ids:
            commission_paid = False
            total_com_paid = 0.0
            comm_balance_amount = abs(mv_com_line.comm_balance_amount)

            if self.refund_payment_check:
                if refund_pay_amount >= comm_balance_amount:
                    total_com_paid = comm_balance_amount + mv_com_line.total_comm_paid
                    commission_paid = True
                elif refund_pay_amount < comm_balance_amount:
                    if mv_com_line.total_comm_paid > 0:
                        total_com_paid = mv_com_line.total_comm_paid + refund_pay_amount
                    else:
                        total_com_paid = refund_pay_amount
            else:
                if payment_amount >= comm_balance_amount:
                    total_com_paid = comm_balance_amount + mv_com_line.total_comm_paid
                    commission_paid = True
                elif payment_amount < comm_balance_amount:
                    if mv_com_line.total_comm_paid > 0:
                        total_com_paid = mv_com_line.total_comm_paid + payment_amount
                    else:
                        total_com_paid = payment_amount

            mv_com_line.write({'paid': commission_paid,  # True,
                               'received_income': True,
                               'total_comm_paid': total_com_paid})  # mv_com_line.total_comm_paid + comm_line.payment_amount
            # Call commission method to compute for balance amount

            mv_com_line._compute_commission_balance_amount()
            comm_balance_amount = abs(mv_com_line.comm_balance_amount)

            if mv_com_line.com_pay_status not in ['fully_paid', 'refund']:
                com_pay_status = 'pending'
                if self.refund_payment_check:
                    if mv_com_line.total_comm_paid == 0:
                        com_pay_status = 'pending'
                    elif mv_com_line.total_comm_paid > 0 and comm_balance_amount > 0:
                        com_pay_status = 'partial_refund'
                    elif mv_com_line.total_comm_paid > 0 and comm_balance_amount == 0:
                        com_pay_status = 'refund'
                else:
                    if mv_com_line.total_comm_paid == 0:
                        com_pay_status = 'pending'
                    elif mv_com_line.total_comm_paid > 0 and comm_balance_amount > 0:
                        com_pay_status = 'partial_paid'
                    elif mv_com_line.total_comm_paid > 0 and comm_balance_amount == 0:
                        com_pay_status = 'fully_paid'
                mv_com_line.write({'com_pay_status': com_pay_status})
        return True


    @api.multi
    def over_reverse_create_journal_entry(self, comm_pay_id, comm_payment_id, comm_line_id, comm_grid_line_id, adjusted_payment):
        """ 31.07.2021
        In case of Over Payments, we need to create 'Received Commission' i.e. Journal Entry..
        But before that from the Original Created Journal(For OverPayments) we should get 'Reversal Entry for that adjusted amounts first
        Then we have to create 'Received Journal Entry' for the same Amount. So that we can keep track of the 'Received Commission Amount' against the 'Over Payment Adjustment'.

        Params:
            comm_pay_id         : Drop-down Commission Over Payments (using for Adjustments)
            comm_payment_id     : For current Commission line adjustment entry in --> commission over/under Table
            comm_line_id        : For current Commission Line ID
            adjusted_payment    : Adjustment amount(Current Commission Payment Amount) by the over payment.
        """
        AccountMove = self.env['account.move']
        comm_payment_move_id = AccountMove
        if comm_pay_id.commission_payment_move_id:
            comm_payment_move_id = comm_pay_id.commission_payment_move_id
        elif comm_pay_id.origin:
            comm_payment_move_id = AccountMove.search([('name', '=', comm_pay_id.origin)], limit=1)
        if len(comm_payment_move_id) > 0:
            _logger.info(_(f"Process for Reverse entry of {adjusted_payment} amount."))
            # Create Over-Reversal Move Entry First
            move_line_list = []
            for mv_line in comm_payment_move_id.line_ids:
                if mv_line.debit > 0:
                    comm_debit_amount = adjusted_payment # mv_line.debit
                    comm_credit_amount = 0.0
                else:
                    comm_debit_amount = 0.0
                    comm_credit_amount = adjusted_payment # mv_line.credit

                comm_pay_reference = ''
                if comm_payment_move_id.name and self.pay_reference:
                    comm_pay_reference = 'Over-Reversal of: ' + str(comm_payment_move_id.name) + ', ' + str(self.pay_reference)
                elif self.pay_reference:
                    comm_pay_reference = 'Over-Reversal of: ' + str(self.pay_reference)
                elif comm_payment_move_id.name:
                    comm_pay_reference = 'Over-Reversal of: ' + str(comm_payment_move_id.name)

                acc_vals = {
                    'name': mv_line.sale_order_id.name,
                    'credit': comm_debit_amount,
                    'debit': comm_credit_amount,
                    'account_id': mv_line.account_id.id,
                    'partner_id': mv_line.partner_id.id,
                    'pay_reference': mv_line.pay_reference,

                    'sale_line_id': mv_line.sale_line_id.id,
                    'parent_commission_id': mv_line.parent_commission_id.id or False,
                    'sda_id': mv_line.sda_id.id or False,
                    'layered_sda_id': mv_line.layered_sda_id.id or False,

                    'branch_id': mv_line.branch_id.id or False,
                    'ref': comm_pay_reference, #mv_line.ref,
                    'commission_partner': mv_line.commission_partner.id or False,
                    'product_id': mv_line.product_id.id or False,

                    'date': comm_grid_line_id.commission_receive_date or self.batch_date,
                }
                move_line_list.append((0, 0, acc_vals))

            move_vals = {
                'journal_id': self.payment_journal.id,
                'date': comm_grid_line_id.commission_receive_date or self.batch_date,
                'state': 'draft',
                'line_ids': move_line_list,
                'ref': comm_pay_reference, #'Over-Reversal of: ' + str(comm_payment_move_id.name), #self.pay_reference,
                'commission_partner': comm_payment_move_id.commission_partner.id or False,
                'partner_id': comm_payment_move_id.partner_id.id or False,
                # 'commission_user': commission_user,
                'adjusted_reverse_move_id': comm_payment_move_id.id,
            }
            reverse_move = self.env['account.move'].create(move_vals)
            _logger.info(f"Reverse Move {reverse_move} is created against the 'Over Commission Payment' Move Entry {comm_payment_move_id}.")
            reverse_move.action_post()

            # Create Over-Adjustment Received Commission Payment Move
            if comm_line_id.commissionable_amount >= 0 and comm_line_id.debit > 0:
                comm_pay_reference = ''
                if comm_line_id.ref and self.pay_reference:
                    comm_pay_reference = 'Over-Adjustment: ' + str(comm_line_id.ref) + ', ' + str(self.pay_reference)
                elif self.pay_reference:
                    comm_pay_reference = 'Over-Adjustment: ' + str(self.pay_reference)
                elif comm_line_id.ref:
                    comm_pay_reference = 'Over-Adjustment: ' + str(comm_line_id.ref)

                acc_receivable_vals = {
                    'name': comm_line_id.sale_order_id.name,
                    'credit': adjusted_payment, # comm_line.debit,
                    'debit': 0.0,
                    'account_id': self.partner_id.property_account_receivable_id.id,
                    'partner_id': self.partner_id.id,
                    'pay_reference': self.pay_reference,

                    'sale_line_id': comm_line_id.sale_line_id.id,
                    'parent_commission_id': comm_line_id.id,
                    'sda_id': comm_line_id.sda_id and comm_line_id.sda_id.id or False,
                    'layered_sda_id': comm_line_id.layered_sda_id and comm_line_id.layered_sda_id.id or False,

                    'branch_id': comm_line_id.branch_id.id,
                    'ref': comm_pay_reference, #'Over-Adjustment: ' + str(comm_line_id.ref),
                    'commission_partner': comm_line_id.commission_partner.id,
                    'product_id': comm_line_id.sale_line_id.product_id.id,
                    # 'sale_order_id': comm_line_id.sale_order_id.id,
                    # 'commission_user': commission_user,

                    'date': comm_grid_line_id.commission_receive_date or self.batch_date,
                    'comm_description': self.comm_description,

                    'paid': True,
                    'com_pay_status': 'fully_paid',
                }
                bank_cash_vals = {
                    'name': comm_line_id.sale_order_id.name,
                    'credit': 0.0,
                    'debit': adjusted_payment,   # comm_line.debit,
                    'account_id': self.payment_journal.default_debit_account_id.id,
                    'partner_id': self.partner_id.id,
                    'pay_reference': self.pay_reference,

                    'sale_line_id': comm_line_id.sale_line_id.id,
                    'parent_commission_id': comm_line_id.id,
                    'sda_id': comm_line_id.sda_id and comm_line_id.sda_id.id or False,
                    'layered_sda_id': comm_line_id.layered_sda_id and comm_line_id.layered_sda_id.id or False,

                    'branch_id': comm_line_id.branch_id.id,
                    'ref': comm_pay_reference, # 'Over-Adjustment: ' + str(comm_line_id.ref),
                    'commission_partner': comm_line_id.commission_partner.id,
                    'product_id': comm_line_id.sale_line_id.product_id.id,
                    # 'sale_order_id': comm_line_id.sale_order_id.id,
                    # 'commission_user': commission_user,
                    'date': comm_grid_line_id.commission_receive_date or self.batch_date,
                    'comm_description': self.comm_description,

                    'paid': True,
                    'com_pay_status': 'fully_paid',
                }

                vals = {
                    'journal_id': self.payment_journal.id,
                    'date': comm_grid_line_id.commission_receive_date or self.batch_date,
                    'state': 'draft',
                    'line_ids': [(0, 0, acc_receivable_vals), (0, 0, bank_cash_vals)],
                    'ref': comm_pay_reference, #self.pay_reference,
                    'commission_partner': comm_line_id.commission_partner.id,
                    'partner_id': comm_line_id.commission_partner.id,
                    # 'commission_user': commission_user,
                    'adjusted_parent_move_id': comm_payment_move_id.id,
                }
                move = self.env['account.move'].create(vals)
                if comm_line_id.commissionable_amount >= 0:
                    _logger.info(
                        f"Commission payment is created i.e. : {move} for the commission line i.e. {comm_line_id}.")
                move.action_post()
                if comm_payment_id:
                    comm_payment_id.actual_payment_amount = adjusted_payment
                    comm_payment_id.origin = move.name
                    comm_payment_id.commission_payment_move_id = move.id

        return True


    @api.multi
    def _prepare_commission_income_draft_values(self):
        """
            This method will prepare the commission lines for 'Draft' to add multiple lines together.
            If any commission lines is already added then just update the data in the existing 'draft' lines.
        """
        commission_draft_lines = []
        # Get all the commission Ids and if that is already added then do not add again for the same
        # commission_line_id_lists = self.commission_income_draft_id.receive_commission_draft_line_ids.mapped('og_acc_mov_line_id')
        for comm_line in self.receive_commission_line_ids.filtered(lambda x: x.receive_check):
            # if comm_line.og_acc_mov_line_id in commission_line_id_lists:
            #     raise ValidationError(_("The Commission Line for Sale Order '%s' with Payment Amount '%s' is already added in 'Draft' Commission !" %(comm_line.sale_order_ref.name,comm_line.payment_amount)))

            draft_line_vals = {
                    'name': comm_line.name,
                    'date': comm_line.date,
                    'journal_id': comm_line.journal_id.id,
                    'commissionable_amount': comm_line.commissionable_amount,
                    'commission_rate': comm_line.commission_rate,
                    'receive_comm_amt': comm_line.receive_comm_amt,
                    'receive_comm_rate': comm_line.receive_comm_rate,
                    'debit': comm_line.debit,
                    'og_acc_mov_line_id': comm_line.og_acc_mov_line_id,
                    'sale_line_id': comm_line.sale_line_id.id,
                    'payment_amount': comm_line.payment_amount,
                    'balance_due': comm_line.balance_due,
                    'over_payment': comm_line.over_payment,
                    'commission_receive_date': comm_line.commission_receive_date,
                    'note_description': comm_line.note_description,
                    'sale_order_ref': comm_line.sale_order_ref.id,
                    'receive_check': comm_line.receive_check,
                    'so_mfg_invoice': comm_line.so_mfg_invoice,
                    'so_mfg_order': comm_line.so_mfg_order,
                    'sale_url': comm_line.sale_url,
            }

            comm_draft_line_id = self.commission_income_draft_id.receive_commission_draft_line_ids.filtered(
                                            lambda x: x.receive_check and x.og_acc_mov_line_id == comm_line.og_acc_mov_line_id)
            if len(comm_draft_line_id) > 0:
                commission_draft_lines.append((1, comm_draft_line_id.id, draft_line_vals))
            else:
                commission_draft_lines.append((0, 0, draft_line_vals))

        if not commission_draft_lines:
            raise ValidationError(_("The Commission lines are already added in the 'Draft' Commission !"))
        if len(commission_draft_lines) > 0:
            values = {'name': self.commission_draft_name,
                      'partner_id': self.partner_id.id,
                      'branch_id': self.branch_id.id,
                      'sale_id': self.sale_id.id,
                      'start_date': self.start_date,
                      'end_date': self.end_date,
                      'commission_journal': self.commission_journal.id,
                      'payment_journal': self.payment_journal.id,
                      'pay_reference': self.pay_reference,
                      'batch_amount': self.batch_amount,
                      'batch_date': self.batch_date,
                      'comm_description': self.comm_description,
                      'receive_commission_draft_line_ids': commission_draft_lines,
                      'commission_wizard_id': self.id,  # Only for reference and next time add into the same lines.
            }
        return values


    @api.multi
    def save_commission_income_draft(self):
        """
            This method will add new commission lines in the 'Draft' Commission Lines/
            1. First Time this will create and add.
            2. Next time add and update.

            Do Code for the Receive Commission Lines for Update data.
        """
        # ctx = self.env.context.copy()
        # If any commission lines is already added to the draft by using 'Receive check' checked..But if it unchecked then remove from the draft.
        unchecked_commission_ids = self.receive_commission_line_ids.filtered(lambda x: not x.receive_check).mapped('og_acc_mov_line_id')
        if len(unchecked_commission_ids) > 0:
            draft_line_unchecked_ids = self.commission_income_draft_id.receive_commission_draft_line_ids.filtered(
                lambda x: x.og_acc_mov_line_id in unchecked_commission_ids)
            if len(draft_line_unchecked_ids) > 0:
                draft_line_unchecked_ids.unlink()

        # If checked then add or update the records indraft lines.
        if len(self.receive_commission_line_ids.filtered(lambda x: x.receive_check)) > 0:
            CommissionIncomeDraft = self.env['receive.commission.income.draft']
            if not self.commission_draft_name:
                draft_name = ''
                if self.pay_reference:
                    draft_name = '[' + self.pay_reference + ']'
                if self.partner_id:
                    if draft_name:
                        draft_name = draft_name + ' ' + self.partner_id.name
                    else:
                        draft_name = self.partner_id.name
                if self.branch_id:
                    if draft_name:
                        draft_name = draft_name + ' (' + self.branch_id.name + ')'
                    else:
                        draft_name = self.branch_id.name
                if draft_name:
                    self.commission_draft_name = draft_name

            if len(self.commission_income_draft_id) > 0:
                draft_update_values = self._prepare_commission_income_draft_values()
                if len(draft_update_values) > 0:
                    self.commission_income_draft_id.write(draft_update_values)
                    _logger.info(_("Commission Income draft added and updated: %s"), self.commission_income_draft_id)
            else:
                draft_create_values = self._prepare_commission_income_draft_values()
                commission_income_draft_id = CommissionIncomeDraft.create(draft_create_values)
                _logger.info(_("Commission Income draft added and Created: %s"), commission_income_draft_id)
                self.commission_income_draft_id = commission_income_draft_id.id

            # Validate the Total Batch Amount with the used amount...Raise Alert Message more than the Batch Given Batch Amount
            used_batch_amount_round = round(self.commission_income_draft_id.used_batch_amount, 3)
            batch_amount_round = round(self.commission_income_draft_id.batch_amount, 3)
            batch_remaining_amount_round = round(self.commission_income_draft_id.batch_remaining_amount, 3)
            if (used_batch_amount_round > batch_amount_round) or batch_remaining_amount_round < 0:
                raise ValidationError(_("The 'Draft' Commission 'Used Payment Amount [%s]' should not be greater than the 'Batch Amount [%s]' !\n"
                                        "Please check the commission lines payment amount."
                                        %(used_batch_amount_round, batch_amount_round)))

            # Update for receive_check: False and return the commission Lines with the original amounts
            # ctx.update({'receive_check_ctx': True})
            # self.receive_commission_line_ids.filtered(lambda x: x.receive_check).write({'receive_check': False})
            # self.receive_commission_line_ids.with_context(ctx).onchange_commission_payment_amount()

            # Display Message with popup screen
            pop_context = dict(self._context or {})
            pop_view = self.env.ref('sh_message.sh_message_wizard')
            pop_view_id = pop_view and pop_view.id or False
            pop_context['message'] = "Commission Income Lines details added/updated successfully to the 'Draft' Lines."
            return {
                'name': 'Draft Commission Lines',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'sh.message.wizard',
                'views': [(pop_view.id, 'form')],
                'view_id': pop_view_id,
                'target': 'new',
                'context': pop_context,
            }
        return True


    @api.multi
    def action_open_commission_draft(self):
        """
            This method will show the Commission Income Drafts
        """
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            draft_form_id = ir_model_data.get_object_reference('gt_commission_under_over_payment', 'income_commission_draft_form_view')[1]
        except ValueError:
            draft_form_id = False
        ctx = {
            'create': 0,
            'edit': 0,
            # 'delete': 0,
            'target': 'new',
        }
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'receive.commission.income.draft',
            'res_id': self.commission_income_draft_id.id,
            'views': [(draft_form_id, 'form')],
            'view_id': draft_form_id,
            'target': 'new',
            'context': ctx,
        }


    @api.onchange('commission_income_draft_id')
    def onchange_commission_draft(self):
        if self.partner_id and self.branch_id and self.commission_income_draft_id:
            if self.commission_income_draft_id.state == 'invalid':
                self.update({'commission_income_draft_id': False})
                warning_message = {
                    'title': _('Invalid Commission Draft '),
                    'message': "The selected Commission 'Draft' is 'Invalid'.\n Please select valid Draft Commission."
                }
                return {'warning': warning_message}

            update_values = {
                'commission_draft_name': self.commission_income_draft_id.name,
                'sale_id': self.commission_income_draft_id.sale_id.id,
                'start_date': self.commission_income_draft_id.start_date,
                'end_date': self.commission_income_draft_id.end_date,
                'commission_journal': self.commission_income_draft_id.commission_journal.id,
                'payment_journal': self.commission_income_draft_id.payment_journal.id,
                'pay_reference': self.commission_income_draft_id.pay_reference,
                'batch_amount': self.commission_income_draft_id.batch_amount,
                'batch_date': self.commission_income_draft_id.batch_date,
                'comm_description': self.commission_income_draft_id.comm_description,
            }
            if self.env.context.get('commission_draft_ctx'):
                update_values.update({
                    'total_payment_amount': self.commission_income_draft_id.total_payment_amount,
                    'used_batch_amount': self.commission_income_draft_id.used_batch_amount,
                    'batch_remaining_amount': self.commission_income_draft_id.batch_remaining_amount,
                })

                # Update the commission lines 'Receive Check' and 'Payment Amount'
                for comm_line in self.receive_commission_line_ids:
                    for draft_line in self.commission_income_draft_id.receive_commission_draft_line_ids:
                        if draft_line.og_acc_mov_line_id == comm_line.og_acc_mov_line_id:
                            comm_line.receive_check = draft_line.receive_check
                            comm_line.payment_amount = draft_line.payment_amount
                            comm_line.used_payment_amount = draft_line.payment_amount

                            # Call commission line onchange and compute method
                            # comm_line.onchange_commission_payment_amount()
                            # comm_line._compute_commission_over_under_payment()
                # Compute method call to update the batch related field data
                self._compute_batch_used_remaining_amount()

            self.update(update_values)


    @api.multi
    def action_payment_receive_commission_draft(self, receive_commission_draft_lines_ids):
        """
            This method will process the 'Draft Commission Lines' for Commission Payments.
        """
        for comm_draft_line in receive_commission_draft_lines_ids:
            commission_draft_id = comm_draft_line.income_draft_id
            # comm_wizard_id = self.env['receive.commission.income'].browse(comm_draft_line.income_draft_id.commission_wizard_id)
            comm_line_id = self.env['account.move.line'].browse(int(comm_draft_line.og_acc_mov_line_id))
            if not comm_line_id:
                continue
            if comm_line_id.move_id.state == 'draft':
                comm_line_id.move_id.action_post()

            now = datetime.now()
            jr_name = comm_line_id.sale_order_id.name
            # Do not process the 'Negative Values i.e. Refunds'
            if comm_line_id.commissionable_amount < 0:
                continue
            if comm_line_id.credit > 0:
                continue

            total_amount = comm_draft_line.payment_amount
            # if total_amount <= 0:
            #     continue
            if total_amount <= 0:
                raise ValidationError(_("Please enter proper commission 'Draft' Payment amount !"))

            credit_payment_amount = total_amount
            debit_payment_amount = 0.0

            comm_pay_reference = ''
            if comm_line_id.ref and commission_draft_id.pay_reference:
                comm_pay_reference = str(comm_line_id.ref) + ', ' + str(commission_draft_id.pay_reference)
            elif commission_draft_id.pay_reference:
                comm_pay_reference = commission_draft_id.pay_reference
            elif comm_line_id.ref:
                comm_pay_reference = comm_line_id.ref

            acc_receivable_vals = {
                'name': jr_name,  # comm_line_id.sale_order_id.name,  # self.partner_id.name,
                'credit': credit_payment_amount,  # total_amount,
                'debit': debit_payment_amount,  # 0.0,
                'account_id': commission_draft_id.partner_id.property_account_receivable_id.id,
                'partner_id': commission_draft_id.partner_id.id,
                'pay_reference': commission_draft_id.pay_reference,

                'sale_line_id': comm_line_id.sale_line_id.id,
                'parent_commission_id': comm_line_id.id,
                'sda_id': comm_line_id.sda_id and comm_line_id.sda_id.id or False,
                'layered_sda_id': comm_line_id.layered_sda_id and comm_line_id.layered_sda_id.id or False,

                'branch_id': comm_line_id.branch_id.id,
                'ref': comm_pay_reference,
                'commission_partner': comm_line_id.commission_partner.id,
                'product_id': comm_line_id.sale_line_id.product_id.id,
                'date': comm_draft_line.commission_receive_date,
                'comm_description': commission_draft_id.comm_description,
            }
            bank_cash_vals = {
                'name': jr_name,  # comm_line_id.sale_order_id.name,  # self.partner_id.name,
                'credit': debit_payment_amount,  # 0.0,
                'debit': credit_payment_amount,  # total_amount,
                'account_id': commission_draft_id.payment_journal.default_debit_account_id.id,
                'partner_id': commission_draft_id.partner_id.id,
                'pay_reference': commission_draft_id.pay_reference,

                'sale_line_id': comm_line_id.sale_line_id.id,
                'parent_commission_id': comm_line_id.id,
                'sda_id': comm_line_id.sda_id and comm_line_id.sda_id.id or False,
                'layered_sda_id': comm_line_id.layered_sda_id and comm_line_id.layered_sda_id.id or False,

                'branch_id': comm_line_id.branch_id.id,
                'ref': comm_pay_reference,
                'commission_partner': comm_line_id.commission_partner.id,
                'product_id': comm_line_id.sale_line_id.product_id.id,
                'date': comm_draft_line.commission_receive_date,
                'comm_description': commission_draft_id.comm_description,
            }
            vals = {
                'journal_id': commission_draft_id.payment_journal.id,
                'date': commission_draft_id.batch_date,
                'state': 'draft',
                'line_ids': [(0, 0, acc_receivable_vals), (0, 0, bank_cash_vals)],
                'ref': commission_draft_id.pay_reference,
                'commission_partner': comm_line_id.commission_partner.id,
                'partner_id': comm_line_id.commission_partner.id,
            }

            move = self.env['account.move'].create(vals)
            _logger.info(f"Commission payment is created i.e. : {move} for the commission line i.e. {comm_line_id}.")
            move.action_post()
            move.line_ids.write({'paid': True, 'com_pay_status': 'fully_paid'})

            comm_line_id.sale_order_id.message_post(body=_(
                f"Income Commission payment({move.name}) is created for the Product commission: {comm_line_id.sale_line_id.product_id.display_name}."))

            if comm_line_id.sda_id:
                comm_line_id.sda_id.write({'commission_status': 'confirmed', 'commission_confirmed_date': now})
            if comm_line_id.layered_sda_id:
                comm_line_id.layered_sda_id.write({'commission_status': 'confirmed', 'commission_confirmed_date': now})

            # Code For Do entry in commission.under.over.payments Model..To Manage Commission Payment History and adjustment
            comm_draft_line.action_commission_payment_manage_entries(comm_line_id, move)
            # This code will update the same commission line Total Paid amount and status...which is associated with the Sale Order
            self.sale_commission_status_update(comm_line_id, comm_draft_line.payment_amount)

        return True


    @api.multi
    def action_clear_draft_field(self):
        if self.commission_income_draft_id:
            self.commission_income_draft_id = False
        if self.commission_draft_name:
            self.commission_draft_name = False

        return True


    @api.multi
    def _get_sale_url(self, sale_order_ref):
        """
        This method will open a Many2one field i.e. sale order in new tab
        """
        # ctx = self.env.context.copy()
        record_sale_url = ''
        if sale_order_ref:
            # ctx.update({'create': 0, 'edit': 0, 'delete': 0})
            view_type = 'form'
            model = 'sale.order'
            menu_id = self.env.ref("sale.menu_sale_order")
            action_id = self.env.ref("sale.action_orders")
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            fragment = dict()
            fragment['id'] = sale_order_ref.id
            if action_id:
                fragment['action'] = action_id.id
            if model:
                fragment['model'] = model
            if view_type:
                fragment['view_type'] = view_type
            if menu_id:
                fragment['menu_id'] = menu_id.id

            record_sale_url = "%s/web#%s" % (base_url, werkzeug.urls.url_encode(fragment))
        return record_sale_url


    def action_load_commission_lines(self):
        """
        This method will load the commission lines data using given form data filters.
        """
        ctx = self.env.context.copy()
        AccountMoveLine = self.env['account.move.line']
        CommissionIncomeLine = self.env["receive.commission.income.line"]
        CommissionIncomeLines = self.env["receive.commission.income.line"]
        receive_commission_list = []
        search_domain = [
            '|',
                ('account_id.user_type_id.name', '=', 'Receivable'),
                ('account_id.internal_type', '=', 'receivable'),
            ('journal_id.name', '=', 'Commission Income'),
            ('branch_id', '=', self.branch_id.id),
        ]

        if self.refund_payment_check:
          search_domain.append(('com_pay_status', 'in', ['pending', 'partial_refund']))
          search_domain.append(('credit', '>', 0))
        else:
            search_domain.append(('com_pay_status', 'in', ['pending', 'partial_paid']))
            search_domain.append(('debit', '>', 0))

        #### we have to discuss for the Balance remaining Payments if any Partial Payments done.
        ### In that case, 'paid' is True, 'com_pay_status' is 'Partial Paid'....27.03.2021

        if self.partner_id:
            search_domain.append(('partner_id', '=', self.partner_id.id))
        if self.sale_id:
            search_domain.append(('sale_order_id', '=', self.sale_id.id))
        if self.start_date:
            search_domain.append(('date', '>=', self.start_date))
        if self.end_date:
            search_domain.append(('date', '<=', self.end_date))

        if not self.branch_id:
            raise UserError(_("Please select Showroom !"))
        if not self.partner_id:
            raise UserError(_("Please select Vendor !"))
        if self.state == 'posted':
            raise UserError(_("The Current Commission Income process is 'Posted' !"))
        # Delete Existing Commission Lines First
        if len(self.receive_commission_line_ids) > 0:
            self.receive_commission_line_ids.unlink()
            _logger.info("All the commission lines is deleted.")

        if self.branch_id and self.partner_id:
            acc_mov_line_ids = AccountMoveLine.search(search_domain)
            _logger.debug(f"Account Commission Lines Counts: {len(acc_mov_line_ids)}")
            for mv_rec in acc_mov_line_ids:

                # Check for the Single Commission Line(Faulty) and skip this...18.02.2024
                if len(mv_rec.move_id.line_ids) == 1:
                    continue

                if mv_rec.comm_balance_amount == 0:
                    continue
                if (mv_rec.commissionable_amount < 0) or (mv_rec.commission_rate < 0):
                    comm_credit_amount = abs(mv_rec.comm_balance_amount) # i.credit  # 17.07.2021
                    comm_debit_amount = 0.0
                else:
                    comm_debit_amount = abs(mv_rec.comm_balance_amount)  # i.debit    # 16.04.2021
                    comm_credit_amount = 0.0

                    ### Checking for the 'Refund'.if commissionable amount is not negative value..But commission record is 'refundable'..june2021..In case any issue in Flow..check this changes
                    if mv_rec.debit > 0:
                        comm_debit_amount = abs(mv_rec.comm_balance_amount) # i.debit # 17.07.2021
                        comm_credit_amount = 0.0
                    else:
                        comm_debit_amount = 0.0
                        comm_credit_amount = abs(mv_rec.comm_balance_amount)
                    #------------------------------------------------

                receive_check = False
                line_payment_amount = not self.process_overunder_payments and comm_debit_amount or 0.0
                line_used_payment_amount = 0.0
                if self.commission_income_draft_id:
                    for draft_line in self.commission_income_draft_id.receive_commission_draft_line_ids:
                        if int(draft_line.og_acc_mov_line_id) == mv_rec.id:
                            receive_check = True
                            line_payment_amount = line_used_payment_amount = draft_line.payment_amount
                            # line_used_payment_amount = draft_line.payment_amount

                record_sale_url = self._get_sale_url(mv_rec.sale_line_id.order_id)
                if record_sale_url and isinstance(record_sale_url, list):
                    record_sale_url = record_sale_url[0]

                commission_values = {
                    'name': mv_rec.partner_id.name,
                    'date': mv_rec.date,
                    'journal_id': mv_rec.journal_id.id,
                    'commissionable_amount': mv_rec.commissionable_amount,
                    'commission_rate': mv_rec.commission_rate,
                    'receive_comm_amt': (mv_rec.receive_comm_amt > 0) and mv_rec.receive_comm_amt or mv_rec.total_commission,
                    'receive_comm_rate': (mv_rec.receive_comm_rate > 0) and mv_rec.receive_comm_rate or mv_rec.commission_rate,
                    'debit': comm_debit_amount, #i.debit,
                    'credit': comm_credit_amount,
                    'total_commission': mv_rec.total_commission,
                    'og_acc_mov_line_id': mv_rec.id,
                    'sale_line_id': mv_rec.sale_line_id.id,
                    'payment_amount': line_payment_amount, #not self.process_overunder_payments and comm_debit_amount or 0.0, #i.debit,
                    'used_payment_amount': line_used_payment_amount,
                    'refund_amount': comm_credit_amount,
                    'receive_check': receive_check, #False,
                    'sale_order_ref': mv_rec.sale_line_id and mv_rec.sale_line_id.order_id.id or False,
                    'commission_receive_date': self.batch_date,
                    'so_mfg_invoice': mv_rec.sale_line_id and mv_rec.sale_line_id.order_id.mfg_invoice or '',
                    'so_mfg_order': mv_rec.sale_line_id and mv_rec.sale_line_id.order_id.x_mfg_order or '',
                    'sale_url': record_sale_url,
                    'wizard_id': self.id,
                }
                CommissionIncomeLines |= CommissionIncomeLine.create(commission_values)
        _logger.info("Create Commission Lines: %s" %(CommissionIncomeLines))


class SaleCommissionIncomeLine(models.TransientModel):
    _inherit = "receive.commission.income.line"


    # @api.one
    # def _compute_sale_url(self):
    #     """
    #     This method will open a Many2one field i.e. sale order in new tab
    #     """
    #     ctx = self.env.context.copy()
    #     if self.sale_order_ref:
    #         ctx.update({'create': 0, 'edit': 0, 'delete': 0})
    #         view_type = 'form'
    #         model = 'sale.order'
    #         menu_id = self.env.ref("sale.menu_sale_order")
    #         action_id = self.env.ref("sale.action_orders")
    #         base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
    #         record_url = base_url
    #
    #         for rec in self:
    #             fragment = dict()
    #             if rec.sale_order_ref:
    #                 fragment['id'] = rec.sale_order_ref.id
    #             if action_id:
    #                 fragment['action'] = action_id.id
    #             if model:
    #                 fragment['model'] = model
    #             if view_type:
    #                 fragment['view_type'] = view_type
    #             if menu_id:
    #                 fragment['menu_id'] = menu_id.id
    #
    #             record_url = "%s/web#%s" % (base_url, werkzeug.urls.url_encode(fragment))
    #             rec.sale_url = record_url
    #

    payment_amount = fields.Float(string="Payment Amount")
    balance_due = fields.Float(string="Balance Due Amount", compute="_compute_commission_over_under_payment", store=True, help="Balance due when under payment done.", )
    over_payment = fields.Float(string="Over Payment Amount", compute="_compute_commission_over_under_payment", store=True)
    commission_payment_ids = fields.Many2many("commission.under.over.payments",
                                              'commission_line_under_payment_rel',
                                              'receive_comm_line_id',
                                              'comm_payment_id',
                                              domain="[('payment_type', '=', 'over_payment'), '|', ('over_payment_remaining', '!=', 0), ('under_payment_remaining', '!=', 0)]",
                                              string="Commission Over/Under Payments Ref",
                                              help="This is Commission Payments Reference i.e. Over/Under Payment. You can select for against which you want to process the current commission payment.")
    commission_receive_date = fields.Date("Commission Received", default=datetime.today().date(), copy=False)
    note_description = fields.Text("Notes")
    refund_amount = fields.Float(string="Refund Amount")
    sale_order_ref = fields.Many2one("sale.order", string="Sale Order")
    receive_check = fields.Boolean("Receive")
    so_mfg_invoice = fields.Char("Supplier Invoice")
    so_mfg_order = fields.Char("Supplier Order")
    # sale_url = fields.Char(string="Sale Url", compute="_compute_sale_url")
    sale_url = fields.Char(string="Sale URL")
    process_overunder_payments = fields.Boolean(related="wizard_id.process_overunder_payments", string="Process Over Payments?")
    refund_payment_check = fields.Boolean(related="wizard_id.refund_payment_check", string="Process Refund?")
    used_payment_amount = fields.Float(string="Used Amount")
    credit = fields.Float(string='Refund Commission Amount')


    @api.one
    @api.depends('debit', 'payment_amount', 'refund_amount')
    def _compute_commission_over_under_payment(self):
        for rec in self:
            comm_line_id = self.env['account.move.line'].browse(int(rec.og_acc_mov_line_id))
            if comm_line_id.commissionable_amount < 0 or rec.refund_amount > 0:
                if rec.refund_amount <= 0:
                    raise ValidationError(_("Please enter proper commission refund amount !"))
            else:
                if rec.payment_amount < 0:
                    raise ValidationError(_("Please enter proper commission payment amount !"))
                if rec.payment_amount > 0:
                    if rec.payment_amount > rec.debit:
                        rec.over_payment = rec.payment_amount - rec.debit
                    elif rec.payment_amount < rec.debit:
                        rec.balance_due = rec.debit - rec.payment_amount
                else:
                    rec.payment_amount = 0.0


    @api.onchange('debit', 'payment_amount', 'refund_amount')
    def _onchange_commission_over_under_payment(self):
        for rec in self:
            comm_line_id = self.env['account.move.line'].browse(int(rec.og_acc_mov_line_id))
            if comm_line_id.commissionable_amount < 0 or rec.refund_amount > 0:
                if rec.refund_amount <= 0:
                    raise ValidationError(_("Please enter proper commission refund amount !"))
            else:
                if rec.payment_amount < 0:
                    raise ValidationError(_("Please enter proper commission payment amount !"))
                if rec.payment_amount > 0:
                    if rec.payment_amount > rec.debit:
                        rec.over_payment = rec.payment_amount - rec.debit
                    elif rec.payment_amount < rec.debit:
                        rec.balance_due = rec.debit - rec.payment_amount
                else:
                    rec.payment_amount = 0.0


    @api.onchange('receive_check', 'debit', 'payment_amount', 'refund_amount')
    def onchange_commission_payment_amount(self):
        """
        Case-1: If Commission payment amount is Greater Than commission original amount i.e. 'Over Payment'.
                In this case, Show all the 'commission.under.over.payments' records for the same Vendor and Who's 'Balance due' is remaining.
                So that, we can adjust the amount in any of that commission payments to 'Fully Paid' status.

        Case-2: If commission payment amount is Less Than commission original amount i.e. 'Under Payment(Balance Due)'.
                In this case, Show all the 'commission.under.over.payments' records for the same Vendor and Who's 'Over Payment' is done.
                So that, we can adjust the amount in any of that commission payments to 'Fully Paid' status.

        case-3: If commission payment amount is Equal To commission original amount i.e. 'Fully Paid'.
                In this case, No need to Handle the commission-payment drop-down(Empty Filter)

        So that, we can manage vice-versa for the Commission Payments to 'Fully Paid' state.

        Added compute and onchange to this method.
        """
        return_domain = {'commission_payment_ids': [('id', 'in', [])]}
        for rec in self:
            account_move_line_id = self.env['account.move.line'].browse(int(rec.og_acc_mov_line_id))
            partner_vendor_id = rec.wizard_id.partner_id and rec.wizard_id.partner_id.id or account_move_line_id.partner_id.id

            # If uncheck the 'Receive Check' field, then original Payment Amount will store the Account Move Line i.e. Commission Amount again.
            if not rec.wizard_id.process_overunder_payments:
                if rec.receive_check:
                    pass
                else:
                    if len(account_move_line_id) and self.env.context.get('receive_check_ctx'):
                        rec.payment_amount = account_move_line_id.debit

            # This is useful in the case of update the 'used_payment_amount' and their fields once 'receive field checked'
            rec.used_payment_amount = 0
            batch_amount_round = round(rec.wizard_id.batch_amount, 3)
            batch_remaining_amount_round = round(rec.wizard_id.batch_remaining_amount, 3)
            payment_amount_round = round(rec.payment_amount, 3)
            if (rec.wizard_id and not rec.wizard_id.process_overunder_payments and batch_amount_round > 0 and rec.receive_check):
                # self.wizard_id._compute_batch_used_remaining_amount()
                if batch_remaining_amount_round <= batch_amount_round:
                    if batch_remaining_amount_round > 0:
                        if batch_remaining_amount_round >= payment_amount_round:
                            rec.used_payment_amount = payment_amount_round
                        else:
                            rec.used_payment_amount = batch_remaining_amount_round
                            rec.payment_amount = rec.used_payment_amount
                    elif batch_remaining_amount_round < 0:
                        if not rec.wizard_id.refund_payment_check:
                            raise UserError(
                                _("The selected Commission Lines 'Total Payment Amount' should be Less Than 'Batch Amount' !\n"
                                  "Please select proper Commission Lines."))
                    else:
                        if not rec.wizard_id.refund_payment_check:
                            raise UserError(
                                _("Batch Amount is already satisfied with the select assigned commission lines ! \n"
                                  "You can not select further 'Receive Check' field to be checked."))

            # This is useful in the case of 'Over/under payment' flow.
            if payment_amount_round < 0:
                raise ValidationError(_("Please enter proper commission amount."))
            elif payment_amount_round >= 0:
                if rec.wizard_id.process_overunder_payments:
                    if payment_amount_round > rec.debit:
                        # In case Over Payment--->Display Under payment(Balance Due) Drop-down records
                        return_domain['commission_payment_ids'] = [('branch_id', '=', rec.wizard_id.branch_id.id),
                                                                   ('partner_vendor_id', '=', partner_vendor_id),
                                                                   ('state', 'in', ['under_payment', 'partial_payment']),
                                                                   ('under_payment_remaining', '>', 0.0),
                                                                   ('actual_payment_amount', '>', 0.0)
                                                                  ]
                    elif payment_amount_round < rec.debit:
                        # In case Under Payment--->Display Over payment(Over Remaining Amount) Drop-down records
                        return_domain['commission_payment_ids'] = [('branch_id', '=', rec.wizard_id.branch_id.id),
                                                                   ('partner_vendor_id', '=', partner_vendor_id),
                                                                   ('state', 'in', ['over_payment']),
                                                                   ('over_payment_remaining', '>', 0.0),
                                                                   ('actual_payment_amount', '>', 0.0)
                                                                  ]

        return {'domain': return_domain}


    def _prepare_commission_payment_history(self, comm_payment_id, comm_line_id, move_id):
        """
        This method will prepare the lines for make payment entry of all the adjustment.
        When do Over-payment and record selected in M2m field-->adjust the under-payment(balance due) records
        When do Under-payment and record selected in M2m field-->adjust the current payment using Over-payment(over remaining) records
        ### use any under-payment and Adjust Under payment i.e. selected commission payment..against the over-payment(current)...
        ### use any over-payment commission payment and Adjust Under payment(Current)..Create Commission Payment History with Amount (adjusted amount)
        """
        commission_debit_amount = abs(comm_line_id.comm_balance_amount) # comm_line_id.comm_balance_amount # comm_line_id.debit # 31.07.2021 Take only Balance Amount instead of Whole Commission Amount..May be some amount is already paid.
        current_remaining_payment = 0.0
        if self.payment_amount > commission_debit_amount:
            # Over Payment Adjustment case
            over_payment_amount = self.payment_amount - commission_debit_amount
            current_remaining_payment = over_payment_amount - current_remaining_payment
            for comm_pay_id in self.commission_payment_ids:
                raise UserError(
                    _(f"The current selected 'Showroom' i.e. {comm_pay_id.branch_id.name} and selected 'Commission Over/Under' Showroom i.e. {self.wizard_id.branch_id.name} is not matched !"))

                if current_remaining_payment == 0.0:
                    break   ## Once Current remaining Payment is become 0.0 Than break the loop and whatever adjustment process is done...after that stop.

                adjusted_payment = current_remaining_payment
                # Now check payment status with the actual_payment and adjusted payment....Whatever payment is remaining take only that much...Other revert to the Current payment as over_remaining_payment
                prev_original_amount = comm_pay_id.original_com_amount
                prev_actual_payment = comm_pay_id.actual_payment_amount
                # get all the adjusted amount
                prev_adjusted_payment = sum([i.payment_amount for i in comm_pay_id.commission_payment_history_ids.filtered(lambda x: x.payment_type == 'over_payment')])
                prev_till_now_payment = prev_actual_payment + prev_adjusted_payment
                prev_remaining_payment = prev_original_amount - prev_till_now_payment
                # Take only prev_remaining_payment only from current over-payment
                if current_remaining_payment > prev_remaining_payment:
                    current_remaining_payment = current_remaining_payment - prev_remaining_payment
                    adjusted_payment = prev_remaining_payment
                    prev_remaining_payment = 0.0
                elif current_remaining_payment < prev_remaining_payment:
                    prev_remaining_payment = prev_remaining_payment - current_remaining_payment
                    adjusted_payment = current_remaining_payment
                    current_remaining_payment = 0.0
                else:
                    current_remaining_payment = 0.0
                    prev_remaining_payment = 0.0

                hist_values = {
                    "commission_payment_id": comm_pay_id.id,
                    "commission_journal_id": comm_line_id.id,
                    "payment_amount": adjusted_payment,
                    "payment_type": 'over_payment',
                    "origin": comm_payment_id.name, # move_id.name,
                }

                ''' Update the Current and Previous selected commission payments entry with above status '''
                state = prev_remaining_payment == 0.0 and 'full_payment' or 'partial_payment'
                comm_pay_id.write({'state': state,
                                   'under_payment_remaining': prev_remaining_payment,
                                   'adjusted_commission_payment_ids': [(4, comm_payment_id.id)],
                                   'commission_payment_history_ids': [(0, 0, hist_values)],
                                 })

                # Update Commission Line Pay status---Which was Under-payment/partial-payment
                if state == 'full_payment':
                    comm_pay_id.commission_journal_id.move_id.line_ids.sudo().write({'com_pay_status': 'fully_paid'})
                else:
                    comm_pay_id.commission_journal_id.move_id.line_ids.sudo().write({'com_pay_status': 'partial_paid'})

                # UPDATE COMMISSION LINES TOTAL PAID AND STATUS
                self.wizard_id.sale_commission_status_update(comm_pay_id.commission_journal_id.move_id.line_ids[0], adjusted_payment)
                #------------------------------------

                comm_update_vals = {'adjusted_commission_payment_ids': [(4, comm_pay_id.id)],
                                    'over_payment_remaining': current_remaining_payment}
                comm_payment_id.write(comm_update_vals)

                comm_pay_id.message_post(body=_(
                    f"{adjusted_payment} amount is adjusted from {comm_payment_id.name} to make balance."))
                comm_payment_id.message_post(body=_(
                    f"This Commission received payment i.e. 'over payment' is adjusted {adjusted_payment} amount in {comm_pay_id.name} ."))

        elif self.payment_amount < commission_debit_amount:
            # Under Payment Adjustment case
            current_remaining_payment = commission_debit_amount - self.payment_amount
            for comm_pay_id in self.commission_payment_ids:
                if comm_pay_id.branch_id.id != self.wizard_id.branch_id.id:
                    raise UserError(_(f"The current selected 'Showroom' i.e. {comm_pay_id.branch_id.name} and selected 'Commission Over/Under' Showroom i.e. {self.wizard_id.branch_id.name} is not matched !"))

                if comm_pay_id.over_payment_remaining <= 0:     # If any selected over-payment does not have over-remaining amount..not use
                    continue
                if current_remaining_payment == 0.0:            # If Current amount fullfilled and not have any under payment than not break process
                    break
                adjusted_payment = current_remaining_payment
                if current_remaining_payment > comm_pay_id.over_payment_remaining:
                    current_remaining_payment = current_remaining_payment - comm_pay_id.over_payment_remaining
                    adjusted_payment = comm_pay_id.over_payment_remaining
                    prev_remaining_payment = 0.0
                elif current_remaining_payment < comm_pay_id.over_payment_remaining:
                    prev_remaining_payment = comm_pay_id.over_payment_remaining - current_remaining_payment
                    adjusted_payment = current_remaining_payment
                    current_remaining_payment = 0.0
                else:
                    current_remaining_payment = 0.0             # Current payment remaining amount
                    prev_remaining_payment = 0.0                # previous adjusting remaining amount

                hist_values = {
                    "commission_payment_id": comm_payment_id.id,
                    "commission_journal_id": comm_pay_id.commission_journal_id.id, # comm_line_id.id,
                    "payment_amount": adjusted_payment,
                    "payment_type": 'under_payment',
                    "origin": comm_pay_id.name, #move_id.name,
                }
                ''' Update the Current and Previous selected commission payments entry with above status '''
                state = current_remaining_payment == 0.0 and 'full_payment' or 'partial_payment'
                comm_payment_id.write({
                        'state': state,
                        'under_payment_remaining': current_remaining_payment,
                        'adjusted_commission_payment_ids': [(4, comm_pay_id.id)],
                        'commission_payment_history_ids': [(0, 0, hist_values)],
                })
                # Update Commission Line Pay status---Which was Under-payment/partial-payment
                if state == 'full_payment':
                    comm_payment_id.commission_journal_id.move_id.line_ids.sudo().write({'com_pay_status': 'fully_paid'})
                else:
                    comm_payment_id.commission_journal_id.move_id.line_ids.sudo().write({'com_pay_status': 'partial_paid'})

                # UPDATE COMMISSION LINES TOTAL PAID AND STATUS
                self.wizard_id.sale_commission_status_update(comm_line_id, adjusted_payment)
                # ------------------------------------

                comm_update_vals = {'adjusted_commission_payment_ids': [(4, comm_payment_id.id)],
                                    'over_payment_remaining': prev_remaining_payment}
                comm_pay_id.write(comm_update_vals)
                comm_pay_id.message_post(body=_(
                    f"This Commission over payment is adjusted {adjusted_payment} amount in {comm_payment_id.name} while doing 'Under Payment' for the same."))

                comm_payment_id.message_post(body=_(
                    f"{adjusted_payment} amount is adjusted from {comm_pay_id.name} to make balance."))

                #### Code for create Over-Reverse Entry and Over-Adjustment Received Commission Payment Journal Entry for same...31.07.2021
                if adjusted_payment > 0:
                    self.wizard_id.over_reverse_create_journal_entry(comm_pay_id, comm_payment_id, comm_line_id, self, adjusted_payment)

            # 04.08.2021: In-case, on the Post Commission wizard, user has selected same 'Over Commission' twice in the multiple Lines..Then we are creating
            # Over-under record for every lines..But in code already checking/updating for the "Over's remaining amounts"..So that if that become 0
            # In this case, system is not creating any Adjustment/Reversal Entry..So that no use of prior created Over/Under record for Lines
            # We are deleting those such over/under records and also based on the Current payment status of the associated commission lines..update status too.
            if not comm_payment_id.commission_payment_move_id:
                self.wizard_id.sale_commission_status_update(comm_line_id, self.payment_amount)
                comm_payment_id.unlink()    # Delete  over/under unnecessary created record..Because No Payment has been done.

        return True


    def _prepare_commission_payments_values(self, commission_line_id, move_id):
        """
        This method will prepare the values for the commission payments
        :param move_id: add for ref in origin, currently created commission payments account.move
        """
        # AccountMoveLine = self.env['account.move.line']
        # commission_line_id = AccountMoveLine.browse(self.og_acc_mov_line_id)

        # Changed 12.05.2021 because , we need to process the remaining balance amount of commission not full amount every time
        # (May be partial payments done already. So that next time take only commission balance amount instead of full amount)
        commission_debit_amount = commission_line_id.comm_balance_amount # commission_line_id.debit
        over_payment_remaining = 0.0
        under_payment_remaining = 0.0
        sale_id = False
        over_payment = 0.0
        balance_due = 0.0

        if commission_line_id.sale_order_id:
            sale_id = commission_line_id.sale_order_id.id
        elif commission_line_id.sale_line_id:
            sale_id = commission_line_id.sale_line_id.order_id.id

        if self.payment_amount > commission_debit_amount:
            payment_type ='over_payment'
            payment_state = 'over_payment'
            over_payment_remaining = self.payment_amount - commission_debit_amount
            over_payment = self.payment_amount - commission_debit_amount
            commission_line_id.move_id.line_ids.sudo().write({'com_pay_status': 'fully_paid'})
        elif self.payment_amount < commission_debit_amount:
            payment_type = 'under_payment'
            payment_state = 'under_payment'
            balance_due = commission_debit_amount - self.payment_amount
            under_payment_remaining = balance_due
            commission_line_id.move_id.line_ids.sudo().write({'com_pay_status': 'partial_paid'})
        else:
            payment_type = 'normal_payment'
            payment_state = 'full_payment'
            commission_line_id.move_id.line_ids.sudo().write({'com_pay_status': 'fully_paid'})

        values = {
            'sale_order_id': sale_id,
            'partner_vendor_id': commission_line_id.partner_id.id,
            'branch_id': commission_line_id.branch_id.id,
            'company_id': commission_line_id.company_id.id,
            'commission_journal_id': commission_line_id.id or False,
            'original_com_amount': commission_debit_amount,
            'actual_payment_amount': self.payment_amount,
            'over_payment_amount': over_payment,
            'under_payment_amount': balance_due,
            'balance_due_amount': balance_due,
            'over_payment_remaining': over_payment_remaining,
            'under_payment_remaining': under_payment_remaining,
            'origin': move_id and move_id.name or '',
            'payment_type': payment_type,
            'state': payment_state,
            # 'commission_payment_history_ids': ''

            'commission_receive_date': self.commission_receive_date,
            'note_description': self.note_description,

            'commission_payment_move_id': move_id and move_id.id or '',
        }

        return values


    def action_commission_payment_manage_entries(self, comm_line_id, move_id=False):
        """
        This method will create entry in commission.under.over.payments Model to manage commission payments.
        :param: move_id-->current commission payment account.move entry ref

        If self.commission_payment_ids if given,
            That means,
            In case of under-payment: we can use and adjust the payment from here(self.commission_payment_id) to 'Fully Paid' under-payment(current) commission record.
            In case of over-payment: we can use and and adjust the payment to here(self.commission_payment_id) to 'Fully Paid' under-payment(selected) commission record.
                                     with respect to the Current Payment.
        """
        CommissionPayments = self.env['commission.under.over.payments']
        payment_values = self._prepare_commission_payments_values(comm_line_id, move_id)
        comm_payment_id = CommissionPayments.create(payment_values)
        _logger.info(f"Commission Payment under/over payments Id: {comm_payment_id}")
        if len(self.commission_payment_ids) > 0:
            update_process = self._prepare_commission_payment_history(comm_payment_id, comm_line_id, move_id)
        return True
