# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import logging
import werkzeug.urls

_logger = logging.getLogger(__name__)


class ReceiveCommissionIncomeDraft(models.Model):
    _name = "receive.commission.income.draft"
    _description = "Receive Commission Income Draft"
    _order = "id desc"


    name = fields.Char("Name")
    partner_id = fields.Many2one('res.partner', string='Vendor', required=1)
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    commission_journal = fields.Many2one('account.journal', string='Commission Journal')
    payment_journal = fields.Many2one('account.journal', string='Payment Journal')
    pay_reference = fields.Char(string='Reference')
    receive_commission_draft_line_ids = fields.One2many("receive.commission.income.draft.line", 'income_draft_id', 'Commission Lines')
    branch_id = fields.Many2one("res.branch", string="Showroom", required=1)
    sale_id = fields.Many2one("sale.order", string="Sale Order")

    batch_amount = fields.Float(string="Batch Amount", digits=(16, 3))
    batch_date = fields.Date("Batch Date", default=datetime.today().date())
    batch_remaining_amount = fields.Float(string="Batch Remaining Amount", digits=(16, 3), compute="_compute_batch_used_remaining_amount")
    used_batch_amount = fields.Float(string="Used Batch Amount", digits=(16, 3), compute="_compute_batch_used_remaining_amount")
    total_payment_amount = fields.Float("Total Payment Amount", digits=(16, 3), compute="_compute_batch_used_remaining_amount")
    comm_description = fields.Text(string="Note")
    active = fields.Boolean(default=True)
    commission_wizard_id = fields.Integer("Commission Wizard")
    state = fields.Selection([('draft', 'Draft'), ('posted', 'Posted'), ('invalid', 'Invalid')], default="draft", string="State")


    @api.one
    @api.depends('receive_commission_draft_line_ids.receive_check', 'receive_commission_draft_line_ids.payment_amount')
    def _compute_batch_used_remaining_amount(self):
        """
            This method will calculate,
            Case-1: If Batch Amount is more than 0--> The total amount used with the 'Receive Check' checked payment_amount.
            Case-2: If Batch Amount is 0--> Then revert the Commission Lines with 'Receive Check' Boolean False and used Payment Amount = 0.
        """
        batch_amount_round = round(self.batch_amount, 3)
        if batch_amount_round > 0:
            used_payment_amount = sum([i.payment_amount for i in self.receive_commission_draft_line_ids.filtered(lambda x: x.receive_check)])
            self.used_batch_amount = used_payment_amount
            self.total_payment_amount = used_payment_amount
            remaining_batch_amount = batch_amount_round - used_payment_amount
            self.batch_remaining_amount = remaining_batch_amount


    @api.multi
    def action_invalid(self):
        for rec in self.filtered(lambda x: x.state == 'draft'):
            rec.state = 'invalid'
        return True


    @api.multi
    def action_draft(self):
        for rec in self.filtered(lambda x: x.state == 'invalid'):
            rec.state = 'draft'
        return True


class ReceiveCommissionIncomeDraftLine(models.Model):
    _name = "receive.commission.income.draft.line"
    _description = "Receive Commission Income Draft Line"


    name = fields.Char(string='Vendor')
    income_draft_id = fields.Many2one("receive.commission.income.draft", 'Income Commission Draft')
    date = fields.Date(string="Date")
    journal_id = fields.Many2one('account.journal', 'Journal')
    commissionable_amount = fields.Float(string="Commissionable Amount")
    commission_rate = fields.Float(string="Commission Rate")
    receive_comm_amt = fields.Float(string='Receivable Commission Amount')
    receive_comm_rate = fields.Float(string='Receive Commission Rate')
    debit = fields.Float(string='Receive Commission Amount')
    og_acc_mov_line_id = fields.Char("Original Account Move line ID")
    sale_line_id = fields.Many2one('sale.order.line', "Sale Line")
    payment_amount = fields.Float(string="Payment Amount")
    balance_due = fields.Float(string="Balance Due Amount)")
    over_payment = fields.Float(string="Over Payment Amount")
    commission_receive_date = fields.Date("Commission Received", default=datetime.today().date())
    note_description = fields.Text(string="Notes")
    sale_order_ref = fields.Many2one("sale.order", string="Sale Order")
    receive_check = fields.Boolean(string="Receive")
    so_mfg_invoice = fields.Char(string="Supplier Invoice")
    so_mfg_order = fields.Char(string="Supplier Order")
    sale_url = fields.Char(string="Sale Url")


    @api.multi
    def _prepare_commission_payments_values(self, commission_line_id, move_id):
        """
        This method will prepare the values for the commission payments
        :param move_id: add for ref in origin, currently created commission payments account.move
        """
        commission_debit_amount = commission_line_id.comm_balance_amount
        over_payment_remaining = 0.0
        under_payment_remaining = 0.0
        sale_id = False
        over_payment = 0.0
        balance_due = 0.0

        if commission_line_id.sale_order_id:
            sale_id = commission_line_id.sale_order_id.id
        elif commission_line_id.sale_line_id:
            sale_id = commission_line_id.sale_line_id.order_id.id
        if self.payment_amount > commission_debit_amount:
            payment_type ='over_payment'
            payment_state = 'over_payment'
            over_payment_remaining = self.payment_amount - commission_debit_amount
            over_payment = self.payment_amount - commission_debit_amount
            commission_line_id.move_id.line_ids.sudo().write({'com_pay_status': 'fully_paid'})
        elif self.payment_amount < commission_debit_amount:
            payment_type = 'under_payment'
            payment_state = 'under_payment'
            balance_due = commission_debit_amount - self.payment_amount
            under_payment_remaining = balance_due
            commission_line_id.move_id.line_ids.sudo().write({'com_pay_status': 'partial_paid'})
        else:
            payment_type = 'normal_payment'
            payment_state = 'full_payment'
            commission_line_id.move_id.line_ids.sudo().write({'com_pay_status': 'fully_paid'})

        values = {
            'sale_order_id': sale_id,
            'partner_vendor_id': commission_line_id.partner_id.id,
            'branch_id': commission_line_id.branch_id.id,
            'company_id': commission_line_id.company_id.id,
            'commission_journal_id': commission_line_id.id,
            'original_com_amount': commission_debit_amount,
            'actual_payment_amount': self.payment_amount,
            'over_payment_amount': over_payment,
            'under_payment_amount': balance_due,
            'balance_due_amount': balance_due,
            'over_payment_remaining': over_payment_remaining,
            'under_payment_remaining': under_payment_remaining,
            'origin': move_id and move_id.name or '',
            'payment_type': payment_type,
            'state': payment_state,
            'commission_receive_date': self.commission_receive_date,
            'note_description': self.note_description,
            'commission_payment_move_id': move_id and move_id.id or '',
        }

        return values


    @api.multi
    def action_commission_payment_manage_entries(self, comm_line_id, move_id=False):
        """
        This method will create entry in commission.under.over.payments Model to manage commission payments.
        :param: move_id-->current commission payment account.move entry ref

        If self.commission_payment_ids if given,
            That means,
            In case of under-payment: we can use and adjust the payment from here(self.commission_payment_id) to 'Fully Paid' under-payment(current) commission record.
            In case of over-payment: we can use and adjust the payment to here(self.commission_payment_id) to 'Fully Paid' under-payment(selected) commission record.
                                     with respect to the Current Payment.
        """
        payment_values = self._prepare_commission_payments_values(comm_line_id, move_id)
        comm_payment_id = self.env['commission.under.over.payments'].create(payment_values)
        _logger.info(f"Commission Draft Process: Commission Payment under/over : {comm_payment_id}")

        self.mark_under_payment_fully_paid(comm_payment_id, comm_line_id)
        return True


    @api.multi
    def unlink(self):
        for rec in self.filtered(lambda x: x.income_draft_id and x.income_draft_id.state not in ['invalid']):
            raise ValidationError(_("You can only delete 'Invalid' state 'Draft Commission Line' !"))
        income_draft_ids = self.mapped('income_draft_id')
        res = super(ReceiveCommissionIncomeDraftLine, self).unlink()
        for draft_rec in income_draft_ids.filtered(lambda x: x.state == 'invalid'):
            draft_rec.state = 'draft'
        return res


    def mark_under_payment_fully_paid(self, comm_payment_id, comm_line_id):
        """
        This method do the following,
            - In case, user is doing Full Payment to the Sale Order Commission Lines,
                - Then if any 'Under Payment' records are available in the system, Find out and Mark then 'Full Paid'
        """
        if comm_payment_id and comm_payment_id.state == 'full_payment' and comm_payment_id.commission_journal_id.com_pay_status == 'fully_paid':
            under_payments_ids = self.env['commission.under.over.payments'].search([
                ('state', '=', 'under_payment'),
                ('commission_journal_id', '=', comm_payment_id.commission_journal_id.id),
                ('sale_order_id', '=', comm_payment_id.sale_order_id.id),
            ])
            for cline in under_payments_ids:
                cline.state = 'full_payment'
                cline.message_post(body=_("This 'under payment' mark 'fully paid' by Post Commission process and Its associated Sale Order Commission is 'Fully Paid'."))
