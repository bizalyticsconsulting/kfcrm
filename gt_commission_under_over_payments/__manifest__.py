 # -*- coding: utf-8 -*-

{
    'name': 'Commission Under and Over Payments',
    'version': '1.0',
    'category': 'Sale',
    'sequence': 2,
    'summary': 'Sales Commission Payments',
    'description': """ Commission under and over payments management """,
    'author': '',
    'website': '',
    'depends': ['gt_sale_commission_extension', 'gt_commission_income', 'gt_supplier_disc_aggrement', 'branch'],
    'data': [
              'security/ir.model.access.csv',
              'views/commission_payments_view.xml',
              'views/account_view.xml',
              'wizard/receive_commission_view.xml',
              'data/ir_sequences.xml',
              'data/scheduler.xml',

              'wizard/receive_commission_draft_view.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
