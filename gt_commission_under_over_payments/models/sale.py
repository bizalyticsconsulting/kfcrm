# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = "sale.order"


    @api.one
    def _compute_user_edit_access(self):
        for rec in self:
            user_edit_access = False
            if self.env.user.has_group('showroom_fields.group_kf_administrator') or \
                    self.env.user.has_group('showroom_fields.group_kf_accounting'):
                user_edit_access = True
            rec.user_edit_access = user_edit_access


    user_edit_access = fields.Boolean(string="User Edit Access", compute="_compute_user_edit_access")


    def action_mark_paid_zero_commission(self):
        """
        This method will Mark To 'Paid' for,
            - Zero Commission Amount Line
        """
        for order in self.filtered(lambda x: x.account_move_line_ids):
            for comm_line in order.account_move_line_ids.filtered(lambda x: x.commission_pay and not x.paid):
                # Update the Counter Entry with the same Debit, Credit, Balance and Residual Amount with 0.
                counter_comm_lines = comm_line.move_id.line_ids.filtered(lambda x: x.sale_line_id and (
                        x.debit > 0 or x.credit > 0 or x.amount_residual > 0 or x.balance > 0))
                if len(counter_comm_lines) > 0:
                    if comm_line.move_id.state == 'posted':
                        comm_line.move_id.button_cancel()
                    for line in counter_comm_lines:
                        line.write({
                            'debit': 0,
                            'credit': 0,
                            'balance': 0,
                            'amount_residual': 0
                        })
                    if comm_line.move_id.state == 'draft':
                        comm_line.move_id.action_post()

                if (comm_line.credit == 0 and comm_line.debit == 0):
                    comm_line.move_id.line_ids.write({
                        'paid': True,
                        'com_pay_status': 'fully_paid'
                    })

        return True

                # elif (comm_line.commissionable_amount < 0 or comm_line.commission_rate < 0):
                #     comm_line.move_id.line_ids.write({
                #         'paid': True,
                #         'com_pay_status': 'refund'
                #     })