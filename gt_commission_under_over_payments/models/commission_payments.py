# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import UserError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class CommissionUnderOverPayment(models.Model):
    _name = "commission.under.over.payments"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Commission Under Over Payment Manage"
    _order = "id desc"


    name = fields.Char(string='Name', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    sale_order_id = fields.Many2one("sale.order", "Sale Order", index=True, copy=False)
    partner_vendor_id = fields.Many2one("res.partner", "Partner/Vendor", index=True,copy=False)
    branch_id = fields.Many2one("res.branch", "Showroom", default=lambda self: self.env.user.branch_id)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
    state = fields.Selection(
        [('draft', 'New'), ('under_payment', 'Under Payment'), ('over_payment', 'Over Payment'), ('partial_payment', 'Partial Paid'),
         ('full_payment', 'Fully Paid')], defaul='draft', string="State", index=True, copy=False, track_visibility='onchange', tack_sequence=1)
    commission_journal_id = fields.Many2one("account.move.line", "Income Commission Ref", copy=False)
    original_com_amount = fields.Monetary("Original Amount", help="Original Commission Amount")
    actual_payment_amount = fields.Monetary("Actual Payment Amount")
    over_payment_amount = fields.Monetary("Over-Payment Amount")
    under_payment_amount = fields.Monetary("Under-Payment Amount")
    balance_due_amount = fields.Monetary("Balance Due Amount")
    origin = fields.Char("Origin")
    payment_type = fields.Selection(
        [("normal_payment", "Normal Payment"), ("under_payment", "Under Payment"), ("over_payment", "Over Payment")],
        string="Payment Type")
    commission_payment_history_ids = fields.One2many("commission.payment.history.line",
                                                     'commission_payment_id',
                                                     'Commission Payment History',
                                                     copy=False,  track_visibility='onchange', tack_sequence=2)

    # When we adjust amount against any of the commission payments....than maintain remaining amount
    over_payment_remaining = fields.Monetary("Remaining Over Payment",  track_visibility='onchange', tack_sequence=3)
    under_payment_remaining = fields.Monetary("Remaining Under Payment",  track_visibility='onchange', tack_sequence=4)
    adjusted_commission_payment_ids = fields.Many2many("commission.under.over.payments",
                                                       'commission_payment_adjusted_rel',
                                                       'comm_payment_id',
                                                       'adjusted_comm_payment_id',
                                                       string="Adjusted Commission Payments",
                                                       help="All the amount adjustment references",
                                                       track_visibility='onchange', tack_sequence=5)
    commission_receive_date = fields.Date("Commission Received", copy=False)
    note_description = fields.Text("Notes")
    commission_payment_move_id = fields.Many2one("account.move", "Commission Payment Ref", copy=False)


    @api.multi
    def name_get(self):
        result = []
        for comm_rec in self.sudo():
            if comm_rec.payment_type == 'over_payment':
                name = f"[{comm_rec.name}][SO-{comm_rec.sale_order_id.name}] Over Remaining: {comm_rec.over_payment_remaining}"
            elif comm_rec.payment_type == 'under_payment':
                name = f"[{comm_rec.name}][SO-{comm_rec.sale_order_id.name}] Under Balance Due: {comm_rec.under_payment_remaining}"
            else:
                name = f"{comm_rec.name}"
            result.append((comm_rec.id, name))
        return result


    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []

        if self.env.context.get('commission_under_over_pay'):
            args += [('branch_id', '=', self.env.context.get('parent_branch_id'))]

        if self.env.context.get('commission_under_over_pay'):
            comm_move_line_id = self.env['account.move.line'].browse(int(self._context.get('comm_move_line')))
            if self.env.context.get('payment_amount') == comm_move_line_id.debit:
                args = [['id', 'in', []]]

            if self.env.context.get('payment_amount') == self.env.context.get('debit_amount'):
                args = [['id', 'in', []]]
        return super(CommissionUnderOverPayment, self).name_search(name=name, args=args, operator=operator, limit=limit)


    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            if 'company_id' in vals:
                vals['name'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code(
                'commission.under.over.payments') or _('New')
            else:
                vals['name'] = self.env['ir.sequence'].next_by_code('commission.under.over.payments') or _('New')
        res = super(CommissionUnderOverPayment, self).create(vals)
        return res


    @api.model
    def action_scheduler_mark_fully_paid(self):
        """
        This method will get the 'Under Payment' lines and check the associated commission payment status,
            - If its associated Commission is 'Fully Paid' then mark the 'under payment' as 'Fully Paid' also.
        """
        under_payments_ids = self.env['commission.under.over.payments'].search([
            ('state', '=', 'under_payment'),
            ('commission_journal_id', '!=', False),
            ('sale_order_id', '!=', False),
        ])
        _logger.warning("Total Under Payment Lines: %s", len(under_payments_ids))
        for rec in under_payments_ids:
            if rec.commission_journal_id and rec.commission_journal_id.paid and rec.commission_journal_id.com_pay_status == 'fully_paid':
                rec.state = 'full_payment'
                rec.message_post(body=_("This under payment mark fully paid by scheduler because sale order's commission is fully paid."))
                # self.env.cr.commit()


class CommissionPaymentHistoryLine(models.Model):
    _name = "commission.payment.history.line"


    commission_payment_id = fields.Many2one("commission.under.over.payments", index=True, copy=False, ondelete="cascade")
    commission_journal_id = fields.Many2one("account.move.line", "Commission", copy=False)
    payment_amount = fields.Float("Adjusted Payment Amount")
    payment_type = fields.Selection([("normal_payment", "Normal Payment"), ("under_payment", "Under Payment Adjusted"), ("over_payment", "Over Payment Adjusted")], string="Payment Type")
    origin = fields.Char("Origin")
    # commission_payment_adj_id = fields.Many2many("commission.under.over.payments", "Adjusted Pay Ref")


