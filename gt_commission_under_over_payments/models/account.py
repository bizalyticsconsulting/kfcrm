# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError
import logging

_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = "account.move"

    adjusted_reverse_move_id = fields.Many2one("account.move", string='Adjusted Reverse Move', copy=False)
    adjusted_parent_move_id = fields.Many2one("account.move", string='Adjusted Parent Move', copy=False)


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"


    comm_description = fields.Text("Commission Note")


    @api.one
    def _compute_commission_balance_amount(self):
        for rec in self:
            if rec.credit > 0:
                if rec.credit >= rec.total_comm_paid:
                    if (rec.commissionable_amount < 0 or rec.commission_rate < 0):
                        rec.comm_balance_amount = rec.total_comm_paid - rec.credit
                    elif (rec.com_pay_status == 'refund'):
                        rec.comm_balance_amount = rec.total_comm_paid - rec.credit
                    else:
                        rec.comm_balance_amount = rec.credit - rec.total_comm_paid
                elif rec.credit < rec.total_comm_paid:
                    rec.comm_balance_amount = 0.0

            elif rec.debit > 0:
                if rec.debit >= rec.total_comm_paid:
                    if (rec.commissionable_amount < 0 or rec.commission_rate < 0):
                        rec.comm_balance_amount = rec.total_comm_paid - rec.debit
                    elif (rec.com_pay_status == 'refund'):
                        rec.comm_balance_amount = rec.total_comm_paid - rec.debit
                    else:
                        rec.comm_balance_amount = rec.debit - rec.total_comm_paid
                elif rec.debit < rec.total_comm_paid:
                    rec.comm_balance_amount = 0.0
            else:
                rec.comm_balance_amount = 0


    com_pay_status = fields.Selection([('pending', 'Pending'), ('partial_paid', 'Partial Paid'), ('fully_paid', 'Fully Paid'), ('refund', 'Refund'), ('partial_refund', 'Partial Refund')], default="pending", string="Pay Status")
    total_comm_paid = fields.Float("Total Paid")
    comm_balance_amount = fields.Float(string="Balance Amount", compute="_compute_commission_balance_amount")


    def _check_reconcile_validity(self):
        """
            This method is overridden and added Logger for Move Line info & Details in alert message
        """
        #Perform all checks on lines
        company_ids = set()
        all_accounts = []
        for line in self:
            company_ids.add(line.company_id.id)
            all_accounts.append(line.account_id)
            if (line.matched_debit_ids or line.matched_credit_ids) and line.reconciled:
                # comment this line...15.12.2021
                # raise UserError(_('You are trying to reconcile some entries that are already reconciled.'))
                _logger.info(_("Account Move Line - Reconciled - Invoice - Payment - Payment Associated Invoices: %s - %s - %s - %s - %s"), line,
                             line.reconciled, line.invoice_id, line.payment_id, line.payment_id.invoice_ids)
                # Unnecessary Payments which is not in Use..Cancel it.
                if line.payment_id and len(line.payment_id.invoice_ids) in [0, 1]:
                    _logger.info(_("Payment State: %s"), line.payment_id.state)
                    line.payment_id.cancel()
                    _logger.info(_("Payment is cancelled: %s"), line.payment_id.state)
                    continue
                raise UserError(_('You are trying to reconcile some entries that are already reconciled.\n\n'
                              f'- Account Move Line : {line.id}\n'
                              f'- Reconciled: {line.reconciled}\n'
                              f'- Account Invoice: {line.invoice_id.number}\n'
                              f'- Account Payment: {line.payment_id.name}\n'
                              f'- Account Payment associated Invoices: {line.payment_id.invoice_ids}'
                            ))

        if len(company_ids) > 1:
            raise UserError(_('To reconcile the entries company should be the same for all entries.'))
        if len(set(all_accounts)) > 1:
            raise UserError(_('Entries are not from the same account.'))
        if not (all_accounts[0].reconcile or all_accounts[0].internal_type == 'liquidity'):
            raise UserError(_('Account %s (%s) does not allow reconciliation. First change the configuration of this account to allow it.') % (all_accounts[0].name, all_accounts[0].code))


    @api.multi
    def unlink(self):
        """
        This method will unlink the commission lines.
            When user click on delete icon of the Sale--> Commission Lines.
            Then also delete the other associated commission lines too which is invisible (The Balanced one Credit/Debit)
        """
        unlink_commission_line = self.filtered(lambda r: r.sale_order_id)
        if len(unlink_commission_line) > 0:
            if unlink_commission_line.filtered(lambda r: r.com_pay_status and r.com_pay_status != 'pending'):
                raise UserError(_("You can only delete 'Pending' state Commission Lines ."))

            for comm_line in unlink_commission_line:
                unlink_other_line = self.env['account.move.line'].search(
                    [('sale_order_id', '=', comm_line.sale_order_id.id), ('move_id', '=', comm_line.move_id.id),
                     ('product_id', '=', comm_line.product_id.id), ('id', '!=', comm_line.id)])
                _logger.info(_("Unlink the associated other Lines too: %s"), unlink_other_line)
                self |= unlink_other_line

            # Check for KF Administrator Role 'KF_KOMPASS_ADMINISTRATOR'
            if not self.env.user.has_group('showroom_fields.group_kf_administrator') and \
               not self.env.user.has_group('showroom_fields.group_kf_accounting'):
                error_msg = f"The requested operation cannot be completed due to security restrictions.\n" \
                            f"Please contact your system administrator.\n\n" \
                            f"You have not permission to delete the Commission Lines.\n\n" \
                            f"(Document type: Account Move Line, Operation: delete) - (Records: {self.ids}, User: {self.env.user.id})"
                raise AccessError(_(error_msg))

        return super(AccountMoveLine, self).unlink()


    @api.multi
    def write(self, vals):
        """
        This method is for restrcict the Sale Order Line --> Commission Lines -->Fields change
            - Only Allow 2-user groups: KF_ACCOUNTING_GROUP & KF_KOMPASS_ADMINISTRATOR
            - Rest of the other groups restrict from Edit/Change/Update
        """
        # 3-fields only in commission line from SaleOrderLine is only edited by 2 user-groups\
        for comm_rec in self.filtered(lambda x: x.sale_order_id and x.com_pay_status == 'pending'):
            if 'com_pay_status' not in vals and \
               ('receive_comm_rate' in vals or 'receive_comm_amt' in vals or 'date' in vals):
                if not self.env.user.has_group('showroom_fields.group_kf_administrator') and \
                   not self.env.user.has_group('showroom_fields.group_kf_accounting'):
                    error_msg = f"The requested operation cannot be completed due to security restrictions.\n" \
                                f"Please contact your system administrator.\n\n" \
                                f"You have not permission to update/change the Commission Lines fields.\n\n" \
                                f"(Document type: Account Move Line, Operation: write) - (Records: {comm_rec.ids}, User: {self.env.user.id})"
                    raise AccessError(_(error_msg))

        return super(AccountMoveLine, self).write(vals)


    @api.multi
    def change_comm_rate_amt(self):
        """
        This method is overriden to 'readonly' the fields which is in wizard view.
        """
        ctx = self.env.context.copy()
        sale_order_id = self.mapped('sale_order_id')
        if not sale_order_id:
            if self.env.context.get('params') and self.env.context.get('params').get('model') == 'sale.order':
                sale_order_id = self.env[self.env.context.get('params').get('model')].browse(self.env.context.get('params').get('id'))

        if not sale_order_id.user_edit_access or self.com_pay_status != 'pending':
            ctx.update({'user_edit_access': True})

        return {
            #'name': self.order_id,
            'res_model': 'commission.rate.amount',
            'type': 'ir.actions.act_window',
            'context': ctx,
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': self.env.ref("gt_commission_income.commission_rate_amount_form_view").id,
            'target': 'new'
        }
