# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    _inherit = "stock.picking"


    @api.multi
    def commission_count_income_entries(self, sale_search, journal_ref):
        """ Income Commission
            Validations: If SaleOrderLine is having Discount but not SDA Line, raise an error.
        """
        _logger.info(f"SDA: Stock Picking income commission process: {self}, {sale_search}, {journal_ref}")

        commission_income_journal = int(self.env['ir.config_parameter'].sudo().get_param('commission_income_journal'))
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_income_journal)
        # account_id = journal_ref
        now = datetime.now()

        if sale_search.branch_id:
            reference = '[' + sale_search.name + '] ' + self.name + ' Picking'
            if len(sale_search.order_line.filtered(lambda x: x.product_id.flag)) == 0:
                _logger.info("In Sale Order Line, No product is not commissionable.")
            for sale_line in sale_search.order_line.filtered(lambda x: x.product_id.flag):
                if not sale_line.product_id.flag:
                    continue
                # Validate SaleOrderLine Before create commission with respect to SDA
                if sale_line.discount > 0 or sale_line.discount_show > 0 or sale_line.discount_amount > 0:
                    if len(sale_line.sda_line_ids) == 0:
                        raise ValidationError(_(f"Sale Order Line is having discount but Suplier Discount Approval Lines(SDA) is not entered for the same.\n You can not process for the Income Commission ! \n\n Sale Order Line: {sale_line.product_id.display_name}"))
                layered_sda_id = False
                vendor = sale_line.x_vendor
                _logger.info(f"Income commission is calculated as per the given priority. Commission percentage: {sale_line.income_comm_percent} and amount is {sale_line.income_commission_cal_amount}")
                if vendor and sale_line.income_commission_cal_amount:
                    comm_amount = sale_line.income_commission_cal_amount
                    commission_vals = {
                            # 'name': account_id.default_credit_account_id.name,
                            'name': vendor.display_name,
                            'debit': 0.0,
                            'credit': comm_amount,
                            'account_id': account_id.default_credit_account_id.id,
                            'partner_id': vendor.id,
                            'commission_user': False,
                            'commission_on': self.order_pay_state,
                            'commission_partner': vendor.id,
                            'commission_pay': "showroom_partner",
                            'commissionable_amount': sale_line.price_subtotal,
                            'commission_rate': sale_line.income_comm_percent,
                            'sales_commission_rate': sale_line.income_comm_percent,
                            'total_commission': comm_amount,
                            'comm_type': 'income',
                            'sale_order_id': sale_search.id,
                            'sale_line_id': sale_line.id,
                            'branch_id': sale_search.branch_id.id,
                            'sda_id': len(sale_line.sda_line_ids) > 0 and sale_line.sda_line_ids.id or False,
                            'layered_sda_id': layered_sda_id,
                            'ref': sale_line.product_id.display_name,
                            'product_id': sale_line.product_id.id,
                    }

                    sale_person_vals = {
                            # 'name': sale_search.branch_id.name,
                            'name': vendor.display_name, # sale_line.product_id.display_name,
                            'debit': comm_amount,
                            'credit': 0.0,
                            'account_id': vendor.property_account_receivable_id.id,
                            'partner_id': vendor.id,
                            'commission_user': False,
                            'commission_on': self.order_pay_state,
                            'commission_partner': vendor.id,
                            'commission_pay': "showroom_partner",
                            'commissionable_amount': sale_line.price_subtotal,
                            'commission_rate': sale_line.income_comm_percent,
                            'sales_commission_rate': sale_line.income_comm_percent,
                            'total_commission': comm_amount,
                            'comm_type': 'income',
                            'sale_order_id': sale_search.id,
                            'sale_line_id': sale_line.id,
                            'branch_id': sale_search.branch_id.id,
                            'sda_id': len(sale_line.sda_line_ids) > 0 and sale_line.sda_line_ids.id or False,
                            'layered_sda_id': layered_sda_id,
                            'ref': sale_line.product_id.display_name,
                            'product_id': sale_line.product_id.id,
                    }

                    vals = {
                        'journal_id': account_id.id,
                        'date': now.strftime('%Y-%m-%d'),
                        'state': 'draft',
                        'ref': reference, #journal_ref,
                        'partner_id': vendor.id,
                        'commission_user': False,
                        'commission_partner': vendor.id,
                        'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                        'branch_id': sale_search.branch_id.id,
                    }
                    move = self.env['account.move'].create(vals)
                    _logger.info(f"Income commission entires generated i.e. account.move entries : {move}")
                else:
                    _logger.warning("Vendor does not have selected in Sale Order Line or no commission percentage setup as per the priority.")

                # Update sda Line data if any
                if len(sale_line.sda_line_ids) > 0:
                    _logger.info("Sale Orde Line Linked SDA: {sale_line.sda_line_ids}, Update commission status and created date.")
                    sale_line.sda_line_ids.write({'commission_status': 'created', 'commission_created_date': now})

        return True