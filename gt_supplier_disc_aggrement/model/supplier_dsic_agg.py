
from odoo import models, fields, api, _,tools
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, date
from odoo.tools import float_is_zero, pycompat
import logging
from odoo.tools import float_round
from odoo.addons import decimal_precision as dp
from pprint import pprint

_logger = logging.getLogger(__name__)


class SupplierDiscountApproval(models.Model):
   _name = "supplier.discount.approval"
   _description = "Supplier Discount Approval"


   @api.depends('sp_disc_order_line_id', 'sda_supplier')
   def _compute_sad_disc_order_supplier(self):
      for rec in self:
         rec.commission_rate = 0.0
         rec.x_sda_order_line_total = 0.0
         rec.x_sda_discount_total = 0.0
         rec.x_sda_disc_percent = 0.0
         rec.x_reduced_total = 0.0
         priority_income_commission_rate = 0.0

         if rec.sp_disc_order_line_id:
            # rec.commission_rate = rec.sp_disc_order_line_id.product_id.prod_comm_per or rec.sp_disc_order_line_id.x_vendor.commissions_percentage
            rec.x_sda_order_line_total = rec.sp_disc_order_line_id.price_subtotal
            rec.x_sda_discount_total = rec.sp_disc_order_line_id.discount_amount
            rec.x_sda_disc_percent = rec.sp_disc_order_line_id.discount_show
            rec.x_reduced_total = rec.sp_disc_order_line_id.price_subtotal - rec.sp_disc_order_line_id.discount_amount

            # Need to get the commission rate as pe the priority set...................
            supplier_id = rec.sda_supplier and rec.sda_supplier or rec.sp_disc_order_line_id.x_vendor
            vendor_commission_id = supplier_id.supplier_commission_lines.filtered(lambda x: x.branch_id.id == rec.sp_disc_order_line_id.order_id.branch_id.id and x.company_id.id == rec.sp_disc_order_line_id.company_id.id)
            vendor_commission_rate = vendor_commission_id and vendor_commission_id.commission_rate or 0.0
            category_commission_id = rec.sp_disc_order_line_id.product_id.categ_id.category_commission_lines.filtered(lambda x: x.branch_id.id == rec.sp_disc_order_line_id.order_id.branch_id.id and x.company_id.id == rec.sp_disc_order_line_id.company_id.id)
            category_commission_rate = category_commission_id and category_commission_id.commission_rate or 0.0
            if rec.sp_disc_order_line_id.product_id.flag:
               if rec.sp_disc_order_line_id.product_id.prod_comm_per > 0:
                  priority_income_commission_rate = rec.sp_disc_order_line_id.product_id.prod_comm_per
               elif category_commission_rate > 0:
                  priority_income_commission_rate = category_commission_rate
               elif vendor_commission_rate > 0:
                  priority_income_commission_rate = vendor_commission_rate

            rec.commission_rate = priority_income_commission_rate

         # if rec.sda_supplier:
         #    if rec.sp_disc_order_line_id:
         #       rec.commission_rate = rec.sp_disc_order_line_id.product_id.prod_comm_per or rec.sp_disc_order_line_id.x_vendor.commissions_percentage
         #    else:
         #       rec.commission_rate = rec.sda_supplier.commissions_percentage


   @api.depends('x_sda_disc_kf_share', 'x_sda_disc_kf_share', 'commission_rate')
   def _compute_sda_discount_kf_supp_calculation(self):
      for rec in self:
         if rec.x_sda_disc_supp_share and rec.x_sda_order_line_total > 0:
            rec.x_sda_disc_supp_share_per = (rec.x_sda_disc_supp_share * 100) / rec.x_sda_order_line_total
         if rec.x_sda_disc_kf_share and rec.x_sda_order_line_total > 0:
            rec.x_sda_disc_kf_share_per = (rec.x_sda_disc_kf_share * 100) / rec.x_sda_order_line_total
         if rec.commission_rate: # and rec.x_sda_disc_kf_share_per and rec.x_sda_disc_kf_share_per > 0:
            if rec.x_sda_disc_type != 'off_floor_otf':
               rec.ac_adjust_kf_commis_pert = rec.commission_rate - rec.x_sda_disc_kf_share_per


   @api.depends('sp_disc_order_line_id', 'x_sda_disc_type', 'commission_rate', 'x_sda_order_line_total', 'x_sda_disc_kf_share', 'x_sda_disc_kf_share', 'ac_adjust_kf_commis_pert', 'ac_sda_commission_reduction2', 'ac_adjust_kf_commis_pert2')
   def _compute_sda_discount_calculation(self):
      for rec in self:
         rec.ac_full_kf_commission = 0.0
         rec.ac_adjust_kf_commission = 0.0
         rec.ac_sda_commission_reduction = 0.0
         rec.ac_adjusted_book_sale = 0.0
         rec.ac_booked_sale_deduction = 0.0
         rec.x_adjusted_book_sale = 0.0
         if not rec.sp_disc_order_line_id:
            continue
         if not rec.commission_rate or not rec.x_sda_order_line_total:
            continue
         try:
            if rec.x_sda_disc_type:
               rec.ac_full_kf_commission = (rec.commission_rate * rec.x_sda_order_line_total) / 100  # [B4 * F4]

            if rec.x_sda_disc_type == 'other_supplier_discount_rrt': # Done
               rec.ac_adjust_kf_commission = (rec.x_reduced_total * rec.ac_adjust_kf_commis_pert) / 100  # [B8 * F5]

            elif rec.x_sda_disc_type == 'flat_commission':
               rec.ac_adjust_kf_commission = rec.ac_full_kf_commission - rec.ac_sda_commission_reduction2  # [E4 - E6]

            elif rec.x_sda_disc_type == 'off_floor_otf':
               rec.ac_adjust_kf_commission = (rec.x_reduced_total * rec.ac_adjust_kf_commis_pert2) / 100  # [B8 * F5]

            elif rec.x_sda_disc_type == 'roger_goffigon_dld':  # Done
               rec.ac_adjust_kf_commission = ((rec.x_reduced_total * rec.commission_rate) / 100) - ((rec.x_sda_order_line_total * rec.ac_adjust_kf_commis_pert) / 100)  # [(B8 * F4) - (B4 * F5)]

            elif rec.x_sda_disc_type == 'holy_hunt_discount_rrot':   # Done
               rec.ac_adjust_kf_commission = (rec.x_sda_order_line_total * rec.ac_adjust_kf_commis_pert) / 100 # [B4 * F5]

            # Common Calculations
            if rec.x_sda_disc_type:
               if rec.x_sda_disc_type not in ['flat_commission', 'added_reduction_before_comm']:
                  rec.ac_sda_commission_reduction = rec.ac_full_kf_commission - rec.ac_adjust_kf_commission  # [E4 - E5]

               if rec.x_sda_disc_type in ['added_reduction_before_comm']:
                  rec.ac_sda_commission_reduction = rec.x_sda_discount_total + rec.ac_full_kf_commission  # B7 + E6
                  rec.ac_adjust_kf_commission = rec.ac_full_kf_commission - rec.ac_sda_commission_reduction  # [E6- E8]

               rec.ac_adjusted_book_sale = (rec.ac_adjust_kf_commission * 100) / rec.commission_rate  # [E5/F4]
               rec.ac_booked_sale_deduction = rec.x_reduced_total - rec.ac_adjusted_book_sale  # [B8 - E7]
               rec.x_adjusted_book_sale = rec.x_reduced_total - rec.ac_booked_sale_deduction  # [B8 - E8]

         except Exception as e:
            print (f"Error while calculation: {e}")


   x_sda_disc_reason = fields.Selection([('Order Accommodation','Order Accommodation'),
                                         ('Split Commission','Split Commission'),('Out of Territory','Out of Territory'),('Personal Use','Personal Use'),
                                         ('Quantity Discount Residential','Quantity Discount/Residential'),('Quantity Discount Non-Residential','Quantity Discount/Non-Residential'),
                                         ('Replacement Order','Replacement Order'),('Showhouse','Showhouse'),('Marketing','Marketing'),('Showroom Error','Showroom Error'),
                                         ('SSOF','SSOF'),('Supplier Promotion','Supplier Promotion'),('Standing Discount','Standing Discount')], string="Reason for Discount")

   x_sda_disc_type = fields.Selection([('other_supplier_discount_rrt', 'Other Suppliers Discounts (Reduced Rate on Reduced Total)'),
                                       ('holy_hunt_discount_rrot', 'Holly Hunt / GP / Pierre Frey/ etc. (Reduced Rate on Original Total)'),
                                       ('off_floor_otf', 'Off-The-Floor / Conrad / Custom Pricing'),
                                       ('roger_goffigon_dld', 'Rogers & Goffigon / Delany & Long Discounts'),
                                       ('flat_commission', 'Flat Commission Deductions'),
                                       ('added_reduction_before_comm', 'Added Reduction Before Commission')],
                                      string='Discount Type')

   name = fields.Char(string='SDA Reference', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
   x_sda_disc_notes = fields.Text(string='Notes')
   commission_rate = fields.Float(string='Commission Rate', store=True, compute='_compute_sad_disc_order_supplier', digits=dp.get_precision('Percentage Value'), help="Supplier Commission Rate (%)")
   # full_commission = fields.Float(string='Full Commission', store=True, compute='sda_discount_calculation')
   sda_supplier = fields.Many2one('res.partner', string='Supplier', domain=[('supplier', '=', 'True')])
   user_id = fields.Many2one('res.users', string='Salesperson', default=lambda self: self.env.user)
   branch_id = fields.Many2one("res.branch", "Showroom", default=lambda self: self.env.user.branch_id)
   company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
   currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
   commission_status = fields.Selection([("new", "New"), ("created", "Created"), ("confirmed", "Confirmed"), ('deleted', 'Deleted'), ("cancel", "Cancel")], string="Commission Status", default="new")
   commission_created_date = fields.Datetime(string="Commission Created", help="Commission Created Date")
   commission_confirmed_date = fields.Datetime(string="Commission Confirm", help="Commission Confirmed Date")
   commission_deleted_date = fields.Datetime(string="Commission Deleted", help="Commission Deleted Date")

   # Sale Related
   x_sda_disc_kf_share = fields.Float(string='KF Share')
   x_sda_disc_supp_share = fields.Float(string='Supplier Share')
   x_sda_disc_kf_share_per = fields.Float(string='KF Share %', store=True, compute='_compute_sda_discount_kf_supp_calculation', digits=dp.get_precision('Percentage Value'), help="KF Share (%)")
   x_sda_disc_supp_share_per = fields.Float(string="Supplier Share %", store=True, compute='_compute_sda_discount_kf_supp_calculation', digits=dp.get_precision('Percentage Value'), help="Supplier Share (%)")
   x_sda_disc_percent = fields.Float(string="Discount %", store=True, compute='_compute_sad_disc_order_supplier', digits=dp.get_precision('Percentage Value'), help="Total Discount (%)")
   x_sda_discount_total = fields.Float(string='Total Discount', store=True, compute='_compute_sad_disc_order_supplier', digits=(12,2))
   x_sda_order_line_total = fields.Float(string='Original Total',store=True, compute='_compute_sad_disc_order_supplier', digits=(12,2))
   x_reduced_total = fields.Float(string="Reduced Total", store=True, compute='_compute_sad_disc_order_supplier', digits=(12,2))
   x_adjusted_book_sale = fields.Float(string="Adjusted Booked Sales", store=True, compute='_compute_sda_discount_calculation', digits=(12,2))

   # Accounting Related
   ac_full_kf_commission = fields.Float(string="Full KF Commision", store=True, compute='_compute_sda_discount_calculation', digits=(12,2))
   ac_adjust_kf_commission = fields.Float(string="Adjusted KF Commission", store=True, compute='_compute_sda_discount_calculation', digits=(12,2))
   ac_adjust_kf_commis_pert = fields.Float(string="Adjust KF Commission %", store=True, compute='_compute_sda_discount_kf_supp_calculation', digits=dp.get_precision('Percentage Value'), help="Adjusted KF Commission (%)")
   ac_sda_commission_reduction = fields.Float(string="SDA Commission Reduction", store=True, compute='_compute_sda_discount_calculation', digits=(12,2))
   ac_adjust_kf_commis_pert2 = fields.Float(string="Adjust KF Commission 2 %", digits=dp.get_precision('Percentage Value'), help="Adjusted KF Commission (%)")
   ac_sda_commission_reduction2 = fields.Float(string="SDA Commission Reduction 2", digits=(12, 2))
   ac_adjusted_book_sale = fields.Float(string="Adjusted Booked Sales", store=True, compute='_compute_sda_discount_calculation', digits=(12,2))
   ac_booked_sale_deduction = fields.Float(string="Booked Sales Deduction", store=True, compute='_compute_sda_discount_calculation', digits=(12,2))

   sp_disc_order_id = fields.Many2one('sale.order', string='supplier approval', copy=False, ondelete='cascade')
   sp_disc_order_line_id = fields.Many2one('sale.order.line', string='Sale Order Line', copy=False, ondelete='cascade')
   layered_approach = fields.Boolean("Layered Commission Discount Approach")
   layered_discount_lines = fields.One2many("sda.layered.commission.discount", "sda_discount_id", "Layered Commission Discount")


   _sql_constraints = [
      ('sda_commission_uniq', 'UNIQUE (sp_disc_order_id, sp_disc_order_line_id, branch_id, company_id)', "You can not have two 'SDA Approval Lines' for one 'Sale Order Line' !")
   ]


   @api.model
   def default_get(self, fields_list):
      res = super(SupplierDiscountApproval, self).default_get(fields_list)
      if self.env.context.get('sale_order_id'):
         sale_order = self.env['sale.order'].browse(self.env.context.get('sale_order_id'))
         commissionable_order_line = sale_order.order_line.filtered(lambda x: x.product_id.flag and x.discount_show > 0)
         res.update({'branch_id': sale_order.branch_id.id,
                     'sp_disc_order_line_id': len(commissionable_order_line) == 1 and commissionable_order_line.id or False,
                     'user_id': sale_order and sale_order.user_id.id or False})
      return res


   @api.onchange('sp_disc_order_line_id')
   def onchange_order_line(self):
      domain = {}
      self.x_sda_disc_kf_share = 0.0
      self.x_sda_disc_supp_share = 0.0
      self.sda_supplier = self.sp_disc_order_line_id.x_vendor.id
      if self.sp_disc_order_line_id and self.sp_disc_order_line_id.x_vendor:
         supplier_list = [self.sp_disc_order_line_id.x_vendor.id]
      else:
         supplier_list = [i.x_vendor.id for i in self.sp_disc_order_id.order_line]
      domain.update({'sda_supplier': [('id', 'in', supplier_list)]})
      return {'domain': domain}


   @api.onchange('x_sda_disc_kf_share', 'x_sda_disc_type')
   def onchange_x_sda_disc_kf_share(self):
      if self.x_sda_disc_kf_share > self.x_sda_discount_total:
         raise UserError(_("Please enter correct amount.\n Entered amount should be less than or equal to 'Total Discount' !"))
      if self.x_sda_disc_kf_share < 0:
         raise UserError(_("Please enter positive amount !"))
      if self.x_sda_disc_kf_share >= 0:
         self.x_sda_disc_supp_share = self.x_sda_discount_total - self.x_sda_disc_kf_share
      if self.x_sda_disc_type in ['flat_commission']:
         self.x_sda_disc_supp_share = 0.0
      if self.x_sda_disc_type in ['added_reduction_before_comm']:
         self.x_sda_disc_supp_share = 0.0


   @api.onchange('x_sda_disc_supp_share', 'x_sda_disc_type')
   def onchange_x_sda_disc_supp_share(self):
      if self.x_sda_disc_supp_share > self.x_sda_discount_total:
         raise UserError(_("Please enter correct amount.\n Entered amount should be less than or equal to 'Total Discount' !"))
      if self.x_sda_disc_supp_share < 0:
         raise UserError(_("Please enter positive amount !"))
      if self.x_sda_disc_supp_share >= 0:
         self.x_sda_disc_kf_share = self.x_sda_discount_total - self.x_sda_disc_supp_share
      if self.x_sda_disc_type in ['flat_commission', 'off_floor_otf']:
         self.x_sda_disc_kf_share = 0.0
      # if self.x_sda_disc_type in ['added_reduction_before_comm']:
      #    self.x_sda_disc_kf_share = 0.0


   @api.multi
   def action_send_showroom_manager_sda(self):
      """ This method will send email using Email-Template to the Sale's Customer --> Showroom --> Manager """
      self.ensure_one()
      ir_model_data = self.env['ir.model.data']
      ctx = self.env.context.copy()
      try:
         template_id = ir_model_data.get_object_reference(
            'gt_supplier_disc_aggrement', 'email_template_sda_exception_mail')[1]
      except ValueError:
         template_id = False
      if template_id:
         sale_order_url = self.sp_disc_order_id._get_sale_url()
         ctx.update({
            'sale_order_url': sale_order_url,
            'user_salesperson_name': self.env.user.name,
            'sale_quote_order': self.sp_disc_order_id.state in ['sale', 'done'] and 'Order' or 'Quote'
         })
         mail_template_id = self.env['mail.template'].browse(template_id)
         mail_id = mail_template_id.with_context(ctx).send_mail(self.id, force_send=True)
         if mail_id:
            #mail_res = self.env['mail.mail'].browse(mail_id)
            #if mail_res and mail_res.mail_message_id and not mail_res.mail_message_id.partner_ids:
            #   mail_res.mail_message_id.partner_ids = [(6, 0, self.sp_disc_order_id.branch_id.manager_user_id.partner_id.ids)]

            # Post Message on Sale Order Log View
            manager_data = f"<a href = # data-oe-model=res.partner data-oe-id={self.sp_disc_order_id.branch_id.manager_user_id.partner_id.id}>{self.sp_disc_order_id.partner_id.branch_id.manager_user_id.name}</a>"
            self.sp_disc_order_id.message_post(
               body=_(f"Sale SDA discount added email has been sent to the Showroom Manager {manager_data} ."))
      return True


   @api.model
   def create(self, vals):
      if vals.get('name', _('New')) == _('New'):
         if 'company_id' in vals:
            vals['name'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code(
               'supplier.discount.approval') or _('New')
         else:
            vals['name'] = self.env['ir.sequence'].next_by_code('supplier.discount.approval') or _('New')
      if vals.get('layered_discount_lines'):
         vals.update({'layered_approach': True})
      if vals.get('x_sda_disc_type') == 'added_reduction_before_comm':
         discounted_amount = self.env['sale.order.line'].browse(vals.get('sp_disc_order_line_id')).discount_amount
         vals.update({'x_sda_disc_kf_share': discounted_amount})

      vals.update({'commission_status': 'new'})
      context = dict(self.env.context or {})
      context.update({'biz_creating_sda': True})
      self = self.with_context(context)
      res = super(SupplierDiscountApproval, self).create(vals)

      _logger.info(f"Supplier Discount-->Sale Order Line: {res.sp_disc_order_line_id}")
      # Update SaleOrderLine commission field as per the above calculations
      if res.sp_disc_order_line_id:
         income_comm_cal_amount = res.x_sda_discount_total and res.ac_adjust_kf_commission or res.ac_full_kf_commission
         income_comm_percent = (res.x_sda_discount_total and res.x_sda_disc_type in ['off_floor_otf']) and res.ac_adjust_kf_commis_pert2 or res.ac_adjust_kf_commis_pert
         query = f"update sale_order_line set income_commission_cal_amount={income_comm_cal_amount} where id={res.sp_disc_order_line_id.id}"
         self.env.cr.execute(query)
         query2 = f"update sale_order_line set income_comm_percent={income_comm_percent} where id={res.sp_disc_order_line_id.id}"
         self.env.cr.execute(query2)

         # Post Message
         res.sp_disc_order_id.message_post(body=_(f"Supplier Discount Approval({res.name}) commission in created for Sale Order Line: '{res.sp_disc_order_line_id.display_name}'. \n The Income Commission percentage and amount is : {income_comm_cal_amount} and {income_comm_percent} %."))

         # Update in sale_order_line fields --> x_adjusted_booked_sale with current adjusted_booked_sale amount..Create Method
         res.sp_disc_order_line_id.x_adjusted_booked_sale = res.x_adjusted_book_sale # res.sp_disc_order_line_id.x_adjusted_booked_sale +
         _logger.info(f"SDA-->Sale Order Line - x_adjusted_booked_sale: {res.sp_disc_order_line_id.x_adjusted_booked_sale}")

         # Once SDA is get created send an email using template to the 'Sale Order Customer-->SHowroom-->Manager'
         sp_sale_order_id = res.sp_disc_order_id or res.sp_disc_order_line_id.order_id
         _logger.info(_("Showroom & Manager: %s - %s"), sp_sale_order_id.partner_id.branch_id, sp_sale_order_id.partner_id.branch_id.manager_user_id)
         if sp_sale_order_id and sp_sale_order_id.partner_id.branch_id and sp_sale_order_id.partner_id.branch_id.manager_user_id:
            res.action_send_showroom_manager_sda()
      return res


   @api.multi
   def write(self, vals):
      """ This method will update the sale order line-->x_adjusted_booked_sale, according to the changes.
          First we can substract the old adjusted booked sale and then add new one
      """
      # previous_adjusted_book_sale = self.x_adjusted_book_sale
      context = dict(self.env.context or {})
      context.update({'biz_creating_sda': True})
      if vals.get('layered_discount_lines'):
         if any(['yes' for i in vals.get('layered_discount_lines') if i[0] in [0, 1, 4]]):
            vals.update({'layered_approach': True})

      self = self.with_context(context)
      res = super(SupplierDiscountApproval, self).write(vals)
      # Update SaleOrderLine commission field as per the above calculations
      if self.sp_disc_order_line_id:
         if 'x_sda_discount_total' in vals or 'ac_adjust_kf_commission' in vals or 'ac_full_kf_commission' in vals or 'x_sda_disc_kf_share' in vals \
                 or 'x_sda_disc_supp_share' in vals or 'ac_sda_commission_reduction2' in vals or 'x_sda_disc_type' in vals or 'ac_adjust_kf_commis_pert2' in vals:
            income_comm_cal_amount = self.x_sda_discount_total and self.ac_adjust_kf_commission or self.ac_full_kf_commission
            income_comm_percent = (self.x_sda_discount_total and self.x_sda_disc_type in ['off_floor_otf']) and self.ac_adjust_kf_commis_pert2 or self.ac_adjust_kf_commis_pert
            query = f"update sale_order_line set income_commission_cal_amount={income_comm_cal_amount} where id={self.sp_disc_order_line_id.id}"
            self.env.cr.execute(query)
            query2 = f"update sale_order_line set income_comm_percent={income_comm_percent} where id={self.sp_disc_order_line_id.id}"
            self.env.cr.execute(query2)

            # Post Message
            self.sp_disc_order_id.message_post(body=_(
               f"Supplier Discount Approval({self.name}) commission in updated for Sale Order Line: '{self.sp_disc_order_line_id.display_name}'. \n The Income Commission percentage and amount is : {income_comm_cal_amount} and {income_comm_percent} %."))

            # # Update Commissions if Sale Order Line SDA get updated.....commission should not be posted
            # # commission_line_ids = self.sp_disc_order_id.account_move_line_ids.filtered(lambda x: x.sale_line_id.x_vendor.id == self.sda_supplier.id and x.sale_line_id.id == self.sp_disc_order_line_id.id and x.sda_id.id == self.id and x.paid == False and x.move_id.state != 'post')
            # commission_line_ids = self.env['account.move.line'].search([('sda_id', '=', self.id),  ('sale_line_id', '=', self.sp_disc_order_line_id.id),
            #                                                             ('sale_line_id.x_vendor', '=', self.sda_supplier.id), ('paid', '=', False), ('move_id.state', '!=', 'post')])
            # if len(commission_line_ids) > 0:
            #    for mv_line in commission_line_ids.sorted(key=lambda x: x.id):
            #       mv_line.commissionable_amount = self.sp_disc_order_line_id.price_subtotal
            #       mv_line.commission_rate = income_comm_percent
            #       mv_line.sales_commission_rate = income_comm_percent
            #       mv_line.total_commission = income_comm_cal_amount
            #       if mv_line.account_id.internal_type == 'receivable' or mv_line.account_id.user_type_id.type == 'receivable':
            #          mv_line.debit = income_comm_cal_amount
            #       else:
            #          mv_line.credit = income_comm_cal_amount

         # First substract the previous adjusted_book_sale amount then add new one
         # Update in sale_order_line fields --> x_adjusted_booked_sale with current adjusted_booked_sale amount
         if 'x_sda_disc_type' in vals or 'x_adjusted_book_sale' in vals or 'x_sda_disc_kf_share' in vals or 'x_sda_disc_supp_share' in vals \
                 or 'ac_adjust_kf_commis_pert2' in vals or 'ac_sda_commission_reduction2' in vals:
            # self.sp_disc_order_line_id.x_adjusted_booked_sale = self.sp_disc_order_line_id.x_adjusted_booked_sale - previous_adjusted_book_sale
            self.sp_disc_order_line_id.x_adjusted_booked_sale = self.x_adjusted_book_sale # self.sp_disc_order_line_id.x_adjusted_booked_sale +

      # Any Changes in the Layered one re-calculate the adjusted book sale
      if 'layered_discount_lines' in vals:
         layered_discount_lines = self.layered_discount_lines.sorted(key=lambda x: x.id, reverse=True)
         if len(layered_discount_lines) > 0:
            layered_discount_lines = layered_discount_lines[0]
            sale_line_id = layered_discount_lines.sale_line_id or layered_discount_lines.sda_discount_id.sp_disc_order_line_id
            if len(sale_line_id) > 0:
               sale_line_id.x_adjusted_booked_sale = layered_discount_lines.x_adjusted_book_sale

      return res


   def unlink(self):
      for rec in self:
         if rec.commission_status in ['created', 'confirmed']:
            raise ValidationError(_(f"You can not delete the 'Supplier Discount Approval Line: {rec.name}'.\n  Because commission is processed for this line !"))

         # If current SDA get deleted then substract same amout from the SaleOrderLine--> x_adjusted_booked_sale field
         if rec.sp_disc_order_line_id:
            rec.sp_disc_order_line_id.x_adjusted_booked_sale = 0 # rec.sp_disc_order_line_id.x_adjusted_booked_sale - rec.x_adjusted_book_sale
         _logger.info(
            f"SDA unlink-->Sale Order Line update - x_adjusted_booked_sale: {rec.sp_disc_order_line_id.x_adjusted_booked_sale}")
      return super(SupplierDiscountApproval, self).unlink()


class SaleOrder(models.Model):
   _inherit = 'sale.order'


   sp_disc_approval_line = fields.One2many('supplier.discount.approval', 'sp_disc_order_id', string='Discount Approval Line')


   @api.multi
   def action_confirm(self):
      """
      Override the Method and check for the SDA Validationd for the Discounted --> Commissinable SaleOrderLines
      """
      for order in self:
         for sale_line in order.order_line.filtered(lambda x: x.product_id.flag):
            if not sale_line.product_id.flag:
               continue
            # Validate SaleOrderLine Before confirm with respect to SDA...If SaleLine haveing Discount and not SDA...Alert for the same
            if (sale_line.discount > 0 or sale_line.discount_show > 0 or sale_line.discount_amount > 0) and len(sale_line.sda_line_ids) == 0:
               # if len(sale_line.sda_line_ids) == 0:
               raise ValidationError(_(
                  f"Sale Order Line is having discount but Suplier Discount Approval Lines(SDA) is not entered for the same."
                  f"\nYou can not process to 'confirm' the Sale Order !"
                  f"\nPlease Enter SDA details."
                  f"\n\n Sale Order Line: {sale_line.product_id.display_name}"))

         res = super(SaleOrder, order).action_confirm()


   @api.multi
   def write(self, vals):
      """
         check if any changes happened in SDA Object.
         If any commissions linked with SaleOrderLine and SDA and not in 'Posted', update the commissions line with commission percentge and amount.
      """
      res = super(SaleOrder, self).write(vals)

      if 'sp_disc_approval_line' in vals:

         if any(['yes' for i in vals.get('sp_disc_approval_line') if i[0] in [1]]): # [0, 4]

            # # Check for Lyared line data is changed
            # layered_lines = [i[2] for i in vals.get('sp_disc_approval_line') if i[0] in [0, 1, 4]]
            # if len(layered_lines) > 0:
            #    for data in layered_lines:
            #       if 'layered_discount_lines' in data:
            #          layered_line_data = any([i[0] for i in data.get('layered_discount_lines') if i[0] in [0, 1, 4]])

            # if 'layered_discount_lines' in vals.get('sp_disc_approval_line'):
            sda_id_list = [i[1] for i in vals.get('sp_disc_approval_line')]  # [[1, 17, {'x_sda_disc_kf_share': 55, 'x_sda_disc_supp_share': 45}]]
            sda_ids = self.env['supplier.discount.approval'].browse(sda_id_list)

            for sda in sda_ids:
               # _logger.info(f"SDA {sda.name} data has been Changed. Checking if any commission is associated with the SDA will get also updated !")
               # Update Commissions if Sale Order Line SDA get updated.....commission should not be posted
               commission_line_ids = self.env['account.move.line'].search([('sda_id', '=', sda.id), ('sale_line_id', '=', sda.sp_disc_order_line_id.id),
                                                                           ('sale_line_id.x_vendor', '=', sda.sda_supplier.id), ('paid', '=', False), ('move_id.state', '!=', 'post')])
               if len(commission_line_ids) > 0:
                  income_comm_cal_amount = sda.x_sda_discount_total and sda.ac_adjust_kf_commission or sda.ac_full_kf_commission
                  income_comm_percent = (sda.x_sda_discount_total and sda.x_sda_disc_type in [
                                        'off_floor_otf']) and sda.ac_adjust_kf_commis_pert2 or sda.ac_adjust_kf_commis_pert

               for mv_line in commission_line_ids.sorted(key=lambda x: x.id):
                  mv_line.commissionable_amount = sda.sp_disc_order_line_id.price_subtotal
                  mv_line.commission_rate = income_comm_percent
                  mv_line.sales_commission_rate = income_comm_percent
                  mv_line.total_commission = income_comm_cal_amount
                  if mv_line.account_id.internal_type == 'receivable' or mv_line.account_id.user_type_id.type == 'receivable':
                     mv_line.debit = income_comm_cal_amount
                  else:
                     mv_line.credit = income_comm_cal_amount

      return res


   @api.onchange('branch_id')
   def onchange_order_showroom(self):
      """
      When change showroom..check for the supplier commission percentage and raise alert message accordingly
      """
      for rec in self:
         warn_message = False
         # If Supplier Commission Line is not added or Commission Percentage is 0..Then raise an alert message.
         vendor_commission_id = rec.x_supplier.supplier_commission_lines.filtered(
            lambda x: x.branch_id.id == rec.branch_id.id and x.company_id.id == rec.company_id.id)
         vendor_commission_rate = vendor_commission_id and vendor_commission_id.commission_rate or 0.0
         for so_line in rec.order_line:
            category_commission_id = so_line.product_id.categ_id.category_commission_lines.filtered(
               lambda x: x.branch_id.id == rec.branch_id.id and x.company_id.id == rec.company_id.id)
            category_commission_rate = category_commission_id and category_commission_id.commission_rate or 0.0

            if so_line.product_id and so_line.product_id.flag:
               if so_line.product_id.prod_comm_per > 0:
                  pass
               elif category_commission_rate > 0:
                  pass
               else:
                  # If Supplier Commission Line is not added or Commission Percentage is 0..Then raise an alert message.
                  if not vendor_commission_id or vendor_commission_rate <= 0:
                     warn_message = (_(
                        f"There is no 'Supplier Commission Line' added for the Showroom i.e. {rec.branch_id.name}. "
                        f"\nOR\n The Showroom Commission Percentage may contains Zero(0) %.\n"
                        f"Please add 'Supplier Commission Line' for the same Showroom or add proper 'Commission Percentage' against the Showroom."))
         if warn_message:
            warning_message = {
               'title': _('Supplier Commission Percentage against the Showroom'),
               'message': warn_message
            }
            return {'warning': warning_message}


class SaleOrderLine(models.Model):
   _inherit = 'sale.order.line'


   sda_line_ids = fields.One2many("supplier.discount.approval", "sp_disc_order_line_id", "SDA Reference")
   x_adjusted_booked_sale = fields.Float("Adjusted Book Sale")


   @api.depends('price_unit', 'price_subtotal', 'x_vendor', 'product_id')
   def _compute_income_commission_amount(self):
      for rec in self:
         income_commission_amount = 0.0
         income_comm_percent = 0.0
         vendor_commission_id = rec.x_vendor.supplier_commission_lines.filtered(
            lambda x: x.branch_id.id == rec.order_id.branch_id.id and x.company_id.id == rec.company_id.id)

         vendor_commission_rate = vendor_commission_id and vendor_commission_id.commission_rate or 0.0
         category_commission_id = rec.product_id.categ_id.category_commission_lines.filtered(
            lambda x: x.branch_id.id == rec.order_id.branch_id.id and x.company_id.id == rec.company_id.id)
         category_commission_rate = category_commission_id and category_commission_id.commission_rate or 0.0
         if rec.product_id and rec.product_id.flag:
            if rec.product_id.prod_comm_per > 0:
               income_comm_percent = rec.product_id.prod_comm_per
               income_commission_amount = rec.price_subtotal * rec.product_id.prod_comm_per / 100
            elif category_commission_rate > 0:
               income_comm_percent = category_commission_rate
               income_commission_amount = rec.price_subtotal * category_commission_rate / 100
            elif vendor_commission_rate > 0:
               income_comm_percent = vendor_commission_rate
               income_commission_amount = rec.price_subtotal * vendor_commission_rate / 100

            # check any SDA LInked with this...if yes take all the income commission calculations from there
            if len(rec.sda_line_ids) > 0:
               # Comment on 21.01.2024
               # income_commission_amount = (rec.sda_line_ids.x_sda_discount_total > 0) and rec.sda_line_ids.ac_adjust_kf_commission or rec.sda_line_ids.ac_full_kf_commission
               income_commission_amount = rec.sda_line_ids.ac_full_kf_commission
               if rec.sda_line_ids.x_sda_discount_total > 0:
                  income_commission_amount = rec.sda_line_ids.ac_adjust_kf_commission
               income_comm_percent = ((rec.sda_line_ids.x_sda_discount_total > 0) and rec.sda_line_ids.x_sda_disc_type in ['off_floor_otf']) and rec.sda_line_ids.ac_adjust_kf_commis_pert2 or rec.sda_line_ids.ac_adjust_kf_commis_pert

            rec.income_commission_cal_amount = income_commission_amount
            rec.income_comm_percent = income_comm_percent


   @api.multi
   def name_get(self):
      result = []
      for so_line in self.sudo():
         # name = '%s - %s' % (so_line.order_id.name, (so_line.name and so_line.name.split('\n')[0]) or so_line.product_id.name) # original code
         name = so_line.product_id.display_name
         if so_line.order_partner_id.ref:
            name = '%s (%s)' % (name, so_line.order_partner_id.ref)
         result.append((so_line.id, name))
      return result


   @api.model
   def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
      args = args or []
      if self._context.get('sda_check'):
         args.append(('discount_show', '>', 0))
         # discount, discount_amount, discount_show
         args.append(('product_id.flag', '=', True))
         args.append(('product_id.type', 'in', ['consu', 'product']))
      sale_line_ids = self._search(args, limit=limit, access_rights_uid=name_get_uid)
      return self.browse(sale_line_ids).name_get()


   def unlink(self):
      for rec in self:
         if len(rec.sda_line_ids) > 0:
            raise ValidationError(_(f"You can not delete Sale Order Line, it is associated with SDA({rec.sda_line_ids.name})"))
      return super(SaleOrderLine, self).unlink()


   @api.onchange('price_unit', 'price_subtotal', 'x_vendor', 'product_id')
   def onchange_product_supplier_commission(self):
      for rec in self:
         vendor_commission_id = rec.x_vendor.supplier_commission_lines.filtered(
            lambda x: x.branch_id.id == rec.order_id.branch_id.id and x.company_id.id == rec.company_id.id)
         vendor_commission_rate = vendor_commission_id and vendor_commission_id.commission_rate or 0.0
         category_commission_id = rec.product_id.categ_id.category_commission_lines.filtered(
            lambda x: x.branch_id.id == rec.order_id.branch_id.id and x.company_id.id == rec.company_id.id)
         category_commission_rate = category_commission_id and category_commission_id.commission_rate or 0.0
         if rec.product_id and rec.product_id.flag:
            if rec.product_id.prod_comm_per > 0:
               pass
            elif category_commission_rate > 0:
               pass
            else:
               # If Supplier Commission Line is not added or Commission Percentage is 0..Then raise an alert message.
               if not vendor_commission_id or vendor_commission_rate <= 0:
                  warn_message = (_(
                     f"There is no 'Supplier Commission Line' added for the Showroom i.e. {rec.order_id.branch_id.name}. "
                     f"\nOR\n The Showroom Commission Percentage may contains Zero(0) %.\n"
                     f"Please add 'Supplier Commission Line' for the same Showroom or add proper 'Commission Percentage' against the Showroom."))
                  warning_message = {
                     'title': _('Supplier Commission Percentage against the Showroom'),
                     'message': warn_message
                  }
                  return {'warning': warning_message}


   @api.model
   def create(self, vals):
      """ If there is no SDA Concept then initially store the 'x_adjusted_booked_sale' same as SaleOrderLine 'price_subtotal' amount """
      if 'product_uom_qty' in vals or 'price_unit' in vals or 'discount_amount' in vals:
         x_adjusted_booked_sale = vals.get('price_unit') * vals.get('product_uom_qty')
         if 'discount_show' in vals or 'discount_amount' in vals:
            x_adjusted_booked_sale = x_adjusted_booked_sale - vals.get('discount_amount')
         vals.update({'x_adjusted_booked_sale': x_adjusted_booked_sale})

      res = super(SaleOrderLine, self).create(vals)
      return res


   @api.multi
   def write(self, vals):
      """ If there is SDA and 'x_adjusted_booked_sale' is written from SDA then no need to update else change of
         price or qty will change the  'x_adjusted_booked_sale' same as SaleOrderLine 'price_subtotal' amount
      """
      # product_qty = 'product_uom_qty' in vals and vals.get('product_uom_qty') or self.product_uom_qty
      product_qty = vals.get('product_uom_qty') if 'product_uom_qty' in vals else self.product_uom_qty
      # price_unit = 'price_unit' in vals and vals.get('price_unit') or self.price_unit
      price_unit = vals.get('price_unit') if 'price_unit' in vals else self.price_unit
      # discount_amount = 'discount_amount' in vals and vals.get('discount_amount') or self.discount_amount
      discount_amount = vals.get('discount_amount') if 'discount_amount' in vals else self.discount_amount
      # In this case, if SDA Lines have filled and changes in the Sale.Order.Line view, then no changes for the 'x_adjusted_booked_sale' field.

      update_adjusted_sale = False
      if vals.get('x_adjusted_booked_sale') or len(self.sda_line_ids) > 0:
         if vals.get('x_adjusted_booked_sale'):
            pass
         elif len(self.sda_line_ids) > 0:
            #702 ..update 'x_adjusted_booked_sale' Even there is SDA
            if 'product_uom_qty' in vals or 'price_unit' in vals or 'discount_amount' in vals or 'discount' in vals:
               update_adjusted_sale = True

      elif 'product_uom_qty' in vals or 'price_unit' in vals or 'discount_amount' in vals or 'discount' in vals:
         update_adjusted_sale = True

      if update_adjusted_sale:
         if (product_qty * price_unit) == 0:
            adjusted_booked_sale_amount = (product_qty * price_unit)
         else:
            adjusted_booked_sale_amount = (product_qty * price_unit) - discount_amount
         vals.update({'x_adjusted_booked_sale': adjusted_booked_sale_amount})
         # vals.update({'x_adjusted_booked_sale': (product_qty * price_unit) - discount_amount})

      res = super(SaleOrderLine, self).write(vals)
      return res


   @api.model
   def cron_update_adjusted_sale(self):
      """ This method will update the sale order line 'x_adjusted_booked_sale' field,
         Case-1: If no Discount or SDA: Then take sale order line price_subtotal amount
         Case-2: If SDA, then take adjusted book sale from SDA it self.
         Case-3: If SDA-Layered then take last layered adjusted book sale.
      """
      sale_order_line_ids = self.search([('state', '!=', 'cancel')], order="id desc")
      for sol_line in sale_order_line_ids:
         x_adjusted_booked_sale = 0
         if len(sol_line.sda_line_ids) > 0:
            if len(sol_line.sda_line_ids.layered_discount_lines) > 0:
               sda_layered_ids = sol_line.sda_line_ids.layered_discount_lines.sorted(key=lambda r: r.id, reverse=True)
               x_adjusted_booked_sale = sda_layered_ids[0].x_adjusted_book_sale
            else:
               x_adjusted_booked_sale = sol_line.sda_line_ids.x_adjusted_book_sale
         else:
            x_adjusted_booked_sale = (sol_line.price_unit * sol_line.product_uom_qty) - sol_line.discount_amount
         sol_line.x_adjusted_booked_sale = x_adjusted_booked_sale


class SDALayeredcommissionDiscount(models.Model):
   _name = "sda.layered.commission.discount"
   _description = "SDA Layered Commission Discount"


   @api.model
   def default_get(self, fields_list):
      res = super(SDALayeredcommissionDiscount, self).default_get(fields_list)
      if self.env.context.get('sale_order_id'):
         sale_order = self.env['sale.order'].browse(self.env.context.get('sale_order_id'))
         res.update({'branch_id': sale_order.branch_id.id, })
      return res


   @api.depends('layered_commission_date')
   def _compute_layered_book_sale(self):
      for rec in self:
         try:
            query = f"select id, x_adjusted_book_sale, ac_adjust_kf_commission, ac_adjusted_book_sale from sda_layered_commission_discount where sda_discount_id={self.env.context['sda_id']} and sale_id={self.env.context['sale_order_id']} order by id desc limit 1"
         except Exception as e:
            print(f"Error : {e}")
            try:
               if isinstance(rec.sda_discount_id.sp_disc_order_id.id, int):
                  sale_order_id = rec.sda_discount_id.sp_disc_order_id.id
               elif self.env.context.get('params').get('model') == 'sale.order':
                  sale_order_id = self.env.context.get('params').get('id')
               else:
                  sale_order_id = False
               if sale_order_id:
                  query = f"select id, x_adjusted_book_sale, ac_adjust_kf_commission, ac_adjusted_book_sale from sda_layered_commission_discount where sda_discount_id={rec.sda_discount_id.id} and sale_id={sale_order_id} and id <> {rec.id} order by id desc limit 1"
               else:
                  query = f"select id, x_adjusted_book_sale, ac_adjust_kf_commission, ac_adjusted_book_sale from sda_layered_commission_discount where sda_discount_id={rec.sda_discount_id.id} and id <> {rec.id} order by id desc limit 1"
            except:
               query = False
         fetch_data = []
         try:
            if query:
               self.env.cr.execute(query)
               fetch_data = self.env.cr.fetchone()
            rec.commission_rate = rec.sda_discount_id.commission_rate
            rec.sale_id = rec.sda_discount_id.sp_disc_order_id.id
            rec.sale_line_id = rec.sda_discount_id.sp_disc_order_line_id.id

            if fetch_data and len(fetch_data) > 0:
               rec.layered_comm_id = fetch_data[0]
               rec.xsda_adjs_book_sale_total = fetch_data[1]
               rec.ac_adjust_kf_commission_main = fetch_data[2]
               rec.ac_adjusted_book_sale_pre = fetch_data[3]
            else:
               rec.xsda_adjs_book_sale_total = rec.sda_discount_id.x_adjusted_book_sale
               rec.ac_adjust_kf_commission_main = rec.sda_discount_id.ac_adjust_kf_commission
               rec.ac_adjusted_book_sale_pre = rec.sda_discount_id.ac_adjusted_book_sale
         except Exception as e:
            print (f"Error: {e}")


   @api.depends('ac_sda_commission_reduction')
   def _compute_layered_discount_calculation(self):
      for rec in self:
         rec.ac_adjust_kf_commission = 0.0
         rec.ac_adjusted_book_sale = 0.0
         rec.ac_booked_sale_deduction = 0.0
         rec.x_adjusted_book_sale = 0.0
         if rec.ac_sda_commission_reduction:
            try:
               rec.ac_adjust_kf_commission = rec.ac_adjust_kf_commission_main - rec.ac_sda_commission_reduction   # E5-E17
               rec.ac_booked_sale_deduction = (rec.ac_sda_commission_reduction * 100) / rec.commission_rate  # E17/F4
               rec.ac_adjusted_book_sale = rec.ac_adjusted_book_sale_pre - rec.ac_booked_sale_deduction  # E7-E19
               rec.x_adjusted_book_sale = rec.ac_adjusted_book_sale                                               # E18
            except Exception as e:
               print(f"Error: {e}")


   name = fields.Char(string='Layered Discount Reference', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
   sda_discount_id = fields.Many2one("supplier.discount.approval", copy=False, ondelete="cascade")
   layered_commission_date = fields.Datetime(string="Layered Date", default=lambda *a: datetime.now(), required=True)
   commission_status = fields.Selection([("new", "New"), ("created", "Created"), ("confirmed", "Confirmed"), ('deleted', 'Deleted'), ("cancel", "Cancel")], string="Commission Status", default="new")
   commission_created_date = fields.Datetime(string="Commission Created", help="Commission Created Date")
   commission_confirmed_date = fields.Datetime(string="Commission Confirm", help="Commission Confirmed Date")
   commission_deleted_date = fields.Datetime(string="Commission Deleted", help="Commission Deleted Date")
   branch_id = fields.Many2one("res.branch", "Showroom", default=lambda self: self.env.user.branch_id)
   company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
   currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
   sale_id = fields.Many2one("sale.order", string="Sale Order", store=True, compute='_compute_layered_book_sale', copy=False)
   sale_line_id = fields.Many2one("sale.order.line", string="Sale Order Line", store=True, compute='_compute_layered_book_sale', copy=False)
   layered_comm_id = fields.Many2one("sda.layered.commission.discount", string="Parent Layered", store=True, compute='_compute_layered_book_sale')

   x_adjusted_book_sale = fields.Float(string="Adjusted Booked Sales 2", store=True, compute='_compute_layered_discount_calculation', digits=(12, 2))
   commission_rate = fields.Float(string="Commission Rate (%)", store=True, compute='_compute_layered_book_sale', digits=(12, 2))
   xsda_adjs_book_sale_total = fields.Float(string='Adjusted Booked Sales 1', store=True, compute='_compute_layered_book_sale', digits=(12, 2),
                                            help="This is previous Adjusted Book Sale Coming from either SDA or Previous Layered data")

   ac_adjust_kf_commission = fields.Float(string="Adjusted KF Commission 2", store=True, compute='_compute_layered_discount_calculation', digits=(12,2))
   ac_sda_commission_reduction = fields.Float(string="SDA Commission Reduction 2", digits=(12, 2))
   ac_adjusted_book_sale = fields.Float(string="Adjusted Booked Sales 2", store=True, compute='_compute_layered_discount_calculation', digits=(12,2))
   ac_booked_sale_deduction = fields.Float(string="Booked Sales Deduction 2", store=True, compute='_compute_layered_discount_calculation', digits=(12,2))

   # Accounting Related
   ac_adjust_kf_commission_main = fields.Float(string="Adjusted KF Commission", store=True, compute='_compute_layered_book_sale', digits=(12,2))
   ac_adjusted_book_sale_pre = fields.Float(string="Adjusted Book Sale Previous", store=True, compute='_compute_layered_book_sale', digits=(12,2))

   x_sda_disc_reason = fields.Selection([('Order Accommodation','Order Accommodation'),
                                         ('Split Commission','Split Commission'),('Out of Territory','Out of Territory'),('Personal Use','Personal Use'),
                                         ('Quantity Discount Residential','Quantity Discount/Residential'),('Quantity Discount Non-Residential','Quantity Discount/Non-Residential'),
                                         ('Replacement Order','Replacement Order'),('Showhouse','Showhouse'),('Marketing','Marketing'),('Showroom Error','Showroom Error'),
                                         ('SSOF','SSOF'),('Supplier Promotion','Supplier Promotion'),('Standing Discount','Standing Discount')], string="Reason for Discount")


   @api.model
   def create(self, vals):
      if vals.get('name', _('New')) == _('New'):
         if 'company_id' in vals:
            vals['name'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code(
               'sda.layered.commission.discount') or _('New')
         else:
            vals['name'] = self.env['ir.sequence'].next_by_code('sda.layered.commission.discount') or _('New')
      vals.update({'layered_commission_date': datetime.now(), 'commission_status': 'new'})

      context = dict(self.env.context or {})
      context.update({'biz_creating_sda': True})
      self = self.with_context(context)

      res = super(SDALayeredcommissionDiscount, self).create(vals)

      if res.sale_id and res.sda_discount_id:
         # Post Message
         res.sale_id.message_post(body=_(
            f"SDA - Layered Discount Line - {res.name}) commission in created for Sale Order Line: '{res.sale_line_id.display_name}'."))

         if res.ac_sda_commission_reduction > 0:
            ''' Create Layered Commission Payment account.move.line....Call income commission account entries '''
            income_commission_config = self.env['ir.config_parameter'].sudo().get_param('commission_income')
            _logger.info(f"Income commission commission configuration: {income_commission_config}")
            if income_commission_config:
               if not res.sale_id.x_skip_commission: # Skip Commission Process if True ... 19dec2020
                  res.commission_count_income_entries(res.sale_id, res.sda_discount_id, res.name, res.ac_sda_commission_reduction)

      layered_sale_line_id = res.sale_line_id or res.sda_discount_id.sp_disc_order_line_id
      if layered_sale_line_id:
         # Update in sale_order_line fields --> x_adjusted_booked_sale with current adjusted_booked_sale amount..Create Method
         layered_sale_line_id.x_adjusted_booked_sale = res.x_adjusted_book_sale # layered_sale_line_id.x_adjusted_booked_sale +

      return res


   def write(self, vals):
      """ This method will update the sale order line-->x_adjusted_booked_sale, according to the changes.
          First we can substract the old adjusted booked sale and then add new one
      """
      # previous_adjusted_book_sale = self.x_adjusted_book_sale
      context = dict(self.env.context or {})
      context.update({'biz_creating_sda': True})
      self = self.with_context(context)

      res = super(SDALayeredcommissionDiscount, self).write(vals)

      # if 'x_adjusted_book_sale' in vals and self.sale_id and self.sda_discount_id:
      layered_sale_line_id = self.sale_line_id or self.sda_discount_id.sp_disc_order_line_id
      if layered_sale_line_id and 'ac_sda_commission_reduction' in vals:
         # First substract the previous adjusted_book_sale amount then add new one
         # Update in sale_order_line fields --> x_adjusted_booked_sale with current adjusted_booked_sale amount..Create Method
         # layered_sale_line_id.x_adjusted_booked_sale = layered_sale_line_id.x_adjusted_booked_sale - previous_adjusted_book_sale
         layered_sale_line_id.x_adjusted_booked_sale = self.x_adjusted_book_sale # layered_sale_line_id.x_adjusted_booked_sale +

      return res


   def unlink(self):
      for rec in self:
         if rec.commission_status in ['created', 'confirmed']:
            raise ValidationError(_(f"You can not delete the 'Layered Commission Line: {rec.name}'.\n  Because commission is processed for this line !"))

         # # If current SDA get deleted then substract same amout from the SaleOrderLine--> x_adjusted_booked_sale field
         # if rec.sale_line_id and rec.sda_discount_id:
         #    rec.sale_line_id.x_adjusted_booked_sale = 0 # rec.sale_line_id.x_adjusted_booked_sale - rec.x_adjusted_book_sale
      return super(SDALayeredcommissionDiscount, self).unlink()


   @api.multi
   def commission_count_income_entries(self, sale_search, sda_id, layered_ref, reduction_amount):
      """ Income Commission: this method will create reverse entry on the sda commission payment journal entries with respect to layered """
      _logger.info(f"Layered Discount Approach: income commission process: {self}, {sale_search}, {layered_ref}")
      AccountMoveLine = self.env['account.move.line']
      now = datetime.now()
      ctx = self.env.context.copy()
      sale_line = self.sale_line_id

      # Get Payment account.move.line(Journal Items) against the SDA Commission hase been created. and DO Reverse entry of the reduction amount on that.
      sda_payment_move_line_ids = AccountMoveLine.search([('move_id.state', '=', 'posted'),
                                                          ('sda_id', '=', sda_id.id),
                                                          ('sale_line_id', '=', sale_line.id),
                                                          ('parent_commission_id', '!=', False),
                                                          ('comm_type', '=', False),
                                                          ('sale_order_id', '=', False),
                                                          ('layered_sda_id', '=', False)]) # ('parent_commisison_id', '=', commission_line_ids)
      if len(sda_payment_move_line_ids) > 0:
         reference = '[' + sale_search.name + '] ' + sda_id.name + ' : ' + layered_ref + ' Layered'
         sda_payment_move_id = sda_payment_move_line_ids.mapped('move_id')
         if sda_payment_move_id:
            reverse_date = fields.Date.today()
            ctx.update({'sda_reverse_entry': True,
                        'layered_reduction_amount': reduction_amount,
                        'sda_id': sda_id.id,
                        'layered_sda_id': self.id,
                        'sale_line_id': sale_line.id,
                        'reference': reference})
            reverse_move_id = sda_payment_move_id.with_context(ctx).reverse_moves(reverse_date, False)   # self.journal_id or
            if len(reverse_move_id) > 0:
               sda_payment_move_id.reverse_entry_ids = [(4, reverse_move_id[0])]

               _logger.info(f"Layered Discount commission payment 'Reverse' entries generated i.e {reverse_move_id[0]} against the SDA Commission payment i.e. {sda_payment_move_id}")
               self.commission_status = 'confirmed'
               self.commission_created_date = now
               self.commission_confirmed_date = now

      return True