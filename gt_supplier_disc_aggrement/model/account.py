# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = "account.move"


    reverse_entry_ids = fields.Many2many('account.move', 'account_move_reverse_rel', 'move_id', 'reverse_move_id',
                                          String="Reverse entries", readonly=True, copy=False)


    @api.multi
    def _reverse_move(self, date=None, journal_id=None, auto=False):
        self.ensure_one()
        date = date or fields.Date.today()
        layered_reduction_amount = self.env.context.get('layered_reduction_amount')
        sda_reverse_entry = self.env.context.get('sda_reverse_entry')

        with self.env.norecompute():
            reversed_move = self.copy(default={
                'date': date,
                'journal_id': journal_id.id if journal_id else self.journal_id.id,
                'ref': (_('Automatic reversal of: %s') if auto else _('Reversal of: %s')) % (self.name),
                'auto_reverse': False})
            for acm_line in reversed_move.line_ids.with_context(check_move_validity=False):
                acm_line_vals = {
                                    'debit': acm_line.credit,
                                    'credit': acm_line.debit,
                                    'amount_currency': -acm_line.amount_currency
                                }

                # If flow coming from the SDA Layered Approach(Sale.Order)...Reverse Commission
                if sda_reverse_entry and layered_reduction_amount:
                    acm_line_vals.update({
                        'debit': acm_line.credit and layered_reduction_amount or 0.0,        # acm_line.credit,
                        'credit': acm_line.debit and layered_reduction_amount or 0.0,      # acm_line.debit,

                        'layered_sda_id': self.env.context.get('layered_sda_id'),
                        'sda_id': self.env.context.get('sda_id'),
                        'sale_line_id': self.env.context.get('sale_line_id'),
                        # 'ref': self.env.context.get('reference'),
                    })
                acm_line.write(acm_line_vals)

            self.reverse_entry_id = reversed_move
        self.recompute()
        return reversed_move


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"


    sda_id = fields.Many2one('supplier.discount.approval', 'SDA', index=1, copy=False)
    layered_sda_id = fields.Many2one('sda.layered.commission.discount', 'Layered SDA', index=1, copy=False)


    def unlink(self):
        for rec in self:
            if rec.move_id.state != 'posted':
                if rec.sda_id:
                    rec.sda_id.commission_status = 'deleted'
                    rec.sda_id.commission_deleted_date = datetime.now()
                if rec.layered_sda_id:
                    rec.layered_sda_id.commission_status = 'deleted'
                    rec.layered_sda_id.commission_deleted_date = datetime.now()

        return super(AccountMoveLine, self).unlink()


class AccountPayment(models.Model):
    _inherit = 'account.payment'


    @api.multi
    def commission_count_income_entries(self, sale_search, journal_ref):
        """ Income Commission
            Validations: If SaleOrderLine is having Discount but not SDA Line, raise an error.
        """
        _logger.info(f"SDA: Account Payment income commission process: {self}, {sale_search}, {journal_ref}")

        commission_income_journal = int(self.env['ir.config_parameter'].sudo().get_param('commission_income_journal'))
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_income_journal)
        # account_id = journal_ref
        now = datetime.now()

        if sale_search.branch_id:
            reference = '[' + sale_search.name + '] ' + self.name + ' Payment'
            if len(sale_search.order_line.filtered(lambda x: x.product_id.flag)) == 0:
                _logger.info("In Sale Order Line, No product is not commissionable.")
            for sale_line in sale_search.order_line.filtered(lambda x: x.product_id.flag):
                if not sale_line.product_id.flag:
                    continue
                # Validate SaleOrderLine Before create commission with respect to SDA
                if sale_line.discount > 0 or sale_line.discount_show > 0 or sale_line.discount_amount > 0:
                    if len(sale_line.sda_line_ids) == 0:
                        raise ValidationError(_(
                            f"Sale Order Line is having discount but Suplier Discount Approval Lines(SDA) is not entered for the same.\n You can not process for the Income Commission ! \n\n Sale Order Line: {sale_line.product_id.display_name}"))
                layered_sda_id = False
                vendor = sale_line.x_vendor
                _logger.info(
                    f"Income commission is calculated as per the given priority. Commission percentage: {sale_line.income_comm_percent} and amount is {sale_line.income_commission_cal_amount}")
                if vendor and sale_line.income_commission_cal_amount:
                    comm_amount = sale_line.income_commission_cal_amount
                    commission_vals = {
                        # 'name': account_id.default_credit_account_id.name,
                        'name': vendor.display_name,
                        'debit': 0.0,
                        'credit': comm_amount,
                        'account_id': account_id.default_credit_account_id.id,
                        'partner_id': vendor.id,
                        'commission_user': False,
                        'commission_on': self.order_pay_state,
                        'commission_partner': vendor.id,
                        'commission_pay': "showroom_partner",
                        'commissionable_amount': sale_line.price_subtotal,
                        'commission_rate': sale_line.income_comm_percent,
                        'sales_commission_rate': sale_line.income_comm_percent,
                        'total_commission': comm_amount,
                        'comm_type': 'income',
                        'sale_order_id': sale_search.id,
                        'sale_line_id': sale_line.id,
                        'branch_id': sale_search.branch_id.id,
                        'sda_id': len(sale_line.sda_line_ids) > 0 and sale_line.sda_line_ids.id or False,
                        'layered_sda_id': layered_sda_id,
                        'ref': sale_line.product_id.display_name,
                        'product_id': sale_line.product_id.id,
                    }

                    sale_person_vals = {
                        # 'name': sale_search.branch_id.name,
                        'name': vendor.display_name, # sale_line.product_id.display_name,
                        'debit': comm_amount,
                        'credit': 0.0,
                        'account_id': vendor.property_account_receivable_id.id,
                        'partner_id': vendor.id,
                        'commission_user': False,
                        'commission_on': self.order_pay_state,
                        'commission_partner': vendor.id,
                        'commission_pay': "showroom_partner",
                        'commissionable_amount': sale_line.price_subtotal,
                        'commission_rate': sale_line.income_comm_percent,
                        'sales_commission_rate': sale_line.income_comm_percent,
                        'total_commission': comm_amount,
                        'comm_type': 'income',
                        'sale_order_id': sale_search.id,
                        'sale_line_id': sale_line.id,
                        'branch_id': sale_search.branch_id.id,
                        'sda_id': len(sale_line.sda_line_ids) > 0 and sale_line.sda_line_ids.id or False,
                        'layered_sda_id': layered_sda_id,
                        'ref': sale_line.product_id.display_name,
                        'product_id': sale_line.product_id.id,
                    }

                    vals = {
                        'journal_id': account_id.id,
                        'date': now.strftime('%Y-%m-%d'),
                        'state': 'draft',
                        'ref': reference, #journal_ref,
                        'partner_id': vendor.id,
                        'commission_user': False,
                        'commission_partner': vendor.id,
                        'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                        'branch_id': sale_search.branch_id.id,
                    }
                    move = self.env['account.move'].create(vals)
                    _logger.info(f"Income commission entires generated i.e. account.move entries : {move}")
                else:
                    _logger.warning(
                        "Vendor does not have selected in Sale Order Line or no commission percentage setup as per the priority.")

                # Update sda Line data if any
                if len(sale_line.sda_line_ids) > 0:
                    _logger.info(
                        "Sale Order Line Linked SDA: {sale_line.sda_line_ids}, Update commission status and created date.")
                    sale_line.sda_line_ids.write({'commission_status': 'created', 'commission_created_date': now})

            return True
