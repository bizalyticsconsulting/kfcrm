# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import logging

_logger = logging.getLogger(__name__)


class Branch(models.Model):
    _inherit = "res.branch"


    manager_user_id = fields.Many2one('res.users', string='Showroom Manager', copy=False)
