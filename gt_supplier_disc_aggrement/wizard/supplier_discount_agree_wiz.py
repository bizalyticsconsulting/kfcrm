from odoo import models, fields, api, _,tools
from datetime import datetime, date
import logging
import re
import io
import xlwt
import base64
from odoo.exceptions import UserError
import re
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception,content_disposition
_logger = logging.getLogger(__name__)

class Binary(http.Controller):
    @http.route('/download', type='http', auth="public")
    @serialize_exception
    def download_document(self, model, field, id, filename=None, **kw):
        Model = request.env[model]
        fields = [field]
        res = Model.browse(int(id))
        if model == 'showroom.vendor.forecast.report':
            filecontent = base64.b64decode(res.report_data_file or '')
        else:
            filecontent = base64.b64decode(res.data or '')
        if not filecontent:
            return request.not_found()
        else:
            if not filename:
                filename = '%s_%s' % (model.replace('.', '_'), id)

        return request.make_response(filecontent,
                       [('Content-Type', 'application/octet-stream'),
                        ('Content-Disposition', content_disposition(filename))])


class SupplierDisWizard(models.TransientModel):
    _name = "supplier.disc.wiz"

    branch_id = fields.Many2one('res.branch', string='Branch',)
    start_date = fields.Date(string="Start Date",)
    end_date = fields.Date(string='End Date')
    data = fields.Binary('File')

    @api.multi
    def get_SupplierDiscount_report(self):
        """
        This method will print xlsx reprot for the SDA/Layered Discount
        """
        date_now = datetime.now().date()
        #sale_order_domain = [('sp_disc_approval_line', '!=', False), ('state', '=', 'sale'), ('branch_id', '=', self.branch_id.id)]
        sale_order_domain = [('branch_id', '=', self.branch_id.id)]
        if self.start_date and self.end_date:
            sale_order_domain.append(('create_date', '>=', self.start_date))
            sale_order_domain.append(('create_date', '<=', self.end_date))
        elif self.start_date:
            sale_order_domain.append(('create_date', '>=', self.start_date))
        elif self.end_date:
            sale_order_domain.append(('create_date', '<=', self.end_date))
        sup_disc_approvals = self.env['supplier.discount.approval'].search(sale_order_domain)
        if not sup_disc_approvals:
            raise UserError(_("No SDA Records were created during this timeframe."))

        ''' All style font and value '''
        bold = xlwt.easyxf('alignment: horiz centre; font: bold on, height 200; borders: left thin, top thin, bottom thin, right thin')
        style1 = xlwt.XFStyle()
        style1.num_format_str = 'MM-DD-YYYY'

        date_style = xlwt.easyxf('alignment: horiz centre;', num_format_str='MM-DD-YYYY')
        font = xlwt.XFStyle()
        font.num_format_str = '$#,##0.00'

        percent_font = xlwt.XFStyle()
        percent_font.num_format_str = '#,##0.0000'

        normal_string = xlwt.easyxf('alignment: horiz left;')
        srno_string = xlwt.easyxf('alignment: horiz right;')

        ''' Create workbook and sheet '''
        workbook = xlwt.Workbook()
        sheet = workbook.add_sheet('Discount Report')

        ''' Increase Columns width '''
        sheet.col(0).width = 256 * 15  # Increase width of col-A
        sheet.col(1).width = 256 * 15  # Increase width of col-B
        sheet.col(2).width = 256 * 15  # Increase width of col-C
        sheet.col(3).width = 256 * 30  # Increase width of col-D
        sheet.col(4).width = 256 * 30  # Increase width of col-E
        sheet.col(5).width = 256 * 30  # Increase width of col-F
        sheet.col(6).width = 256 * 15  # Increase width of col-G
        sheet.col(7).width = 256 * 20  # Increase width of col-H
        sheet.col(8).width = 256 * 20  # Increase width of col-I
        sheet.col(9).width = 256 * 20  # Increase width of col-J
        sheet.col(10).width = 256 * 20  # Increase width of col-K
        sheet.col(11).width = 256 * 30  # Increase width of col-L
        sheet.col(12).width = 256 * 30  # Increase width of col-M
        sheet.col(13).width = 256 * 25  # Increase width of col-N
        sheet.col(14).width = 256 * 25  # Increase width of col-O
        sheet.col(15).width = 256 * 25  # Increase width of col-P
        sheet.col(16).width = 256 * 25  # Increase width of col-Q
        sheet.col(17).width = 256 * 25  # Increase width of col-R
        sheet.col(18).width = 256 * 25  # Increase width of col-S
        sheet.col(19).width = 256 * 30  # Increase width of col-T
        sheet.col(20).width = 256 * 20  # Increase width of col-U
        sheet.col(21).width = 256 * 20  # Increase width of col-V
        sheet.col(22).width = 256 * 20  # Increase width of col-W
        sheet.col(23).width = 256 * 20  # Increase width of col-X
        sheet.col(24).width = 256 * 20  # Increase width of col-Y
        sheet.col(25).width = 256 * 25  # Increase width of col-Z
        sheet.col(26).width = 256 * 25  # Increase width of col-AA
        sheet.col(27).width = 256 * 20  # Increase width of col-AB
        sheet.col(28).width = 256 * 20  # Increase width of col-AC
        sheet.col(29).width = 256 * 25  # Increase width of col-AD
        sheet.col(30).width = 256 * 30  # Increase width of col-AE

        ''' All the columns headers '''
        Hedaer_Text = str(self.branch_id.name) + ' SUMMARY OF ALL DISCOUNTS'
        sheet.write_merge(0, 1, 0, 6, Hedaer_Text, bold,)
        if self.start_date:
            sheet.write_merge(2, 2, 0, 0, 'START DATE', bold,)
            sheet.write_merge(2, 2, 1, 1, self.start_date, date_style,)
        if self.end_date:
            sheet.write_merge(3, 3, 0, 0, 'END DATE', bold,)
            sheet.write_merge(3, 3, 1, 1, self.end_date, date_style,)
        sheet.write_merge(2, 2, 5, 5, 'DATE', bold,)
        sheet.write_merge(2, 2, 6, 6, date_now, date_style,)
        sheet.write_merge(5, 6, 0, 0, 'SR.NO.', bold,)
        sheet.write_merge(5, 6, 1, 1, 'DATE', bold,)
        sheet.write_merge(5, 6, 2, 2, 'ORDER', bold,)
        sheet.write_merge(5, 6, 3, 3, 'CUSTOMER', bold, )
        sheet.write_merge(5, 6, 4, 4, 'SUPPLIER', bold,)
        sheet.write_merge(5, 6, 5, 5, 'SALESPERSON', bold, )
        sheet.write_merge(5, 6, 6, 6, 'SDA', bold, )
        sheet.write_merge(5, 6, 7, 7, 'ORIGINAL TOTAL', bold, )
        sheet.write_merge(5, 6, 8, 8, 'TOTAL DISCOUNT', bold, )
        sheet.write_merge(5, 6, 9, 9, 'REDUCE TOTAL', bold, )
        sheet.write_merge(5, 6, 10, 10, 'COMMISSION RATE', bold, )
        sheet.write_merge(5, 6, 11, 11, 'DISCOUNT TYPE', bold,)
        sheet.write_merge(5, 6, 12, 12, 'DISCOUNT REASON', bold,)
        sheet.write_merge(5, 6, 13, 13, 'FULL KF COMMISSION', bold, )
        sheet.write_merge(5, 6, 14, 14, 'ADJ KF COMMISSION', bold, )
        sheet.write_merge(5, 6, 15, 15, 'ADJ KF COMMISSION(%)', bold, )
        sheet.write_merge(5, 6, 16, 16, 'SDA DEDUCTION', bold, )
        sheet.write_merge(5, 6, 17, 17, 'BOOKED SALE DEDUCTION', bold, )
        sheet.write_merge(5, 6, 18, 18, 'ADJ BOOKED SALE', bold, )
        sheet.write_merge(5, 6, 19, 19, 'NOTES', bold,)
        sheet.write_merge(5, 6, 20, 20, 'LAYERED REF', bold, )
        sheet.write_merge(5, 6, 21, 21, 'LAYERED DATE', bold, )
        sheet.write_merge(5, 6, 22, 22, 'PARENT LAYERED', bold, )
        sheet.write_merge(5, 6, 23, 23, 'PARENT SDA', bold, )
        sheet.write_merge(5, 6, 24, 24, 'ADJ BOOKED SALE', bold, )
        sheet.write_merge(5, 6, 25, 25, 'ADJ KF COMMISSION', bold, )
        sheet.write_merge(5, 6, 26, 26, 'ADJ KF COMMISSION-2', bold, )
        sheet.write_merge(5, 6, 27, 27, 'SDA DEDUCTION-2', bold, )
        sheet.write_merge(5, 6, 28, 28, 'ADJ BOOKED SALE-2', bold, )
        sheet.write_merge(5, 6, 29, 29, 'BOOKED SALE DEDUCTION-2', bold, )
        sheet.write_merge(5, 6, 30, 30, 'LAYERED DISCOUNT REASON', bold, )

        count = 7
        sr_no = 1
        #for order_id in sale_order_ids:
        for sda_rec in sup_disc_approvals:
            #if sda_rec.sp_disc_order_id.state != 'sale':
            #    continue

            sda_layered_bool = False
            sda_disc_notes = ''
            if sda_rec.x_sda_disc_notes:
                sda_disc_notes = sda_rec.x_sda_disc_notes
            discount_type = dict(sda_rec._fields['x_sda_disc_type'].selection).get(sda_rec.x_sda_disc_type)

            sheet.write_merge(count, count, 0, 0, sr_no, srno_string, )
            sheet.write_merge(count, count, 1, 1, sda_rec.sp_disc_order_id.confirmation_date, date_style)
            sheet.write_merge(count, count, 2, 2, sda_rec.sp_disc_order_id.name, normal_string,)
            sheet.write_merge(count, count, 3, 3, sda_rec.sp_disc_order_id.partner_id.name, normal_string,)
            sheet.write_merge(count, count, 4, 4, sda_rec.sda_supplier.name, normal_string, )
            sheet.write_merge(count, count, 5, 5, sda_rec.sp_disc_order_id.user_id.name, normal_string, )
            sheet.write_merge(count, count, 6, 6, sda_rec.name, normal_string, )
            sheet.write_merge(count, count, 7, 7, sda_rec.x_sda_order_line_total, font, )
            sheet.write_merge(count, count, 8, 8, sda_rec.x_sda_discount_total, font, )
            sheet.write_merge(count, count, 9, 9, sda_rec.x_reduced_total, font, )
            sheet.write_merge(count, count, 10, 10, sda_rec.commission_rate, percent_font, )
            sheet.write_merge(count, count, 11, 11, discount_type, normal_string, )
            sheet.write_merge(count, count, 12, 12, sda_rec.x_sda_disc_reason, normal_string, )
            sheet.write_merge(count, count, 13, 13, sda_rec.ac_full_kf_commission, font, )
            sheet.write_merge(count, count, 14, 14, sda_rec.ac_adjust_kf_commission, font, )
            if sda_rec.x_sda_disc_type == 'off_floor_otf':
                sheet.write_merge(count, count, 15, 15, sda_rec.ac_adjust_kf_commis_pert2, percent_font, )
            else:
                sheet.write_merge(count, count, 15, 15, sda_rec.ac_adjust_kf_commis_pert, percent_font, )
            if sda_rec.x_sda_disc_type == 'flat_commission':
                sheet.write_merge(count, count, 16, 16, sda_rec.ac_sda_commission_reduction2, font, )
            else:
                sheet.write_merge(count, count, 16, 16, sda_rec.ac_sda_commission_reduction, font, )

            sheet.write_merge(count, count, 17, 17, sda_rec.ac_booked_sale_deduction, font, )
            sheet.write_merge(count, count, 18, 18, sda_rec.ac_adjusted_book_sale, font, )
            sheet.write_merge(count, count, 19, 19, sda_disc_notes, font, )

            ### calculation for SDA-Layered Discount Amount
            if sda_rec.layered_discount_lines:
                count += 1
                sr_no += 1
                for layered_rec in sda_rec.layered_discount_lines:
                    sheet.write_merge(count, count, 0, 0, sr_no, srno_string, )
                    sheet.write_merge(count, count, 20, 20, layered_rec.name, normal_string, )
                    sheet.write_merge(count, count, 21, 21, layered_rec.layered_commission_date, date_style, )
                    sheet.write_merge(count, count, 22, 22, layered_rec.layered_comm_id.name, normal_string, )
                    sheet.write_merge(count, count, 23, 23, layered_rec.sda_discount_id.name, normal_string, )
                    sheet.write_merge(count, count, 24, 24, layered_rec.xsda_adjs_book_sale_total, font, )
                    sheet.write_merge(count, count, 25, 25, layered_rec.ac_adjust_kf_commission_main, font, )
                    sheet.write_merge(count, count, 26, 26, layered_rec.ac_adjust_kf_commission, font, )
                    sheet.write_merge(count, count, 27, 27, layered_rec.ac_sda_commission_reduction, font, )
                    sheet.write_merge(count, count, 28, 28, layered_rec.ac_adjusted_book_sale, font, )
                    sheet.write_merge(count, count, 29, 29, layered_rec.ac_booked_sale_deduction, font, )
                    sheet.write_merge(count, count, 30, 30, layered_rec.x_sda_disc_reason, normal_string, )

                    sda_layered_bool = True
                    count += 1
                    sr_no += 1

            if not sda_layered_bool:
                sr_no += 1
                count += 1

        stream = io.BytesIO()
        workbook.save(stream)
        out = base64.encodestring(stream.getvalue())
        self.write({'data': out})

        return {
            'type': 'ir.actions.act_url',
            'url': '/download?model=supplier.disc.wiz&field=data&id=%s&filename=discount_report.xls' % (self.id),
            'target': 'new',
        }


class SDAMissingReport(models.Model):
    _name = "sda.missing.report"


    name = fields.Char(string='Name')


    @api.multi
    def get_sda_missing_report(self):
        sale_ids = self.env['sale.order'].search([], limit=1).ids
        return self.env.ref('gt_supplier_disc_aggrement.sda_missing_report_id').report_action(sale_ids)


    def missing_sda(self):
        sale_ids = self.env['sale.order'].search([('x_supp_disc_agree','=',True),('sp_disc_approval_line','=',False)])
        print('sale_id', sale_ids)
        return sale_ids