# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from datetime import datetime
import logging
from pprint import pprint

_logger = logging.getLogger(__name__)


class ReceiveCommissionIncome(models.TransientModel):
    _inherit = "receive.commission.income"


    @api.multi
    def do_payment_receive_commission_income(self):
        """
            This method will do payment journal entries against the commission and maintain data on that linke, sda_id, layered_id, sale_line_id, parent_id etc.
        """
        for comm_line in self.receive_commission_line_ids:
            comm_line_id = self.env['account.move.line'].browse(int(comm_line.og_acc_mov_line_id))
            if not comm_line_id:
                continue
            if comm_line_id.move_id.state == 'draft':
                comm_line_id.move_id.action_post()

            now = datetime.now()
            # total_amount = comm_line.receive_comm_amt
            total_amount = (comm_line_id.receive_comm_amt > 0) and comm_line_id.receive_comm_amt or comm_line_id.total_commission
            if total_amount <= 0:
                continue

            acc_receivable_vals = {
                'name': comm_line_id.sale_order_id.name, # self.partner_id.name,
                'credit': total_amount,
                'debit': 0.0,
                'account_id': self.partner_id.property_account_receivable_id.id,
                'partner_id': self.partner_id.id,
                'pay_reference': self.pay_reference,

                'sale_line_id': comm_line_id.sale_line_id.id,
                'parent_commission_id': comm_line_id.id,
                'sda_id': comm_line_id.sda_id and comm_line_id.sda_id.id or False,
                'layered_sda_id': comm_line_id.layered_sda_id and comm_line_id.layered_sda_id.id or False,

                'branch_id': comm_line_id.branch_id.id,
                'ref': comm_line_id.ref,
                'commission_partner': comm_line_id.commission_partner.id,
                'product_id': comm_line_id.sale_line_id.product_id.id,
                # 'sale_order_id': comm_line_id.sale_order_id.id,
                # 'commission_user': commission_user,
            }
            bank_cash_vals = {
                'name': comm_line_id.sale_order_id.name, # self.partner_id.name,
                'credit': 0.0,
                'debit': total_amount,
                'account_id': self.payment_journal.default_debit_account_id.id,
                'partner_id': self.partner_id.id,
                'pay_reference': self.pay_reference,

                'sale_line_id': comm_line_id.sale_line_id.id,
                'parent_commission_id': comm_line_id.id,
                'sda_id': comm_line_id.sda_id and comm_line_id.sda_id.id or False,
                'layered_sda_id': comm_line_id.layered_sda_id and comm_line_id.layered_sda_id.id or False,

                'branch_id': comm_line_id.branch_id.id,
                'ref': comm_line_id.ref,
                'commission_partner': comm_line_id.commission_partner.id,
                'product_id': comm_line_id.sale_line_id.product_id.id,
                # 'sale_order_id': comm_line_id.sale_order_id.id,
                # 'commission_user': commission_user,
            }
            vals = {
                'journal_id': self.payment_journal.id,
                'date': now.strftime('%Y-%m-%d'),
                'state': 'draft',
                'line_ids': [(0, 0, acc_receivable_vals), (0, 0, bank_cash_vals)],
                'ref': self.pay_reference,
                'commission_partner': comm_line_id.commission_partner.id,
                'partner_id': comm_line_id.commission_partner.id,
                # 'commission_user': commission_user,
            }
            pprint (vals)
            move = self.env['account.move'].create(vals)
            _logger.info(f"Commission payment is created i.e. : {move} for the commission line i.e. {comm_line_id}.")
            move.action_post()
            move.line_ids.write({'paid': True})

            comm_line_id.move_id.line_ids.sudo().write({'paid': True, 'received_income': True})
            comm_line_id.sale_order_id.message_post(body=_(
                f"Income Commission payment({move.name}) is created for the Product commission: {comm_line_id.sale_line_id.product_id.display_name}."))

            if comm_line_id.sda_id:
                comm_line_id.sda_id.write({'commission_status': 'confirmed', 'commission_confirmed_date': now})
            if comm_line_id.layered_sda_id:
                comm_line_id.layered_sda_id.write({'commission_status': 'confirmed', 'commission_confirmed_date': now})

        return True