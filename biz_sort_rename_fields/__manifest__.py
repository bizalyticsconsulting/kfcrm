# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2018 BizAlytics Consulting
# Author :
# www.bizalytics.com
#
##############################################################################

{
    'name': 'Sort & Rename Fields',
    'version': '1.0',
    'author': 'BizAlytics Consulting',
    'description': """Sort and Rename Fields in the system as needed""",
    'category': 'Sale Management',
    'sequence': 1,
    'depends': ['sale', 'purchase', 'base', 'gt_studio_it_import_fields', 'branch'],
    'data': [
            'security/biz_security_groups.xml',
            'views/all_view_changes.xml',
            'report/sale_report.xml'
    ],
}
