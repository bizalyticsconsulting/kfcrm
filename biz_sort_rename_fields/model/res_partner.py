# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)


class Partner(models.Model):
    _inherit = "res.partner"

    comment = fields.Html(string='Notes')

    @api.model
    def _get_default_address_format(self):
        return "%(street)s\n%(street2)s\n%(city)s %(state_code)s %(zip)s %(country_name)s"

    @api.model
    def _get_address_format(self):
        return self._get_default_address_format()

    def _get_addr_str(self, addr_str, address):
        if addr_str:
            addr_str = addr_str + ' | ' + address
        else:
            addr_str = address
        return addr_str

    def _get_address_str(self):
        addr_str = ''
        if self.parent_id:
            addr_str = self._get_addr_str(addr_str, self.parent_id.name)
        if self.street:
            addr_str = self._get_addr_str(addr_str, self.street)
        if self.street2:
            addr_str = self._get_addr_str(addr_str, self.street2)
        if self.city:
            addr_str = self._get_addr_str(addr_str, self.city)
        if self.state_id:
            addr_str = self._get_addr_str(addr_str, self.state_id.name)
        if self.zip:
            addr_str = self._get_addr_str(addr_str, self.zip)
        if self.branch_id:
            addr_str = self._get_addr_str(addr_str, 'SHRM:' + self.branch_id.name)

        return addr_str

    def _get_name(self):
        """ Utility method to allow name_get to be overrided without re-browse the partner """
        partner = self
        name = partner.name or ''

        if partner.company_name or partner.parent_id:
            if not name and partner.type in ['invoice', 'delivery', 'other']:
                name = dict(self.fields_get(['type'])['type']['selection'])[partner.type]
            # if not partner.is_company:
            #     name = self._get_contact_name(partner, name)
        if self._context.get('show_address_only'):
            name = partner._display_address(without_company=True)
        if self._context.get('show_address'):
            addrs_str = partner._get_address_str()
            if addrs_str:
                name = name + ' | ' + addrs_str
            #name = name + "\n" + partner._display_address(without_company=True)
        name = name.replace('\n\n', '\n')
        name = name.replace('\n\n', '\n')
        if self._context.get('address_inline'):
            name = name.replace('\n', ', ')
        if self._context.get('show_email') and partner.email:
            name = "%s %s" % (name, partner.email)
        if self._context.get('html_format'):
            name = name.replace('\n', '<br/>')
        if self._context.get('show_vat') and partner.vat:
            name = "%s ‒ %s" % (name, partner.vat)
        return name

