# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2017 webkul
# Author :
# www.webkul.com
#
##############################################################################

from . import inherited_sale_order
from . import inherited_purchase_order
from . import inherited_ir_attachment
from . import inherited_res_users
from . import inherited_sale_order_line
from . import res_partner
from . import product
