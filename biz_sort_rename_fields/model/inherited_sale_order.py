# See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    _order = 'id desc'
    branch_id = fields.Many2one('res.branch', string="Showroom")
    #tag_ids = fields.Many2many('crm.lead.tag', string="Expected Close")

#    It will set the default payment term to 50%
    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        res = super(SaleOrder, self).onchange_partner_id()
        payment_term = self.env['account.payment.term'].search(
            [('name', '=', '50% Deposit')])
        if self.partner_id:
            if payment_term:
                self.payment_term_id = payment_term[0].id
        return res
