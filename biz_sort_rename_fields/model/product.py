from odoo import api, fields, models, _


class ProductTemplate(models.Model):
    _inherit = "product.template"

    delay = fields.Integer(
        'Delivery Lead Time', default=1, required=True,
        help="Lead time in days between the confirmation of the purchase order and the receipt of the products in your warehouse. Used by the scheduler for automatic computation of the purchase order planning.")
