# See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _

class IRAttachment(models.Model):
    _inherit = 'ir.attachment'

    _order  = 'name asc'

