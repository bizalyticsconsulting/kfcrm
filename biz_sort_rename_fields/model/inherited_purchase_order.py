# See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    _order  = 'name desc, date_order desc'

