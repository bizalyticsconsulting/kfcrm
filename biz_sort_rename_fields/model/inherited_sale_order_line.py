# See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    product_uom_qty = fields.Float(string="QTY")

    @api.model
    def create(self, vals):
        if vals.get('name'):
            vals.update({'name': vals.get('name').replace('\n', '')})
        res = super(SaleOrderLine, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        if vals.get('name'):
            vals.update({'name': vals.get('name').replace('\n', '')})
        res = super(SaleOrderLine, self).write(vals)
        return res
