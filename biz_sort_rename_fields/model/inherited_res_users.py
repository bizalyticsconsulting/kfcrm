# See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _

class InhResUsers(models.Model):
    _inherit = 'res.users'

    branch_ids = fields.Many2many('res.branch',string="Allowed Showrooms")
    branch_id = fields.Many2one('res.branch', string= 'Showroom')
