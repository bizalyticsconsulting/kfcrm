=====================
Instagram Integration
=====================

With this module you can get instagram user account 
full name, profile picture, biography, external URL
followers count, media count and engagement rate

Installation
============

To install this module, you need to:

Download the module and add it to your Odoo addons folder. Afterward, log on to
your Odoo server and go to the Apps menu. Trigger the debug mode and update the
list by clicking on the "Update Apps List" link. Now install the module by
clicking on the install button.

Upgrade
============

To upgrade this module, you need to:

Download the module and add it to your Odoo addons folder. Restart the server
and log on to your Odoo server. Select the Apps menu and upgrade the module by
clicking on the upgrade button.

Configuration
=============

No additional configuration is needed to use this module.

Usage
=============

Go to any contact form and look for the Instagram page. Enter the instagram user in 
the corresponding field. Press the "Get Data" button and wait for the process.

Credits
=======

Contributors
------------

* Jean Paul Casis

Images
------------

Author & Maintainer
-------------------

This module is maintained by Jean Paul Casis