from odoo import api, fields, models, _
from datetime import datetime


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    return_picking_id = fields.Many2one('stock.picking', 'Return Stock Picking')


    def auto_fill_product_qty(self):
        """ Auto-fill done qty before process the validation picking
            Auto-fill the lot/serial number while processing the pickings
            Tracking: None/Serial/Lot.Process according to tracking
        """
        for rec in self:
            for rec_mv in rec.move_ids_without_package:
                if rec_mv.product_id.tracking == 'serial':
                    rec_mv.quantity_done = rec_mv.product_uom_qty
                elif rec_mv.product_id.tracking == 'lot':
                    pass
                else:
                    rec_mv.quantity_done = rec_mv.product_uom_qty
        return True
