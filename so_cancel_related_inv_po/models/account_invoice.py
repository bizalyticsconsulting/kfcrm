from odoo import api, exceptions, fields, models, _


class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    _description = "Invoice"

    state = fields.Selection(selection_add=[('refund_&_cancel', 'Refund & Cancel'),\
    										('outstanding_&_cancel', 'Outstanding & Cancel')])