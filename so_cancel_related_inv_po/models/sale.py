# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.addons.sale_purchase.models.sale_order import SaleOrder as PurchaseSaleOrder


class SaleOrder(models.Model):
    _inherit = "sale.order"


    @api.multi
    def action_cancel(self):
        documents = None
        for sale_order in self:
            if sale_order.state == 'sale' and sale_order.order_line:
                sale_order_lines_quantities = {order_line: (order_line.product_uom_qty, 0) for order_line in sale_order.order_line}
                documents = self.env['stock.picking']._log_activity_get_documents(sale_order_lines_quantities, 'move_ids', 'UP')

        if self.env.context.get('cancel_sale_process'):
            pass
        else:
            self.mapped('picking_ids').action_cancel()
        if documents:
            filtered_documents = {}
            for (parent, responsible), rendering_context in documents.items():
                if parent._name == 'stock.picking':
                    if parent.state == 'cancel':
                        continue
                filtered_documents[(parent, responsible)] = rendering_context
            self._log_decrease_ordered_quantity(filtered_documents, cancel=True)
        return super(PurchaseSaleOrder, self).action_cancel()


    @api.multi  # Returns data
    def ks_confirm_oneclick_sale(self):
        """
        This method is overridden,
            - In case, user is again calling the Confirm button after the 'Cancel Order Process'.
            - Then by pass the 'Cancelled' invoice condition in the sale order.
        """
        self.ks_my_validations_oneaugust("KsSaleOneModel")
        if self.env['ir.config_parameter'].get_param('ks_popup_sale_confirmation'):
            return {
                'name': _('Sale Order Confirmation'),
                'view_mode': 'form',
                'res_model': 'ks.purchase.order.confirmation',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }

        if self.ks_payment_repeat == False and self.state not in ('done', 'cancel'):
            # for loop to run once to generate invoice only
            self.action_confirm()

            ks_check_total_signed = 0
            for ks in self.invoice_ids.filtered(lambda x: x.state != 'cancel'):
                ks_check_total_signed += abs(ks.amount_total_signed)

            if self.invoice_status == 'to invoice' and ks_check_total_signed < self.amount_total:
                ks_sale_inv_id = self.env['sale.advance.payment.inv'].create(
                    {'advance_payment_method': 'all'})
                ks_sale_inv_id = ks_sale_inv_id.with_context(
                    active_id=self.id, active_ids=self.ids)
                ks_sale_inv_id.create_invoices()
        self._ks_sale_invoice_payment()
        self._ks_sale_payment_amount_update()
        # Will confirm delivery at the end of the sale cycle......
        if self.balance_due == 0:
            self._ks_sale_confirm_delivery()
