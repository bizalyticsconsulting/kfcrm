from odoo import api, fields, models, _
from datetime import datetime

class CancelOrder(models.TransientModel):
    _name = 'cancel.order'
    _description = 'Cancel Related Orders'

    name = fields.Char('Sale Order')
    quotation = fields.Boolean('Sales Order --> Quotation')
    cancel_customer_payments = fields.Boolean('Cancel Payments / Invoices')
    customer_refund_outstanding = fields.Selection([('refund', 'Refund && Cancel'),('outstanding', 'Outstanding && Cancel')],string='Refund / Outstanding')
    cancel_outgoing_shipments = fields.Boolean('Cancel Outgoing Shipment')
    do_nothing_stock_back = fields.Selection([('do_nothing', 'Do Nothing'),('stock_back', 'Stock Back')])
    cancel_purchase_order = fields.Boolean('Cancel Purchase Orders')
    cancel_vendor_payments = fields.Boolean('Cancel Payments / Invoices')
    vendor_refund_outstanding = fields.Selection([('refund', 'Refund && Cancel'),('outstanding', 'Outstanding && Cancel')])
    cancel_incoming_shipments = fields.Boolean('Cancel Incoming Shipments')
    incoming_do_nothing_stock_back = fields.Selection([('in_do_nothing', 'Do Nothing'),('in_stock_back', 'Stock Back')])
    cancel_mrp_order = fields.Boolean('Cancel Manufacturing Order(s)')
    cancel_workorder = fields.Boolean('Cancel Work Orders')
    cancel_revert_commissions = fields.Boolean('Cancel / Revert any commissions')


    @api.multi
    def action_cancel_order_process(self):
        """
            This method will Cancel Sale Order, Payments, Invoices, Outgoing Process.
        """
        StockReturnPicking = self.env['stock.return.picking']
        SaleOrder = self.env['sale.order']
        active_sale_order_id = SaleOrder.browse(self.env.context.get('active_id'))
        ctx = self.env.context.copy()
        ctx.update({'cancel_sale_process': True})
        if self.env.context.get('active_model') == 'sale.order' and active_sale_order_id:
            ### Incase of SaleOrder state in ['draft', 'sent', 'cancel']
            if active_sale_order_id.state in ['draft', 'sent']:
                active_sale_order_id.action_cancel()
                return True
            elif active_sale_order_id.state in ['cancel']:
                return True

            ### Incase of SaleOrder state in ['sale', 'done']
            # Cancel Payments and Invoices respectively
            if len(active_sale_order_id.invoice_ids) > 0:
                paid_invoice_ids = active_sale_order_id.invoice_ids.filtered(lambda x: x.state == 'paid')
                open_invoice_ids = active_sale_order_id.invoice_ids.filtered(lambda x: x.state == 'open')
                draft_invoice_ids = active_sale_order_id.invoice_ids.filtered(lambda x: x.state == 'draft')
                if len(draft_invoice_ids) > 0:
                    draft_invoice_ids.action_invoice_cancel()
                if len(open_invoice_ids) > 0:
                        open_invoice_ids.action_invoice_cancel()
                if len(paid_invoice_ids) > 0:
                    if len(paid_invoice_ids.mapped('payment_ids')) > 0:
                        paid_invoice_ids.mapped('payment_ids').cancel()
                    paid_invoice_ids.action_invoice_cancel()

                ## If any Invoice then check for payment. if payment registered then unconciled it....Only For Refer
                # for inv in active_sale_order_id.invoice_ids.filtered(lambda x: x.state != 'cancel'):
                #     ctx.update({'invoice_id': inv.id})
                #     for pay_move_line in inv.payment_move_line_ids:
                #         pay_move_line.with_context(ctx).remove_move_reconcile()
                #     inv.action_invoice_cancel()  # Cancel Invoice(open State)

            # Cancel Stock.Pickings i.e. Delivery Orders
            if len(active_sale_order_id.picking_ids) > 0:
                not_validated_picking_ids = active_sale_order_id.picking_ids.filtered(lambda x: x.picking_type_id and x.picking_type_id.code == 'outgoing' and x.state not in ['done', 'cancel'])
                validated_picking_ids = active_sale_order_id.picking_ids.filtered(lambda x: x.picking_type_id and x.picking_type_id.code == 'outgoing' and x.state in ['done'])
                if len(not_validated_picking_ids):
                    not_validated_picking_ids.action_cancel()
                if len(validated_picking_ids) > 0:
                    for pick in validated_picking_ids:
                        ctx.update({ 'active_model': 'stock.picking',
                                     'active_id': pick.id,
                                     'active_ids': pick.ids,
                                     'contact_display': 'partner_address',
                                })

                        stk_return_fields = ['move_dest_exists', 'product_return_moves', 'parent_location_id', 'original_location_id', 'location_id']
                        stk_return_values = StockReturnPicking.with_context(ctx).default_get(stk_return_fields)
                        stk_return_picking_id = StockReturnPicking.with_context(ctx).create(stk_return_values)
                        if len(stk_return_picking_id) > 0:
                            new_return_picking_values = stk_return_picking_id.with_context(ctx).create_returns()
                            if new_return_picking_values and new_return_picking_values.get('res_id'):
                                new_return_picking_id = self.env['stock.picking'].browse(new_return_picking_values.get('res_id'))
                                if new_return_picking_id:
                                    new_return_picking_id.auto_fill_product_qty()
                                    new_return_picking_id.button_validate()

                                    # update reverse picking in current picking(So that we can get reverse entry against that)
                                    pick.return_picking_id = new_return_picking_id.id

            # Cancel Commission entries and its payment entries too (if any)
            # if len(active_sale_order_id.account_move_line_ids) > 0:
            #     print ("account move lines==============================", active_sale_order_id.account_move_line_ids)
            #     commission_move_ids = active_sale_order_id.account_move_line_ids.mapped('move_id').filtered(lambda x: x.state == 'posted')
            #     paid_commission_line_ids = self.env['account.move.line'].search([('parent_commission_id', 'in', list(set(active_sale_order_id.account_move_line_ids.ids)))])
            #     print ("paid commission lines ids=========================", paid_commission_line_ids)
            #     paid_commission_move_ids = paid_commission_line_ids.mapped('move_id').filtered(lambda x: x.state == 'posted')
            #     print ("commission move ids------------------------", commission_move_ids, paid_commission_move_ids)

            if active_sale_order_id.state in ['sale', 'done']:
                active_sale_order_id.ks_payment_repeat = False  # ks_order_confirm module

                active_sale_order_id.action_draft()
                active_sale_order_id.with_context(ctx).action_cancel()
        # print (erorrrrrrrrrrrrrrrrrrrrrr)
        return True




    @api.multi
    def cancel_order_process(self):
        active_recordset = self.env['sale.order'].browse(self._context.get('active_id'))
        uid = self.env.user.id
        today = datetime.today().date()
        physical_location_id = self.env['stock.location'].search([('name', '=', 'Physical Locations'),('usage', '=', 'view')]).id
        wh_location_id = self.env['stock.location'].search([('name', '=', 'WH'), ('location_id', '=', physical_location_id)]).id
        wh_stock_location_id = self.env['stock.location'].search([('name', '=', 'Stock'), ('location_id', '=', wh_location_id)]).id
        currency_id = active_recordset.pricelist_id.currency_id.id
        partner_id = active_recordset.partner_id.id
        payment_method = self.env['account.payment.method'].search([('name', '=', 'Manual')], limit=1).id
        bank_journal = self.env['account.journal'].search([('type', '=', 'bank')],limit=1).id
        po_obj = self.env['purchase.order']

        # if self.quotation == True:
        #     active_recordset.action_cancel()
        # # Cancel Outgoing Shipment
        # if self.cancel_outgoing_shipments == True:
        #     customer_location_id = self.env['stock.location'].search([('usage', '=', 'customer')], limit=1).id
        #     print ('\n\n_________customer_location_id_______',customer_location_id)
        #     if self.do_nothing_stock_back == 'do_nothing':
        #         pass
        #     if self.do_nothing_stock_back == 'stock_back':
        #         done_stock_picking_ids = self.env['stock.picking'].search([('origin', '=', active_recordset.name),
        #                                                                   ('state', 'in', ['done']),
        #                                                                   ('location_dest_id', '=', customer_location_id)])
        #         not_done_stock_picking_ids = self.env['stock.picking'].search([('origin', '=', active_recordset.name),
        #                                                                   ('state', 'not in', ['done']),
        #                                                                   ('location_dest_id', '=', customer_location_id)])
        #         if not_done_stock_picking_ids:
        #             for pick_id in not_done_stock_picking_ids:
        #                 pick_id.action_cancel()
        #         if done_stock_picking_ids:
        #             for pick_id in done_stock_picking_ids:
        #                 ctx = {
        #                         # 'from_sale_order': True,
        #                         'active_model': 'stock.picking',
        #                         'params': {'model': 'sale.order',
        #                                    'id': active_recordset.id,
        #                                    },
        #                         'active_id': pick_id.id,
        #                         'contact_display': 'partner_address',
        #                         'uid': uid
        #                     }
        #                 return_pick_dict = {
        #                     'parent_location_id': wh_location_id,
        #                     'location_id': wh_stock_location_id,
        #                     'original_location_id': wh_stock_location_id
        #                 }
        #                 return_pick_wizard_id = self.env['stock.return.picking'].with_context(ctx).create(return_pick_dict)
        #                 serial_no = []
        #                 for pack_id in pick_id.move_ids_without_package:
        #                     product_uom =  pack_id.product_id.uom_id.id
        #                     move_id_dict = {'product_id': pack_id.product_id.id,
        #                                     'product_uom_qty': pack_id.quantity_done,
        #                                     'product_uom': product_uom,
        #                                     'name': pack_id.product_id.name,
        #                                     'date': today,
        #                                     'location_id': wh_stock_location_id,
        #                                     'location_dest_id': customer_location_id,
        #                                     # 'picking_id': pick_id.id,
        #                                     'procure_method': 'make_to_stock',
        #                                     'partner_id': partner_id,
        #                                     # 'weight': pick_id.weight,
        #                                     # 'weight_uom_id': pick_id.weight_uom_id.id,
        #                                     # 'quant_ids': [(4,quant_id.id, 0) for quant_id in pack_id.pack_lot_ids]
        #                                     }
        #                     move_id = self.env['stock.move'].create(move_id_dict)
        #                     for move_line_id in pack_id.move_line_ids:
        #                         serial_no.append((pack_id.product_id.id, move_line_id.lot_id.name, move_line_id.lot_id.id, move_line_id.qty_done))
        #                         lot_id = self.env['stock.production.lot'].search([('name', '=', move_line_id.lot_id.name)]).id
        #                         move_line_id = self.env['stock.move.line'].create({'date': today,
        #                                                              'product_id': pack_id.product_id.id,
        #                                                              'location_id': wh_stock_location_id,
        #                                                              'location_dest_id': customer_location_id,
        #                                                              'lot_id': lot_id,
        #                                                              'product_uom_id': product_uom,
        #                                                              'move_id': move_id.id
        #                                                             })
        #                     return_pick_wizard_id.product_return_moves.mapped('move_id')
        #                 return_pick_id = return_pick_wizard_id.create_returns()
        #                 return_pick_id = return_pick_id['res_id']
        #                 return_pick_id = self.env['stock.picking'].browse(return_pick_id)
        #                 for pack_id in return_pick_id.move_ids_without_package:
        #                     exact_serial_no = []
        #                     for index, i in enumerate(serial_no):
        #                         if i[0] == pack_id.product_id.id:
        #                             exact_serial_no.append((index, i[0], i[1], i[2], i[3]))
        #                     for lot_id in pack_id.move_line_ids:
        #                         if not lot_id.lot_id:
        #                             lot_id.lot_id = exact_serial_no[0][3]
        #                             lot_id.qty_done = exact_serial_no[0][4]
        #                             exact_serial_no.pop(0)
        #                 return_pick_id.button_validate()
        # Cancel customer payment
        #if self.cancel_customer_payments == True:
            # if self.customer_refund_outstanding == 'refund':
            #     print ('\n\n________ customer Payment refund __________')
            #     paid_inv_ids = self.env['account.invoice'].search([('origin', '=', active_recordset.name),
            #                                                        ('state', '=', 'paid')])
            #     open_inv_ids = self.env['account.invoice'].search([('origin', '=', active_recordset.name),
            #                                                        ('state', '=', 'open')])
            #     draft_inv_ids = self.env['account.invoice'].search([('origin', '=', active_recordset.name),
            #                                                         ('state', '=', 'draft')])
            #     #if draft_inv_ids:
            #         #for inv_id in draft_inv_ids:
            #             #inv_id.action_invoice_cancel()
            #             #inv_id.state = 'refund_&_cancel'
            #     if open_inv_ids:
            #         for inv_id in open_inv_ids:
            #             journal_id = self.env['account.journal'].search([('name', '=', inv_id.journal_id.name)])
            #             if journal_id:
            #                 journal_id.update_posted = True
            #             journal_entries_ids = self.env['account.move'].search([('ref', '=', inv_id.reference),
            #                                             ('journal_id', '=', bank_journal)])
            #             for journal_entries_id in journal_entries_ids:
            #                 reverse_moves_ctx = {
            #                     'active_ids': journal_entries_id.id,
            #                     'active_model': 'account.move',
            #                 }
            #                 account_move_reversal_wizard_id = self.env["account.move.reversal"].with_context(reverse_moves_ctx).\
            #                                                     create({'date': today})
            #                 account_move_reversal_wizard_id.reverse_moves()
            #             #if inv_id.amount_total == inv_id.residual:
            #                 #inv_id.action_invoice_cancel()
            #                 #inv_id.state = 'refund_&_cancel'
            #     if paid_inv_ids:
            #         for inv_id in paid_inv_ids:
            #             journal_id = self.env['account.journal'].search([('name', '=', inv_id.journal_id.name)])
            #             if journal_id:
            #                 journal_id.update_posted = True
            #             journal_entries_ids = self.env['account.move'].search([('ref', '=', inv_id.reference),
            #                                             ('journal_id', '=', bank_journal)])
            #             for journal_entries_id in journal_entries_ids:
            #                 reverse_moves_ctx = {
            #                     'active_ids': journal_entries_id.id,
            #                     'active_model': 'account.move',
            #                 }
            #                 account_move_reversal_wizard_id = self.env["account.move.reversal"].with_context(reverse_moves_ctx).\
            #                                                     create({'date': today})
            #                 account_move_reversal_wizard_id.reverse_moves()
            #             #inv_id.action_invoice_cancel()
            #             #inv_id.state = 'refund_&_cancel'
            #
        #    if self.customer_refund_outstanding == 'outstanding':
        print ('\n\n________ customer Payment Outstanding __________')
        paid_inv_ids = self.env['account.invoice'].search([('origin', '=', active_recordset.name),
                                                           ('state', '=', 'paid')])
        open_inv_ids = self.env['account.invoice'].search([('origin', '=', active_recordset.name),
                                                           ('state', '=', 'open')])
        draft_inv_ids = self.env['account.invoice'].search([('origin', '=', active_recordset.name),
                                                            ('state', '=', 'draft')])
        if draft_inv_ids:
            for inv_id in draft_inv_ids:
                journal_id = self.env['account.journal'].search([('name', '=', inv_id.journal_id.name)])
                if journal_id:
                    journal_id.update_posted = True
                #inv_id.action_invoice_cancel()
                #inv_id.state = 'cancel'
        if open_inv_ids:
            for inv_id in open_inv_ids:
                journal_id = self.env['account.journal'].search([('name', '=', inv_id.journal_id.name)])
                if journal_id:
                    journal_id.update_posted = True
                inv_id.action_invoice_cancel()
                inv_id.action_invoice_draft()
                inv_id.action_invoice_open()
                #inv_id.state = 'cancel'
        if paid_inv_ids:
            # when fully paid invoice is cancelled than that amount goes at outstanding bal.
            for inv_id in paid_inv_ids:
                journal_id = self.env['account.journal'].search([('name', '=', inv_id.journal_id.name)])
                if journal_id:
                    journal_id.update_posted = True
                inv_id.action_invoice_cancel()
                inv_id.action_invoice_draft()
                inv_id.action_invoice_open()
                #inv_id.state = 'cancel'
        # # Cancel Purchase Order
        # if self.cancel_purchase_order == True:
        #     print ('\n\n____cancel_purchase_order________',self, active_recordset)
        #     draft_po_ids = po_obj.search([('state', 'in', ['draft', 'purchase', 'sent', 'draft']),('origin', '=', active_recordset.name)])
        #     for po_id in draft_po_ids:
        #         po_id.button_cancel()
        # # Incoming Shipment
        # if self.cancel_incoming_shipments == True:
        #     supplier_location_id = self.env['stock.location'].search([('usage', '=', 'supplier')],limit=1).id
        #     draft_po_ids = po_obj.search([('origin', '=', active_recordset.name),\
        #                                   ('state', 'in', ['draft', 'sent'])])
        #     done_po_ids = po_obj.search([('origin', '=', active_recordset.name), \
        #                                  ('state', 'in', ['purchase', 'done'])])
        #     if self.incoming_do_nothing_stock_back == 'in_do_nothing':
        #         pass
        #     if self.incoming_do_nothing_stock_back == 'in_stock_back':
        #         if done_po_ids:
        #             for po in done_po_ids:
        #                 done_stock_picking_ids = self.env['stock.picking'].search([('origin', '=', po.name),
        #                                                                   ('state', 'in', ['done']),
        #                                                                   ('location_id', '=', supplier_location_id)])
        #                 not_done_stock_picking_ids = self.env['stock.picking'].search([('origin', '=', po.name),
        #                                                                   ('state', 'not in', ['done']),
        #                                                                   ('location_id', '=', supplier_location_id)])
        #                 if not_done_stock_picking_ids:
        #                     for pick_id in not_done_stock_picking_ids:
        #                         pick_id.action_cancel()
        #                 if done_stock_picking_ids:
        #                     for pick_id in done_stock_picking_ids:
        #                         ctx = {
        #                                 'active_model': 'stock.picking',
        #                                 'active_id': pick_id.id,
        #                             }
        #                         return_pick_dict = {
        #                             'location_id': supplier_location_id,
        #                             'original_location_id': supplier_location_id
        #                         }
        #                         return_pick_wizard_id = self.env['stock.return.picking'].with_context(ctx).create(return_pick_dict)
        #                         serial_no = []
        #                         for pack_id in pick_id.move_ids_without_package:
        #                             print ('\n\n_______pack_id.product_uom_qty__________',pack_id.product_uom_qty)
        #                             product_uom =  pack_id.product_id.uom_po_id.id
        #                             move_id_dict = {'product_id': pack_id.product_id.id,
        #                                             'product_uom_qty': pack_id.product_uom_qty,
        #                                             'product_uom': product_uom,
        #                                             'name': pack_id.product_id.name,
        #                                             'date': today,
        #                                             'location_id': wh_stock_location_id,
        #                                             'location_dest_id': supplier_location_id,
        #                                             # 'picking_id': pick_id.id,
        #                                             'procure_method': 'make_to_stock',
        #                                             'picking_partner_id': po.partner_id.id,
        #                                             # 'product_qty': pack_id.product_uom_qty,
        #                                             # 'weight': pick_id.weight,
        #                                             # 'weight_uom_id': pick_id.weight_uom_id.id,
        #                                             }
        #                             move_id = self.env['stock.move'].create(move_id_dict)
        #                             for move_line_id in pack_id.move_line_ids:
        #                                 serial_no.append((pack_id.product_id.id, move_line_id.lot_id.name, move_line_id.lot_id.id, move_line_id.qty_done))
        #                                 lot_id = self.env['stock.production.lot'].search([('name', '=', move_line_id.lot_id.name)]).id
        #                                 move_line_id = self.env['stock.move.line'].create({'date': today,
        #                                                                      'product_id': pack_id.product_id.id,
        #                                                                      'location_id': wh_stock_location_id,
        #                                                                      'location_dest_id': supplier_location_id,
        #                                                                      'lot_id': lot_id,
        #                                                                      'product_uom_id': product_uom,
        #                                                                      'move_id': move_id.id,
        #                                                                      # 'product_qty': pack_id.product_uom_qty,
        #                                                                      # 'product_uom_qty': pack_id.product_uom_qty
        #                                                                     })
        #                             return_pick_wizard_id.product_return_moves.mapped('move_id')
        #                             for line in return_pick_wizard_id.product_return_moves:
        #                                 if line.product_id.id == pack_id.product_id.id:
        #                                     line.quantity = pack_id.product_uom_qty
        #                         return_pick_id = return_pick_wizard_id.create_returns()
        #                         return_pick_id = return_pick_id['res_id']
        #                         return_pick_id = self.env['stock.picking'].browse(return_pick_id)
        #                         for pack_id in return_pick_id.move_ids_without_package:
        #                             exact_serial_no = []
        #                             for index, i in enumerate(serial_no):
        #                                 if i[0] == pack_id.product_id.id:
        #                                     exact_serial_no.append((index, i[0], i[1], i[2], i[3]))
        #                             for lot_id in pack_id.move_line_ids:
        #                                 if not lot_id.lot_id:
        #                                     lot_id.lot_id = exact_serial_no[0][3]
        #                                     lot_id.qty_done = exact_serial_no[0][4]
        #                                     exact_serial_no.pop(0)
        #                             print ('\n\n_____________return pick id__________',return_pick_id)
        #                         apply_button = self.env['stock.immediate.transfer'].create({'pick_ids':[(4, return_pick_id.id)]})
        #                         print ('_________apply_button________',apply_button)
        #                         apply_button.process()
        #                         # return_pick_id.button_validate()
        # # Supplier Payment
        # if self.cancel_vendor_payments == True:
        #     po_ids = po_obj.search([('origin', '=', active_recordset.name)])
        #     if self.vendor_refund_outstanding == 'refund':
        #         print ('\n\n________ Supplier Payment refund __________')
        #         for po_id in po_ids:
        #             print ('________po_id_________',po_id)
        #             draft_inv_ids = self.env['account.invoice'].search([('origin', '=', po_id.name),
        #                                                                ('state', '=', 'draft')])
        #             paid_inv_ids = self.env['account.invoice'].search([('origin', '=', po_id.name),
        #                                                                ('state', '=', 'paid')])
        #             print ('______paid_inv_ids_______',paid_inv_ids)
        #             open_inv_ids = self.env['account.invoice'].search([('origin', '=', po_id.name),
        #                                                                ('state', '=', 'open')])
        #             print ('______open_inv_ids_______',open_inv_ids)
        #             if draft_inv_ids:
        #                 for inv_id in draft_inv_ids:
        #                     inv_id.action_invoice_cancel()
        #                     inv_id.state = 'refund_&_cancel'
        #
        #             if open_inv_ids:
        #                 for inv_id in open_inv_ids:
        #                     journal_id = self.env['account.journal'].search([('name', '=', inv_id.journal_id.name)])
        #                     if journal_id:
        #                         journal_id.update_posted = True
        #                     journal_entries_ids = self.env['account.move'].search([('ref', '=', inv_id.number),
        #                                                                     ('journal_id', '=', bank_journal)])
        #                     for journal_entries_id in journal_entries_ids:
        #                         reverse_moves_ctx = {
        #                             'active_ids': journal_entries_id.id,
        #                             'active_model': 'account.move',
        #                         }
        #                         account_move_reversal_wizard_id = self.env["account.move.reversal"]\
        #                                                     .with_context(reverse_moves_ctx).create({'date': today})
        #                         account_move_reversal_wizard_id.reverse_moves()
        #                     inv_id.action_invoice_cancel()
        #                     inv_id.state = 'refund_&_cancel'
        #
        #             if paid_inv_ids:
        #                 for inv_id in paid_inv_ids:
        #                     journal_id = self.env['account.journal'].search([('name', '=', inv_id.journal_id.name)])
        #                     if journal_id:
        #                         journal_id.update_posted = True
        #                     journal_entries_ids = self.env['account.move'].search([('ref', '=', inv_id.number),
        #                                                     ('journal_id', '=', bank_journal)])
        #                     for journal_entries_id in journal_entries_ids:
        #                         reverse_moves_ctx = {
        #                             'active_ids': journal_entries_id.id,
        #                             'active_model': 'account.move',
        #                         }
        #                         account_move_reversal_wizard_id = self.env["account.move.reversal"]\
        #                                                     .with_context(reverse_moves_ctx).create({'date': today})
        #                         account_move_reversal_wizard_id.reverse_moves()
        #                     inv_id.action_invoice_cancel()
        #                     inv_id.state = 'refund_&_cancel'
        #     if self.vendor_refund_outstanding == 'outstanding':
        #         print ('\n\n________ vendor Payment Outstanding __________')
        #         po_ids = po_obj.search([('origin', '=', active_recordset.name)])
        #         print ('____________po_ids_________',po_ids)
        #         for po_id in po_ids:
        #             draft_inv_ids = self.env['account.invoice'].search([('origin', '=', po_id.name),
        #                                                                ('state', '=', 'draft')])
        #             paid_inv_ids = self.env['account.invoice'].search([('origin', '=', po_id.name),
        #                                                                ('state', '=', 'paid')])
        #             open_inv_ids = self.env['account.invoice'].search([('origin', '=', po_id.name),
        #                                                                ('state', '=', 'open')])
        #             if draft_inv_ids:
        #                 for inv_id in draft_inv_ids:
        #                     inv_id.action_invoice_cancel()
        #                     inv_id.state = 'cancel'
        #             if open_inv_ids:
        #                 for inv_id in open_inv_ids:
        #                     journal_id = self.env['account.journal'].search([('name', '=', inv_id.journal_id.name)])
        #                     if journal_id:
        #                         journal_id.update_posted = True
        #                     inv_id.action_invoice_cancel()
        #                     inv_id.state = 'cancel'
        #             if paid_inv_ids:
        #                 # when fully paid invoice is cancelled than that amount goes at outstanding bal.
        #                 for inv_id in paid_inv_ids:
        #                     journal_id = self.env['account.journal'].search([('name', '=', inv_id.journal_id.name)])
        #                     if journal_id:
        #                         journal_id.update_posted = True
        #                     inv_id.action_invoice_cancel()
        #                     inv_id.state = 'cancel'