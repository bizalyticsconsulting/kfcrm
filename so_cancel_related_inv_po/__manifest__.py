# -*- coding: utf-8 -*-
{
    'name': "Sale Order Cancel related Invoice's and Purchase orders",
    'summary': """ Sale Order Cancel related Invoice's and Purchase orders """,
    'author': "Globalteckz",
    'website': "https://www.globalteckz.com/",
    'category': 'Sale',
    'version': '1.1',
    'license': 'AGPL-3',
    'description': """
        In Journals tick 'Allow Cancelling Entries'.
        For Finance User tick Show Full Accounting Features.

""",
    'depends': [
        'sale_management','account','account_cancel','purchase','stock',
    ],
    'data': [
        'wizard/cancel_order_view.xml',
        'views/sale_view.xml',
        'views/stock_picking_view.xml',
        'security/so_security.xml',
    ],
    'installable': True,
}
