# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import odoo
from odoo import fields, models, tools, api, _
import base64
import urllib.request, urllib.error
import requests
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import logging
import pandas as pd
import mimetypes
import threading
import concurrent.futures
from threading import Thread

from pprint import pprint
# try:
#     import queue
# except ImportError:
#     import Queue as queue

_logger = logging.getLogger(__name__)
excel_csv_format = [
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
]


class ResImportConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    num_import_threads = fields.Integer("Product Import Threads", default=10, help="Number of import threads")

    @api.model
    def get_values(self):
        res = super(ResImportConfigSettings, self).get_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        res.update(
            num_import_threads=int(ICPSudo.get_param('num_import_threads')),
        )
        return res

    @api.multi
    def set_values(self):
        super(ResImportConfigSettings, self).set_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        ICPSudo.set_param("num_import_threads", self.num_import_threads)


class VendorProductImportWizard(models.Model):
    _inherit = 'res.partner'

    x_import_mapping = fields.Boolean(string="Import Mapping Available", help="Import Mapping available")


class ProductImportWizard(models.TransientModel):
    _name = 'product.import.wizard'
    _description = 'Product Import Wizard'

    data_file               = fields.Binary('Upload Excel File', required=True, attachment=False)
    filename                = fields.Char('File Name', required=True)
    vendor_id               = fields.Many2one('res.partner', string='Supplier', required=False, domain=[('is_company', '=', True), ('supplier', '=', True)])
    archive_product         = fields.Boolean(string='Archive Current Products')
    parallel_upload_thread  = fields.Boolean("Parallel Upload?")
    skip_images             = fields.Boolean("Skip Image Upload / Download?")

    @api.multi
    def import_product_order(self):
        ctx = self.env.context.copy()
        log_obj = self.env['log.management']
        Partner = self.env['res.partner']
        Template = self.env['product.template']
        Category = self.env['product.category']
        Uom = self.env['uom.uom']
        product_fields = Template.fields_get()

        # This is use in query method to create the ir_property for standard_price
        product_model_id = self.env['ir.model'].sudo().search([('model', '=', 'product.product')], limit=1)
        standard_field_id = self.env['ir.model.fields'].sudo().search(
            [('name', '=', 'standard_price'), ('model_id', '=', product_model_id.id)], limit=1)
        ctx.update({'standard_field_id': standard_field_id.id})

        # Validate files format...only allow Excel sheet format files
        mimetype = None
        if mimetype is None and self.filename:
            mimetype = mimetypes.guess_type(self.filename)[0]
            if not mimetype in excel_csv_format:
                raise UserError('Please upload only excel sheet (.xls, .xlsx) format files !')
        try:
            excel_file_data = pd.read_excel(io=base64.decodestring(self.data_file), dtype=str)
            mandatory_columns = ['Name', 'Default_Code', 'Category', 'Type', 'Barcode', 'UOM', 'List_Price', 'Net_Price', 'Product_Default_Code', 'Default_Code_Option', 'Display_Name', 'Description', 'Active','Image_URL']
        except Exception as e:
            _logger.exception('File not get imported !')
            raise UserError(
                _('File %r not imported due to a malformed file.'
                  '\n\nTechnical Details:\n%s') % \
                (self.filename, tools.ustr(e))
            )

        if len(excel_file_data.columns) < 14:
            raise UserError(_("Please upload an excel sheet with the minimum 14 mandatory columns !"))
        if any([i for i in mandatory_columns if i not in excel_file_data.columns]):
            raise UserError(_("Please upload an excel sheet with the mandatory columns !\n\n"
                              "Mandatory columns are: \n%s" %(', '.join(map(str, mandatory_columns)))))

        excel_data_df = pd.DataFrame(data=excel_file_data)
        #       Fetch default values for the fields
        pro_def_val = Template.default_get(product_fields)
        new_pro_up = pro_def_val.copy()

        skip_images = self.skip_images
        _logger.debug("Skip Images is %s", skip_images)

        partner_id = self.vendor_id
        _logger.debug("Vendor is %s", partner_id)
        _logger.debug("Vendor Partner Code is %s", partner_id.partner_code)

        _logger.debug("Archive product is %s", self.archive_product)
        if self.archive_product:
            _logger.debug("Will search for all the products and mark them archived ....")
            products = Template.search([('default_code', '=ilike', partner_id.partner_code + "%")])
            _logger.debug("Products searched ..... %s", products)
            products.write({'active': False})

            #for product in products:
            #    _logger.debug("Making product inactive --- %s", product)
            #    product.write({'active': False})
            #    product.env.cr.commit()

        pop_view = self.env.ref('sh_message.sh_message_wizard')
        pop_view_id = pop_view and pop_view.id or False
        pop_context = dict(self._context or {})
        message = ""
        lin_num = 0

        partner_code = partner_id.partner_code
        rp_id = partner_id.id

        base_data_accurate = True
        uom_id_dict = {}
        uniq_uom = excel_data_df.UOM.unique()
        for uom_name in uniq_uom:
            ## Need to Validate the UOM's first
            uom_id = Uom.search([('name', '=like', str(uom_name).strip())], limit=1)
            if uom_id:
                uom_id_dict[uom_name] = uom_id
                _logger.debug("UOM found ... %s", str(uom_name))
            else:
                message = message + "UOM " + str(uom_name) + " is INVALID!!!!\n"
                base_data_accurate = False

        category_id_dict = {}
        uniq_cat = excel_data_df.Category.unique()
        for cat_name in uniq_cat:
            categ_id = Category.search([('name', '=', str(cat_name).strip())], limit=1)
            if categ_id:
                category_id_dict[cat_name] = categ_id
                _logger.debug("Category found ... %s", str(cat_name))
            else:
                message = message + "Category " + str(cat_name) + " is INVALID!!!!\n"
                base_data_accurate = False

        uniq_typ = excel_data_df.Type.unique()
        for typ_name in uniq_typ:
            if typ_name and typ_name in ['consu', 'service', 'product']:
                _logger.debug("Type found ... %s", typ_name)
            else:
                message = message + "Type name " + str(typ_name) + " is INVALID!!!!\n"
                base_data_accurate = False

        ctx.update({
            'category_id_dict': category_id_dict,
            'uom_id_dict': uom_id_dict
        })
        ### Implement Multi-threading
        thread_list = []
        thread_list_counter = 0
        # queue_obj = queue.Queue()
        line_count = 0

        if base_data_accurate:
            ctx.update({
                'create_product_tmpl_count': 0,
                'update_product_tmpl_count': 0
            })
            create_product_tmpl_count = 0
            update_product_tmpl_count = 0
            error_product_tmpl_count = 0
            product_template_create_lists = []
            prod_template_update_lists = []
            for index, pdata in excel_data_df.iterrows():
                _logger.warning("Working on ......%s", index)
                _logger.warning("Working on %s", pdata)
                try:
                    ### Create Product Template and other using SQL Query
                    message, create_product_tmpl_c, update_product_tmpl_c, product_template_create_id, prod_tmpl_res_id = self.with_context(ctx).action_product_import_database_query(pdata, lin_num, message)
                    message += message
                    create_product_tmpl_count += create_product_tmpl_c
                    update_product_tmpl_count += update_product_tmpl_c
                    if product_template_create_id:
                        product_template_create_lists.append(product_template_create_id)
                    if prod_tmpl_res_id:
                        prod_template_update_lists.append(prod_tmpl_res_id)
                except Exception as e:
                    _logger.info(_("Error on Sheet Data Loop: %s"), e)
                    error_product_tmpl_count += 1

            _logger.info(_("Total Product Template Created: %s, Updated: %s and Error while process import sheet: %s:"), create_product_tmpl_count, update_product_tmpl_count, error_product_tmpl_count)
            _logger.info(_("Product Template Created count/Ids: %s: %s "), len(product_template_create_lists), product_template_create_lists)
            _logger.info(_("Product Template Updated count/Ids: %s: %s "), len(prod_template_update_lists), prod_template_update_lists)

        if message:
            message = "ERRORS\n" + message
        else:
            message = "File imported successfully"

        pop_context['message'] = message
        return {
            'name': 'Import Results',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(pop_view.id, 'form')],
            'view_id': pop_view_id,
            'target': 'new',
            'context': pop_context,
        }

    @api.multi
    def action_product_import_database_query(self, pdata, lin_num, message):
        """
        This method will check & compare and create Product in the system using the Excel Sheet data using Direct SQL Query.
        # INSERT Record into 2 Product Table, 4-Relational Table and Company Property & Ir Model Data Tables
        # Updates Product Tables Only

        INSERT INTO product_template
        INSERT INTO product_product
        INSERT INTO stock_route_product
        INSERT INTO product_supplierinfo
        INSERT INTO product_supplier_taxes_rel
        INSERT INTO product_taxes_rel

        UPDATE product_template
        UPDATE product_product
        """
        create_product_tmpl_count = 0
        update_product_tmpl_count = 0
        today_date = datetime.now()
        log_obj = self.env['log.management']
        Template = self.env['product.template']
        product_template_create_id = self.env['product.template']
        ProductProduct = self.env['product.product']
        Category = self.env['product.category']
        ProductSellerInfo = self.env['product.supplierinfo']
        Uom = self.env['uom.uom']

        standard_field_id = self.env.context.get('standard_field_id')
        category_id_dict = self.env.context.get('category_id_dict')
        uom_id_dict = self.env.context.get('uom_id_dict')
        skip_images = self.skip_images

        # Product Template Default Values
        product_fields = Template.fields_get()
        pro_def_val = Template.default_get(product_fields)
        new_pro_up = pro_def_val.copy()

        partner_id = self.vendor_id
        partner_code = partner_id.partner_code
        rp_id = partner_id.id

        import_row = True
        internal_ref = ""

        categ_id = False
        if len(category_id_dict) > 0:
            categ_id = category_id_dict.get(str(pdata['Category']).strip())
        if not categ_id:
            categ_id = Category.search([('name', '=', str(pdata['Category'].strip()))], limit=1)

        uom_id = False
        if len(uom_id_dict) > 0:
            uom_id = uom_id_dict.get(str(pdata['UOM']).strip())
        if not uom_id:
            uom_id = Uom.search([('name', '=like', str(pdata['UOM'].strip()))], limit=1)

        def_code = str(pdata['Default_Code'])
        if isinstance(def_code, float):
            def_code = str(int(def_code)).strip()
        elif isinstance(def_code, int):
            def_code = str(def_code).strip()
        elif isinstance(def_code, str):
            def_code = def_code.strip()

        if partner_code:
            internal_ref = partner_code + "-" + def_code
        else:
            import_row = False
            message += "Missing Vendor Partner code \n "

        if categ_id:
            categ_id = categ_id.id
        else:
            message += "Line " + str(lin_num) + " -- " + str(pdata['Category']) + "Bad Product Category\n"
            import_row = False

        if uom_id:
            uom_id = uom_id.id
        else:
            message += "Line " + str(lin_num) + " -- " + str(pdata['UOM']) + " Bad UOM\n "
            import_row = False

        image_path = str(pdata['Image_URL'])
        image_base64 = ""
        if skip_images:
            _logger.warning("SKIPPING .... %s", image_path)
        else:
            _logger.warning("Opening .... %s", image_path)
        if isinstance(image_path, str) and not skip_images:
            if "http://" in image_path or "https://" in image_path:
                try:
                    req = urllib.request.Request(
                        image_path.strip(),
                        data=None,
                        headers={
                            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
                        }
                    )
                    with urllib.request.urlopen(req) as link:
                        info = link.info()
                        image_type = info.get_content_maintype()
                        _logger.debug("FILE TYPE IS .... %s", image_type)
                        if image_type == 'image':
                            try:
                                _logger.debug("Opening .... %s", image_path)
                                link = link.read()
                                image_base64 = base64.encodebytes(link)
                            except urllib.error.HTTPError as e:
                                _logger.debug('HTTPError: {}'.format(e.code))
                                message += "Line " + str(
                                    lin_num) + " -- Bad URL " + image_path + " " + 'HTTPError: {}'.format(
                                    e.code) + "\n"
                                log_vals = {'operation': 'p_purchased',
                                            'message': 'Image upload failed for image url %s ' % (image_path),
                                            'date': datetime.now()}
                                log_obj.create(log_vals)
                            except:
                                message += "Line " + str(lin_num) + " -- Bad URL " + image_path + "\n"
                                log_vals = {'operation': 'p_purchased',
                                            'message': 'Image upload failed for image url %s ' % (image_path),
                                            'date': datetime.now()}
                                log_obj.create(log_vals)
                        elif requests.get(image_path.strip(), stream=True).ok:
                            _logger.debug("TRYING ALTERNATE DOWNLOAD TECHNIQUE ....")
                            req = requests.get(image_path.strip(), stream=True)
                            if not req.is_redirect:
                                _logger.debug("Response was not a redirect .... downloading ... %s", req.url)
                                link = req.content
                                image_base64 = base64.encodebytes(link)
                        else:
                            _logger.debug("RESPONSE NOT AN IMAGE %s", image_path)
                            message += "Line " + str(lin_num) + " -- Bad URL " + image_path + "\n"
                            log_vals = {'operation': 'p_purchased',
                                        'message': 'Image upload failed for image url %s ' % (image_path),
                                        'date': datetime.now()}
                            log_obj.create(log_vals)
                except urllib.error.URLError as e:
                    _logger.debug('URLError: {}'.format(e.reason))
                    _logger.debug("TRYING ALTERNATE METHOD .......")
                    req = requests.get(image_path.strip(), stream=True)
                    if req.ok and not req.is_redirect:
                        _logger.debug("Response was not a redirect .... downloading ... %s", req.url)
                        link = req.content
                        image_base64 = base64.encodebytes(link)
                    else:
                        message += "Line " + str(
                            lin_num) + " -- Bad URL " + image_path + " " + 'URLError: {}'.format(e.reason) + "\n"
                        log_vals = {'operation': 'p_purchased',
                                    'message': 'Image upload failed for image url %s ' % (image_path),
                                    'date': datetime.now()}
                        log_obj.create(log_vals)
                except:
                    _logger.debug("BAD URL")
                    message += "Line " + str(lin_num) + " -- Bad URL " + image_path + "\n"
                    log_vals = {'operation': 'p_purchased',
                                'message': 'Image upload failed for image url %s ' % (image_path),
                                'date': datetime.now()}
                    log_obj.create(log_vals)

        elif isinstance(image_path, str) and not skip_images:
            try:
                with open(image_path.strip(), 'rb') as image:
                    image_base64 = image.read().encode("base64")
            except:
                message += "Line " + str(lin_num) + " -- " + " Bad URL " + image_path + "\n"
        elif not skip_images:
            message += "Line " + str(lin_num) + " -- " + " Bad URL " + internal_ref + "\n"

        if import_row:
            if not pd.isna(pdata['List_Price']):
                _logger.debug("List Price NOT NULL")
                l_price = pdata['List_Price']
            else:
                _logger.debug("List Price NULL")
                l_price = 0

            if not pd.isna(pdata['Net_Price']):
                _logger.debug("Net Price NOT NULL")
                n_price = pdata['Net_Price']
            else:
                _logger.debug("Net Price NULL")
                n_price = 0

            barcode_data = None
            if not pd.isna(pdata['Barcode']):
                _logger.debug("Barcode NOT NULL")
                barcode_data = pdata['Barcode']
            else:
                _logger.debug("Barcode NULL")

            if not pd.isna(pdata['Description']):
                description_sale = str(pdata['Description']).replace('\n', '<br/>')  # .encode('utf-8')
            else:
                description_sale = ''

            if not pd.isna(pdata['Description']):
                description_purchase = str(pdata['Description']).replace('\n', '<br/>')  # .encode('utf-8')
            else:
                description_purchase = ''

            _logger.debug("Starting database update for .... %s", str(pdata['Name']).encode('utf-8'))

            new_pro_up.update({
                'name': str(pdata['Name']),  # str(pdata['Name']).encode('utf-8'),
                'product_default_code': (str(partner_code.encode('ascii', 'ignore').decode(
                    'ascii') + "-" + def_code) if partner_code else ''),
                'default_code_option': int(def_code) if isinstance(def_code, float) else def_code,
                'default_code': (str(partner_code.encode('ascii', 'ignore').decode(
                    'ascii') + "-" + def_code) if partner_code else ''),
                'type': str(pdata['Type']),
                'list_price': l_price,
                'standard_price': n_price,
                'categ_id': categ_id,
                'uom_id': uom_id,
                'uom_po_id': uom_id,
                'first_supplier': rp_id,
                'active': True if pdata['Active'] in (True, 1, '1', 'True', 'TRUE') else False,
                'description_sale': description_sale,
                'description_purchase': description_purchase,
                'seller_ids': [(0, 0, {'name': rp_id, 'price': n_price})],

                # 'image_medium': image_base64,
                # 'image_small': image_base64,
                # 'image': image_base64,
            })

            if barcode_data:
                new_pro_up.update({'barcode': barcode_data})

            prod_tmpl_res_id = Template.search(['|', ('active', '=', True),
                                                ('active', '=', False),
                                                ('default_code', '=',
                                                 (str(partner_code.encode('ascii', 'ignore').decode(
                                                     'ascii') + "-" + def_code) if partner_code else ''))
                                                ], limit=1)
            _logger.debug("Product Template Exist ?: %s", prod_tmpl_res_id)

            new_product_values = new_pro_up.copy()
            new_product_values.update({
                'write_date': today_date,
                'write_uid': self.env.user.id,
            })

            prod_taxes_id = []
            prod_route_ids = []
            prod_supplier_taxes_id = []
            product_sell_default_values = {}

            ### Using the below fields data...we have to create data in the 'Relational Tables'
            if 'route_ids' in new_product_values:
                prod_route_ids = new_product_values.pop('route_ids')
                prod_route_ids = prod_route_ids[0][2]
            if 'supplier_taxes_id' in new_product_values:
                prod_supplier_taxes_id = new_product_values.pop('supplier_taxes_id')
                prod_supplier_taxes_id = prod_supplier_taxes_id[0][2]
            if 'taxes_id' in new_product_values:
                prod_taxes_id = new_product_values.pop('taxes_id')
                prod_taxes_id = prod_taxes_id[0][2]
            if 'seller_ids' in new_product_values:
                prod_seller_ids = new_product_values.pop('seller_ids')
                try:
                    # Prepare Product.SupplierInfo Values and Create
                    product_sellerinfo_fields = ProductSellerInfo.fields_get()
                    product_sell_default_values = ProductSellerInfo.default_get(product_sellerinfo_fields)
                    if prod_seller_ids:
                        seller_dict_data = prod_seller_ids[0][2]
                        product_sell_default_values.update({
                            'name': seller_dict_data.get('name'),
                            'price': seller_dict_data.get('price'),
                            'write_date': today_date,
                            'write_uid': self.env.user.id,
                        })
                except Exception as e:
                    _logger.info(_("Product SupplierInfo Error: %s"), e)

            property_data_dict = {}
            ### Remove all the fields which is not direct available in the 'Table'..And work for these with All the related Tables Entry
            if 'property_account_creditor_price_difference' in new_product_values:
                property_account_creditor_price_difference = new_product_values.pop(
                    'property_account_creditor_price_difference')
            if 'property_account_expense_id' in new_product_values:
                property_account_expense_id = new_product_values.pop('property_account_expense_id')
            if 'property_account_income_id' in new_product_values:
                property_account_income_id = new_product_values.pop('property_account_income_id')
            if 'property_cost_method' in new_product_values:
                property_cost_method = new_product_values.pop('property_cost_method')
            if 'property_stock_account_input' in new_product_values:
                property_stock_account_input = new_product_values.pop('property_stock_account_input')
            if 'property_stock_account_output' in new_product_values:
                property_stock_account_output = new_product_values.pop('property_stock_account_output')
            if 'property_valuation' in new_product_values:
                property_valuation = new_product_values.pop('property_valuation')
            if 'asset_category_id' in new_product_values:
                asset_category_id = new_product_values.pop('asset_category_id')
            if 'deferred_revenue_category_id' in new_product_values:
                deferred_revenue_category_id = new_product_values.pop('deferred_revenue_category_id')

            if 'property_stock_inventory' in new_product_values:
                # property_data_dict['property_stock_inventory'] = \
                property_stock_inventory = new_product_values.pop('property_stock_inventory')
            if 'property_stock_production' in new_product_values:
                # property_data_dict['property_stock_production'] = \
                property_stock_production = new_product_values.pop('property_stock_production')
            if 'standard_price' in new_product_values:
                property_data_dict['standard_price'] = new_product_values.pop('standard_price')

            # Create in ir_property Table..Only company-dependent fields with values: 'property_stock_inventory', 'property_stock_production', 'standard_price'
            ir_property_dict_values = {}
            if len(property_data_dict) > 0:
                ir_property_values = {
                    'company_id': self.env.user.company_id.id,
                    'write_date': today_date,
                    'write_uid': self.env.user.id,
                }
                for key, pvals in property_data_dict.items():
                    ir_property_values['name'] = key
                    if key == "standard_price":
                        ir_property_values.update({
                            'name': key,
                            'type': 'float',
                            'value_float': pvals,
                            'fields_id': standard_field_id,
                        })
                        ir_property_dict_values.update({
                            key: ir_property_values
                        })

            # Prepare Product Values and Create
            product_default_values = {
                # 'active': True,
                'default_code': new_pro_up.get('default_code'),
                'volume': new_pro_up.get('volume', 0.0),
                'weight': new_pro_up.get('weight', 0.0),
                'active': new_pro_up.get('active'),
                'write_date': today_date,
                'write_uid': self.env.user.id,
            }
            if new_pro_up.get('barcode'):
                product_default_values.update({'barcode': new_pro_up.get('barcode')})

            # If Product Template Exist then Update using Query Else Create New Record
            if prod_tmpl_res_id:
                _logger.info(_("Existing Product Template Record Updation Process......."))
                if prod_tmpl_res_id.seller_ids:
                    prod_tmpl_res_id.seller_ids.unlink()

                if len(new_product_values) > 0:
                    # Update queries for product_template
                    pro_tmpl_update_columns = ', '.join("" + str(i) + "" + "= %s" for i in new_product_values.keys())
                    pro_tmpl_update_values = tuple(new_product_values.values())
                    product_tmpl_update_query = f"UPDATE product_template SET {pro_tmpl_update_columns} WHERE id={prod_tmpl_res_id.id}"
                    self.env.cr.execute(product_tmpl_update_query, pro_tmpl_update_values)
                    _logger.info(_("Product Template %s is Updated Successfully."), prod_tmpl_res_id.id)

                    # If image_base64 is available : Update Images after Product.Template is created by Query
                    if image_base64 and not skip_images:
                        prod_tmpl_res_id.write({
                            'image_medium': image_base64,
                            'image_small': image_base64,
                            'image': image_base64,
                        })

                    # NOTE: Few Relational (M2M), ir_property & ir_model_data Table Not need to update the record data.
                    # Because when Product template First Time Created All the 'Default Fields' Data is already created.
                    # And we are not passing these fields data from the Excel/CSV Sheet.

                    if len(product_sell_default_values) > 0:
                        product_sell_default_values.update({
                            'product_tmpl_id': prod_tmpl_res_id.id,
                            'create_uid': self.env.user.id,
                            'create_date': today_date,
                        })
                        # Insert query for product_supplierinfo
                        pro_supplierinfo_columns = ', '.join("" + str(x) + "" for x in product_sell_default_values.keys())
                        pro_supplierinfo_columns_str = ', '.join("%s" for i in product_sell_default_values.keys())
                        pro_supplierinfo_values = tuple(product_sell_default_values.values())
                        product_supplierinfo_insert_query = f"INSERT INTO product_supplierinfo ({pro_supplierinfo_columns}) VALUES ({pro_supplierinfo_columns_str}) RETURNING id;"
                        self.env.cr.execute(product_supplierinfo_insert_query, pro_supplierinfo_values)
                        product_supplierinfo_create_id = self.env.cr.fetchone()[0]
                        _logger.info(_("New Product SupplierInfo create ID: %s"), product_supplierinfo_create_id)

                    if len(product_default_values) > 0:

                        # In case of product is 'Active: False, will not come in the prod_tmpl_res_id.product_variant_ids...That case needs to search and get..Process
                        if len(prod_tmpl_res_id.product_variant_ids) > 0:
                            product_product_variant_ids = prod_tmpl_res_id.product_variant_ids
                        else:
                            product_product_variant_ids = self.env['product.product'].search(
                                [('active', '=', False), ('product_tmpl_id', '=', prod_tmpl_res_id.id)])

                        # if len(prod_tmpl_res_id.product_variant_ids) > 0:
                        if len(product_product_variant_ids) > 0:
                            # Update queries for product_product
                            pro_prod_update_columns = ', '.join(
                                "" + str(i) + "" + "= %s" for i in product_default_values.keys())
                            pro_prod_update_values = tuple(product_default_values.values())
                            product_update_query = f"UPDATE product_product SET {pro_prod_update_columns} WHERE id={product_product_variant_ids[0].id}"     # prod_tmpl_res_id.product_variant_ids
                            self.env.cr.execute(product_update_query, pro_prod_update_values)
                            _logger.info(_("Product Product %s is Updated Successfully."),
                                         product_product_variant_ids[0].id)

                            # For product ir_property Table record check and update
                            if len(ir_property_dict_values) > 0:
                                for key, pvals in ir_property_dict_values.items():
                                    prod_ir_id = 'product.product' + ',' + str(
                                        product_product_variant_ids[0].id)
                                    prod_ir_id = "'" + prod_ir_id + "'"
                                    # Check for existance of ir_property for standard price for same product...If not exist and standard price is there..Than create else update
                                    ir_property_query = "SELECT id,value_float FROM ir_property WHERE name='standard_price' AND res_id=%s AND fields_id=%s AND company_id=%s" \
                                                        % (prod_ir_id, str(standard_field_id),
                                                           str(self.env.user.company_id.id))
                                    self.env.cr.execute(ir_property_query)
                                    product_ir_property_data = self.env.cr.fetchone()
                                    if not product_ir_property_data:
                                        pvals.update({
                                            'create_uid': self.env.user.id,
                                            'create_date': today_date,
                                        })
                                        if key == "standard_price":
                                            if not pvals.get('value_float'):
                                                continue
                                            pvals.update({
                                                'res_id': 'product.product' + ',' + str(
                                                    product_product_variant_ids[0].id)
                                            })
                                        # Insert query for ir_property
                                        ir_property_columns = ', '.join("" + str(x) + "" for x in pvals.keys())
                                        ir_property_columns_str = ', '.join("%s" for i in pvals.keys())
                                        ir_property_values = tuple(pvals.values())
                                        ir_property_insert_query = f"INSERT INTO ir_property ({ir_property_columns}) VALUES ({ir_property_columns_str}) RETURNING id;"
                                        self.env.cr.execute(ir_property_insert_query, ir_property_values)
                                        ir_property_create_id = self.env.cr.fetchone()[0]
                                        _logger.info(_("New IrProperty create ID: %s"), ir_property_create_id)
                                    else:
                                        product_ir_property_id = product_ir_property_data[0]
                                        product_ir_property_value = product_ir_property_data[1]
                                        if not float(pvals.get('value_float')):
                                            ir_property_delete_query = "DELETE from ir_property WHERE id=%s" % (
                                                str(product_ir_property_id))
                                            self.env.cr.execute(ir_property_delete_query)
                                        else:
                                            if product_ir_property_value != float(pvals.get('value_float')):
                                                ir_property_update_query = "UPDATE %s SET value_float=%s WHERE id=%s" % (
                                                    'ir_property', pvals.get('value_float'),
                                                    str(product_ir_property_id))
                                                self.env.cr.execute(ir_property_update_query)
                    log_vals = {'operation': 'p_purchased',
                                'message': 'Updated product with name %s ' % (prod_tmpl_res_id.name),
                                'date': datetime.now()}
                    log_obj.create(log_vals)

                    prod_tmpl_res_id = prod_tmpl_res_id.id
                    update_product_tmpl_count += 1
                    self.env.cr.commit()
                    # prod_tmpl_res_id.env.cr.commit()  # one record commited
            else:
                ### Create Product Template, Product SupplierInfo and Product Product Data in Table by Query

                _logger.info(_("New Product Template Record Creation Process......."))
                if len(new_product_values) > 0:
                    new_product_values.update({
                        'create_uid': self.env.user.id,
                        'create_date': today_date,
                    })
                    # Insert queries for product_template  # .replace('/', '_')
                    pro_tmpl_columns = ', '.join("" + str(x) + "" for x in new_product_values.keys())
                    pro_tmpl_columns_str = ', '.join("%s" for i in new_product_values.keys())
                    pro_tmpl_values = tuple(new_product_values.values())
                    product_insert_query = f"INSERT INTO product_template ( {pro_tmpl_columns} ) VALUES ({pro_tmpl_columns_str}) RETURNING id;"
                    self.env.cr.execute(product_insert_query, pro_tmpl_values)
                    product_template_create_id = self.env.cr.fetchone()[0]
                    # print ("product_template_create_id: ", product_template_create_id)
                    _logger.info(_("new Product Template create ID: %s"), product_template_create_id)

                    # If image_base64 is available : Update Images after Product.Template is created by Query
                    if image_base64 and not skip_images:
                        product_tmpl_res_id = Template.browse(product_template_create_id)
                        product_tmpl_res_id.write({
                            'image_medium': image_base64,
                            'image_small': image_base64,
                            'image': image_base64,
                        })

                    if len(prod_route_ids) > 0:
                        for pr in prod_route_ids:
                            prod_route_insert_query = "INSERT INTO %s ( route_id,  product_id ) VALUES ( %s, %s );" % (
                            'stock_route_product', pr, product_template_create_id)
                            self.env.cr.execute(prod_route_insert_query)

                    if len(prod_supplier_taxes_id) > 0:
                        for pst in prod_supplier_taxes_id:
                            prod_supp_tax_insert_query = "INSERT INTO %s ( prod_id, tax_id ) VALUES ( %s, %s );" % (
                            'product_supplier_taxes_rel', product_template_create_id, pst)
                            self.env.cr.execute(prod_supp_tax_insert_query)

                    if len(prod_taxes_id) > 0:
                        for pdt in prod_taxes_id:
                            prod_tax_insert_query = "INSERT INTO %s ( prod_id, tax_id ) VALUES ( %s, %s );" % (
                            'product_taxes_rel', product_template_create_id, pdt)
                            self.env.cr.execute(prod_tax_insert_query)

                    if len(product_sell_default_values) > 0:
                        product_sell_default_values.update({
                            'product_tmpl_id': product_template_create_id,
                            'create_uid': self.env.user.id,
                            'create_date': today_date,
                        })
                        # Insert query for product_supplierinfo
                        pro_supplierinfo_columns = ', '.join("" + str(x) + "" for x in product_sell_default_values.keys())
                        pro_supplierinfo_columns_str = ', '.join("%s" for i in product_sell_default_values.keys())
                        pro_supplierinfo_values = tuple(product_sell_default_values.values())
                        product_supplierinfo_insert_query = f"INSERT INTO product_supplierinfo ({pro_supplierinfo_columns}) VALUES ({pro_supplierinfo_columns_str}) RETURNING id;"
                        self.env.cr.execute(product_supplierinfo_insert_query, pro_supplierinfo_values)
                        product_supplierinfo_create_id = self.env.cr.fetchone()[0]
                        _logger.info(_("New Product SupplierInfo create ID: %s"), product_supplierinfo_create_id)

                    if len(product_default_values) > 0:
                        product_default_values.update({
                            'product_tmpl_id': product_template_create_id,
                            'create_uid': self.env.user.id,
                            'create_date': today_date,
                        })
                        # Insert queries for product_product
                        prod_prod_columns = ', '.join("" + str(x) + "" for x in product_default_values.keys())
                        prod_prod_columns_str = ', '.join("%s" for i in product_default_values.keys())
                        prod_prod_values = tuple(product_default_values.values())
                        product_product_insert_query = f"INSERT INTO product_product ({prod_prod_columns}) VALUES ({prod_prod_columns_str}) RETURNING id;"
                        self.env.cr.execute(product_product_insert_query, prod_prod_values)
                        prod_prod_create_id = self.env.cr.fetchone()[0]
                        # print ("prod_prod_create_id: ", prod_prod_create_id)
                        _logger.info(_("New Product Product create ID: %s"), prod_prod_create_id)

                        # Create in ir_property Table..Only company-dependent fields with values: 'property_stock_inventory', 'property_stock_production', 'standard_price'
                        if len(ir_property_dict_values) > 0:
                            for key, pvals in ir_property_dict_values.items():
                                pvals.update({
                                    'create_uid': self.env.user.id,
                                    'create_date': today_date,
                                })
                                if key == "standard_price":
                                    if not pvals.get('value_float'):
                                        continue
                                    pvals.update({
                                        'res_id': 'product.product' + ',' + str(prod_prod_create_id),
                                    })
                                # Insert query for ir_property
                                ir_property_columns = ', '.join("" + str(x) + "" for x in pvals.keys())
                                ir_property_columns_str = ', '.join("%s" for i in pvals.keys())
                                ir_property_values = tuple(pvals.values())
                                ir_property_insert_query = f"INSERT INTO ir_property ({ir_property_columns}) VALUES ({ir_property_columns_str}) RETURNING id;"
                                self.env.cr.execute(ir_property_insert_query, ir_property_values)
                                ir_property_create_id = self.env.cr.fetchone()[0]
                                _logger.info(_("New IrProperty create ID: %s"), ir_property_create_id)

                    log_vals = {'operation': 'p_purchased',
                                'message': 'Imported product with name %s ' % (new_product_values.get('name')),
                                'date': datetime.now()}
                    log_obj.create(log_vals)

                    create_product_tmpl_count += 1
                    self.env.cr.commit()
                    # p_id.env.cr.commit()  # one record commited

            _logger.debug("Completed database update for .... %s", str(pdata['Name']))  # encode('utf-8')

        return message, create_product_tmpl_count, update_product_tmpl_count, product_template_create_id, prod_tmpl_res_id
