# -*- coding: utf-8 -*-
{
    'name' : 'Executive Sales Report',
    'version' : '1.0',
    'author': 'BizAlytics Consulting LLC',
    'summary': 'Custom report for the company sales',
    'sequence': 16,
    'category': 'Sales',
    'description': """
Executive Sales Report
=====================================
Wizard is displayed for Employee / Officer group which will allow the executives to dynamically input query parameters for Sales Order reports.  
    """,
    'category': 'Sales',
    'website': 'http://www.bizalytics.com',
    'images' : [],
    'depends' : ['base', 'base_setup', 'sale', 'showroom_fields', 'branch'],
    'data': [
        'wizard/salesperson_report_view.xml',
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
