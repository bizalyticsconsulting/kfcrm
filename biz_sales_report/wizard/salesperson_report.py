# -*- coding: utf-8 -*-

from odoo import api, fields, models
from dateutil.parser import parse
import logging

_logger = logging.getLogger(__name__)

class SalespersonWizard(models.TransientModel):
    _name = "salesperson.wizard"
    _description = "Salesperson wizard"

    salesperson_id  = fields.Many2many('res.users', string='Salesperson', required=False)
    vendor_id       = fields.Many2many('res.partner', string='Manufacturer', required=False, domain=[('is_company','=',True),('supplier','=',True)])
    customer_id     = fields.Many2many('res.partner', string='Customer', required=False, domain=[('is_company','=',True),('customer','=',True)])
    category_id     = fields.Many2many('product.category', string='Product Category', required=False)
    sales_team_id   = fields.Many2many('crm.team', string='Sales Team', required=False)
    showroom_id     = fields.Many2many('res.branch', string='Showroom', required=False)
    order_date_from = fields.Date(string='Order From Date')
    ship_date_from  = fields.Date(string='Ship From Date')
    order_date_to   = fields.Date(string='Order To Date')
    ship_date_to    = fields.Date(string='Ship To Date')
    sidemark        = fields.Char(string='Sidemark')
    report_type     = fields.Selection([('quotes_by_supplier','Quotes by Suppplier'),
                                        ('sales_by_supplier','Sales by Suppplier'),
                                        ('quotes_by_customer', 'Quotes by Customer'),
                                        ('sales_by_customer','Sales by Customer'),
                                        ('quotes_by_sales_team', 'Quotes by Territory'),
                                        ('sales_by_sales_team','Sales by Territory'),
                                        ('quotes_by_salesperson','Quotes by Salesperson')], required=True)

    @api.multi
    def check_report(self):
        data = {}
        data = self.read(['salesperson_id', 'vendor_id', 'customer_id', 'category_id', 'sales_team_id',
                          'order_date_from', 'order_date_to', 'ship_date_from', 'ship_date_to',
                          'showroom_id', 'report_type', 'sidemark'])[0]

        _logger.info(data)
        self.ensure_one()
        treeview_id = self.env.ref('showroom_fields.sale_order_line_report_tree_view').id
        tree_search_id = self.env.ref('showroom_fields.sale_order_line_report_search_view').id
        pivot_view_id = self.env.ref('showroom_fields.sale_order_line_report_tree_view_pivot').id

        search_domain = [('x_internal_cat1', 'not ilike', 'MISC')]
        if data['ship_date_to']:
            search_domain.append(('x_order_shipdate', '<=', data['ship_date_to']))

        if data['ship_date_from']:
            search_domain.append(('x_order_shipdate', '>=', data['ship_date_from']))

        if data['order_date_to']:
            search_domain.append(('x_order_date', '<=', data['order_date_to']))

        if data['order_date_from']:
            search_domain.append(('x_order_date', '>=', data['order_date_from']))

        if data['salesperson_id']:
            search_domain.append(('salesman_id', 'in', data['salesperson_id']))

        if data['vendor_id']:
            search_domain.append(('x_vendor', 'in', data['vendor_id']))

        if data['customer_id']:
            search_domain.append(('x_customer', 'in', data['customer_id']))

        if data['sales_team_id']:
            search_domain.append(('x_sales_team', 'in', data['sales_team_id']))

        if data['showroom_id']:
            search_domain.append(('x_branch', 'in', data['showroom_id']))

        if data['category_id']:
            search_domain.append(('x_internal_cat1', 'in', data['category_id']))

        if data['sidemark']:
            search_domain.append(('x_sidemark', 'ilike', data['sidemark']))

        context = self._context.copy()
        report_title = ''
        domain_string = ' '.join(map(str, search_domain))

        if data['report_type'] == 'sales_by_customer':
            search_domain.append(('state', '=', 'sale'))
            context.update({
                'pivot_row_groupby': ['order_partner_id'],
                'pivot_column_groupby': ['x_order_date:year'],
                'group_by': ['order_partner_id', 'x_vendor', 'x_order_state_id', 'x_order_city'],
                'pivot_measures': ['price_subtotal'],
            })
            report_title = 'Sales by Customer'

        elif data['report_type'] == 'quotes_by_customer':
            search_domain.append(('state', '<>', 'sale'))
            context.update({
            'pivot_row_groupby': ['order_partner_id'],
            'pivot_column_groupby': ['x_order_date:year'],
            'group_by': ['order_partner_id', 'x_vendor', 'x_order_state_id', 'x_order_city'],
            'pivot_measures': ['price_subtotal'],
            })
            report_title = 'Quotes by Customer'

        elif data['report_type'] == 'quotes_by_supplier':
            search_domain.append(('state', '<>', 'sale'))
            context.update({
                'pivot_row_groupby': ['order_partner_id'],
                'pivot_column_groupby': ['x_order_date:year'],
                'group_by': ['x_vendor', 'x_customer'],
                'pivot_measures': ['price_subtotal'],
            })
            report_title = 'Quotes by Supplier'

        elif data['report_type'] == 'sales_by_supplier':
            search_domain.append(('state', '=', 'sale'))
            context.update({
                'pivot_row_groupby': ['order_partner_id'],
                'pivot_column_groupby': ['x_order_date:year'],
                'group_by': ['x_vendor', 'x_customer'],
                'pivot_measures': ['price_subtotal'],
            })
            report_title = 'Sales by Supplier'

        elif data['report_type'] == 'sales_by_sales_team':
            search_domain.append(('state', '=', 'sale'))
            context.update({
                'pivot_row_groupby': ['order_partner_id'],
                'pivot_column_groupby': ['x_order_date:year'],
                'group_by': ['x_sales_team', 'x_customer'],
                'pivot_measures': ['price_subtotal'],
            })
            report_title = 'Sales by Territory'

        elif data['report_type'] == 'quotes_by_sales_team':
            search_domain.append(('state', '<>', 'sale'))
            context.update({
                'pivot_row_groupby': ['order_partner_id'],
                'pivot_column_groupby': ['x_order_date:year'],
                'group_by': ['x_sales_team', 'x_customer'],
                'pivot_measures': ['price_subtotal'],
            })
            report_title = 'Quotes by Territory'

        elif data['report_type'] == 'quotes_by_salesperson':
            search_domain.append(('state', '<>', 'sale'))
            context.update({
                'pivot_row_groupby': ['order_partner_id'],
                'pivot_column_groupby': ['x_order_date:year'],
                'group_by': ['salesman_id', 'x_vendor'],
                'pivot_measures': ['price_subtotal'],
            })
            report_title = 'Quotes by Salesperson'

        return {
            'name': report_title,
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,pivot,graph',
            'res_model': 'sale.order.line',
            'views': [(treeview_id, 'tree'), (pivot_view_id, 'pivot')],
            'target': 'current',
            'domain': search_domain,
            'context': context,
            ### in domain pass ids if you want to show only filter data else it will display all data of that model.
        }
