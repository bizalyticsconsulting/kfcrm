{
    'name': 'Outbound Email Format',
    'summary': """overridden the mail email template.""",
    'description': """ """,
    'author': 'BizAlytics Consulting LLC',
    'website': 'http://www.bizalytics.com',
    'category': 'mail',
    'version': '12.0.1.0',
    'depends': ['mail'],
    'data': [
        'data/email_templates.xml',
    ],
}
