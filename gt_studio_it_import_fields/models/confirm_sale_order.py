from odoo import models, fields, api
from odoo.exceptions import UserError
import logging


logger = logging.getLogger(__name__)

class ConfirmSaleOrder(models.Model):

    _inherit= 'sale.order'


    client_ship_via = fields.Many2one('delivery.carrier','Client Ship Via')
    client_terms = fields.Many2one('account.payment.term','Client Terms')
    client_freight_terms = fields.Many2one('account.payment.term','Client Freight Terms')
    client = fields.Char("Client")
    specifier_client = fields.Many2one('res.partner','Specifier Client')
    specifier = fields.Char("Specifier")
    x_supp_disc_agree = fields.Boolean(string="Supplier Discount Agreement", help='Supplier Discount Agreement')
    amount_charges = fields.Monetary(compute='_amount_all', string='Total Charges')
    ship_total = fields.Monetary(compute='_amount_all', string='Packing', readonly=True)
    misc_total = fields.Monetary(compute='_amount_all', string='MISC', readonly=True)
    credit_total = fields.Monetary(compute='_amount_all', string='CREDIT', readonly=True)
    backing_total = fields.Monetary(compute='_amount_all', string='BACKING', readonly=True)
    fsfa_total = fields.Monetary(compute='_amount_all', string='FSFA', readonly=True)
    freight_total = fields.Monetary(compute='_amount_all', string='Freight', readonly=True)
    crating_total = fields.Monetary(compute='_amount_all', string='CRATE', readonly=True)
    install_total = fields.Monetary(compute='_amount_all', string='Install', readonly=True)
    other_char_total = fields.Monetary(compute='_amount_all', string='Other Charges', readonly=True)
    # x_order_shipdate = fields.Datetime(_compute='_compute_ship_date',string='Ship Date')
    is_picking_completed = fields.Boolean(string='Picking Done')
    x_order_shipdate = fields.Date(string="Ship Date", help="Sales Order Ship Date")
    x_sda_tag = fields.Char("SDA tag")
    x_kf_invoice = fields.Char("KF Invoice #")
    deposite_request = fields.Float(string='Deposit Requested', compute='_amount_other')
    Payments = fields.Float(string='Payments', compute='_amount_other')
    packing = fields.Float(string='Packing', )
    balance_due = fields.Float(string='Balance due', compute='_amount_other')


    ### 26.08.2020 Need to work for correct the flow
    #### Need to discuss with Vishal for request to desposit ...if no payment term available
    @api.depends('invoice_ids.residual','invoice_ids.amount_total','payment_term_id')
    def _amount_other(self):
        """
        This method will compute for deposit request amount i.e. based on the payment terms calculations.
        Calculates all the payments done for the same invoice and get balance due amount
        """
        payment_amount = 0.0
        for order in self:
            deposite_request_amount = order.amount_total
            logger.debug ("deposite_request_amount=====================, %s", deposite_request_amount)
            if order.payment_term_id:
                payment_term_lines = order.payment_term_id.line_ids.sorted(key=lambda r: r.sequence)
                if len(payment_term_lines) > 0:
                    fixed_term_line = payment_term_lines.filtered(lambda r: r.value == 'fixed')
                    percent_term_line = payment_term_lines.filtered(lambda r: r.value == 'percent')
                    balance_term_line = payment_term_lines.filtered(lambda r: r.value == 'balance')
                    if percent_term_line:
                        percent_amount = percent_term_line.value_amount / 100.0
                        if percent_amount >= 0:
                            deposite_request_amount = order.amount_total * percent_amount
                    elif fixed_term_line:
                        if fixed_term_line.value_amount >= 0:
                            deposite_request_amount = fixed_term_line.value_amount
                    else:
                        pass # need to code for balance if any
                else:
                    deposite_request_amount = order.amount_total / 2
            order.deposite_request = deposite_request_amount
            logger.debug ("deposite_request_amount --- After calculating Terms, %s", deposite_request_amount)

            if len(order.invoice_ids) > 0:
                payment_ids = order.invoice_ids.mapped('payment_ids').filtered(lambda x: x.state == 'posted')
                # print(payment_ids)
                if len(payment_ids) > 0:
                    # payment_amount = sum([i.amount for i in payment_ids])
                    payment_amount = sum([i.amount for i in payment_ids for mvl in i.move_line_ids if not mvl.move_id.reverse_entry_id and mvl.amount_residual > 0])
                # print(payment_amount)
                order.Payments = payment_amount
                order.balance_due = order.amount_total - order.Payments
            else:
                order.balance_due = order.amount_total
            logger.debug ("Order Balance Due=====================, %s", order.balance_due)

            # Old code commented 27.08.2020
            # for invoice in order.invoice_ids:
            #     if invoice.state != 'draft':
            #         payment += invoice.amount_total - invoice.residual
            # order.Payments = payment
            # order.balance_due = order.amount_total - payment

    @api.model
    def run_amount_all(self, sale_order_id):
        order_obj = self.env['sale.order']
        # Take only 10 Sale Order Initially...Then process and check. If working then ok..Otherwise we can also call SaleOrderLine compute method i.e. _compute_amount()
        # all_sales_orders = order_obj.browse([list of ids])

        all_sales_orders = order_obj.search([('archive_sale', '=', True)], order='id desc')
        #all_sales_orders = order_obj.browse(sale_order_id)
        for order in all_sales_orders:
            logger.warning("Running update for order ... %s", order.name)
            order._amount_all()
            # Run Compute on sale order line also ... only if above does not work
            # for order_line in order.order_line:
            #     logger.warning("Running update for order lines ... %s", order_line)
            #     order_line._compute_amount()
            order._ks_sale_payment_amount_update()


    @api.depends('order_line.price_total')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        ship_prod_list = misc_prod_list = credit_prod_list = crate_prod_list = backing_prod_list = fsfa_prod_list = freight_prod_list = other_prod_list = 0.0

        for order in self:
            amount_untaxed = disc = amount_tax = amount_charges = 0.0
            crating = freight = install = other_charges = 0.0
            for line in order.order_line:
                if line.product_id.default_code == 'SHIP':
                    freight_prod_list += line.price_subtotal

                    # amount_untaxed += line.price_subtotal - ship_prod_list
                elif line.product_id.default_code == 'MISC' or line.product_id.default_code == 'PACK':
                    misc_prod_list += line.price_subtotal

                elif line.product_id.default_code == 'CREDIT':
                    credit_prod_list += line.price_subtotal

                elif line.product_id.default_code == 'BACKING':
                    backing_prod_list += line.price_subtotal

                elif line.product_id.default_code == 'FSFA':
                    fsfa_prod_list += line.price_subtotal

                elif line.product_id.default_code == 'CRATE':
                    crate_prod_list += line.price_subtotal

                elif line.product_id.default_code == 'FREIGHT':
                    freight_prod_list += line.price_subtotal

                elif line.product_id.default_code == 'OTHER':
                    other_prod_list += line.price_subtotal
                else:
                    amount_untaxed += line.price_subtotal
                    disc += line.discount_amount

                amount_charges += line.total_charges
                # Commentted on 23.12.2020...Because already added in line.total_charges fields in sale_order_line_items.py(def _compute_total_charges())
                if line.crating or line.freight or line.install or line.other_charges:
                    crate_prod_list += line.crating if line.crating else 0.0
                    freight_prod_list += line.freight if line.freight else 0.0
                    install += line.install if line.install else 0.0
                    other_charges += line.other_charges if line.other_charges else 0.0

                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    # price = line.price_unit #* (1 - (line.discount or 0.0) / 100.0)
                    ### Calculate Tax after discount amount...31.01.2021 ====================>>
                    price = line.price_unit * (1 - (line.discount_show or 0.0) / 100.0)
                    taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
                                                    product=line.product_id, partner=order.partner_shipping_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
            # self.amount_without_discount_tax = self.order_line.x_price_sub_total
    
            other = ship_prod_list + freight_prod_list + misc_prod_list + credit_prod_list + \
                    backing_prod_list + fsfa_prod_list + crate_prod_list + other_prod_list
                    #crating + install + other_charges

            updated_amount_charges = 0.0
            if other:
                updated_amount_charges += other

            #if amount_charges:
            #    updated_amount_charges += amount_charges

            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed - disc),
                'amount_charges': updated_amount_charges, #+ install + other_charges,
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                # 2020.12.23 - Removing other from the total of amount_total as that is covered in amount_charges
                'amount_total': (amount_untaxed + amount_tax + updated_amount_charges) - order.discount,
                'freight_total': freight_prod_list,
                'crating_total': crate_prod_list,
                'install_total': install,
                'other_char_total': other_prod_list,
                'ship_total': ship_prod_list,
                'misc_total': misc_prod_list,
                'credit_total': credit_prod_list,
                'backing_total': backing_prod_list,
                'fsfa_total': fsfa_prod_list,
                'crating_total': crate_prod_list,
            })

    # @api.onchange('discount')
    @api.onchange('discount',)
    def _onchange_discount(self):
        if self.discount > 0.0:
            self.x_supp_disc_agree = True

            return {
                'warning': {
                    'title': "SDA Form",
                    'message': "Please remember to fill / change SDA form for this Order.",
                },
            }
# class ProcurementGroup(models.Model):
#     _inherit = 'procurement.group'
#     _description = 'Procurement Group'
#     _order = "id desc"
#
#     @api.model
#     def run(self, product_id, product_qty, product_uom, location_id, name, origin, values):
#
#         values.setdefault('company_id', self.env['res.company']._company_default_get('procurement.group'))
#         values.setdefault('priority', '1')
#         values.setdefault('date_planned', fields.Datetime.now())
#         rule = self._get_rule(product_id, location_id, values)
#         if not rule:
#             raise UserError(
#                 _('No procurement rule found in location "%s" for product "%s".\n Check routes configuration.') % (
#                 location_id.display_name, product_id.display_name))
#         action = 'pull' if rule.action == 'pull_push' else rule.action
#         if hasattr(rule, '_run_%s' % action):
#             getattr(rule, '_run_%s' % action)(product_id, product_qty, product_uom, location_id, name, origin, values)
#         else:
#             _logger.error("The method _run_%s doesn't exist on the procument rules" % action)
#         return True


    # @api.one
    # def _get_approval_for_admin(self):
    #     if self.env.ref('base.group_system'):
    #         self.is_picking_completed == True
    #         print "llllllllllllllllllll"
    #     else:
    #         self.is_picking_completed == False