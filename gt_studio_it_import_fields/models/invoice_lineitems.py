from odoo import models, fields, api
from odoo.exceptions import UserError

import logging

_logger = logging.getLogger(__name__)

class AccountInvoiceAdditionalCharge(models.Model):
    _inherit = 'account.invoice'

    amount_charges = fields.Monetary(compute='_compute_amount', string='Total Charges')
    # amount_charges_tax = fields.Monetary(compute='_compute_amount', string='Charges Tax', readonly=True)
    freight_total = fields.Monetary(compute='_compute_amount', string='Freight', readonly=True)
    crating_total = fields.Monetary(compute='_compute_amount', string='Crating', readonly=True)
    install_total = fields.Monetary(compute='_compute_amount', string='Install', readonly=True)
    other_char_total = fields.Monetary(compute='_compute_amount', string='Other Charges', readonly=True)

    ship_total = fields.Monetary(compute='_compute_amount',string='SHIP',readonly=True)
    misc_total = fields.Monetary(compute='_compute_amount',string='MISC',readonly=True)
    credit_total = fields.Monetary(compute='_compute_amount',string='CREDIT',readonly=True)
    backing_total = fields.Monetary(compute='_compute_amount',string='BACKING',readonly=True)
    fsfa_total = fields.Monetary(compute='_compute_amount',string='FSFA',readonly=True)
    payments = fields.Float(string='Payments', compute='_amount_other')
    packing = fields.Float(string='Packing',  compute='_compute_amount') #old compute _amount_other
    balance_due = fields.Float(string='Balance due', compute='_amount_other')
    deposite_request = fields.Float(string='Deposit Request', compute='_amount_other')
    # freight_total = fields.Monetary(compute='_compute_amount',string='Freight',readonly=True)
    # crating_total = fields.Monetary(compute='_compute_amount',string='CRATE',readonly=True)

    sub_total_with_discount = fields.Monetary(compute='_compute_amount',string="Subtotal w/Discount",readonly=True)


    ### 26.08.2020 Need to work for correct the flow
    @api.depends('residual', 'amount_total', 'payment_term_id')
    def _amount_other(self):
        """
        This method will compute for deposit reuest amount i.e. based on the payment terms calculations.
        Calculates all the payments done for the same invoice and get balance due amount
        """
        payment_amount = 0.0
        for rec in self:
            deposite_request_amount = rec.amount_total
            if rec.payment_term_id:
                payment_term_lines = rec.payment_term_id.line_ids.sorted(key=lambda r: r.sequence)
                if len(payment_term_lines) > 0:
                    fixed_term_line = payment_term_lines.filtered(lambda r: r.value == 'fixed')
                    percent_term_line = payment_term_lines.filtered(lambda r: r.value == 'percent')
                    balance_term_line = payment_term_lines.filtered(lambda r: r.value == 'balance')
                    if percent_term_line:
                        percent_amount = percent_term_line.value_amount / 100.0
                        if percent_amount >= 0:
                            deposite_request_amount = rec.amount_total * percent_amount
                    elif fixed_term_line:
                        if fixed_term_line.value_amount >= 0:
                            deposite_request_amount = fixed_term_line.value_amount
                    else:
                        pass  # need to code for balance if any
                else:
                    deposite_request_amount = rec.amount_total / 2
            rec.deposite_request = deposite_request_amount

            payment_ids = rec.mapped('payment_ids').filtered(lambda x: x.state == 'posted')
            if len(payment_ids) > 0:
                # payment_amount = sum([i.amount for i in payment_ids])
                payment_amount = sum([i.amount for i in payment_ids for mvl in i.move_line_ids if
                                      not mvl.move_id.reverse_entry_id and mvl.amount_residual > 0])

            rec.payments = payment_amount
            rec.balance_due = rec.amount_total - rec.payments

            # old code commented 27.08.2020
            # for invoice in self:
            #         if invoice.state != 'draft':
            #             payment += invoice.amount_total - invoice.residual
            #     self[0].payments = payment
            #     self[0].balance_due = self[0].amount_total - payment

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice', 'type')
    def _compute_amount(self):
        ship_prod_list = misc_prod_list = credit_prod_list = crate_prod_list = backing_prod_list = fsfa_prod_list = freight_prod_list = misc_discount = other_prod_list = pack_prod_list = 0.0
        amount_untaxed = disc = amount_tax = amount_charges = 0.0

        for rec in self:
            # amount_untaxed = disc = amount_tax = amount_charges = 0.0
            crating = freight = install = other_charges = 0.0
            for line in rec.invoice_line_ids:
                # misc_discount += line.discount_amount
                # amount_untaxed += line.price_subtotal - ship_prod_list
                if line.freight:
                    freight_prod_list += line.freight
                    misc_discount += line.discount_amount
                if line.crating:
                    crate_prod_list += line.crating
                    misc_discount += line.discount_amount
                if line.install:
                    other_prod_list += line.install
                    misc_discount += line.discount_amount
                if line.other_charges:
                    other_prod_list += line.other_charges
                    misc_discount += line.discount_amount

                if line.product_id.default_code == 'SHIP':
                    freight_prod_list += line.price_subtotal
                    # ship_prod_list += line.price_subtotal
                    misc_discount += line.discount_amount

                elif line.product_id.default_code == 'MISC':
                    misc_prod_list += line.price_subtotal
                    misc_discount += line.discount_amount

                elif line.product_id.default_code == 'PACK':
                    pack_prod_list += line.price_subtotal
                    misc_discount += line.discount_amount

                elif line.product_id.default_code == 'CREDIT':
                    credit_prod_list += line.price_subtotal
                    misc_discount += line.discount_amount

                elif line.product_id.default_code == 'BACKING':
                    backing_prod_list += line.price_subtotal
                    misc_discount += line.discount_amount

                elif line.product_id.default_code == 'FSFA':
                    fsfa_prod_list += line.price_subtotal
                    misc_discount += line.discount_amount

                elif line.product_id.default_code == 'CRATE':
                    crate_prod_list += line.price_subtotal
                    misc_discount += line.discount_amount

                elif line.product_id.default_code == 'FREIGHT':
                    freight_prod_list += line.price_subtotal
                    misc_discount += line.discount_amount

                elif line.product_id.default_code == 'OTHER':
                    other_prod_list += line.price_subtotal
                    misc_discount += line.discount_amount

                else:
                    amount_untaxed += line.price_subtotal
                    disc += line.discount_amount

        order_id = self.env['sale.order'].search([('name', '=', self.origin)])
        round_curr = self.currency_id.round
        # self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_untaxed = amount_untaxed - disc

        #self.amount_charges = sum(line.amount_charges for line in order_id if line.amount_charges)
        self.amount_tax = sum(round_curr(line.amount) for line in self.tax_line_ids)

        # self.amount_charges_tax = sum(round_curr(line.amount_charges_tax) for line in order_id)
        #self.freight_total =  sum(round_curr(line.freight_total) for line in order_id)
        #self.crating_total = sum(round_curr(line.crating_total) for line in order_id)
        #self.install_total = sum(round_curr(line.install_total) for line in order_id)
        #self.other_char_total = sum(round_curr(line.other_char_total) for line in order_id)

        other = ship_prod_list + freight_prod_list + misc_prod_list + credit_prod_list + backing_prod_list + fsfa_prod_list + \
                crate_prod_list + crating + install + other_charges + pack_prod_list + other_prod_list
        _logger.debug("Other Total amount in invoice : %s", other)
        
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)

        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign

        # self.sub_total_with_discount = sum(line.price_subtotal for line in self.invoice_line_ids)
        # self.sub_total_with_discount = ((self.amount_untaxed + self.amount_tax + self.amount_charges)- other) - self.discount

        _logger.debug("Amount Untaxed : %s", self.amount_untaxed)
        _logger.debug("Amount Tax: %s", self.amount_tax)
        _logger.debug("Amount Charges: %s", self.amount_charges)
        _logger.debug("Other Amount: %s", other)
        _logger.debug("Misc Discounted : %s", misc_discount)

        self.amount_total = (self.amount_untaxed + self.amount_tax + self.amount_charges + other) - misc_discount
        _logger.debug("Amount Total : %s", self.amount_total)

        self.ship_total = ship_prod_list
        self.misc_total = misc_prod_list
        self.packing = pack_prod_list # for packing
        self.credit_total = credit_prod_list
        self.backing_total = backing_prod_list
        self.fsfa_total = fsfa_prod_list
        self.freight_total = freight_prod_list
        self.crating_total = credit_prod_list
        self.other_char_total = other_prod_list

    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line_ids.price_subtotal',
        'move_id.line_ids.amount_residual',
        'move_id.line_ids.currency_id')
    def _compute_residual(self):
        res = super(AccountInvoiceAdditionalCharge, self)._compute_residual()
        self.residual = self.residual - self.discount
        self.residual_signed = self.residual

    @api.model
    def invoice_line_move_line_get(self):
        res = []
        for line in self.invoice_line_ids:
            if line.quantity == 0:
                continue
            tax_ids = []
            for tax in line.invoice_line_tax_ids:
                tax_ids.append((4, tax.id, None))
                for child in tax.children_tax_ids:
                    if child.type_tax_use != 'none':
                        tax_ids.append((4, child.id, None))
            analytic_tag_ids = [(4, analytic_tag.id, None) for analytic_tag in line.analytic_tag_ids]

            move_line_dict = {
                'invl_id': line.id,
                'type': 'src',
                'name': line.name.split('\n')[0][:64],
                'price_unit': line.price_unit,
                'quantity': line.quantity,
                'price': line.price_subtotal + line.freight + line.crating + line.install + line.other_charges,
                'account_id': line.account_id.id,
                'product_id': line.product_id.id,
                'uom_id': line.uom_id.id,
                'account_analytic_id': line.account_analytic_id.id,
                'tax_ids': tax_ids,
                'invoice_id': self.id,
                'analytic_tag_ids': analytic_tag_ids
            }
            res.append(move_line_dict)
        return res

    @api.model
    def tax_line_move_line_get(self):
        res = []
        # keep track of taxes already processed
        done_taxes = []
        # loop the invoice.tax.line in reversal sequence
        for tax_line in sorted(self.tax_line_ids, key=lambda x: -x.sequence):
            if tax_line.amount:
                tax = tax_line.tax_id
                if tax.amount_type == "group":
                    for child_tax in tax.children_tax_ids:
                        done_taxes.append(child_tax.id)
                res.append({
                    'invoice_tax_line_id': tax_line.id,
                    'tax_line_id': tax_line.tax_id.id,
                    'type': 'tax',
                    'name': tax_line.name,
                    'price_unit': tax_line.amount,
                    'quantity': 1,
                    'price': tax_line.amount,
                    'account_id': tax_line.account_id.id,
                    'account_analytic_id': tax_line.account_analytic_id.id,
                    'invoice_id': self.id,
                    'tax_ids': [(6, 0, list(done_taxes))] if tax_line.tax_id.include_base_amount else []
                })
                done_taxes.append(tax.id)
        return res


class AccountInvoiceLineSubtotal(models.Model):

    _inherit = 'account.invoice.line'

    total_charges = fields.Monetary(compute='_compute_price', string='Total Charges')
    freight = fields.Integer(string="Freight Charges")
    crating = fields.Integer(string="Crating Charges",)
    install = fields.Integer(string="Installation Charges",)
    other_charges = fields.Integer(string="Other Charges",)
    # x_price_sub_total = fields.Monetary(compute='subtotal_without_discount_amount11', string='Subtotal', readonly=True, store=True)

    # discounted_price_subtotal = fields.Monetary(string='Discounted Subtotal (without Taxes)',
    #                                             store=True, readonly=True, compute='_compute_price',
    #                                             help="Discounted Subtotal amount without taxes")

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity',
                 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
                 'invoice_id.date_invoice', 'invoice_id.date')
    def _compute_price(self):
        sale_ids = self.env['sale.order'].search([('name','=',self.invoice_id.origin)])
        if sale_ids:
            for line in sale_ids[0].order_line:
                self.total_charges =  line.total_charges if line.total_charges else 0.0
        currency = self.invoice_id and self.invoice_id.currency_id or None
        price = self.price_unit
        # discounted_unit_price = self.price_unit  * (1 - (self.discount or 0.0) / 100.0)

        taxes = False
        if self.invoice_line_tax_ids:
            taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id,
                                                          partner=self.invoice_id.partner_id)

            # Use for store discounted subtotal
            # new_taxes_cal = self.invoice_line_tax_ids.compute_all(discounted_unit_price, currency, self.quantity, product=self.product_id,
            #                                               partner=self.invoice_id.partner_id)

        self.price_subtotal = price_subtotal_signed = taxes['total_excluded'] if taxes else self.quantity * price

        # # Use for store discounted subtotal
        # if sale_ids and not sale_ids.x_skip_commission:
        #     self.discounted_price_subtotal = new_taxes_cal['total_excluded'] if taxes else self.quantity * discounted_unit_price # New field to store the discounted subtotal without the Tax

        if self.invoice_id.currency_id and self.invoice_id.company_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
            #In odoo-10 _get_currency_rate_date() method is not found so that, changes this code as date_invoice or date 20.02.2019
            # price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id._get_currency_rate_date()).compute(price_subtotal_signed,self.invoice_id.company_id.currency_id)
            price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id.date_invoice or self.invoice_id.date).compute(price_subtotal_signed,self.invoice_id.company_id.currency_id)
        sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
        self.price_subtotal_signed = price_subtotal_signed * sign




    # @api.depends('quantity','price_unit')
    # def subtotal_without_discount_amount11(self):
    #     sub_total = 0.0
    #     print("_discount_amount_all+++++++++++++++++++++++++",self)
    #     for line in self:
    #         sub_total += line.quantity * line.price_unit
    #         print("total$$$$$$$$$$$$$$$$$$$$$$$$$$$$$=",sub_total)
    #         line.update({
    #             'x_price_sub_total':sub_total
    #             })

