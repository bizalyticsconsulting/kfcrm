from odoo import models, fields, api

class Vendors(models.Model):
    _inherit= 'res.partner'

    fob = fields.Char("FOB")
    specifier = fields.Char("Specifier")
    federal_id =  fields.Char("Federal Id")
    social_security = fields.Char("Social Security ")
    entered_date = fields.Datetime("Entered Date")
    changed_by =fields.Boolean("Changed By")
    changed_date = fields.Datetime("Changed Date")
    sales_vendor_tax = fields.Boolean("Taxable Selling")
    freightable_tax = fields.Boolean("Taxable Freight")
    crating_tax = fields.Boolean("Taxable Crating")
    other_tax = fields.Boolean("Taxable Other")
    client_ship_via = fields.Many2one('delivery.carrier','Client Ship Via')
    client_terms = fields.Many2one('account.payment.term','Client terms')
    client_freight_terms = fields.Many2one('account.payment.term','Client Freight Terms')
    sidemark = fields.Char("Sidemark", index=1)


    partner_type = fields.Selection([('showroom', 'Showroom'),('client','Customer'), ('ship to','Ship To'),('vendor', 'Supplier'), ('specifier', 'Specifier'),('purchasing','Purchasing'),
                                     ('personal','Personal'),('employee','Employee')], string="Type")
    x_state_territory = fields.Char(string="Territory", size=100, help="Territory", index=1)

    @api.onchange('supplier', 'customer')
    def onchange_supplier_fob(self):
        for rec in self:
            rec.fob = False

    @api.onchange('zip')
    def onchange_state_territory(self):
        for rec in self:
            zip_code = rec.zip
            if type(zip_code) == str and "-" in zip_code:
                zip_fields = zip_code.split("-", 1)
                zip_code = zip_fields[0]
            county = self.env['state.county'].search([('zip', '=', zip_code)], limit=1)
            if county:
                rec.x_state_territory = county.name

    @api.model
    def create(self, vals):
        if vals.get('zip'):
            zip_code = vals.get('zip')
            if "-" in zip_code:
                zip_fields = zip_code.split("-", 1)
                zip_code = zip_fields[0]
            county = self.env['state.county'].search([('zip', '=', zip_code)], limit=1)
            if county:
                vals.update({'x_state_territory': county.name})
        return super(Vendors, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'zip' in vals:
            vals.update({'x_state_territory': False})
            if vals.get('zip'):
                zip_code = vals.get('zip')
                if "-" in zip_code:
                    zip_fields = zip_code.split("-", 1)
                    zip_code = zip_fields[0]
                county = self.env['state.county'].search([('zip', '=', zip_code)], limit=1)
                if county:
                    vals.update({'x_state_territory': county.name})
        return super(Vendors, self).write(vals)
