from odoo import models, fields, api

class SalesPurchaseTaxLocation(models.Model):

    _inherit= 'account.tax'

    tax_description = fields.Text("Description")