from odoo import models, fields, api
from odoo.exceptions import UserError


class SalesPurchaseTaxLocation(models.Model):

    _inherit= 'sale.order.line'

    freight = fields.Float("Freight Charges", copy=False)
    crating = fields.Float("Crating Charges", copy=False)
    install = fields.Float("Installation Charges", copy=False)
    other_charges = fields.Float("Other Charges", copy=False)
    sales_tax = fields.Float("Sales Tax", copy=False)
    entered_by = fields.Many2one('res.users', 'Entered By')
    total_charges = fields.Monetary(compute='_compute_total_charges', string='Total Charges')
    entered_date = fields.Date('Entered Date')
    # x_price_sub_total = fields.Monetary(compute='subtotal_without_discount_amount11', string='Subtotal', readonly=True, store=True)

    # discounted_price_subtotal = fields.Monetary(compute='_compute_amount', readonly=True, store=True,
    #                                             string='Discounted Subtotal (without Taxes)',
    #                                             help="Discounted Subtotal amount without taxes")


    @api.depends('freight','crating','install','other_charges')
    def _compute_total_charges(self):
        for line in self:
            total = 0.0
            if line.crating or line.freight or line.install or line.other_charges:
                crating = line.crating if line.crating else 0.0
                freight = line.freight if line.freight else 0.0
                install = line.install if line.install else 0.0
                other_charges = line.other_charges if line.other_charges else 0.0
                total = crating + freight + install + other_charges
            line.update({
                'total_charges':total
            })

#    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
#    def _compute_amount(self):
#        """
#        Compute the amounts of the SO line.
        #"""
#        for line in self:
#            total = 0.0
#            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
#            if line.crating or line.freight or line.install or line.other_charges:
#                crating = line.crating if line.crating else 0.0
#                freight = line.freight if line.freight else 0.0
#                install = line.install if line.install else 0.0
#                other_charges = line.other_charges if line.other_charges else 0.0
#                total = crating + freight + install + other_charges
#            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
#                                            product=line.product_id, partner=line.order_id.partner_shipping_id)
#            line.price_subtotal = taxes['total_excluded']
#            line.update({
#                'price_tax': taxes['total_included'] - taxes['total_excluded'],
#                'price_total': taxes['total_included'],
#            })

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(SalesPurchaseTaxLocation, self)._prepare_invoice_line(qty)
        res.update({
            'freight':self.freight,
            'crating':self.crating,
            'install':self.install,
            'other_charges':self.other_charges
        })
        return res

    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        res = super(SalesPurchaseTaxLocation,self)._compute_amount()
        for line in self:
            ###price = line.price_unit # * (1 - (line.discount or 0.0) / 100.0)
            ### Calculate Tax after discount amount...31.01.2021====================>>
            price = line.price_unit * (1 - (line.discount_show or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)

            # disocunted_amount = 0.0
            # if not line.order_id.x_skip_commission:
            #     disocunted_amount = taxes['total_excluded']

            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': line.price_unit * line.product_uom_qty, # taxes['total_excluded'], # 31.01.2021

                # 'discounted_price_subtotal': disocunted_amount,   # Amount after discount
            })


    # @api.depends('product_uom_qty','price_unit')
    # def subtotal_without_discount_amount11(self):
    #     sub_total = 0.0
    #     print("_discount_amount_all+++++++++++++++++++++++++",self)
    #     for line in self:
    #         sub_total += line.product_uom_qty * line.price_unit
    #         print("total$$$$$$$$$$$$$$$$$$$$$$$$$$$$$=",sub_total)
    #         line.update({
    #             'x_price_sub_total':sub_total
    #             })


