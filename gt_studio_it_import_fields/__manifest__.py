# -*- coding: utf-8 -*-
{
    'name': "Studio it Import Fields",
    'summary': """""",
    'description': """""",
    'author': "Globalteckz",
    'website': "http://www.globalteckz.com",
    'category': 'Sale',
    'version': '0.1',
    'depends': ['base','sale','delivery','purchase','account','pragmatic_state_county'],
    'data': [
        'views/account_payment_check.xml',
        'views/confirm_sale_order_views.xml',
        'views/invoice_lineitem_views.xml',
        'views/product_temlpate_views.xml',
        'views/purchase_order_line_views.xml',
        'views/purchase_order_views.xml',
        'views/sale_order_lineitem_views.xml',
        'views/ship_via_delivery_view.xml',
        'views/tax_location_view.xml',
        'views/vendor_views.xml',
    ],
}