# -*- coding: utf-8 -*-
{
    'name': "gt_generate_purchase_rfq",

    'summary': """
        """,

    'description': """
        Generate The RFQ From Sale order 
    """,

    'author': "Globalteckz",
    'website': "http://www.globalteckz.com",


    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale','purchase','stock'],

    # always loaded
    'data': [
        'views/sale_view.xml',
    ],
    # only loaded in demonstration mode
    
}
