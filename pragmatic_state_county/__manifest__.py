# -*- coding: utf-8 -*-
{
    'name': 'USA States, County and Zip Codes',
    'version': '12.0',
    'author': 'Pragmatic TechSoft Pvt Ltd.',
    'category': 'Tools',
    'depends': ['contacts', 'sale'],
    'summary': 'Map US zip codes, counties and states US states finder US county finder US zip code finder zip code finder',
    'description': """
Based on US Census information(updated 2014) - Adds dropdown list on the partner form view to map zip code with US country.
<keywords>
US states finder
US county finder
US zip code finder
zip code finder
""",
    'website': 'http://pragtech.co.in',
    'data': [
        'security/ir.model.access.csv',
        'views/res_partner.xml',
        'views/res_county.xml',
        'data/state.county.csv',
    ],
    'images': ['images/Animated-USAState-management.gif'],
    'live_test_url': 'https://www.pragtech.co.in/company/proposal-form.html?id=103&name=state-country-and-name',
    'price': 30,
    'currency': 'EUR',
    'license': 'OPL-1',
    'installable': True,
    'application': True,
    'auto_install': False,
}
