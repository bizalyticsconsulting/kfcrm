﻿from odoo import models, fields, api
import logging

_logger = logging.getLogger(__name__)

class res_partner(models.Model):
    _inherit = 'res.partner'

    zip_county_id = fields.Many2one('state.county', 'County')
    
    @api.onchange('zip')
    def _onchange_zip(self):
        '''On change of zip code, it sets values for state,county and country. as per zip code'''
        if self.zip:
            if len(self.zip) <= 5:
                zip_county = self.env['state.county'].search([('zip', '=', self.zip)])
                if zip_county:
                    self.zip_county_id = zip_county.id
                    self.state_id = zip_county.state_id.id
                    self.country_id = zip_county.state_id.country_id.id
                if not zip_county:
                    self.zip_county_id = None
            if len(self.zip) > 5:
                zip_county = self.env['state.county'].search([('zip', '=', self.zip[0:5])])
                if zip_county:
                    self.zip_county_id = zip_county.id
                    self.state_id = zip_county.state_id.id
                    self.country_id = zip_county.state_id.country_id.id
                if not zip_county:
                    self.zip_county_id = None

    @api.model
    def cron_update_county(self):
        '''Calling for all res.partner accounts that are in the system '''
        all_contacts_obj = self.env['res.partner']
        all_contacts = all_contacts_obj.search([('active', '=', True)])
        for contact in all_contacts:
            _logger.debug("Updating .... %s", contact.name)
            contact._onchange_zip()
            self.env.cr.commit()
            # if contact.zip:
            #     if len(contact.zip) <= 5:
            #         zip_county = self.env['state.county'].search([('zip', '=', contact.zip)])
            #         if zip_county:
            #             contact.zip_county_id = zip_county.id
            #             contact.state_id = zip_county.state_id.id
            #             contact.country_id = zip_county.state_id.country_id.id
            #         if not zip_county:
            #             contact.zip_county_id = None
            #     if len(contact.zip) > 5:
            #         zip_county = self.env['state.county'].search([('zip', '=', contact.zip[0:5])])
            #         if zip_county:
            #             contact.zip_county_id = zip_county.id
            #             contact.state_id = zip_county.state_id.id
            #             contact.country_id = zip_county.state_id.country_id.id
            #         if not zip_county:
            #             contact.zip_county_id = None