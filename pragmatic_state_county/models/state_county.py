﻿from odoo import models, fields, api


class StateCounty(models.Model):
    _name = 'state.county'

    state_id = fields.Many2one('res.country.state', 'State', domain=[('country_id','=', 'United States')], required=True)
    name = fields.Char('County', size=33, required=True, help='United States second level administrative boundaries.')
    zip = fields.Char('Zip Code')
    
    _sql_constraints = [
        ('zip_uniq', 'unique(zip)', 'Zip Code must be unique per County and State!'),
    ]

class ResCountryState(models.Model):
    _inherit = 'res.country.state'
    
    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if args is None:
            args = [] 
        if self._context.get('module') == 'pragmatic_state_county':
            search_id = self.search([('code','=', name), ('country_id','=', 'United States')])
            if self.env.context.get('country_id'):
                args = args + [('country_id', '=', self.env.context.get('country_id'))]
            firsts_records = self.search([('code', '=ilike', search_id.name)] + args, limit=limit)
            search_domain = [('name', operator, search_id.name)]
            search_domain.append(('id', 'not in', firsts_records.ids))
            records = firsts_records + self.search(search_domain + args, limit=limit)
            return [(record.id, record.display_name) for record in records]
        return super(ResCountryState, self).name_search(name=name, args=args, operator=operator, limit=limit)
