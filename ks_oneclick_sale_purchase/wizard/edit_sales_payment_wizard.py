from odoo import models, fields, api, _
import json


class EditSalePayementAmount(models.Model):
	_name = 'edit.sales.payments.amt'

	rev_date = fields.Date(string='Reversal date', default=fields.Date.context_today)
	sale_order_id = fields.Char('Sale Order ID')
	edit_sale_pay_ids = fields.One2many('edit.sales.payments','edit_sale_amt_id', string="Payments")

	@api.multi
	def write(self, vals):
		if 'edit_sale_pay_ids' in vals:
			for i in vals.get('edit_sale_pay_ids'):
				if i[2] and 'memo' in i[2]:
					edit_sale_pay_id = self.env['edit.sales.payments'].browse(i[1])
					account_move_id = self.env['account.move'].browse(int(edit_sale_pay_id.acount_move_id))
					account_move_id.write({'ref': i[2]['memo']})
		return super(EditSalePayementAmount, self).write(vals)

	def reverse_move(self, rec):
		#It will call the functionality of "Reverse Entry" button of Journal Entries.
		invoice_id = self.env['account.invoice'].browse(int(rec.invoice_id))
		reverse_move_wizard_id = self.env['account.move.reversal'].create({
													'date':self.rev_date,
													'journal_id': rec.journal_id.id
												})
		reverse_moves_ctx = {
								'active_ids': [int(rec.acount_move_id)],
								'active_model': 'account.move',
							}
		reverse_move_wizard_id.with_context(reverse_moves_ctx).reverse_moves()
		# edit_sale_payment_id = self.env['edit.sales.payments'].search(
		# 	[('edit_sale_amt_id', '=', self.id), ('remove', '=', True)])
		# print("!!!!!!!!!!!!!!!!!!!!!", edit_sale_payment_id)
		so_id = self.env['sale.order'].search([('name', '=', invoice_id.origin)])
		# so_id.ks_amount = invoice_id.residual
		# 2020.01.31 ks_amount always 0
		so_id.ks_amount = 0
		so_id.ks_remaining_balance = invoice_id.residual if invoice_id.balance_due > 0 else 0
		so_id.Payments = invoice_id.payments
		so_id.balance_due = invoice_id.residual
		if invoice_id.residual > 0:
			so_id.ks_residual_zero = False
			so_id.ks_refund_repeat = False
			so_id.ks_print_invoice = False
		return True

	def create_reverse_entry(self):
		for rec in self.edit_sale_pay_ids:
			if rec.remove:
				self.reverse_move(rec)
				rec.edited_amount = 0
			if rec.edited_amount > 0:
				self.reverse_move(rec)
				#It will call the functionality of "Register Payment" button of Invoice.
				payment_method_id = self.env['account.payment.method'].search([('code', '=', 'manual'),\
																				('payment_type', '=', 'inbound')])
				invoice_id = self.env['account.invoice'].browse(int(rec.invoice_id))
				account_payment_wizard_id = self.env['account.payment'].create({
																'amount': float(rec.edited_amount),
																'journal_id': int(rec.journal_id.id),
																'payment_date': rec.payment_date, # self.rev_date,
																'communication': rec.memo,
																'payment_type': 'inbound',
																'payment_method_id': int(payment_method_id.id),
																'invoice_ids': [(6, 0, invoice_id.ids)],
																'partner_type': 'customer',
																'partner_id': int(rec.partner_id)
															})
				account_payment_ctx = {
											'active_model': 'account.invoice',
											'type': 'out_invoice',
											'journal_type': 'sale',
											'active_id': invoice_id.id,
											'active_ids':invoice_id.ids,
										}
				account_payment_id = account_payment_wizard_id.with_context(account_payment_ctx)
				account_payment_id.action_validate_invoice_payment()
				# edit_sale_payment_id = self.env['edit.sales.payments'].search([('edit_sale_amt_id','=',rec.id),('remove','=',True)])
				# print("22222222222222222222222222222", edit_sale_payment_id)
				so_id = self.env['sale.order'].search([('name', '=', invoice_id.origin)])
				# 2020.01.31 ks_amount always 0
				so_id.ks_amount = 0
				# so_id.ks_amount = invoice_id.residual
				so_id.ks_remaining_balance = invoice_id.residual if invoice_id.balance_due > 0 else 0
				so_id.Payments = invoice_id.payments
				so_id.balance_due = invoice_id.residual
				if invoice_id.residual > 0:
					so_id.ks_residual_zero = False
					so_id.ks_refund_repeat = False
					so_id.ks_print_invoice = False

			if not rec.remove and rec.edited_amount == 0:
				if rec.acount_move_id and rec.payment_date :
					account_move_id = self.env['account.move'].browse(int(rec.acount_move_id))
					account_payment_id = account_move_id.line_ids.mapped('payment_id')
					if len(account_payment_id) > 0 and account_payment_id[0] != rec.payment_date:
						account_payment_id[0].payment_date = rec.payment_date


class EditSalePayements(models.Model):
	_name = 'edit.sales.payments'

	edit_sale_amt_id = fields.Many2one('edit.sales.payments.amt',string="Edit Sales Payments Amount")
	payment_date = fields.Date(string='Payment Date')
	journal_id = fields.Many2one('account.journal', string='Journal')
	memo = fields.Char('Memo')
	amount_paid = fields.Float(string="Amount Paid")
	edited_amount = fields.Float(string="Edited Amount")
	acount_move_id = fields.Char(string="Account Move ID")
	invoice_id = fields.Char(string="Invoice ID")
	partner_id = fields.Char(string="Partner ID")
	rev_date = fields.Date(string='Reversal date', default=fields.Date.context_today)
	remove = fields.Boolean(string="Remove")


class KsSaleOneModel(models.Model):
	_inherit = 'sale.order'

	def payment_details(self):
		amt_env = self.env['edit.sales.payments.amt']
		pay_env = self.env['edit.sales.payments']
		vals  = {'sale_order_id': self.id}
		invoice_id = self.env['account.invoice'].search([('origin', '=', self.name)])
		#later remove the limit parameter of below line
		amt_rec_id = amt_env.search([('sale_order_id', '=', self.id)],limit=1)
		if amt_rec_id:
			for rec in amt_rec_id.edit_sale_pay_ids:
				rec.unlink()
			if invoice_id and invoice_id.payments_widget != 'false' and json.loads(invoice_id.payments_widget):
				for pay_info in json.loads(invoice_id.payments_widget).get('content'):
					journal_id = self.env['account.journal'].search([('name', '=', pay_info.get('journal_name'))])
					memo = pay_info.get('ref')
					#memo = memo.split('(')[1].split(')')[0]
					pay_info_vals = {
										'edit_sale_amt_id': amt_rec_id.id,
										'payment_date': pay_info.get('date'),
										'journal_id': journal_id.id,
										'memo': memo,
										'amount_paid': pay_info.get('amount'),
										'acount_move_id': int(pay_info.get('move_id')),
										'invoice_id': invoice_id.id,
										'partner_id': invoice_id.partner_id.id
										}
					pay_env.create(pay_info_vals)
		else:
			amt_rec_id = amt_env.create({'sale_order_id': self.id})
			if invoice_id and invoice_id.payments_widget and json.loads(invoice_id.payments_widget):
				for pay_info in json.loads(invoice_id.payments_widget).get('content'):
					journal_id = self.env['account.journal'].search([('name', '=', pay_info.get('journal_name'))])
					memo = pay_info.get('ref')
					memo = memo.split('(')[1].split(')')[0] 
					pay_info_vals = {
										'edit_sale_amt_id': amt_rec_id.id,
										'payment_date': pay_info.get('date'),
										'journal_id': journal_id.id,
										'memo': memo,
										'amount_paid': pay_info.get('amount'),
										'acount_move_id': int(pay_info.get('move_id')),
										'invoice_id': invoice_id.id,
										'partner_id': invoice_id.partner_id.id
										}
					pay_env.create(pay_info_vals)
		return {
				'name': _('Edit Payment'),
				'type': 'ir.actions.act_window',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'edit.sales.payments.amt',
				'res_id' : amt_rec_id.id,
				'target': 'new'
			}


