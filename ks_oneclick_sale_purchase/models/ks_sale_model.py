from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, AccessError, UserError
from odoo.http import request
from odoo.addons import decimal_precision as dp
import logging
_logger = logging.getLogger(__name__)


class KsSaleOneModel(models.Model):
    _inherit = 'sale.order'

    def get_default_journal_id(self):
        # It returns default journal payment method = bank so user doesn't need to select bank on opening invoice
        return self.env['account.journal'].search([('type', '=', 'bank'),
                                                   ('company_id', '=', self.env.user.company_id.id)],
                                                  limit=1, order='id').id

    # My Custom Ks Fields inheriting sale.order
    ks_name = fields.Char(string=('Ks_Sale_Oneclick_invoice'))
    ks_global_ir_config = fields.Boolean(
        string="Checks Global preferences", compute="compute_default_values")
    ks_journal_id = fields.Many2one('account.journal', string=('Payment Method'),
                                    domain=[('type', 'in', ('bank', 'cash'))],
                                    default=get_default_journal_id)  # it makes track of cash or bank amount
    ks_amount = fields.Monetary(string=('Payment Amount'), default=0, required=True,
                                store=True,  digits=dp.get_precision('Product Price'))  # send the amount to invoice
    ks_residual = fields.Monetary(string=('Pay Residual Amount'), copy=False,
                                  compute="compute_default_values")  # keeps record of amount unpaid
    ks_residual_zero = fields.Boolean(string="Checks if Residual Amount is zero for quick sale to appear",
                                      default=False, copy=False)  # If true then hide repeat payment button QuickSale
    ks_payment_repeat = fields.Boolean(
        string="Allows Payments untill residue is zero, without affecting existing sale",
        default=False, copy=False)  # If true then you can repeat payment using QuickSale
    ks_refund_repeat = fields.Boolean(string="Checks if Refund Amount is not zero, without affecting existing sale",
                                      default=False, copy=False)  # if true then you can get refund on single click
    ks_print_invoice = fields.Boolean(string="Once Quick Sale occurred then show print invoice button",
                                      default=False,
                                      copy=False)
    ks_visible_on_debug = fields.Boolean(string="Make button visible on debug off and my button's off",
                                         default=False, copy=False, compute="compute_default_values")
    ks_remaining_balance = fields.Monetary(string=('Remaining Amount'), default=0,
                                           compute='change_remaining_balance')
    ks_sale_communication = fields.Char('SO Memo')
    ks_payment_date = fields.Date(string='Payment Date', required=False, copy=False) # default=fields.Date.context_today,


    # Future Reference
    ks_refund_journal_id = fields.Many2one('account.journal', string='Refund payment',
                                           domain=[
                                               ('type', 'in', ('bank', 'cash'))],
                                           default=get_default_journal_id)
    ks_repeat_refund = fields.Boolean(
        string="Allows repeat untill payment is zero", default=True, copy=False)


    @api.multi
    @api.depends('ks_amount')
    def change_remaining_balance(self):
        for rec in self:
            if not rec.ks_residual:
                rec.ks_remaining_balance = rec.amount_total - rec.Payments
                return rec.ks_remaining_balance
            rec.ks_remaining_balance = rec.ks_residual if rec.balance_due > 0 else 0

    @api.multi
    @api.constrains('ks_amount')
    def ks_check_amount_total(self):
        for rec in self:
            if rec.ks_residual:
                if rec.ks_amount > rec.ks_residual:
                    raise ValidationError(
                        _("Reselect your Amount. You have entered amount greater than residual amount"))
            # applying constraint for total amount > input
            if not rec.ks_payment_repeat and rec.ks_amount > rec.amount_total and len(rec.order_line):
                raise ValidationError(
                    _("Reselect your Amount. You have enter amount higher than required amount"))
            if rec.ks_amount < 0:
                raise UserError(_('The payment amount cannot be negative.'))

    @api.multi
    @api.depends('name')
    def compute_default_values(self):
        for rec in self:
            rec.ks_global_ir_config = bool(
                self.env['ir.config_parameter'].sudo().get_param('ks_sale_global_ir_config'))

            if rec.ks_global_ir_config == True and not request.debug:
                rec.ks_visible_on_debug = True
            else:
                rec.ks_visible_on_debug = False

            for ks_single_order in rec:
                ks_amount_total_residual_signed = 0
                ks_amount_total_signed = 0
                ks_counter = 0
                if ks_single_order.invoice_ids:
                    for ks_single_invoice in ks_single_order.invoice_ids:
                        if ks_single_invoice.state in ('draft', 'paid'):
                            ks_counter += 1
                            if len(ks_single_order.invoice_ids) == ks_counter:
                                rec.ks_residual = rec.ks_amount
                                break
                            continue
                        ks_amount_total_signed += ks_amount_total_signed
                        
                        ks_amount_total_residual_signed += ks_single_invoice.residual_signed

                    rec.ks_residual = ks_amount_total_residual_signed

    @api.multi
    @api.onchange('partner_id')
    def ks_amount_not_updated(self):
        for rec in self:
            if len(rec.order_line) > 0 and not rec.currency_id:
                raise UserError(
                    _("Please delete your products and select customer name first to reflect prices"))

    @api.multi
    @api.onchange('order_line', 'order_line.product_id', 'order_line.name', 'order_line.price_subtotal')
    def ks_change_ks_amount(self):
        for rec in self:
            # if rec.amount_total: rec.ks_amount = rec.amount_total
            ## 2020.01.31 comment out any validation. Make ks_amount = 0.
            #if rec.amount_total == 0:
            rec.ks_amount = 0.0

            ### comment out 29.01.2021..Dont show amount in this field..show only 0
            # elif rec.amount_total:
            #     # 27.08.2020 changes done
            #     if len(rec.invoice_ids) > 0:
            #         payment_ids = rec.invoice_ids.mapped('payment_ids').filtered(lambda x: x.state == 'posted')
            #         if len(payment_ids) > 0:
            #             payment_amount = sum([i.amount for i in payment_ids])
            #             rec.ks_amount = rec.amount_total - payment_amount
                
            if not rec.ks_journal_id:
                rec.ks_journal_id = rec.get_default_journal_id()

    def ks_product_stock_picking_done(self, stock_qty):
        ks_local_counter = 0
        for ks_move_line in stock_qty:  # for validating initial == demand
            if ks_move_line.quantity_done == 0:
                ks_move_line.quantity_done = ks_move_line.product_uom_qty
                ks_local_counter += 1
        if ks_local_counter == 0:
            return 0
        else:
            return 1

    def ks_my_validations_oneaugust(self, model=""):
        if len(self.order_line) < 1:
            # either use return or leave as it is.
            raise UserError(_("Select one product to continue"))

    def _ks_sale_confirm_delivery(self):
        for ks_single_picking in self.picking_ids:
            if ks_single_picking.state in ('cancel', 'done'):
                continue

            if self.env['ir.config_parameter'].sudo().get_param('sale.auto_done_setting'):
                self.action_done()

            self.action_view_delivery()  # delivery Start

            picking_id = ks_single_picking

            ks_local_counter = self.ks_product_stock_picking_done(
                picking_id.move_lines)
            if ks_local_counter == 1:
                picking_id.button_validate()  # delivery Ends

    def _ks_sale_invoice_validate(self):
        for ks_single_invoice in self.invoice_ids:
            if ks_single_invoice.state == 'draft':
                ks_single_invoice.action_invoice_open()

    def _ks_sale_invoice_payment(self):
        ctx = self.env.context.copy()

        for ks_single_invoice in self.invoice_ids:
            invoice_id = ks_single_invoice

            if invoice_id.state == 'draft':
                invoice_id.action_invoice_open()

            if invoice_id.state != 'paid':
                invoice_id = invoice_id.with_context(
                    active_id=self.id, active_ids=self.ids)
                method = invoice_id.journal_id.inbound_payment_method_ids

                # checks for Payments and create a dict full of values of either cash or bank to be sent for invoice
                if self.ks_amount > 0:
                    # if self.ks_journal_id.name == 'Cash':  # Checks Many2One field's selected value
                    ks_values = {'amount': self.ks_amount,
                                 'journal_id': self.ks_journal_id.id,
                                 'currency_id': invoice_id.currency_id.id,
                                 # 'payment_date': invoice_id.create_date,
                                 'payment_date': self.ks_payment_date,
                                 'payment_method_id': method[0].id or False,
                                 'payment_type': "inbound",
                                 # recieve money, seller paid and waiting for customer to pay
                                 'invoice_ids': [(6, 0, invoice_id.ids)],
                                 'partner_type': 'customer',
                                 'partner_id': self.partner_id.id,
                                 'communication': self.ks_sale_communication if self.ks_sale_communication else invoice_id.number,
                                 }

                    # Creating invoice of Bank only & can be called multiple times
                    ks_payment_card = self.env['account.payment'].create(
                        ks_values)
                    ks_payment_card._onchange_amount()
                    ks_payment_card._compute_payment_difference()
                    if ks_payment_card.payment_difference > 0:
                        ks_payment_card.payment_difference_handling = "open"
                    ks_payment_id = ks_payment_card.with_context(active_id=invoice_id.id,
                                                                 active_ids=invoice_id.ids)

                    # 27.02.2021...Changes for the Add Payment button ...Process the commission part also.
                    ctx.update({'ks_oneclick_add_payment': True, 'ks_onclick_invoice_id': invoice_id.id})
                    ks_payment_id.with_context(ctx).action_validate_invoice_payment()

                    # get the unpaid amount and show it to user
                    self.ks_residual = invoice_id.residual

                    # If invoice has full payment and not in paid state
                    if invoice_id.residual <= 0 and invoice_id.state != 'paid':
                        invoice_id.state = 'paid'
                    # end of payment creation

            # My validations

    def _ks_sale_payment_amount_update(self):
        self.ks_payment_repeat = True  # Allowing payments to be repeated
        # set it by default unpaid amount and show it to user
        # self.ks_amount = self.ks_residual
        # 2020.01.31 Show ks_amount = 0
        self.ks_amount = 0
        if self.ks_residual == 0:
            self.ks_residual_zero = True  # if residual is 0 then show quick invoice button
            # if residual is 0 then only allow quick refunds to appear
            self.ks_refund_repeat = True
            # if residual is 0 then only allow quick sale's invoice to appear
            self.ks_print_invoice = True
            # self.ks_cancel_sale_hide = True  # if residual is 0 then only allow cancel sale to appear

        ### 30.01.2021 ..changes for add payment button visible
        if self.ks_residual > 0:
            self.ks_residual_zero = False

        # one click Quick Sale from quotation to invoice
        # 2020.01.31 ks_amount always 0
        #if self.ks_amount > 0:
        self.ks_amount = 0

    @api.multi  # Returns data
    def ks_confirm_oneclick_sale(self):
        self.ks_my_validations_oneaugust("KsSaleOneModel")
        if self.env['ir.config_parameter'].get_param('ks_popup_sale_confirmation'):
            return {
                'name': _('Sale Order Confirmation'),
                'view_mode': 'form',
                'res_model': 'ks.purchase.order.confirmation',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
        if self.ks_payment_repeat == False and self.state not in ('done', 'cancel'):
            # for loop to run once to generate invoice only
            self.action_confirm()

            ks_check_total_signed = 0
            for ks in self.invoice_ids:
                ks_check_total_signed += abs(ks.amount_total_signed)
            if self.invoice_status == 'to invoice' and ks_check_total_signed < self.amount_total:
                ks_sale_inv_id = self.env['sale.advance.payment.inv'].create(
                    {'advance_payment_method': 'all'})
                ks_sale_inv_id = ks_sale_inv_id.with_context(
                    active_id=self.id, active_ids=self.ids)
                ks_sale_inv_id.create_invoices()
        self._ks_sale_invoice_payment()
        self._ks_sale_payment_amount_update()
        # Will confirm delivery at the end of the sale cycle......
        if self.balance_due == 0:
            self._ks_sale_confirm_delivery()

    def ks_product_stock_picking_select(self, ks_return_pick_moves):
        ks_local_counter = 0
        for rec in ks_return_pick_moves:  # boolean enforcing to get refund for a list of product's
            if not rec.to_refund:
                rec.to_refund = True
                ks_local_counter += 1
        if ks_local_counter == 0:
            return 0
        else:
            return 1

    def ks_sale_refund_pickings(self):
        for ks_single_picking_id in self.picking_ids:
            if ks_single_picking_id.state == 'done':
                ks_all_return_done = ks_single_picking_id.name.find('/IN/')
                if ks_all_return_done == 2:
                    continue
                ks_return_exists = False
                for ks in self.picking_ids:
                    ks_return_exists = ks.search(
                        [('origin', 'ilike', ks_single_picking_id.name)])
                if not ks_return_exists:
                    ks_return_pick = self.env['stock.return.picking'].with_context(
                        active_id=ks_single_picking_id.id,
                        active_ids=ks_single_picking_id.ids).create(
                        {})
                    ks_return_obj = ks_return_pick.default_get(
                        ks_return_pick._fields)  # singleton error if not used
                    ks_return_pick.picking_id = ks_return_obj[
                        'picking_id']  # retrieve only a single value & why not using ids

                    ks_local_counter = self.ks_product_stock_picking_select(
                        ks_return_pick.product_return_moves)

                    if ks_local_counter == 1:
                        ks_return_pick = ks_return_pick.create_returns()

                    picking_ids = self.env['stock.picking'].search(
                        [('id', '=', ks_return_pick['res_id'])])
                    picking_ids = picking_ids.with_context(active_id=ks_single_picking_id.id,
                                                           active_ids=ks_single_picking_id.ids)

                    ks_local = self.ks_product_stock_picking_done(
                        picking_ids.move_lines)

                    if ks_local == 1:
                        picking_ids.button_validate()

            if ks_single_picking_id.state == 'assigned':
                ks_local = self.ks_product_stock_picking_done(
                    ks_single_picking_id.move_lines)
                if ks_local == 1:
                    ks_single_picking_id.button_validate()

    def ks_sale_refund_invoice(self):
        refund_state_paid = []
        for ks_single_invoice in self.invoice_ids:
            ks_invoice_cancel = ks_single_invoice.filtered(
                lambda x: x.origin == self.name and x.state == "cancel")
            if ks_invoice_cancel.state == 'cancel' or ks_single_invoice.state == 'cancel':
                continue
            ks_invoice_refund = self.invoice_ids.filtered(
                lambda x: x.type == 'out_refund')
            if not ks_invoice_refund:
                refund_state_paid = self.invoice_ids.filtered(
                    lambda x: x.state == "paid" and x.origin == self.name and not x.refund_invoice_id
                )
                for ks_refund in refund_state_paid:
                    refund = self.env['account.invoice.refund'].with_context(active_id=ks_single_invoice.id,
                                                                             active_ids=ks_single_invoice.ids).create(
                        {})
                    refund.description = self.name + "/" + ks_single_invoice.number + "refund"
                    refund.compute_refund('refund')
                    refund_invoice_id = ks_refund.refund_invoice_ids
                    refund_invoice_id.action_invoice_open()
            if ks_single_invoice.state == 'draft':
                # refund_invoice_id = ks_invoice_refund.refund_invoice_id
                refund_state_paid = ks_single_invoice.refund_invoice_id
                refund_invoice_id = ks_single_invoice
                refund_invoice_id.action_invoice_open()  # check if it makes two or not
            if ks_single_invoice.state == 'open':
                refund_state_paid = ks_single_invoice.refund_invoice_id
                refund_invoice_id = ks_single_invoice

            for ks_new_invoice in self.invoice_ids:
                if ks_new_invoice.state == 'draft':
                    # refund_invoice_id = ks_invoice_refund.refund_invoice_id
                    refund_state_paid = ks_new_invoice.refund_invoice_id
                    refund_invoice_id = ks_new_invoice
                    refund_invoice_id.action_invoice_open()  # check if it makes two or not
                if ks_new_invoice.state == 'open':
                    refund_state_paid = ks_new_invoice.refund_invoice_id
                    refund_invoice_id = ks_new_invoice

            for ks_refund in refund_state_paid:
                payment = ks_refund.payment_ids
                for refund_invoice_id in refund_invoice_id:
                    for pay in payment:
                        method = pay.journal_id.outbound_payment_method_ids
                        # refunding money back
                        # if pay.journal_id.name == 'Cash':  # Checks Many2One field's selected value
                        values = {'amount': pay.amount,
                                  'currency_id': pay.currency_id.id,
                                  'journal_id': pay.journal_id.id,
                                  'payment_date': pay.payment_date,
                                  # default payment method
                                  'payment_method_id': method[0].id or False,
                                  'payment_type': "outbound",
                                  # Send Money, seller recieved and customer paid for his order
                                  # Replace all the values if 6,0
                                  'invoice_ids': [(6, 0, refund_invoice_id.ids)],
                                  'partner_type': 'customer',
                                  'partner_id': pay.partner_id.id,
                                  'communication': pay.communication
                                  }
                        ks_payment_card = self.env['account.payment'].create(
                            values)

                        ks_payment_id = ks_payment_card.with_context(active_id=refund_invoice_id.id,
                                                                     active_ids=refund_invoice_id.ids)
                        ks_payment_id.action_validate_invoice_payment()

    # One click Quick refund for sale.order model
    @api.multi  # First for delivery and then for invoice  <t t-if="" />
    def ks_refund_oneclick(self):
        if self.env['ir.config_parameter'].get_param('ks_popup_sale_confirmation'):
            return {
                'name': _('Order Refund Confirmation'),
                'view_mode': 'form',
                'res_model': 'ks.order.refund.confirmation',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
        self.ks_sale_refund_pickings()
        self.ks_sale_refund_invoice()
        self.invoice_status = 'invoiced'
        self.ks_refund_repeat = False  # once refund done then hide it

    def ks_oneclick_sale_print_invoice(self):
        if self.invoice_ids:
            ks_one = self.invoice_ids.filtered(lambda x: x.origin == self.name and
                                               x.state == 'paid')
            data = {'active_ids': ks_one.ids, 'active_id': ks_one.ids[0]}
            return {
                'name': 'Invoice English',
                'type': 'ir.actions.report',
                'report_name': "account.report_invoice_with_payments",
                'report_file': "account.report_invoice_with_payments",
                'report_type': 'qweb-pdf',
                'context': data,
            }
