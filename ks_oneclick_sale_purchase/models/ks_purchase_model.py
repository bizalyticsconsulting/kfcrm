from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from odoo.http import request
from odoo.addons import decimal_precision as dp
from .ks_sale_model import KsSaleOneModel as Ks_sale_import


class KsPurchaseOneModel(models.Model):
    _inherit = 'purchase.order'

    ks_purchase_name = fields.Char(string="KS_Purchase_OneClick_invoice")
    ks_global_ir_config = fields.Boolean(string="Checks Global preferences", copy=False,
                                         compute="compute_purchase_global_values", )  # global settings value for my module's visibility
    ks_purchase_journal_id = fields.Many2one('account.journal', string="Payment Method",
                                             domain=[('type', 'in', ('cash', 'bank'))],
                                             default=Ks_sale_import.get_default_journal_id)
    ks_purchase_amount = fields.Monetary(string='Payment Amount', default=0,  digits=dp.get_precision('Product Price'),
                                         required=True, store=True)  # send the amount to invoice
    ks_purchase_residual = fields.Monetary(string='Pay Residual Amount', default=0, copy=False,
                                           compute="compute_purchase_global_values")  # keeps record of amount unpaid
    ks_purchase_residual_zero = fields.Boolean(string="Checks if Residual Amount is zero for purchase", default=False,
                                               copy=False)  # If true then you can repeat payment using QuickSale
    ks_purchase_payment_repeat = fields.Boolean(string="Allows Payments untill residue is zero, without affecting",
                                                default=False,
                                                copy=False)  # If true then you can repeat payment using QuickSale
    ks_purchase_refund_repeat = fields.Boolean(string="Checks if Refund Amount is not zero, without affecting existing",
                                               default=False,
                                               copy=False)  # if true then you can get refund on single click
    ks_purchase_print_invoice = fields.Boolean(string="Once quick purchase occurred then show print invoice button",
                                               default=False,
                                               copy=False)  # if true then show print invoice for single click
    ks_visible_on_debug = fields.Boolean(string="Make button visible on debug off and my button's off",
                                         default=False, copy=False, compute="compute_purchase_global_values")
    ks_remaining_balance = fields.Monetary(string=('Remaining Amount'), default=0,
                                           compute='change_remaining_balance')
    ks_purchase_communication = fields.Char('PO Memo')

    # Future Reference
    ks_refund_journal_id = fields.Many2one('account.journal', string='Refund payment',
                                           domain=[('type', 'in', ('bank', 'cash'))],
                                           default=Ks_sale_import.get_default_journal_id)
    ks_repeat_refund = fields.Boolean(string="Allows repeat untill payment is zero", default=True, copy=False)

    @api.multi
    @api.depends('ks_purchase_amount')
    def change_remaining_balance(self):
        for rec in self:
            if not rec.ks_purchase_residual:
                rec.ks_remaining_balance = rec.amount_total - rec.ks_purchase_amount
                return rec.ks_remaining_balance
            rec.ks_remaining_balance = rec.ks_purchase_residual - rec.ks_purchase_amount

    @api.multi
    @api.constrains('ks_purchase_amount')
    def ks_check_amount_total(self):
        for rec in self:
            if rec.ks_purchase_residual:
                if rec.ks_purchase_amount > rec.ks_purchase_residual:
                    raise ValidationError(_("Reselect your Amount. You have entered amount greater than residual amount"))
            if not rec.ks_purchase_payment_repeat and rec.ks_purchase_amount > rec.amount_total and len(rec.order_line):
                raise ValidationError(_("Reselect your Amount. You have enter amount higher than required amount"))
            if rec.ks_purchase_amount < 0:
                raise UserError(_('The payment amount cannot be negative.'))

    @api.multi
    @api.depends('name')
    def compute_purchase_global_values(self):
        for rec in self:
            rec.ks_global_ir_config = bool(
                self.env['ir.config_parameter'].sudo().get_param('ks_purchase_global_ir_config'))

            if rec.ks_global_ir_config == True and not request.debug:
                rec.ks_visible_on_debug = True
            else:
                rec.ks_visible_on_debug = False

            for ks_single_purchase in rec:
                ks_amount_total_residual_signed = 0
                ks_amount_total_signed = 0
                ks_counter = 0
                if ks_single_purchase.invoice_ids:
                    for ks_single_invoice in ks_single_purchase.invoice_ids:
                        if ks_single_invoice.state in ('draft', 'paid'):
                            ks_counter += 1
                            if len(ks_single_purchase.invoice_ids) == ks_counter:
                                rec.ks_purchase_residual = rec.ks_purchase_amount
                                break
                            continue
                        ks_amount_total_signed += ks_amount_total_signed
                        ks_amount_total_residual_signed += ks_single_invoice.residual_signed
                    rec.ks_purchase_residual = ks_amount_total_residual_signed

    @api.multi
    @api.onchange('partner_id')
    def ks_purchase_amount_not_updated(self):
        for rec in self:
            if len(rec.order_line) > 0 and not rec.currency_id:
                raise UserError(_("Please delete your products and select Vendor first to reflect prices"))

    @api.multi
    @api.onchange('order_line')
    def ks_purchase_change_ks_amount(self):
        for rec in self:
            if rec.amount_total: rec.ks_purchase_amount = rec.amount_total
            if rec.amount_total == 0: rec.ks_purchase_amount = 0
            if not rec.ks_purchase_journal_id: rec.ks_purchase_journal_id = Ks_sale_import.get_default_journal_id(rec)

    def _ks_purchase_confirm_delivery(self):
        for ks_single_picking in self.picking_ids:
            if ks_single_picking.state in ('cancel', 'done'):
                continue
            self.button_done()

            self.action_view_picking()

            picking_id = ks_single_picking

            ks_local_counter = Ks_sale_import.ks_product_stock_picking_done(self, picking_id.move_lines)

            if ks_local_counter == 1: picking_id.button_validate()

    def ks_create_validate_invoice(self):
        if self.invoice_status == 'to invoice':
            ks_invoice_create = self.env['account.invoice'].create(
                {'partner_id': self.partner_id.id, 'type': 'in_invoice',  # because Invoice isn't created like sales
                 'journal_id': self.ks_purchase_journal_id.id, 'origin': self.name, 'purchase_id': self.id})
            ks_invoice_create = ks_invoice_create.with_context(active_id=self.id, active_ids=self.ids,
                                                               type='in_invoice')
            ks_invoice_create.purchase_order_change()
            ks_invoice_create._onchange_invoice_line_ids()  # Ask if required?
            ks_invoice_create._compute_amount()
            ks_invoice_create.action_invoice_open()

    def ks_invoice_payment_register(self):
        for ks_single_vendor in self.invoice_ids:
            invoice_id = ks_single_vendor.with_context(active_id=self.id, active_ids=self.ids,
                                                       type='in_invoice')
            if invoice_id.state == 'draft':
                invoice_id.action_invoice_open()
            elif invoice_id.state == 'open':
                pass

            if invoice_id.state != 'paid':
                invoice_id = invoice_id.with_context(active_id=self.id, active_ids=self.ids)
                method = invoice_id.journal_id.outbound_payment_method_ids
                if self.ks_purchase_amount > 0:
                    ks_purchase_values = {'amount': self.ks_purchase_amount,
                                          'journal_id': self.ks_purchase_journal_id.id,
                                          'currency_id': invoice_id.currency_id.id,
                                          'payment_date': invoice_id.create_date,
                                          'payment_method_id': method[0].id or False,
                                          'payment_type': "outbound",
                                          # recieve money, seller paid and waiting for supplier pay
                                          'invoice_ids': [(6, 0, invoice_id.ids)],
                                          'partner_type': 'supplier',
                                          'partner_id': self.partner_id.id,
                                          'communication': self.ks_purchase_communication if self.ks_purchase_communication else invoice_id.number,
                                          }
                    ks_payment_card = self.env['account.payment'].create(ks_purchase_values)

                    ks_payment_card._compute_payment_difference()
                    if ks_payment_card.payment_difference >= 0:
                        ks_payment_card.payment_difference_handling = "open"
                    ks_payment_id = ks_payment_card.with_context(active_id=invoice_id.id,
                                                                 active_ids=invoice_id.ids)
                    ks_payment_id.action_validate_invoice_payment()
                    self.ks_purchase_residual = invoice_id.residual  # get the unpaid amount and show it to user

        self.ks_purchase_payment_repeat = True  # Allowing payments to be repeated

        self.ks_purchase_amount = self.ks_purchase_residual  # set it by default unpaid amount and show it to user

    @api.multi
    def ks_purchase_confirm_oneclick_purchase(self):
        if self.env['ir.config_parameter'].get_param('ks_popup_purchase_confirmation'):
            return {
                'name': _('Purchase Order Confirmation'),
                'view_mode': 'form',
                'res_model': 'ks.purchase.order.confirmation',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
        Ks_sale_import.ks_my_validations_oneaugust(self, "KsPurchaseOneModel")

        if self.ks_purchase_payment_repeat == False and self.state not in ('done', 'cancel'):
            self.button_confirm()
            self._ks_purchase_confirm_delivery()
            self.ks_create_validate_invoice()
        self.ks_invoice_payment_register()
        if self.ks_purchase_residual == 0:
            self.ks_purchase_residual_zero = True  # if residual is 0 then hide button
            self.ks_purchase_refund_repeat = True  # if residual is 0 then only allow refunds to appear
            self.ks_purchase_print_invoice = True

    def _refund_purchase_delivery(self):
        for ks_single_picking_id in self.picking_ids:
            if ks_single_picking_id.state == 'done':
                ks_all_return_done = ks_single_picking_id.name.find('/OUT/')
                if ks_all_return_done == 2: continue

                for ks in reversed(self.picking_ids):
                    ks_return_exists = ks.search([('origin', 'ilike', ks_single_picking_id.name)])

                if not ks_return_exists:
                    ks_return_pick = self.env['stock.return.picking'].with_context(active_id=ks_single_picking_id.id,
                                                                                   active_ids=ks_single_picking_id.ids).create(
                        {})
                    ks_return_obj = ks_return_pick.default_get(ks_return_pick._fields)
                    ks_return_pick.picking_id = ks_return_obj[
                        'picking_id']  # retrieve only a single value & why not using ids

                    ks_local_counter = Ks_sale_import.ks_product_stock_picking_select(self,
                                                                                      ks_return_pick.product_return_moves)

                    if ks_local_counter == 1: ks_return_pick_id = ks_return_pick.create_returns()

                    picking_id = self.env['stock.picking'].search([('id', '=', ks_return_pick_id['res_id'])])
                    picking_id = picking_id.with_context(active_id=ks_single_picking_id.id,
                                                         active_ids=ks_single_picking_id.ids)

                    ks_local = Ks_sale_import.ks_product_stock_picking_done(self, picking_id.move_lines)

                    if ks_local == 1:
                        picking_id.button_validate()
                    # self.env['stock.immediate.transfer'].search([('pick_ids', 'in', self.picking_ids.ids)]).process()
            if ks_single_picking_id.state == 'assigned':
                ks_local = Ks_sale_import.ks_product_stock_picking_done(self, ks_single_picking_id.move_lines)
                if ks_local == 1:
                    ks_single_picking_id.button_validate()

    def _refund_purchase_invoice_ids(self):
        refund_state_paid = []
        for ks_single_invoice in self.invoice_ids:
            if ks_single_invoice.state == 'paid':
                ks_invoice_cancel = ks_single_invoice.filtered(lambda x: x.origin == self.name and x.state == "cancel")
                if ks_invoice_cancel.state == 'cancel' or ks_single_invoice.state == 'cancel':
                    continue
                ks_invoice_refund = self.invoice_ids.filtered(lambda x: x.type == 'in_refund')
                if not ks_invoice_refund:
                    refund_state_paid = self.invoice_ids.filtered(
                        lambda x: x.state == "paid" and x.origin == self.name and not x.refund_invoice_id
                    )
                    for ks_refund in refund_state_paid:
                        refund = self.env['account.invoice.refund'].with_context(active_id=ks_single_invoice.id,
                                                                                 active_ids=ks_single_invoice.ids).create({
                            'description':  self.name + "/" + ks_single_invoice.number + "refund"
                        })
                        refund.compute_refund('refund')
                        refund_invoice_id = ks_refund.refund_invoice_ids
                        refund_invoice_id.action_invoice_open()
            if ks_single_invoice.state == 'draft':
                refund_state_paid = ks_single_invoice.refund_invoice_id
                refund_invoice_id = ks_single_invoice
                refund_invoice_id.action_invoice_open()  # check if it makes two or not
            if ks_single_invoice.state == 'open':
                refund_state_paid = ks_single_invoice.refund_invoice_id
                refund_invoice_id = ks_single_invoice

            for ks_refund in refund_state_paid:
                payment = ks_refund.payment_ids
                for refund_invoice_id in refund_invoice_id:
                    for pay in payment:
                        method = pay.journal_id.outbound_payment_method_ids
                        # refunding money back
                        # if pay.journal_id.name == 'Cash':  # Checks Many2One field's selected value
                        values = {'amount': pay.amount,
                                  'currency_id': pay.currency_id.id,
                                  'journal_id': pay.journal_id.id,
                                  'payment_date': pay.payment_date,
                                  'payment_method_id': method[0].id or False,  # default payment method
                                  'payment_type': "inbound",  # Send Money, supplier recieved and purchaser paid for his order
                                  'invoice_ids': [(6, 0, refund_invoice_id.ids)],  # Replace all the values if 6,0
                                  'partner_type': 'supplier',
                                  'partner_id': pay.partner_id.id,
                                  'communication': pay.communication
                                  }
                        ks_payment_card = self.env['account.payment'].create(values)
                        ks_payment_id = ks_payment_card.with_context(active_id=refund_invoice_id.id,
                                                                     active_ids=refund_invoice_id.ids)
                        ks_payment_id.action_validate_invoice_payment()

    # One click Quick refund for purchase.order model
    @api.multi  # First for delivery and then for invoice
    def ks_purchase_refund_oneclick(self):
        if self.env['ir.config_parameter'].get_param('ks_popup_purchase_confirmation'):
            return {
                'name': _('Order Refund Confirmation'),
                'view_mode': 'form',
                'res_model': 'ks.order.refund.confirmation',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
        for rec in self:
            rec._refund_purchase_delivery()
            rec._refund_purchase_invoice_ids()
            rec.ks_purchase_refund_repeat = False

    def ks_purchase_oneclick_print_invoice(self):
        if self.invoice_ids:
            ks_one = self.invoice_ids.filtered(lambda x: x.origin == self.name and
                                                         x.state == 'paid')
            data = {'active_ids': ks_one.ids, 'active_id': ks_one.ids[0]}
            return {
                'name': 'Invoice English',
                'type': 'ir.actions.report',
                'report_name': "account.report_invoice_with_payments",
                'report_file': "account.report_invoice_with_payments",
                'report_type': 'qweb-pdf',
                'context': data,
            }
