from odoo import models, fields, api

class KsResConfigView(models.TransientModel):
    _inherit = 'res.config.settings'

    ks_sale_global_ir_config = fields.Boolean(string="Quick Sale Management", required=True)
    ks_purchase_global_ir_config = fields.Boolean(string="Quick Purchase Management", required=True)
    ks_popup_confirmation = fields.Boolean(string="Popup confirmation")
    ks_popup_purchase_confirmation = fields.Boolean(string="Popup confirmation")
    ks_popup_sale_confirmation = fields.Boolean(string="Popup confirmation")

    def get_values(self):
        ks_res_here = super(KsResConfigView, self).get_values()
        ks_res_here.update(
            ks_sale_global_ir_config=bool(self.env['ir.config_parameter'].get_param('ks_sale_global_ir_config')),
            ks_purchase_global_ir_config=bool(self.env['ir.config_parameter'].get_param('ks_purchase_global_ir_config')),
            ks_popup_purchase_confirmation=bool(self.env['ir.config_parameter'].
                                                get_param('ks_popup_purchase_confirmation')),
            ks_popup_sale_confirmation=bool(self.env['ir.config_parameter'].get_param('ks_popup_sale_confirmation'))
        )
        return ks_res_here

    def set_values(self):
        super(KsResConfigView, self).set_values()
        self.env['ir.config_parameter'].set_param('ks_sale_global_ir_config', self.ks_sale_global_ir_config)
        self.env['ir.config_parameter'].set_param('ks_purchase_global_ir_config', self.ks_purchase_global_ir_config)
        self.env['ir.config_parameter'].set_param('ks_popup_confirmation', self.ks_popup_confirmation)
        self.env['ir.config_parameter'].set_param('ks_popup_purchase_confirmation', self.ks_popup_purchase_confirmation)
        self.env['ir.config_parameter'].set_param('ks_popup_sale_confirmation', self.ks_popup_sale_confirmation)

        if not self.ks_sale_global_ir_config:
            self.env.ref('ks_oneclick_sale_purchase.ks_sale_one_form_view').active = False

        if self.ks_sale_global_ir_config:
            self.env.ref('ks_oneclick_sale_purchase.ks_sale_one_form_view').active = True

        if not self.ks_purchase_global_ir_config:
            self.env.ref('ks_oneclick_sale_purchase.ks_purchase_oneclick_order_form_inherit').active = False

        if self.ks_purchase_global_ir_config:
            self.env.ref('ks_oneclick_sale_purchase.ks_purchase_oneclick_order_form_inherit').active = True
