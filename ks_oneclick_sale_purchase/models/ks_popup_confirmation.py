from odoo import fields, models, api


class KSPurchaseOrderConfirmation(models.TransientModel):
    _name = 'ks.purchase.order.confirmation'

    delivery_done = fields.Boolean('Complete Delivery', default=True)
    create_invoice = fields.Boolean('Create and Validate Invoice', default=True)
    create_payment = fields.Boolean('Create Payment', default=True)

    @api.onchange('create_payment')
    def check_delivery_options(self):
        if self.create_payment:
            self.create_invoice = True

    @api.onchange('create_invoice')
    def check_payment_options(self):
        if self.create_invoice:
            self.delivery_done = True

    def ks_complete_purchase_order(self):
        if self._context.get('active_ids'):
            if self._context.get('active_model') == 'purchase.order':
                order_ids = self.env['purchase.order'].browse(self._context.get('active_ids'))
                for order in order_ids:
                    if order.ks_purchase_payment_repeat == False and order.state not in ('done', 'cancel'):
                        order.button_confirm()
                    if self.delivery_done:
                        order._ks_purchase_confirm_delivery()
                    if self.create_invoice:
                        if not order.invoice_ids:
                            order.ks_create_validate_invoice()
                    if self.create_payment:
                        order.ks_invoice_payment_register()
                    if order.ks_purchase_residual == 0 and order.invoice_ids:
                        order.ks_purchase_residual_zero = True  # if residual is 0 then hide button
                        order.ks_purchase_refund_repeat = True  # if residual is 0 then only allow refunds to appear
                        order.ks_purchase_print_invoice = True
            elif self._context.get('active_model') == 'sale.order':
                order_ids = self.env['sale.order'].browse(self._context.get('active_ids'))
                for order in order_ids:
                    if order.ks_payment_repeat == False and order.state not in ('done', 'cancel'):
                        # for loop to run once to generate invoice only
                        order.action_confirm()
                    if self.delivery_done:
                        order._ks_sale_confirm_delivery()
                    if self.create_invoice:
                        ks_check_total_signed = 0
                        for ks in order.invoice_ids:
                            ks_check_total_signed += abs(ks.amount_total_signed)
                        if order.invoice_status == 'to invoice' and ks_check_total_signed < order.amount_total:
                            ks_sale_inv_id = order.env['sale.advance.payment.inv'].create(
                                {'advance_payment_method': 'all'})
                            ks_sale_inv_id = ks_sale_inv_id.with_context(active_id=order.id, active_ids=order.ids)
                            ks_sale_inv_id.create_invoices()
                        order._ks_sale_invoice_validate()
                    if self.create_payment:
                        order._ks_sale_invoice_payment()
                    if order.invoice_ids:
                        order._ks_sale_payment_amount_update()


class KSOrderRefundConfirmation(models.TransientModel):
    _name = "ks.order.refund.confirmation"

    refund_delivery = fields.Boolean('Delivery Return', default=True)
    refund_invoice = fields.Boolean('Invoice Refund', default=True)

    def ks_order_refund_confirmation(self):
        if self._context.get('active_ids'):
            if self._context.get('active_model') == 'purchase.order':
                order_ids = self.env['purchase.order'].browse(self._context.get('active_ids'))
                for order in order_ids:
                    if self.refund_delivery:
                        order._refund_purchase_delivery()
                    if self.refund_invoice:
                        order._refund_purchase_invoice_ids()
                    if self.refund_delivery or self.refund_invoice:
                        order.ks_purchase_refund_repeat = False
            elif self._context.get('active_model') == 'sale.order':
                order_ids = self.env['sale.order'].browse(self._context.get('active_ids'))
                for order in order_ids:
                    if self.refund_delivery:
                        order.ks_sale_refund_pickings()
                    if self.refund_invoice:
                        order.ks_sale_refund_invoice()
                    if self.refund_delivery or self.refund_invoice:
                        order.invoice_status = 'invoiced'
                        order.ks_refund_repeat = False
