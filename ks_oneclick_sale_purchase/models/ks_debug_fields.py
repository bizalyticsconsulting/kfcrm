from odoo import models, fields, api
from odoo.http import request


class KsDebugHideInvoice(models.Model):
    _inherit = 'account.invoice'
    ks_visible_on_debug = fields.Boolean(string="Make button visible on debug off and my button's off",
                                         default=False, copy=False,
                                         compute="sale_purchase_compute_validate")

    @api.depends('name')
    def sale_purchase_compute_validate(self):
        for rec in self:
            ks_ir_config = bool(self.env['ir.config_parameter'].sudo().get_param('ks_sale_global_ir_config'))

            if ks_ir_config and not request.debug:
                rec.ks_visible_on_debug = True
            else:
                rec.ks_visible_on_debug = False


class KsDebugHidePayment(models.Model):
    _inherit = 'account.payment'

    ks_visible_on_debug = fields.Boolean(string="Make button visible on debug off and my button's off",
                                         default=False, copy=False,
                                         compute="sale_purchase_compute_validate")

    @api.depends('name')
    def sale_purchase_compute_validate(self):
        for rec in self:
            ks_ir_config = bool(self.env['ir.config_parameter'].sudo().get_param('ks_sale_global_ir_config'))

            if ks_ir_config and not request.debug:
                rec.ks_visible_on_debug = True
            else:
                rec.ks_visible_on_debug = False


class KsDebugHidePicking(models.Model):
    _inherit = 'stock.picking'

    ks_visible_on_debug = fields.Boolean(string="Make button visible on debug off and my button's off",
                                         default=False, copy=False, compute="sale_purchase_compute_validate")

    @api.depends('name')
    def sale_purchase_compute_validate(self):
        for rec in self:
            ks_ir_config = bool(self.env['ir.config_parameter'].sudo().get_param('ks_sale_global_ir_config'))

            if ks_ir_config and not request.debug:
                rec.ks_visible_on_debug = True
            else:
                rec.ks_visible_on_debug = False


class KsDebugHidePurchase(models.Model):
    _inherit = 'purchase.order'

    ks_visible_on_debug = fields.Boolean(string="Make button visible on debug off and my button's off",
                                         default=False, copy=False,
                                         compute="sale_purchase_compute_validate")

    @api.depends('name')
    def sale_purchase_compute_validate(self):
        for rec in self:
            ks_ir_config = bool(self.env['ir.config_parameter'].sudo().get_param('ks_sale_global_ir_config'))

            if ks_ir_config and not request.debug:
                rec.ks_visible_on_debug = True
            else:
                rec.ks_visible_on_debug = False
