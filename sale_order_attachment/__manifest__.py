# Part of Odoo. See COPYRIGHT & LICENSE files for full copyright and licensing details.

{
    'name': 'Sale Order Attachment',
    'summary': """ """,
    'description': """ """,
    'author': 'Shoaib Anwar',
    'website': 'shoaib.anwar0707@gmail.com',
    'category': 'Sales',
    'version': '12.0',
    'depends': ['sale'],
    'data': [
        'views/sale_order_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
