from odoo import models, fields, api, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    attachment_ids = fields.One2many('ir.attachment', 'order_id', string='Attachments')