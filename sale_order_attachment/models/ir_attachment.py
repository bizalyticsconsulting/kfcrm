from odoo import fields, models, api


class IrAttachment(models.Model):
    _inherit = 'ir.attachment'

    @api.depends('res_model','res_id')
    def _compute_order_id(self):
        for attachment in self:
            if attachment.res_model == 'sale.order' and attachment.res_id:
                attachment.order_id = attachment.res_id
            else:
                attachment.order_id = False

    order_id = fields.Many2one('sale.order', string='Sale Order', compute='_compute_order_id', store=True)