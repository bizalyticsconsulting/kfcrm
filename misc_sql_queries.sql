-- Getting ready for tax changes at the sales order line level.
kompass_validation=# select * from xtax_sale_line_rel;
 sol_id | tax_id
--------+--------
(0 rows)

kompass_validation=# insert into xtax_sale_line_rel (select * from account_tax_sale_order_line_rel);
INSERT 0 12393
kompass_validation=# select * from xtax_sale_line_rel ;


-- Check for product_tmpl_id at the Sales Order Line level
select count(1) from sale_order_line where product_tmpl_id is null;

update sale_order_line
set product_tmpl_id = product_product.product_tmpl_id
from product_product
where sale_order_line.product_id = product_product.id and sale_order_line.product_tmpl_id is null;

select id, name, state from sale_order where x_product_names is null;

select id, order_id, product_id, product_tmpl_id from sale_order_line where order_id in (select id from sale_order where x_product_names is null);

-- update x_product_names for old sales orders.....
update sale_order so SET x_product_names = sq.product_names
FROM (
select sol.order_id as order_id, string_agg(pt.name, ', ') as product_names
from sale_order_line sol, product_product pp, product_template pt
where sol.product_id = pp.id and
pp.product_tmpl_id = pt.id
group by sol.order_id) as sq
where sq.order_id = so.id and so.x_product_names is null;



-- 1. Update Sale Order Total amount

-- Create View for Calculate total based on Sale Order Line

create view x_so_total as
select sol.id, sol.order_id, so.name, (sol.price_subtotal + sol.freight + sol.crating + sol.install + other_charges) - discount_amount as total_amount 
from sale_order_line sol, sale_order so 
where so.id = sol.order_id


-- Update Sale Order based  on View Total Amount

update sale_order set amount_total = so_sq.total_order_amount
from (select xst.order_id as order_id, sum(xst.total_amount) + sum(so.amount_tax) as total_order_amount
      from x_so_total xst, sale_order so
      where xst.order_id = so.id
      group by xst.order_id
) as so_sq
where sale_order.id = so_sq.order_id




--2.update invoice line using view


create view x_invoice_total as
select id, invoice_id, (price_subtotal_signed + freight + crating + install + other_charges) - discount_amount as total_amount
from account_invoice_line ail


--

update account_invoice set amount_total = acc_sq.total_invoice_amount
from (select xit.invoice_id as invoice_id, sum(xit.total_amount) + sum(ai.amount_tax) as total_invoice_amount
	from 
	x_invoice_total xit,
	account_invoice ai
where xit.invoice_id = ai.id
group by xit.invoice_id
) as acc_sq
where id=acc_sq.invoice_id and id=28587


update sale_order_line
set x_adjusted_booked_sale = supplier_discount_approval.x_adjusted_book_sale
from supplier_discount_approval
where sale_order_line.id = supplier_discount_approval.sp_disc_order_line_id

select name from sale_order where id in
(select distinct res_id from mail_message where body like '%Quantity%' and body like
'%The ordered quantity has been updated%' and write_date >= '2020-02-01'
and model = 'sale.order');




--10.07.2021: Update res_partner x_state_territory field with zip value

update res_partner set x_state_territory = rsp.zip
from res_partner rsp
where rsp.id = res_partner.id


select rp.name, sc.name, sc.zip from res_partner rp, state_county sc where rp.zip = sc.zip and rp.zip is not null;

update res_partner set x_state_territory = sc.name
from state_county sc
where sc.zip = res_partner.zip and res_partner.zip is not null;


--13.07.2021: update sale order for x_product_code ..take from sale.order.line -->Product-->Product_template
-- First create view with concate of sale_order_lines products--?product default_code..Then Update sale_order with this view and fields

CREATE or Replace VIEW sale_order_line_product_tmpl_relation AS select sol.order_id, string_agg(pt.default_code, ', ') as product_code 
from sale_order_line sol
join product_product pp on sol.product_id = pp.id
join product_template pt on pp.product_tmpl_id = pt.id
group by sol.order_id

    
update sale_order set x_product_code = solpt.product_code
from sale_order_line sol, sale_order_line_product_tmpl_relation solpt
where sale_order.id = sol.order_id
and solpt.order_id = sol.order_id


--31.07.2021: Above query changed from default_code-->product_default_code...Incase wants to make changes...Run below query to override the above in DB
-- If product template have product_default_code(Default Code) then take it otherwise take default_code(Internal Reference)

CREATE or Replace VIEW sale_order_line_product_tmpl_relation 
AS select sol.order_id, string_agg(CASE WHEN pt.product_default_code IS NOT NULL THEN pt.product_default_code ELSE pt.default_code END, ', ') as product_code 
       
from sale_order_line sol
join product_product pp on sol.product_id = pp.id
join product_template pt on pp.product_tmpl_id = pt.id
group by sol.order_id


    
update sale_order set x_product_code = solpt.product_code
from sale_order_line sol, sale_order_line_product_tmpl_relation solpt
where sale_order.id = sol.order_id
and solpt.order_id = sol.order_id









