from odoo import models, fields, api, _ ,tools
from datetime import date
import logging

_logger = logging.getLogger(__name__)

class productTemplate(models.Model):
    _inherit="product.template"

    x_order_cfa = fields.Selection([('CFA Requested','CFA Requested'),
                                    ('CFA Approved', 'CFA Approved'),
                                    ('CFA Waived', 'CFA Waived'),
                                    ('CFA Not Requested', 'CFA Not Requested'),
                                    ('CFA Not Available', 'CFA Not Available')], string='CFA')


class saleOderLine(models.Model):
    _inherit = "sale.order.line"

    x_order_cfa = fields.Selection([('CFA Requested', 'CFA Requested'),
                                   ('CFA Approved', 'CFA Approved'),
                                   ('CFA Waived', 'CFA Waived'),
                                   ('CFA Not Requested', 'CFA Not Requested'),
                                   ('CFA Not Available', 'CFA Not Available')], string='CFA')
    x_alt_address = fields.Boolean('Alternate Address')

    @api.onchange('product_id')
    def _auto_populate_cfa_value(self):
       for line in self:
           if line.product_id.x_order_cfa:
               line.x_order_cfa = line.product_id.x_order_cfa

    @api.model
    def create(self, vals):
      if vals.get('name') and vals.get('x_order_cfa'):
          vals['name'] = vals.get('name') + vals.get('x_order_cfa')

      res = super(saleOderLine, self).create(vals)
      if 'x_order_cfa' in vals:
          res.order_id.message_post(body=_(f"CFA Changes in Sale Order Line:--> {res.x_order_cfa}"))
      return res

    @api.multi
    def write(self, vals):
      _logger.debug("IN WRITE METHOD ....... %s", vals.get('name'))
      if vals.get('name') or self.name:
         desc = vals.get('name') or self.name
         desc = desc.replace("CFA Not Available", "")
         desc = desc.replace("CFA Requested", "")
         desc = desc.replace("CFA Waived", "")
         desc = desc.replace("CFA Approved", "")
         desc = desc.replace("CFA Not Requested", "")
         if vals.get('x_order_cfa') or self.x_order_cfa:
             cfa_text = vals.get('x_order_cfa') or self.x_order_cfa
             desc = desc + cfa_text
         _logger.debug("IN WRITE METHOD ....... %s", desc)
         vals.update({'name': desc})
      res = super(saleOderLine, self).write(vals)
      if 'x_order_cfa' in vals:
          self.order_id.message_post(body=_(f"CFA Changes in Sale Order Line:--> {vals.get('x_order_cfa')}"))

      return res


    ##429: comment this code...25.07.2021
    # @api.onchange('x_order_cfa')
    # def onchange_x_order_cfa(self):
    #     for rec in self:
    #         if rec.x_order_cfa == 'CFA Requested':
    #             warning_message = {
    #                 'title': _('Enter Alternate Address on Sale Order Form View.'),
    #                 'message': "Please see Alternate Address then you could enter it on Sale Order Form View in 'Other Information' Tab."
    #             }
    #             return {'warning': warning_message}




class SaleOrder(models.Model):
    _inherit = "sale.order"

    x_alt_address = fields.Boolean('Alt Address ?', compute="_compute_alternate_address", store=True)

    @api.one
    @api.depends('order_line')
    def _compute_alternate_address(self):
        for rec in self:
            rec.x_alt_address = any(rec.order_line.filtered(lambda x: x.x_alt_address))
