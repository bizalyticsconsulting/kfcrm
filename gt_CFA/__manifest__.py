# -*- coding: utf-8 -*-
{
    'name': "CFA Fields",

    'summary': """
	Module adds custom fields to Product and Sales Orders Line. 
    """,
    'description': """
	The fields are 
        - Order CFA 
    """,

    'author': "BizAlytics Consulting LLC",
    'website': "http://www.bizalytics.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sales',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale','product','showroom_fields', 'gt_sale_commission_extension'],

    # always loaded
    'data': [
#         'security/ir.model.access.csv',
         # 'wizard/supplier_disc_wiz_view.xml',
          'order_cfa_view.xml',

        
        
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
