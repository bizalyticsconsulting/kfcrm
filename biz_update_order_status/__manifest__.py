# -*- coding: utf-8 -*-
##############################################################################
#
#    BizAlytics Consulting LLC
#    Copyright (C) 2013-Today(www.bizalytics.com).
#
##############################################################################
{
    'name' : 'Import Sales Order Status',
    'version' : '1.0',
    'author' : 'BizAlytics',
    'category' : 'Extra Tools',
    'description' : """
""",
    "summary":"This Module can be used to import your Sales Order Status",
    'website': 'https://www.bizalytics.com',
    'depends' : ['showroom_fields'],
    'data': [
        'security/upload_security.xml',
        'wizard/import_data_view.xml',
        'data/mail_data.xml',
    ],
    'qweb' : [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
