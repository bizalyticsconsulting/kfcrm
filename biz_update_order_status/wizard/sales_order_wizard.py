# -*- coding: utf-8 -*-
##############################################################################
#
#    BizAlytics Consulting LLC
#    Copyright (C) 2013-Today(www.bizalytics.com).
#
##############################################################################
from odoo import fields, models, tools, api, _
import base64
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import logging
import pandas as pd
import numpy as np
import mimetypes

_logger = logging.getLogger(__name__)
excel_csv_format = [
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
]

class SalesOrderUpdateWizard(models.TransientModel):
    _name = 'sale.order.update.wizard'
    _description = 'Sale Order Update Wizard'
    _rec_name = 'filename'


    data_file = fields.Binary('Upload Excel File', required=True, attachment=False)
    filename = fields.Char('File Name', required=True)


    @api.multi
    def action_send_ecd_report(self, sale_order_id):
        """
            This method will send an email to Sales Team related to the following details,
                - Current ECD
                - Original ECD
                - Updated ECD to Sales Order based on upload Sheet current ECD

            If Sale Order-->Customer-->Sales team-->Email Alias Field data and Email Domain should come from the system parameter.
                - Send email to the System Parameters defined 'email.alias.domain'. If not found don't send email
                - If not Alias Email is not present in Sales Team then dont send email
        """
        self.ensure_one()
        ctx = self.env.context.copy()
        MailTemplate = self.env['mail.template']
        ir_model_data = self.env['ir.model.data']
        if not sale_order_id:
            return False
        if not sale_order_id.partner_id.team_id or not sale_order_id.partner_id.team_id.alias_name:
            return False
        email_alias_domain = self.env['ir.config_parameter'].sudo().get_param('email.alias.domain')
        _logger.info(_("'Email Alias Domain' system parameter: %s"), email_alias_domain)
        if not email_alias_domain:
            return False
        if sale_order_id.partner_id.team_id and sale_order_id.partner_id.team_id.alias_name and email_alias_domain:
            recipient_email_to = str(sale_order_id.partner_id.team_id.alias_name) + '@' + str(email_alias_domain)
            sale_order_url = sale_order_id._get_sale_url()
            ctx.update({
                'recipient_email_to': recipient_email_to,
                'sale_order_url': sale_order_url,
            })
            try:
                template_id = ir_model_data.get_object_reference(
                    'biz_update_order_status', 'email_template_ecd_detail_partner_recipients')[1]
            except ValueError:
                template_id = False
            if template_id:
                mail_template_id = MailTemplate.browse(template_id)
                mail_id = mail_template_id.with_context(ctx).send_mail(sale_order_id.id, force_send=True)
                if mail_id:
                    # Post Message on Sale Order Log View
                    sale_order_id.message_post(
                        body=_(f"Sale Original ECD field is updated with sheet and email send to the recipient: '{recipient_email_to}'."))
        return True


    @api.multi
    def update_sale_order(self):
        """
        This method will update the current ECD from sheet and also if Current ECD & Sale Original ECD is not same then
        Update the Original ECD and send email to the Sales Team.
        """
        pop_context = dict(self._context or {})
        log_obj = self.env['log.management']
        SaleOrder = self.env['sale.order']
        # Validate files format...only allow Excel sheet format files
        mimetype = None
        if mimetype is None and self.filename:
            mimetype = mimetypes.guess_type(self.filename)[0]
            if not mimetype in excel_csv_format:
                raise UserError('Please upload only excel sheet (.xls, .xlsx) format files !')
        try:
            excel_file_data = pd.read_excel(io=base64.decodestring(self.data_file), dtype=str)
            mandatory_columns = ['Order Reference', 'Customer', 'Production Status', 'Supplier Order Number', 'Supplier Invoice Number',
                                 'RDD', 'ECD', 'Complete Date', 'Ship Date', 'Tracking Number']

        except Exception as e:
            _logger.exception('File not get imported !')
            raise UserError(
                _('File %r not imported due to a malformed file.'
                  '\n\nTechnical Details:\n%s') % \
                (self.filename, tools.ustr(e))
            )

        if len(excel_file_data.columns) < 10:
            raise UserError(_("Please upload an excel sheet with the minimum 9 mandatory columns !"))
        if any([i for i in mandatory_columns if i not in excel_file_data.columns]):
            raise UserError(_("Please upload an excel sheet with the mandatory columns !\n\n"
                              "Mandatory columns are: \n%s" %(', '.join(map(str, mandatory_columns)))))

        excel_data_df = pd.DataFrame(data=excel_file_data)

        message = ""
        so_count = 0
        ecd_updated = False
        # Get Sale Order Database query at once for all the Sale Order Reference
        order_reference_list = [str(i).strip() for i in list(excel_data_df['Order Reference'])]
        sale_order_ids = SaleOrder.search(['|', ('archive_sale', '=', True), ('archive_sale', '=', False), ('name', 'in', order_reference_list)])
        _logger.info(_("Total Sale Order Count based on Order Reference: %s"), len(sale_order_ids))
        for index, pdata in (excel_data_df.iterrows()):
            _logger.info(_(f"Sheet Index Count: {index}"))
            try:
                select_order_id = sale_order_ids.filtered(lambda x: x.name == (str(pdata['Order Reference'])).strip())
                _logger.info(_(f"Selected Sale Order for {str(pdata['Order Reference'])}: {select_order_id}"))
                if len(select_order_id) > 0:
                    so_update_vals = {}
                    if not pd.isnull(pdata['Production Status']):
                        so_update_vals.update({'x_supp_production_status': pdata['Production Status']})
                    if not pd.isnull(pdata['Complete Date']):
                        so_update_vals.update({'x_supp_completion_date': pdata['Complete Date']})
                    if not pd.isnull(pdata['Supplier Order Number']):
                        so_update_vals.update({'x_mfg_order': pdata['Supplier Order Number']})
                    if not pd.isnull(pdata['Supplier Invoice Number']):
                        so_update_vals.update({'mfg_invoice': pdata['Supplier Invoice Number']})
                    if not pd.isnull(pdata['RDD']):
                        so_update_vals.update({'rdd': pdata['RDD']})
                    if not pd.isnull(pdata['Ship Date']):
                        so_update_vals.update({'x_order_shipdate': pdata['Ship Date']})
                    if not pd.isnull(pdata['Tracking Number']):
                        so_update_vals.update({'x_tracking_num': pdata['Tracking Number']})

                    if not pd.isnull(pdata['ECD']):
                        # so_update_vals.update({'ecd': pdata['ECD']})
                        so_update_vals.update({'current_ecd': pdata['ECD']})
                        ecd_str_date = datetime.strptime(pdata['ECD'], "%Y-%m-%d %H:%M:%S")
                        ecd_str_date = ecd_str_date.date()
                        # If current ECD i.e. Importing Sheet Column values is different then Origin ECD (Sale Order field on Form view)
                        # Then update current ECD to Original ECD field...Note for both values to Send Email to Salesperson/Sales Team
                        if not select_order_id.ecd:
                            so_update_vals.update({'ecd': pdata['ECD']})

                        if select_order_id.ecd and (select_order_id.ecd != ecd_str_date):
                            ecd_updated = True
                            pop_context.update({
                                'current_ecd': ecd_str_date,
                                'previous_original_ecd': select_order_id.ecd,
                            })
                            # Send an email to each sale order update to the Given Email Alias.
                            self.with_context(pop_context).action_send_ecd_report(select_order_id)

                    if len(so_update_vals) > 0:
                        select_order_id.write(so_update_vals)
                        so_count += 1

            except Exception as e:
                _logger.info(_(f"Error in DataFrame Loop: {e}"))
                message += "\n" + str(pdata['Order Reference'])

        pop_view = self.env.ref('sh_message.sh_message_wizard')
        pop_view_id = pop_view and pop_view.id or False
        if message:
            message = "ERRORS\n" + message
        else:
            message = "File imported successfully. \n\nTotal Sale Order updated count: %s" %(so_count)

        pop_context['message'] = message
        return {
            'name': 'Import Results',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(pop_view_id, 'form')],
            'view_id': pop_view_id,
            'target': 'new',
            'context': pop_context,
        }
