# -*- coding: utf-8 -*-
{
    'name': 'Delivery and Invoice Status in Sale Order',
    "author": "Edge Technologies",
    'version': '12.0.1.0',
    'live_test_url': "https://youtu.be/DaJ9xEe6-yQ",
    "images":['static/description/main_screenshot.png'],
    'summary': "This app helps to know the status of the sale delivery and invoice.",
    'description': """This app helps users to know the status of the invoice and status of the delivery in Sale. 



Sale status Report
invoice status 
Sale Order Status Invoicing Status
Sale Order Delivery and Invoicing status status of delivery and invoices of sale orders
so status 
Invoice Status on Sale Order 
Invoice Status on Sale Order Sale Delivery Status
Delivery status on Sale Order

Sale Order Status Report Sale status Report 
status of sale order delivery on sale order
add Partial Ship, Delivery
partial sale status 
partial delivery status 
Partial Invoice and Invoice Status on Sale Order
so do status 
do status 
partial do status 
do status invoice 

status of shipment 
shipment status 


    """,
    "license" : "OPL-1",
    'depends': ['sale_management','stock','account'],
    'data': [
            'security/sale_delivery_status_security.xml',
            'views/sale_order.xml',
            ],
    'installable': True,
    'auto_install': False,
    'price': 8,
    'currency': "EUR",
    'category': 'Sales',

}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
