# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class SaleOrder(models.Model):    
    _inherit = 'sale.order'

    delivery_check_status = fields.Selection([('to_be_deliver', 'To Be Delivered'),('partial_deliver', 'Partial Delivered'),('delivered', 'Fully Delivered')], compute='compute_delivery_status')
    invoice_check_status = fields.Selection([('to_be_invoice', 'To Invoice'),('partial_paid', 'Partial Paid'),('paid', 'Fully Paid')], compute='compute_invoice_status')

    def compute_delivery_status(self):
        for rec in self:
            if rec.picking_ids:
                if all(d.state=="draft" for d in rec.picking_ids) or all(d.state=="assigned" for d in rec.picking_ids) or all(d.state=="confirmed" for d in rec.picking_ids):
                    rec.delivery_check_status = 'to_be_deliver'
                if any(d.state=="draft" for d in rec.picking_ids) or any(d.state=="assigned" for d in rec.picking_ids) or any(d.state=="confirmed" for d in rec.picking_ids) and not any(d.state=="done" for d in rec.picking_ids):
                    rec.delivery_check_status = 'to_be_deliver'
                if all(d.state=="done" for d in rec.picking_ids):
                    rec.delivery_check_status='delivered'
                if any(d.state=="done" for d in rec.picking_ids) and any(d.state=="assigned" for d in rec.picking_ids):
                    rec.delivery_check_status="partial_deliver"
                if any(d.state=="done" for d in rec.picking_ids) and any(d.state=="confirmed" for d in rec.picking_ids):
                    rec.delivery_check_status="partial_deliver"
                if any(d.state=="done" for d in rec.picking_ids) and any(d.state=="draft" for d in rec.picking_ids):
                    rec.delivery_check_status="partial_deliver"
                if any(d.state=="done" for d in rec.picking_ids) and any(d.state=="cancel" for d in rec.picking_ids) and not any(d.state=="confirmed" for d in rec.picking_ids) and not any(d.state=="assigned" for d in rec.picking_ids):
                    rec.delivery_check_status="delivered"

    def compute_invoice_status(self):
        for rec in self:
            if rec.invoice_ids:
                if all(d.state=="draft" for d in rec.invoice_ids) or all(d.state=="in_payment" for d in rec.invoice_ids):
                    rec.invoice_check_status = 'to_be_invoice'
                if any(d.state=="draft" for d in rec.invoice_ids) or any(d.state=="in_payment" for d in rec.invoice_ids) and not any(d.state=="paid" for d in rec.invoice_ids):
                    rec.invoice_check_status = 'to_be_invoice'
                if any(d.state=="open" for d in rec.invoice_ids) and any(d.amount_total == d.residual for d in rec.invoice_ids) and not any(d.state=="paid" for d in rec.invoice_ids):
                    rec.invoice_check_status = 'to_be_invoice'
                if any(d.state=="open" for d in rec.invoice_ids) and any(d.amount_total != d.residual for d in rec.invoice_ids) and not any(d.state=="paid" for d in rec.invoice_ids):
                    rec.invoice_check_status = 'partial_paid'
                if any(d.state=="paid" for d in rec.invoice_ids) and any(d.residual == 0.00 for d in rec.invoice_ids):
                    rec.invoice_check_status="paid"

    def action_sale_status(self):
        return 

