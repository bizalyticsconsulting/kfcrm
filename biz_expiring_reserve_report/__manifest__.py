{
    'name': 'Quote Automatic Expiring Reserve Report',
    'summary': """ """,
    'description': """ """,
    'author': 'BizAlytics Consulting LLC',
    'website': 'http://www.bizalytics.com',
    'category': 'Sales/Sales',
    'version': '12.0.1.0',
    'depends': ['showroom_fields','report_xlsx','gt_supplier_disc_aggrement'],
    'data': [
        'views/report_xls_view.xml',
        'data/email_template.xml',
        'data/expiring_reserve_sheduler.xml',
        # 'wizard/expiring_reserve_wizard_view.xml',
    ],
}
