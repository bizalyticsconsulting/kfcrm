from odoo import models
DF = '%m/%d/%Y'


class ExpiringReserveReport(models.AbstractModel):
    _name = 'report.biz_expiring_reserve_report.expiring_reserve_report_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, sales_invoice_report_wizard):
        header = workbook.add_format({'bold': True, 'font_size': 12,'bg_color': '#dee6ef'})
        bold = workbook.add_format({'bold': True, 'font_size': 12})
        money = workbook.add_format({'num_format': '#,##0.00'})

        salesperson_user_id = self.env.context.get('salesperson_user_id', False)
        filter_dates = self.env.context.get('filter_dates', False)
        sheet = workbook.add_worksheet(salesperson_user_id[1][0].branch_id.name + ' ' + salesperson_user_id[0].name)
        sheet.set_column(0, 0, 40)
        sheet.set_column(1, 1, 30)
        sheet.set_column(2, 2, 20)
        sheet.set_column(3, 3, 30)
        sheet.set_column(4, 4, 30)
        sheet.set_column(5, 5, 15)
        sheet.set_column(6, 6, 15)
        sheet.set_column(7, 7, 10)
        sheet.set_column(8, 8, 10)
        sheet.set_column(9, 9, 40)

        sheet.merge_range(0, 0, 0, 1, 'TEXTILE RESERVE EXPIRATION REPORT', bold)
        sheet.merge_range(1, 0, 1, 1, 'Date Range ' + filter_dates[0].strftime(DF) + ' To ' + filter_dates[1].strftime(DF), bold)

        row_index = 2
        sheet.write(row_index, 0, 'Customer', header)
        sheet.write(row_index, 1, 'Supplier', header)
        sheet.write(row_index, 2, 'Quote No.', header)
        sheet.write(row_index, 3, 'Products #', header)
        sheet.write(row_index, 4, 'Products', header)
        sheet.write(row_index, 5, 'Supplier No.', header)
        sheet.write(row_index, 6, 'Quote Date', header)
        sheet.write(row_index, 7, 'Tags', header)
        sheet.write(row_index, 8, 'Total', header)
        sheet.write(row_index, 9, 'Reserve Expiration Date', header)

        row_index = 3
        for order in salesperson_user_id[1]:
            sheet.write(row_index, 0, order.partner_id.name)
            sheet.write(row_index, 1, order.x_supplier.name if order.x_supplier else '')
            sheet.write(row_index, 2, order.name)
            sheet.write(row_index, 3, ' '.join(order.mapped('order_line.product_id.default_code')))
            sheet.write(row_index, 4, ' '.join(order.mapped('order_line.product_id.name')))
            sheet.write(row_index, 5, order.x_mfg_order if order.x_mfg_order else '')
            sheet.write(row_index, 6, order.date_order.strftime(DF) if order.date_order else '')
            sheet.write(row_index, 7, ' '.join(order.mapped('tag_ids.name')) if order.tag_ids else '')
            sheet.write(row_index, 8, order.amount_total, money)
            sheet.write(row_index, 9, order.x_reserve_exp_date.strftime(DF) if order.x_reserve_exp_date else '')
            row_index += 1