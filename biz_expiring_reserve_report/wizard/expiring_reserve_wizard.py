from odoo import models


class ExpiringReserveWizard(models.TransientModel):
    _name = 'expiring.reserve.wizard'

    def action_expiring_reserve_sheduler_cron1(self):
        branches = self.env['res.branch'].search([])
        for branch in branches:
            email_template = self.env.ref('biz_expiring_reserve_report.email_template_biz_expiring_reserve_report')
            if email_template:
                email_template.with_context(branch_id=branch).send_mail(email_template.report_template.id, force_send=True)