from datetime import timedelta
from odoo.tools import groupby
from odoo import models
import datetime


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def action_expiring_reserve_sheduler_cron(self):
        today = datetime.date.today()
        days_until_sunday = 6 - today.weekday()  # 6 represents Sunday
        next_sunday = today + datetime.timedelta(days=days_until_sunday)
        to_date = next_sunday + timedelta(days=6)

        branches = self.env['res.branch'].search([])
        for branch in branches:
            sale_orders = self.env['sale.order'].search([('x_reserve_exp_date', '>=', next_sunday),
                                                         ('x_reserve_exp_date', '<=', to_date),
                                                         ('state', 'in', ['draft', 'sent']),
                                                         ('branch_id', '=', branch.id)
                                                         ], order='x_reserve_exp_date ASC')
            for user_id, sale_orders in groupby(sale_orders, lambda l: l.user_id):
                email_template = self.env.ref('biz_expiring_reserve_report.email_template_biz_expiring_reserve_report')
                if email_template:
                    email_template.partner_to = branch.manager_user_id.partner_id.id
                    email_template.email_to = ''
                    email_template.subject = branch.name + ' ' + 'Reserve Expiration Report' + ' ' + user_id.name
                    email_template.with_context(salesperson_user_id=[user_id, sale_orders], filter_dates=[next_sunday, to_date]).send_mail(email_template.report_template.id, force_send=True)