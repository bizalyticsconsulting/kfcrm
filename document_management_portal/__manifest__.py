#╔══════════════════════════════════════════════════════════════════╗
#║                                                                  ║
#║                ╔═══╦╗       ╔╗  ╔╗     ╔═══╦═══╗                 ║
#║                ║╔═╗║║       ║║ ╔╝╚╗    ║╔═╗║╔═╗║                 ║
#║                ║║ ║║║╔╗╔╦╦══╣╚═╬╗╔╬╗ ╔╗║║ ╚╣╚══╗                 ║
#║                ║╚═╝║║║╚╝╠╣╔╗║╔╗║║║║║ ║║║║ ╔╬══╗║                 ║
#║                ║╔═╗║╚╣║║║║╚╝║║║║║╚╣╚═╝║║╚═╝║╚═╝║                 ║
#║                ╚╝ ╚╩═╩╩╩╩╩═╗╠╝╚╝╚═╩═╗╔╝╚═══╩═══╝                 ║
#║                          ╔═╝║     ╔═╝║                           ║
#║                          ╚══╝     ╚══╝                           ║
#║ SOFTWARE DEVELOPED AND SUPPORTED BY ALMIGHTY CONSULTING SERVICES ║
#║                   COPYRIGHT (C) 2016 - TODAY                     ║
#║                   http://www.almightycs.com                      ║
#║                                                                  ║
#╚══════════════════════════════════════════════════════════════════╝
{
    'name': 'Document Management Portal',
    'version': '1.0.1',
    'category': 'Document Management',
    'author': 'Almighty Consulting Services',
    'support': 'info@almightycs.com',
    'summary': """Share Documents With Portal Users.""",
    'description': """Share Documents With Portal Users.
    portal document management system , customer document management system , document portal
    protal user document access , portal documents , document management system - customer portal 
    Document Management System to manage your company documents inside odoo properly. document management software document directory document access acs
    document management system documents manage document attachment management manage attachments. Document revisions document revision management almightycs
    document preview manage documents manage attachments document portal management portal document access

    Compartir documentos con los usuarios del portal.
     sistema de gestión de documentos del portal, sistema de gestión de documentos del cliente, portal de documentos
     Acceso a documentos de usuario protegido, documentos del portal, sistema de gestión de documentos - portal del cliente
     Sistema de gestión de documentos para gestionar adecuadamente los documentos de su empresa dentro de odoo. software de gestión de documentos directorio de documentos acceso a documentos acs
     sistema de gestión de documentos documentos de gestión de documentos adjuntos gestión de archivos adjuntos. Revisiones de documentos revisión de documentos gestión almightycs
     Vista previa de documentos Administrar documentos Administrar archivos adjuntos Portal de administración Acceso a documentos del portal

    Partager des documents avec les utilisateurs du portail.
     portail système de gestion de documents, système de gestion de documents clients, portail de documents
     accès aux documents utilisateur protal, documents portail, système de gestion de documents - portail client
     Système de gestion de documents pour gérer les documents de votre entreprise dans odoo correctement. logiciel de gestion de documents répertoire de documents accès aux documents acs
     système de gestion des documents documents gérer la gestion des pièces jointes, gérer les pièces jointes. Révisions de document gestion de révision de document tout-puissant
     aperçu des documents gérer les documents gérer les pièces jointes portail de documents portail de gestion accès aux documents
    
    Dokumente für Portalbenutzer freigeben.
     Portal-Dokumentenverwaltungssystem, Kundendokumentenverwaltungssystem, Dokumentenportal
     protal user dokumentenzugriff, portaldokumente, dokumentenmanagementsystem - kundenportal
     Document Management System, um Ihre Unternehmensdokumente in odoo richtig zu verwalten. Dokumentverwaltungssoftware Dokumentverzeichnis Dokumentzugriff acs
     Dokumentverwaltungssystemdokumente Verwalten Sie die Verwaltung von Anhängen. Dokumentrevisionen Dokumentverwaltungsmanagement-Allmächtige
     Dokumentvorschau Dokumente verwalten Anhänge verwalten Dokumentportal-Verwaltungsportal Dokumentzugriff
    """,
    'website': 'https://www.almightycs.com',
    'license': 'OPL-1',
    'depends': ['base', 'document_management', 'portal'],
    'data': [
        'security/ir.model.access.csv',
        'security/portal_security.xml',
        'views/document_management_template.xml',
    ],
    'images': [
        'static/description/document_management_portal_cover.jpg',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'price': 25,
    'currency': 'EUR',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
