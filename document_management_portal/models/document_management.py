# -*- coding: utf-8 -*-

from odoo import _, api, models


class Document(models.Model):
    _name = "document.document"
    _inherit = ['document.document', 'portal.mixin']

    @api.multi
    def get_access_action(self):
        """ Override method that generated the link to access the document. Instead
        of the classic form view, redirect to the post on the website directly """
        self.ensure_one()
        return {
            'type': 'ir.actions.act_url',
            'url': '/my/documents/%s' % self.id,
            'target': 'self',
            'res_id': self.id,
        }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
