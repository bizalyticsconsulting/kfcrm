# -*- coding: utf-8 -*-
from collections import OrderedDict

from odoo import http, _
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager, get_records_pager
from odoo.http import request


class DocumentPortal(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(DocumentPortal, self)._prepare_portal_layout_values()
        user = request.env.user
        document_count = request.env['document.document'].sudo().search_count(['|','|',
                ('directory_id.user_ids', 'in', [user.id]),
                ('message_partner_ids', 'in', [user.partner_id.id]),
                ('directory_id.department_id.member_ids', 'in', user.employee_ids.ids)])
        values.update({
            'document_count': document_count,
        })
        return values

    @http.route(['/my/documents', '/my/documents/page/<int:page>'], type='http', auth="user", website=True)
    def my_documents(self, page=1, sortby=None, **kw):
        values = self._prepare_portal_layout_values()
        user = request.env.user
        if not sortby:
            sortby = 'date'

        sortings = {
            'date': {'label': _('Newest'), 'order': 'create_date desc'},
            'name': {'label': _('Name'), 'order': 'name'},
        }

        order = sortings.get(sortby, sortings['date'])['order']
 
        pager = portal_pager(
            url="/my/documents",
            url_args={},
            total=values['document_count'],
            page=page,
            step=self._items_per_page
        )
        # content according to pager and archive selected
        documents = request.env['document.document'].sudo().search(['|','|',
                ('directory_id.user_ids', 'in', [user.id]),
                ('message_partner_ids', 'in', [user.partner_id.id]),
                ('directory_id.department_id.member_ids', 'in', user.employee_ids.ids)], 
                order=order, limit=self._items_per_page, offset=pager['offset'])

        values.update({
            'sortings': sortings,
            'sortby': sortby,
            'documents': documents,
            'page_name': 'document',
            'default_url': '/my/documents',
            'searchbar_sortings': sortings,
            'pager': pager
        })
        return request.render("document_management_portal.my_documents", values)

    @http.route(['/my/documents/<int:document_id>'], type='http', auth="user", website=True)
    def my_documents_document(self, document_id=None, **kw):
        document = request.env['document.document'].browse(document_id)
        return request.render("document_management_portal.my_documents_document", {'document': document})
