  # -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    'name' : 'Drag and Drop Multiple Files in Odoo',
    'version' : '12.0.0.2',
    'category' : 'Website',
    'summary': 'Drag & Drop Multi Attachments',
    'description': '''
    
    Drag & Drop Multi Attachments
    Drag & Drop Multi Attachments
    Drag & Drop Multi fils
    mutliple files uploader
    multi attachments upload
    drag drop feature
    drag drop files
    drag drop multiple file
    Drag & Drop Multi Attachments in Form View
    Upload multiple files
    dragging and dropping images
    Drag'n'drop Multi File Uploader
    upload multiple files
    Drag & Drop Multiple Files Upload
    Multiple Files Upload
    multiple image upload
    Drag and Drop Multiple File
    Drag and Drop Multiple Attachments
    
    ''',
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 25,
    'currency': 'EUR',
    'depends' : ['base','web'],
    'data': [
        'views/assets.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url':'https://youtu.be/gG4hA3mmFnY',
    'images':['static/description/Banner.png'],
}

