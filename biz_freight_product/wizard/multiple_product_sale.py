# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2019-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)

# create a new class for MultipleProductSale.
class MultipleProductSaleFreight(models.TransientModel):
    _name = "multiple.product.sale.freight"

    # product_ids = fields.Many2many('product.product',string='Products')
    x_freight_cost = fields.Float(string="Freight Cost")
    x_freight_bool = fields.Boolean(string="Taxable")
    x_packing_cost = fields.Float(string="Packing Cost ")
    x_packing_bool = fields.Boolean(string="Taxable")
    x_other_cost = fields.Float(string="Other Cost")
    x_other_bool = fields.Boolean(string="Taxable")
    x_shipping_com = fields.Char(string="Delivery Notes", size=150, help="Delivery Notes")

    @api.model
    def default_get(self, fields):
        defaults = super(MultipleProductSaleFreight, self).default_get(fields)
        if self.env.context.get('sale_order'):
            product_template = self.env['product.template']
            ship_product = product_template.search([('default_code', '=', 'SHIP')], limit=1)
            pack_product = product_template.search([('default_code', '=', 'PACK')], limit=1)
            other_product = product_template.search([('default_code', '=', 'OTHER')], limit=1)

            sale_order_obj = self.env['sale.order'].browse(self.env.context.get('sale_order'))

            if ship_product:
                freight_order_line = sale_order_obj.order_line.filtered(lambda prod: prod.product_id.id == ship_product.id)
                defaults['x_freight_cost'] = freight_order_line.price_unit
            if pack_product:
                pack_order_line = sale_order_obj.order_line.filtered(lambda prod: prod.product_id.id == pack_product.id)
                defaults['x_packing_cost'] = pack_order_line.price_unit
            if other_product:
                other_order_line = sale_order_obj.order_line.filtered(lambda prod: prod.product_id.id == other_product.id)
                defaults['x_other_cost'] = other_order_line.price_unit
            if sale_order_obj.x_shipping_com:
                defaults['x_shipping_com'] = sale_order_obj.x_shipping_com

        return defaults

    def add_multiple_product_sale_freight(self):
        ctx = self.env.context.copy()
        ctx.update({'freight_pack_data': True, 'freight_pack_data_update': True})
        order_line_object = self.env['sale.order.line']
        product_template = self.env['product.template']

        if self.env.context.get('active_model') == 'sale.order':
            active_id = self.env.context.get('active_id', False)
            order_id = self.env['sale.order'].search([('id', '=', active_id)])
            freight_product_id = product_template.search([('default_code', '=', 'SHIP')], limit=1)
            pack_product_id = product_template.search([('default_code', '=', 'PACK')], limit=1)
            other_product_id = product_template.search([('default_code', '=', 'OTHER')], limit=1)

            if order_id:
                tax_id = False
                if self.x_freight_bool or self.x_packing_bool or self.x_other_bool:
                    # Need to get the first item for tax purposes
                    self.env.cr.execute(
                        f"select id from sale_order_line where product_id in (select pp.id from product_product pp,product_template pt where pp.product_tmpl_id = pt.id and pt.default_code not in ('SHIP','PACK','OTHER')) and order_id = {order_id.id} order by id limit 1")
                    so_line = self.env.cr.fetchone()[0]
                    so_line_id = order_line_object.browse(so_line)
                    tax_id = so_line_id.tax_id if so_line_id.tax_id else False

                if freight_product_id and self.x_freight_cost > 0:
                    order_line_id = order_line_object.search(
                        [('order_id', '=', order_id.id), ('product_id', '=', freight_product_id.id)])
                    if order_line_id:
                        #self.x_freight_cost = order_line_id.price_unit + self.x_freight_cost
                        # order_line_id.price_unit = self.x_freight_cost
                        order_line_id.with_context(ctx).write({'price_unit': self.x_freight_cost})
                    else:
                        order_line_dict = {
                            'order_id': order_id.id,
                            'product_id': freight_product_id.id,
                            # 'product_id': product_id.id,
                            'price_unit': self.x_freight_cost,
                            'product_qty': 1,
                            'product_uom_qty': 1,
                            'sequence': 100000
                        }
                        order_line_id = order_line_object.with_context(ctx).create(order_line_dict)

                    if self.x_freight_bool:
                        order_line_id.tax_id = tax_id

                if pack_product_id and self.x_packing_cost > 0:
                    order_line_id = order_line_object.search(
                        [('order_id', '=', order_id.id), ('product_id', '=', pack_product_id.id)])
                    if order_line_id:
                        #self.x_packing_cost = order_line_id.price_unit + self.x_packing_cost
                        # order_line_id.price_unit = self.x_packing_cost
                        order_line_id.with_context(ctx).write({'price_unit': self.x_packing_cost})
                    else:
                        order_line_dict = {
                            'order_id': order_id.id,
                            'product_id': pack_product_id.id,
                            'price_unit': self.x_packing_cost,
                            'product_qty': 1,
                            'product_uom_qty': 1,
                            'sequence': 100000
                        }
                        order_line_id = order_line_object.with_context(ctx).create(order_line_dict)

                    if self.x_packing_bool:
                        order_line_id.tax_id = tax_id

                if other_product_id and self.x_other_cost > 0:
                    order_line_id = order_line_object.search(
                        [('order_id', '=', order_id.id), ('product_id', '=', other_product_id.id)])
                    if order_line_id:
                        #self.x_other_cost = order_line_id.price_unit + self.x_other_cost
                        # order_line_id.price_unit = self.x_other_cost
                        order_line_id.with_context(ctx).write({'price_unit': self.x_other_cost})
                    else:
                        order_line_dict = {
                            'order_id': order_id.id,
                            'product_id': other_product_id.id,
                            'price_unit': self.x_other_cost,
                            'product_qty': 1,
                            'product_uom_qty': 1,
                            'sequence': 100000
                        }
                        order_line_id = order_line_object.with_context(ctx).create(order_line_dict)

                    if self.x_other_bool:
                        order_line_id.tax_id = tax_id

                if self.x_shipping_com:
                    order_id.x_shipping_com = self.x_shipping_com