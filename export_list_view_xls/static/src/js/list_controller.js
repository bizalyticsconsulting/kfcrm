
odoo.define('export_list_view_xls.ListController', function (require) {
"use strict";


var core = require('web.core');
var DataExport = require('web.DataExport');
var ListController = require('web.ListController');
var session = require('web.session');
var _t = core._t;
var qweb = core.qweb;


ListController.include({                // Add the menu in side bar in icon
	init: function () {
			this._super.apply(this, arguments);
			var button = this.$buttons
		},
		renderButtons: function() {
			this._super.apply(this, arguments)
			if(this.$buttons) {
					this.$buttons.on('click', '.export_treeview_xls', this._onDirectExport.bind(this));
			}
		},

	_getExportDialogWidget() {
        let state = this.model.get(this.handle);
        let defaultExportFields = this.renderer.columns.filter(field => field.tag === 'field').map(field => field.attrs.name);
        let groupedBy = this.renderer.state.groupedBy;
        return new DataExport(this, state, defaultExportFields, groupedBy,
            this.getActiveDomain(), this.getSelectedIds())
    },

     _onDirectExport: function (event, node) {
        this._getExportDialogWidget().export();
     },
});
});
