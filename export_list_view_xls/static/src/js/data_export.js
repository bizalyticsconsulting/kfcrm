odoo.define('export_list_view_xls.DataExport', function (require) {
"use strict";


var core = require('web.core');
var DataExport = require('web.DataExport');
var framework = require('web.framework');
var pyUtils = require('web.py_utils');
var crash_manager = require('web.crash_manager');
var data = require('web.data');
var _t = core._t;



DataExport.include({                // Add the menu in side bar in icon
	init: function (parent, record, defaultExportFields, groupedBy, activeDomain, idsToExport) {
			this._super.apply(this, arguments);
			this.record = record;
            this.defaultExportFields = defaultExportFields;
            this.groupby = groupedBy;
            this.domain =  this.record.domain;
            this.idsToExport = this.domain  ? false: idsToExport;
		},

	export() {
        let exportedFields = this.defaultExportFields.map(field => ({
            name: field,
            label: this.record.fields[field].string,
        }));
        this._exportData(exportedFields, 'xls', false);
    },

	 _exportData(exportedFields, exportFormat, idsToExport) {

        if (_.isEmpty(exportedFields)) {
            Dialog.alert(this, _t("Please select fields to export..."));
            return;
        }

        framework.blockUI();
        this.getSession().get_file({
            url: '/web/export/' + exportFormat,
            data: {
                data: JSON.stringify({
                    model: this.record.model,
                    fields: exportedFields,
                    ids: idsToExport,
                    domain: this.domain,
                    context: pyUtils.eval('contexts', [this.record.getContext()]),
                    import_compat: false,
                })
            },
            complete: framework.unblockUI,
        });
    },
});
});
