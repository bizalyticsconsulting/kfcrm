{
    'name': 'Export List View to Excel',
    'version': '12.0.1.0.1',
    'summary': """
        This module will enable you to export all list view records or search filter records to excel.""",

    'description': """
This module will enable you to export all list view records or search filter records to excel.
Export list view
Export to Excel
list view export
export all list view record
export list view with filter
export all records
export
excel
list
list view
save to excel
     """,
    'category': 'Web',
    'author': 'CorTex IT Solutions Ltd.',
    'website': 'https://cortexsolutions.net',
    'license': 'OPL-1',
    'currency': 'EUR',
    'price': 25,
    'support': 'support@cortexsolutions.net',
    'depends': [
        'web',
    ],
    "data": [
        'views/export_list_view_xls_views.xml',
    ],
    'qweb': [
        "static/src/xml/export_list_view_xls_template.xml",
    ],
    'installable': True,
    'auto_install': False,
    'images': ['static/description/main_screenshot.png','static/description/main_1.png'],
}
