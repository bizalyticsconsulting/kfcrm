v1.0.1
======
* Change the style of export button.
* Add keyboard shortcut ALT + x for export button.

v1.0.0
======
* This module will enable you to export all list view records or search filter records to excel.