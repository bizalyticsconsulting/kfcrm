# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime
# from odoo.exceptions import ValidationError

class SaleCommissionReport(models.TransientModel):
    _inherit = 'sale.commission.report'

    partner_id = fields.Many2one('res.partner', 'Showroom Partner', domain=[('partner_type','=','showroom')])
    commission_pay = fields.Selection([('salesperson','Sales Person'), ('showroom_partner','Showroom Partner')], string="Commission Pay")
     
    @api.onchange('commission_pay')
    def _get_commission_pay(self):
        for rec in self:
            rec.partner_id = False
            rec.sale_person = False
            
class AllCommissionReport(models.TransientModel):
    _inherit = 'all.commission.report'
    
    paid_unpaid_group = fields.Boolean('Paid/Unpaid Group')
    
    @api.onchange('type')
    def onchange_paid_unpaid(self):
        self.paid_unpaid_group = False
 