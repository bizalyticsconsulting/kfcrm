# -*- coding: utf-8 -*-

from odoo import fields,models ,api, _
import time

class AllCommission(models.AbstractModel):
    _inherit = 'commission.report'

    def get_movelines(self, commission_report_id):
        wizard_obj = self.env['all.commission.report']
        wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        move_line_obj = self.env['account.move.line']
        unpaid_domain = [('paid','=',False), ('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date), ('credit', '!=', 0)]
        paid_domain = [('paid','=',True), ('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date), ('credit', '!=', 0)]
        if wizard_search.type == 'payable':
            move_line_search = move_line_obj.search(unpaid_domain)
        if wizard_search.type == 'paid':
            move_line_search = move_line_obj.search(paid_domain)
        if wizard_search.type == 'all':
            move_line_search = move_line_obj.search([('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date), ('credit', '!=', 0)])
        if wizard_search.paid_unpaid_group:
            unpaid_move_line_search  = move_line_obj.search(unpaid_domain)
            paid_move_line_search = move_line_obj.search(paid_domain)
            return [paid_move_line_search,unpaid_move_line_search]
        return move_line_search

    @api.model
    def render_html(self, docids, data=None):
        wizard_obj = self.env['all.commission.report']
        wizard_search = wizard_obj.search([('id','=',docids)])
        ctx = self._context.copy()
        ctx.update({'active_ids': docids})
        self.context = ctx
        docargs = {
            'doc_ids': docids,
            'doc_model': 'res.partner',
            'docs': self.context['active_ids'],
            'type': wizard_search.type,
            'data': {'docs': docids},
            'get_movelines': self.get_movelines,
            'currency_id': self.env.user.company_id.currency_id,
            'paid_unpaid_group': wizard_search.paid_unpaid_group,
        }
        return self.env['report'].render('sale_commission_gt.all_commission_report', docargs)


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: