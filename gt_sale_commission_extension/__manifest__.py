 # -*- coding: utf-8 -*-

{
    'name': 'GT Sales Commission Extension',
    'version': '1.0',
    'category': 'Sale',
    'sequence': 2,
    'summary': 'GT Sales Commission Extension',
    'description': """ Odoo 10 Sales Commission/Incentive Calculations Based on Sales, Payments Received, Invoicing and Fully invoice payment & Shipped and showroom partners, Easily Manage Complex Commission or Incentive Plans to Enhance Corporate Goals """,
    'author': 'Globalteckz',
    'website': 'https://www.globalteckz.com',
    'depends': ['sale_commission_gt', 'showroom_fields'],
    'data': [
                'views/product_view.xml',
                'views/res_partner_view.xml',
                'views/sale_view.xml',
                 'views/sale_commission_view.xml',
                'wizard/commission_report_views.xml',
                'report/payable_commission_report.xml',
                'report/paid_commission_report.xml',
#                 'report/commission_check_report.xml',
                'report/all_commission_report.xml',
#                 'views/check_print_view.xml',
                'security/ir.model.access.csv',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
