from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    partner_type = fields.Selection([('showroom', 'Showroom'),('client','Customer'), ('ship to','Ship To'),('vendor', 'Supplier'), ('specifier', 'Specifier'),
                                     ('purchasing','Purchasing'),('personal','Personal'),('employee','Employee')], string='Type')
    showroom_type = fields.Selection([('showroom_bill','Showroom Bill'),('vendor_bill','Vendor Bill')], string='Billing Option', index=1)
    commissions_percentage = fields.Float(string='Commission Percentage')
    is_part_comm = fields.Boolean('Is Partner Commission ?')
    
    

