# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"
    
    commission_pay = fields.Selection([('salesperson','Sales Person'), ('showroom_partner','Showroom Partner')], string="Commission Pay")
    sale_order_id = fields.Many2one('sale.order', 'Sale Order', copy=False)
    commission_partner = fields.Many2one('res.partner', string="Supplier", context="{'so_supplier_ctx': True}")   # 'Partner'
    check_no = fields.Integer("Check Number #")
    paid_move_id = fields.Many2one('account.move', 'Paid Move')
    pay_reference = fields.Char('Reference #')
    commission_on = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
        ('3', 'Full Payment & Shipped Validation'),
    ], "When Paid")
    commissionable_amount = fields.Float(string="Rec Comm Amt",
                                help="This will carry the Product total amount without Tax") # Commissionable Amount
    commission_rate = fields.Float("Rec Comm Rate")   # Commission Rate
    sales_commission_rate = fields.Float("Sales Commission Rate")
    total_commission = fields.Float("Commisison Amount")
    # modified_commission = fields.Float("Modified Commission Amount")

    @api.onchange('sales_commission_rate')
    def onchange_commission_rate_amount(self):
        for rec in self:
            if rec.commissionable_amount and rec.commission_rate and rec.total_commission:
                rec.credit = rec.commissionable_amount * rec.sales_commission_rate / 100
                # rec.sales_commission_rate = rec.commissionable_amount / rec.credit

    # @api.onchange('credit')
    # def onchange_commission_amount(self):
    #     for rec in self:
    #         if rec.commissionable_amount and rec.commission_rate and rec.total_commission:
    #             rec.sales_commission_rate = rec.commissionable_amount / rec.credit

class AccountMove(models.Model):
    _inherit = 'account.move'

    commission_partner = fields.Many2one('res.partner', 'Partner')
    check_no = fields.Integer("Check Number #")

    @api.multi
    def assert_balanced(self):
        """
        This method is orverride for 2 time.
        1. For calculate the commission amount changes task
        2. For add new sale order line after confiremd and invoice is in 'open' state'. update journal entries(account.move.line)
        """
        if not self.ids:
            return True
        # If the Commission Lines, then modify the debit amount based on the credit amount(if any changes from the SaleOrder Commission Lines)
        check_commission_lines = any([i for i in self.line_ids if i.commissionable_amount and i.commission_rate and i.total_commission])
        if check_commission_lines:
            credit_line_id = self.env['account.move.line']
            debit_line_id = self.env['account.move.line']
            if len(self.line_ids) == 2:
                # credit_line_id = debit_line_id = False
                for line in self.line_ids:
                    if line.debit == 0:        # line.credit:  # Comment for correctly come both values credit/debit in case changes any one
                        credit_line_id = line
                    if line.credit == 0:       # line.debit:   # 09.12.2021
                        debit_line_id = line
                if credit_line_id and debit_line_id:
                    # float("{:.3f}".format(x))
                    credit_line_amount = credit_line_id.credit
                    debit_line_amount = debit_line_id.debit
                    credit_line_amount = float("{:.3f}".format(credit_line_id.credit)) # credit_line_id.credit
                    debit_line_amount = float("{:.3f}".format(debit_line_id.debit)) #debit_line_id.debit
                    if credit_line_amount != debit_line_amount:
                        debit_line_id.debit = credit_line_id.credit
                    
        elif self.env.context.get('showroom_move_line_update'):
            pass
            ### 28.08.2020 Allow to modify in that case....Showroom_fields-->models.py-->[create_new_account_move_line()]
                    
        else:
            # /!\ As this method is called in create / write, we can't make the assumption the computed stored fields
            # are already done. Then, this query MUST NOT depend of computed stored fields (e.g. balance).
            # It happens as the ORM makes the create with the 'no_recompute' statement.
            self._cr.execute('''
                       SELECT line.move_id, ROUND(SUM(line.debit - line.credit), currency.decimal_places)
                       FROM account_move_line line
                       JOIN account_move move ON move.id = line.move_id
                       JOIN account_journal journal ON journal.id = move.journal_id
                       JOIN res_company company ON company.id = journal.company_id
                       JOIN res_currency currency ON currency.id = company.currency_id
                       WHERE line.move_id IN %s
                       GROUP BY line.move_id, currency.decimal_places
                       HAVING ROUND(SUM(line.debit - line.credit), currency.decimal_places) != 0.0;
                   ''', [tuple(self.ids)])

            res = self._cr.fetchone()
            if res:
                raise UserError(
                    _("Cannot create unbalanced journal entry.") +
                    "\n\n{}{}".format(_('Difference debit - credit: '), res[1])
                )

            ## This is old code for odoo-10 version default 21.01.2021

            # prec = self.env['decimal.precision'].precision_get('Account')
            # self._cr.execute("""\
            #     SELECT      move_id
            #     FROM        account_move_line
            #     WHERE       move_id in %s
            #     GROUP BY    move_id
            #     HAVING      abs(sum(debit) - sum(credit)) > %s
            #     """, (tuple(self.ids), 10 ** (-max(5, prec))))
            #
            # if len(self._cr.fetchall()) != 0:
            #     raise UserError(_("Cannot create unbalanced journal entry."))
        return True
    
class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
        ('3', 'Full Payment & Shipped Validation'),
    ], "When Paid",copy=False)
    
    @api.model
    def create(self, vals):
        if vals.get('origin'):
            sale_order_id = self.env['sale.order'].search([('name','=',vals.get('origin'))])
            if sale_order_id:
                vals.update({'order_pay_state': sale_order_id.order_pay_state})
        res = super(AccountInvoice, self).create(vals)
        return res
        
    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref):
#         commission_journal = self.env['ir.values'].get_default('account.config.settings', 'commission_journal')
        commission_journal = int(self.env['ir.config_parameter'].sudo().get_param('account.commission_journal'))
        _logger.debug("commission_journalllllllllll - " + commission_journal)
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_journal)
        sale_obj = self.env['sale.order']
        sale_search = sale_obj.search([('name', '=', self.origin)])
        prod_commissinable_amt = sum([i.price_subtotal for i in sale_search.order_line if i.product_id.flag])
        if self.env.context.get('validate_partner_contxt'):
            commission_pay = 'showroom_partner'
            comm_partner_id = sale_person.id
            sales_user = False
            commission_partner = comm_partner_id
        else:
            commission_pay = 'salesperson'
            comm_partner_id = sale_person.partner_id.id
            sales_user = sale_person.id
            commission_partner = False
        commission_vals = {
            'name': account_id.default_debit_account_id.name,
            'debit': commission_amt,
            'credit': 0.0,
            'account_id': account_id.default_debit_account_id.id,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_on': self.order_pay_state,
            'commission_partner': commission_partner,
            'sale_order_id': sale_search.id,
            'commission_pay': commission_pay,
            'commissionable_amount': prod_commissinable_amt,
            'commission_rate': sale_search.x_showroom.commissions_percentage,
            'total_commission': commission_amt,
            'sales_commission_rate': sale_search.x_showroom.commissions_percentage,
        }
        sale_person_vals = {
            'name': sale_person.name,
            'debit': 0.0,
            'credit': commission_amt,
            'account_id': sale_person.property_account_payable_id.id,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_on': self.order_pay_state,
            'commission_partner': commission_partner,
            'sale_order_id': sale_search.id,
            'commission_pay': commission_pay,
            'commissionable_amount': prod_commissinable_amt,
            'commission_rate': sale_search.x_showroom.commissions_percentage,
            'total_commission': commission_amt,
            'sales_commission_rate': sale_search.x_showroom.commissions_percentage,
        }
        now = datetime.now()
        vals = {
            'journal_id': account_id.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': journal_ref,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_partner': commission_partner,
        }
        move = self.env['account.move'].create(vals)
        # move.post()
        return move.id

    @api.multi
    def action_invoice_open(self):
        commission_type = self.env['ir.config_parameter'].search([('key', '=', 'gt_sale_commission_extension.sale.commission_calculation')]).value
        res = super(AccountInvoice, self).action_invoice_open()
        sale_obj = self.env['sale.order']
        sale_search = sale_obj.search([('name', '=', self.origin)])
        if self.origin and self.number:
            reference = '[' + self.origin + '] ' + self.number
        elif self.number:
            reference = '[' + self.number + '] '
        elif self.origin:
            reference = '[' + self.origin + '] '
        else:
             reference = False
        ctx = self.env.context.copy()
        if self.order_pay_state == '1':
            if commission_type == '0':
                total_invoice_commission = 0
                for invoice_line_id in self.invoice_line_ids:
                    _logger.debug("invoice_line_id - " + invoice_line_id)
                    for product_line in sale_search.order_line:
                        _logger.debug("product_line  " + product_line)
                        if product_line.product_id.prod_comm_per and product_line.product_id.prod_comm_per > 0:
                            product_commission = (invoice_line_id.price_subtotal * product_line.product_id.prod_comm_per) / 100
                            total_invoice_commission += product_commission
                self.commission_count_entries(total_invoice_commission, self.user_id, reference)

            if commission_type == '1':
                total_invoice_commission = 0
                for invoice_line_id in self.invoice_line_ids:
                    for product_line in sale_search.order_line:
                        if product_line.product_id.categ_id.prod_categ_comm_per and product_line.product_id.categ_id.prod_categ_comm_per > 0:
                            product_categ_commission = (invoice_line_id.price_subtotal * product_line.product_id.categ_id.prod_categ_comm_per) / 100
                            total_invoice_commission += product_categ_commission
                self.commission_count_entries(total_invoice_commission, self.user_id, reference)

            if commission_type == '2':
                if self.team_id.sale_manage_comm and self.team_id.sale_manage_comm > 0:
                    manager_commission = (self.amount_untaxed * self.team_id.sale_manage_comm) / 100
                    self.commission_count_entries(manager_commission, self.team_id.user_id, reference)

                if self.team_id.sale_member_comm and self.team_id.sale_member_comm > 0:
                    member_commission = (self.amount_untaxed * self.team_id.sale_member_comm) / 100
                    self.commission_count_entries(member_commission, self.user_id, reference)
                    
            #Showroom Partner Commission
            if commission_type == '3':
                ctx.update({'validate_partner_contxt': True})
                total_invoice_commission = 0
                commissionable_so = False
                for invoice_line_id in self.invoice_line_ids:
                    for line_id in sale_search.order_line:
                        if sale_search.x_showroom and sale_search.x_showroom.commissions_percentage > 0 and line_id.product_id.flag: #self.x_showroom
                            product_commission = (invoice_line_id.price_subtotal * sale_search.x_showroom.commissions_percentage) / 100
                            total_invoice_commission += product_commission
                            commissionable_so = True
                #if commissionable_so:
                #    #if sale_search.x_showroom:
                #    #    self.with_context(ctx).commission_count_entries(total_invoice_commission, sale_search.x_showroom, reference)
                #    #else:
                #    #    raise UserError(_("Please select Showroom Partner in SaleOrder for the commission !"))
        return res

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
        ('3', 'Full Payment & Shipped Validation'),
    ], "When Paid", copy=False)

    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref):
        commission_journal = int(self.env['ir.config_parameter'].sudo().get_param('account.commission_journal'))
        _logger.debug("commission_journal22222222222 - " + commission_journal)
        commission_type = self.env['ir.config_parameter'].search([('key', '=', 'gt_sale_commission_extension.sale.commission_calculation')]).value
        _logger.debug("commission_journal22222222222- - " + commission_type)
        account_obj = self.env['account.journal']
        invoice_obj = self.env['account.invoice']
        sale_obj = self.env['sale.order']
        account_id = account_obj.browse(commission_journal)
        invoice_id = invoice_obj.browse(self._context.get('active_id'))
        if not invoice_id:
            invoice_id = self.invoice_ids[0]
        sale_search = sale_obj.search([('name', '=', invoice_id.origin)])
        if not sale_search:
            sale_search = sale_obj.search([('name', '=', self.communication)], order="id desc", limit=1)
            self.write({'order_pay_state': sale_search.order_pay_state})
        prod_commissinable_amt = sum([i.price_subtotal for i in sale_search.order_line if i.product_id.flag])
        
        if self.env.context.get('payment_partner_contxt'):
            commission_pay = 'showroom_partner'
            comm_partner_id = sale_person.id
            sales_user = False
            commission_partner = comm_partner_id
        else:
            commission_pay = 'salesperson'
            comm_partner_id = sale_person.partner_id.id
            sales_user = sale_person.id
            commission_partner = False
        
        commission_vals = {
            'name': account_id.default_debit_account_id.name,
            'debit': commission_amt,
            'credit': 0.0,
            'account_id': account_id.default_debit_account_id.id,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_on': self.order_pay_state,
            'commission_partner': commission_partner,
            'sale_order_id': sale_search.id,
            'commission_pay': commission_pay,
            'commissionable_amount': prod_commissinable_amt,
            'commission_rate': sale_search.x_showroom.commissions_percentage,
            'total_commission': commission_amt,
            'sales_commission_rate': sale_search.x_showroom.commissions_percentage,
        }
        sale_person_vals = {
            'name': sale_person.name,
            'debit': 0.0,
            'credit': commission_amt,
            'account_id': sale_person.property_account_payable_id.id,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_on': self.order_pay_state,
            'commission_partner': commission_partner,
            'sale_order_id': sale_search.id,
            'commission_pay': commission_pay,
            'commissionable_amount': prod_commissinable_amt,
            'commission_rate': sale_search.x_showroom.commissions_percentage,
            'total_commission': commission_amt,
            'sales_commission_rate': sale_search.x_showroom.commissions_percentage,
        }
        now = datetime.now()
        vals = {
            'journal_id': account_id.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': journal_ref,
            'partner_id': comm_partner_id,
            'commission_user': sales_user,
            'commission_partner': commission_partner,
        }
        move = self.env['account.move'].create(vals)
        move.post()
        return move.id

    @api.multi
    def post(self):
#         commission_type = self.env['ir.values'].get_default('sale.config.settings', 'commission_calculation')
        commission_type = self.env['ir.config_parameter'].search([('key', '=', 'gt_sale_commission_extension.sale.commission_calculation')]).value
        res = super(AccountPayment, self).post()
        invoice_obj = self.env['account.invoice']
        sale_obj = self.env['sale.order']
        invoice_id = invoice_obj.browse(self._context.get('active_id'))
        _logger.debug("invoice_id///// - " + str(invoice_id.order_pay_state))
        ctx = self.env.context.copy()
        for rec in self:
            _logger.debug("recrecrec<<<<<<<< - " + str(rec.order_pay_state))

            #### 27.02.2021..No need of this flow now for Income commission
            continue  # Just skipping the below code...Unnecessary calling code


            if not invoice_id:
                invoice_id = rec.invoice_ids[0]
            sale_id = sale_obj.search([('name', '=', invoice_id.origin)])
            rec.write({'order_pay_state': invoice_id.order_pay_state})
            if not sale_id:
                sale_id = sale_obj.search([('name', '=', rec.communication)], order="id desc", limit=1)
                rec.write({'order_pay_state': sale_id.order_pay_state})

            if sale_id and invoice_id:
                
                reference = '[' + sale_id.name + '] ' + invoice_id.number + ' Payment'
                _logger.debug("referencereference- " + reference)
            elif sale_id:
                reference = '[' + sale_id.name + '] ' + ' Payment'
            elif invoice_id:
                reference = invoice_id.number + ' Payment'

            # sale_id = sale_obj.search([('name', '=', invoice_id.origin)])
            if rec.order_pay_state == '2':
                if commission_type == '0':
                    total_commission = 0
                    for product_line in sale_id.order_line:
                        if product_line.product_id.prod_comm_per and product_line.product_id.prod_comm_per > 0:
                            total_commission = (rec.amount * product_line.product_id.prod_comm_per) / 100
                    rec.commission_count_entries(total_commission, sale_id.user_id, reference)
    
                if commission_type == '1':
                    for product_line in sale_id.order_line:
                        if product_line.product_id.categ_id.prod_categ_comm_per and product_line.product_id.categ_id.prod_categ_comm_per > 0:
                            total_commission = (rec.amount * product_line.product_id.categ_id.prod_categ_comm_per) / 100
                    rec.commission_count_entries(total_commission, sale_id.user_id, reference)
    
                if commission_type == '2':
                    manage_total_commission = (rec.amount * sale_id.commission_count_manage) / 100
                    rec.commission_count_entries(manage_total_commission, sale_id.team_id.user_id, reference)
                    member_total_commission = (rec.amount * sale_id.commission_count_member) / 100
                    rec.commission_count_entries(member_total_commission, sale_id.team_id.user_id, reference)
                
                #Showroom Partner Commission
                if commission_type == '3':
                    ctx.update({'payment_partner_contxt': True})
                    total_commission = 0
                    commissionable_so = False
                    for product_line in sale_id.order_line:
                        if sale_id.x_showroom and sale_id.x_showroom.commissions_percentage > 0 and product_line.product_id.flag: #self.x_showroom
                            total_commission = (rec.amount * sale_id.x_showroom.commissions_percentage) / 100
                            commissionable_so = True
                    if commissionable_so:
                        if sale_id.x_showroom:
                            rec.with_context(ctx).commission_count_entries(total_commission, sale_id.x_showroom, reference)
                        else:
                            raise UserError(_("Please select Showroom Partner in SaleOrder for the commission !"))
               
            #Full Payment and Shipped Delivery Order     
            if rec.order_pay_state == '3':
                #Check for the FUll PAYMENT against the SaleOrder and Full DeliveryOrder FUlly Shipped
                invoice_payments = sum([k.amount for j in sale_id.invoice_ids for k in j.payment_ids if k.state in ['posted','reconciled']])
                if sale_id.amount_total <= invoice_payments:
                    #Check for the Fully Shipped
                    not_done_state = any([i for i in sale_id.picking_ids if i.state != 'done'])
                    if not_done_state:
                        return res
                    line_prod_not_full_delvd = any([i for i in sale_id.order_line if i.product_id.type != 'service' if i.product_uom_qty > i.qty_delivered])
                    if line_prod_not_full_delvd:
                        return res
                    if not line_prod_not_full_delvd and not sale_id.account_move_line_ids:
                        if commission_type == '0':
                            for product_line in sale_id.order_line:
                                if product_line.product_id.prod_comm_per and product_line.product_id.prod_comm_per > 0:
                                    total_commission = (rec.amount * product_line.product_id.prod_comm_per) / 100
                            rec.commission_count_entries(total_commission, sale_id.user_id, reference)
            
                        if commission_type == '1':
                            for product_line in sale_id.order_line:
                                if product_line.product_id.categ_id.prod_categ_comm_per and product_line.product_id.categ_id.prod_categ_comm_per > 0:
                                    total_commission = (rec.amount * product_line.product_id.categ_id.prod_categ_comm_per) / 100
                            rec.commission_count_entries(total_commission, sale_id.user_id, reference)
                            
                        if commission_type == '2':
                            if sale_id.team_id.sale_manage_comm and sale_id.team_id.sale_manage_comm > 0:
                                manager_commission = (sale_id.amount_untaxed * sale_id.team_id.sale_manage_comm) / 100
                                rec.commission_count_entries(manager_commission, sale_id.team_id.user_id, reference)
                            if sale_id.team_id.sale_member_comm and sale_id.team_id.sale_member_comm > 0:
                                member_commission = (sale_id.amount_untaxed * sale_id.team_id.sale_member_comm) / 100
                                rec.commission_count_entries(member_commission, sale_id.user_id, reference)
                        # if commission_type == '2':
                        #     manage_total_commission = (self.amount * sale_id.commission_count_manage) / 100
                        #     reference = '[' + sale_id.name + '] ' + invoice_id.number + ' Payment'
                        #     self.commission_count_entries(manage_total_commission, sale_id.team_id.user_id, reference)
                        #     member_total_commission = (self.amount * sale_id.commission_count_member) / 100
                        #     self.commission_count_entries(member_total_commission, sale_id.team_id.user_id, reference)
                            
                        #Showroom Partner Commission
                        if commission_type == '3':
                            _logger.debug("Commission Type 3 ... ")
                            ctx.update({'payment_partner_contxt': True})
                            total_order_commission = 0
                            commissionable_so = False

                            for product_line in sale_id.order_line:
                                if sale_id.x_showroom and sale_id.x_showroom.commissions_percentage > 0 and product_line.product_id.flag: #self.x_showroom
                                    product_commission = (
                                                         product_line.price_subtotal * sale_id.x_showroom.commissions_percentage) / 100
                                    total_order_commission += product_commission
                                    commissionable_so = True
                                    # total_order_commission = (self.amount * sale_id.x_showroom.commissions_percentage) / 100
                            if commissionable_so:
                                if sale_id.x_showroom:
                                    rec.with_context(ctx).commission_count_entries(total_order_commission, sale_id.x_showroom, reference)
                                else:
                                    raise UserError(_("Please select Showroom Partner in SaleOrder for the commission !"))
                                # sale_id.commission_generated = True
        return res
