# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime

class SaleCommission(models.TransientModel):
    _inherit = 'sale.commission'
    
    partner_id = fields.Many2one('res.partner', 'Showroom Partner', domain=[('partner_type','=','showroom')])
    commission_pay = fields.Selection([('salesperson','Sales Person'), ('showroom_partner','Showroom Partner')], string="Commission Pay")
    p_move_lines = fields.One2many('account.move.line', 'commission_partner', string='Commission Lines')
    check_payment = fields.Boolean("Check Payment")
    check_number = fields.Integer('Check Number #')
    pay_reference = fields.Char('Reference #')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    commission_account_lines = fields.One2many("sale.commission.account.line", "sale_commission_id", "Commission Lines")
    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id)
    
    @api.onchange('partner_id','start_date','end_date')
    def _get_partner_move_lines(self):
        move_line_obj = self.env['account.move.line']
        search_domain = [('commission_partner','=',self.partner_id.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)]
        if self.start_date:
            search_domain.append(('date','>=',self.start_date))
        if self.end_date:
            search_domain.append(('date','<=',self.end_date))
        move_line_search = move_line_obj.search(search_domain)
        # total_amount = 0
        commission_lines = []
        if self.partner_id:
            for mid in move_line_search:
                # total_amount += mid.credit
                commission_lines.append((0,0,{'date':mid.date, 'reference':mid.ref, 'move_ref': mid.move_id.name, 'amount':mid.credit, 'account_move_line_id':mid.id, 'sale_commission_id': self.id, 'commissionable_amount': mid.commissionable_amount, 'commission_rate': mid.commission_rate, 'sales_commission_rate': mid.sales_commission_rate, 'total_commission': mid.total_commission, 'journal_id': mid.journal_id.id}))
                # self.p_move_lines = move_line_search.ids
                # self.payment_amt = total_amount
        self.update({'commission_account_lines': commission_lines})

    @api.onchange('sale_person','start_date','end_date')
    def _get_move_lines(self):
        move_line_obj = self.env['account.move.line']
        search_domain = [('commission_user','=',self.sale_person.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)]
        if self.start_date:
            search_domain.append(('date','>=',self.start_date))
        if self.end_date:
            search_domain.append(('date','<=',self.end_date))
        move_line_search = move_line_obj.search(search_domain)
        total_amount = 0
        commission_lines = []
        if self.sale_person:
            for mid in move_line_search:
                # total_amount += mid.credit
                commission_lines.append((0,0,{'date':mid.date, 'reference':mid.ref, 'move_ref': mid.move_id.name,'amount':mid.credit, 'account_move_line_id':mid.id, 'sale_commission_id': self.id}))
                # self.move_lines = move_line_search.ids
                # self.payment_amt = total_amount
        self.update({'commission_account_lines': commission_lines})
                
    @api.onchange('commission_pay')
    def _get_commission_pay(self):
        for rec in self:
            rec.partner_id = False
            rec.sale_person = False
            rec.commission_lines = False

    @api.onchange('commission_account_lines')
    def onchange_payment_amt(self):
        self.payment_amt = sum([i.amount for i in self.commission_account_lines])
        
    @api.multi
    def do_payment(self):
        move_line_obj = self.env['account.move.line']
        paid_commission_obj = self.env['search.commission.paid.check']
        if self.commission_pay == 'salesperson':
            search_domain = [('commission_user', '=', self.sale_person.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)]
            if self.start_date:
                search_domain.append(('date','>=',self.start_date))
            if self.end_date:
                search_domain.append(('date','<=',self.end_date))
            move_line_search = move_line_obj.search(search_domain)
            commission_partner = self.sale_person.partner_id
            commission_user = self.sale_person.id
            comm_partner = False
        elif self.commission_pay == 'showroom_partner':
            search_domain = [('commission_partner', '=', self.partner_id.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)]
            if self.start_date:
                search_domain.append(('date','>=',self.start_date))
            if self.end_date:
                search_domain.append(('date','<=',self.end_date))
            move_line_search = move_line_obj.search(search_domain)
            commission_partner = self.partner_id
            commission_user = False
            comm_partner = commission_partner.id
        else:
            pass
            return False
        
        # Post The Journal Entry
        post_move = [i.account_move_line_id.move_id for i in self.commission_account_lines if i.account_move_line_id and i.account_move_line_id.move_id.state == 'draft']
        for jmove in post_move:
            jmove.post()
        # total_amount = 0
        # for id in move_line_search:
            # total_amount += id.credit
        total_amount = sum([i.amount for i in self.commission_account_lines])
        commission_vals = {
            'name': self.payment_journal.default_credit_account_id.name,
            'credit': total_amount,
            'debit': 0.0,
            'account_id': self.payment_journal.default_credit_account_id.id,
            'partner_id': commission_partner.id,
            'commission_user': commission_user,
            'commission_partner': comm_partner,
        }
        sale_person_vals = {
            'name': commission_partner.name,
            'credit': 0.0,
            'debit': total_amount,
            'account_id': commission_partner.property_account_payable_id.id,
            'partner_id': commission_partner.id,
            'commission_user': commission_user,
            'commission_partner': comm_partner,
        }
        now = datetime.now()
        vals = {
            'journal_id': self.payment_journal.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': self.pay_reference,
            'partner_id': commission_partner.id,
            'commission_user': commission_user,
            'commission_partner': comm_partner,
            'check_no': self.check_number,
            
        }
        move = self.env['account.move'].create(vals)
        move.post()
        for mlid in self.commission_account_lines:
            mlid.account_move_line_id.write({'paid': True, 'check_no': self.check_number, 'paid_move_id': move.id, 'pay_reference': self.pay_reference})
        #Create Check Payment record
        paid_commission_id = paid_commission_obj.create({'partner_id': commission_partner.id, 'commission_pay':self.commission_pay, 'check_number': self.check_number, 'sale_person': self.sale_person.id, 'pay_reference': self.pay_reference, 'pay_amount': total_amount, 'paid_move_id': move.id, 'date':now.strftime('%Y-%m-%d'),})
        return move.id
      
class SearchCommissionPaidCheck(models.Model):
    _name = "search.commission.paid.check"
    _rec_name = "check_number"
    _order = "id desc"
          
    partner_id = fields.Many2one('res.partner', 'Showroom Partner', domain=[('partner_type','=','showroom')])
    commission_pay = fields.Selection([('salesperson','Sales Person'), ('showroom_partner','Showroom Partner')], string="Commission Pay")
    check_number = fields.Integer('Check Number #')
    sale_person = fields.Many2one('res.users', 'Sale Person')
    date = fields.Date('Payment Date')
    pay_reference = fields.Char("Reference #")
    pay_amount = fields.Float('Amount')
    paid_move_id = fields.Many2one('account.move','Account Move')
    check_printed = fields.Boolean('Check Printed ?')
    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id)
            
    @api.multi
    def get_com_account_move_lines(self):
        AccountMoveLine = self.env['account.move.line']
        account_lines = AccountMoveLine.search([('paid_move_id','=',self.paid_move_id.id)])
        return account_lines
        
#     @api.multi
#     def print_commission_checks(self):
#         """ Check that the recordset is valid, set the payments state to sent and call print_checks() """
#         # if any(payment.journal_id != self[0].journal_id for payment in self):
#         #     raise UserError(_("In order to print multiple checks at once, they must belong to the same bank journal."))
# 
#         if not self[0].paid_move_id.journal_id.check_manual_sequencing:
#             # The wizard asks for the number printed on the first pre-printed check
#             # so payments are attributed the number of the check the'll be printed on.
#              # ('check_number', '!=', 0)
#             last_printed_check = self.search([('paid_move_id.journal_id', '=', self[0].paid_move_id.journal_id.id), ('check_number', '!=', 0)], order="check_number desc", limit=1)
#             next_check_number = last_printed_check and last_printed_check.check_number + 1 or 1
#             return {
#                 'name': _('Print Pre-numbered Checks'),
#                 'type': 'ir.actions.act_window',
#                 'res_model': 'print.prenumbered.checks',
#                 'view_type': 'form',
#                 'view_mode': 'form',
#                 'target': 'new',
#                 'context': {
#                     'check_commission_ids': self.ids,
#                     'default_next_check_number': next_check_number,
#                      'commission_check_print': True,
#                 }
#             }
#         else:
#             # self.filtered(lambda r: r.state == 'draft').post()
#              return {
#                 'name': _('Print Commission Checks'),
#                 'type': 'ir.actions.act_window',
#                 'res_model': 'wizard.dynamic.cheque.print',
#                 'view_type': 'form',
#                 'view_mode': 'form',
#                 'target': 'new',
#                 'context': {
#                     'commission_check_ids': self.ids,
#                     'commission_check_print': True,
#                     'commission_id': self.ids,
#                 }
#             }
#             # return self.do_print_checks()
#         
#         return True
    
class SaleCommissionAccountLine(models.TransientModel):
    _name = "sale.commission.account.line"
    
    date = fields.Date("Date")
    reference = fields.Char("Reference")
    move_ref = fields.Char("Journal Entry")
    amount = fields.Float("Amount")
    account_move_line_id = fields.Many2one("account.move.line", "Journal Item")
    sale_commission_id = fields.Many2one("sale.commission", "Sale Commission")
    check_no = fields.Integer('Check Number #')
    journal_id = fields.Many2one('account.journal', 'Journal')
    commissionable_amount = fields.Float(string="Commissionable Amount", help="This will carry the Product total amount without Tax")
    commission_rate = fields.Float("Commission Rate")
    sales_commission_rate = fields.Float("Sales Commission Rate")
    total_commission = fields.Float("Commisison Amount")
            
            
            
            