# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': "GT Bulk Sale Order Cancel and set Draft",
    'summary': """
        Cancel the Bulk Sale order and Set it in Cancel State""",
    'author': "Globalteckz",
    'website': "https://www.globalteckz.com/",
    'category': 'Sale',
    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'depends': ['sale','account_cancel'],
    'data': [
        'views/sale_view.xml',
    ],
    'installable': True,
}
