# -*- coding: utf-8 -*-

{
    'name': 'Biz Security Permission Access',
    'version': '1.0',
    'author': 'Biz',
    'category': 'sales',
    'description': """
""",
    "summary": "This Module is used for Security Permission Access",
    'website': 'https://www.example.com',
    'depends': ['base', 'sale', 'purchase', 'product', 'stock', 'branch', 'showroom_fields'],
    'data': [
        'security/access_security.xml',
        'security/ir.model.access.csv'
    ],
    'qweb': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
