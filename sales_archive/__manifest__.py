# -*- coding: utf-8 -*-
###############################################################################
#
#   Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#   Copyright (C) 2016-today Geminate Consultancy Services (<http://geminatecs.com>).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

{
    'name': 'Sales Archive',
    'summary': 'Archive Sale related Documents',
    'author': 'Geminate Consultancy Services',
    'company': 'Geminate Consultancy Services',
    'website': 'https://www.geminatecs.com/',
    'category': 'Sales',
    'version': '12.0.0.1',

    'description': """ 
            Geminate comes with a feature to manage un-necessary and old data of sales related document(s)
            by archive them and unarchive them in future to review for analysis and stretagic planning. 
            it will helps to manage the 'Quotations and Orders' from Sales, 'Invoices, Credit Notes and Payments'
            from the Invoicing and 'delivery orders' from the inventory.
     """,

    'license': 'Other proprietary',
    'depends': [
        'sale','stock', 'account','archive_connector',
    ],
    'data': [
		'security/ir.model.access.csv',
        'views/settings.xml',
        'views/sale_order.xml',
        'views/account_payment.xml',
        'views/invoice_order.xml',
        'wizard/sale_archive.xml',
        'wizard/sale_unarchive.xml',


    ],
    'installable': True,
    'installable': True,
    "images": ['static/description/sale-archive.jpg'],
    'price': 30.00,
    'currency': 'EUR'
}
