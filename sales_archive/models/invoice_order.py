# -*- coding: utf-8 -*-
###############################################################################
#
#   Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#   Copyright (C) 2016-today Geminate Consultancy Services (<http://geminatecs.com>).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


from odoo import fields, models, _


class AccountMoveArchive(models.Model):
    _inherit = "account.invoice"

    def toggle_archive1(self):
        for record in self:
            if record.type in ['out_invoice','out_refund']:
                if record.archive:
                    record.write({'archive': False})
                else:
                    record.write({'archive': True})

    def action_archive(self):
        return self.filtered(lambda record: record.archive).toggle_archive1()

    def action_unarchive(self):
        return self.filtered(lambda record: not record.archive).toggle_archive1()

    archive = fields.Boolean(default=False)
