# -*- coding: utf-8 -*-
###############################################################################
#
#   Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#   Copyright (C) 2016-today Geminate Consultancy Services (<http://geminatecs.com>).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


from odoo import api, fields, models, _


class ResConfigSettings(models.TransientModel):

    _inherit = 'res.config.settings'

    payment_archived = fields.Boolean(string='Account Payments',
                                      help="Payements Archive - Visible / Invisible")




    def set_values(self):
        super(ResConfigSettings, self).set_values()
        if self.payment_archived:
            self.env['ir.config_parameter'].sudo().set_param('sales_archive.payment_archived', self.payment_archived)
            self.env.ref('base.group_user').write({'implied_ids': [(4, self.env.ref('sales_archive.group_payment_archived').id)]})

        else:
            self.env['ir.config_parameter'].sudo().set_param('sales_archive.payment_archived', self.payment_archived)
            self.env.ref('base.group_user').write({'implied_ids': [(3, self.env.ref('sales_archive.group_payment_archived').id)]})
            user_ids = self.env['res.users'].sudo().search([])
            user_ids.write({'groups_id': [(3, self.env.ref('sales_archive.group_payment_archived').id)]})


    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            payment_archived=self.env['ir.config_parameter'].sudo().get_param('sales_archive.payment_archived'),
        )
        return res
