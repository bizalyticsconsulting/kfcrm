# -*- coding: utf-8 -*-
###############################################################################
#
#   Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#   Copyright (C) 2016-today Geminate Consultancy Services (<http://geminatecs.com>).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


from odoo import models, fields, api, _

class StockPickingUnarchive(models.TransientModel):
    _inherit = 'picking.unarchive.connector'
    _description = "Stock Picking Unrchived"

    @api.multi
    def submit_unarchive_transfer(self):
        res=super(StockPickingUnarchive,self).submit_unarchive_transfer()
        active_id = self._context.get('active_ids')
        p_ids = self.env['stock.picking'].browse(active_id)
        for tr in p_ids:
        	if  self.user_has_groups("sales_archive.group_transfer_archived"):
        		if tr.picking_type_id.code == 'outgoing':
        			tr.write({'archive_transfer': False})
        return res
