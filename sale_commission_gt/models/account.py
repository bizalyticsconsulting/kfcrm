# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    #378401
##############################################################################

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime

import logging

_logger = logging.getLogger(__name__)

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    commission_journal = fields.Many2one('account.journal', 'Commission Journal')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        res.update(
            commission_journal = int(ICPSudo.get_param('account.commission_journal'))
        )
        return res

    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        ICPSudo.set_param("account.commission_journal", self.commission_journal.id)


class AccountMove(models.Model):
    _inherit = 'account.move'

    commission_user = fields.Many2one('res.users', 'Sale Person')
    branch_id = fields.Many2one("res.branch", "Showroom", default=lambda self: self.env.user.branch_id)


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    commission_user = fields.Many2one('res.users', 'Sale Person')
    paid = fields.Boolean('Commission Paid')
    source_amount = fields.Float('Source Amount', )
    source_currency = fields.Many2one('res.currency', store=True, string="Source Currency")
    commission_on = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
    ], "When Paid")
    branch_id = fields.Many2one("res.branch", "Showroom", default=lambda self: self.env.user.branch_id)
    parent_commission_id = fields.Many2one("account.move.line", "Parent Commission", help="This field store the parent commission reference in case we do Payment against the commisisons.", copy=False)


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
    ], "When Paid")

    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref):
        # commission_journal = self.env['ir.config_parameter'].get_values('res.config.settings', 'commission_journal')
        commission_journal = int(self.env['ir.config_parameter'].sudo().get_param('account.commission_journal'))
        journal_obj = self.env['account.journal']
        journal_id = journal_obj.browse(commission_journal)
        commission_vals = {
            'name': journal_id.default_debit_account_id.name,
            'debit': commission_amt,
            'credit': 0.0,
            'account_id': journal_id.default_debit_account_id.id,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id,
            'commission_on': self.order_pay_state,
        }
        sale_person_vals = {
            'name': sale_person.name,
            'debit': 0.0,
            'credit': commission_amt,
            'account_id': sale_person.property_account_payable_id.id,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id,
            'commission_on': self.order_pay_state,
        }
        now = datetime.now()
        vals = {
            'journal_id': journal_id.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': journal_ref,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id
        }
        move = self.env['account.move'].create(vals)
        move.post()
        return move.id

    @api.multi
    def action_invoice_open(self):
        commission_type = str(self.env['ir.config_parameter'].sudo().get_param('sale.commission_calculation'))
        res = super(AccountInvoice, self).action_invoice_open()
        if self.order_pay_state == '1':
            if commission_type == '0':
                total_invoice_commission = 0
                for invoice_line_id in self.invoice_line_ids:
                    sale_obj = self.env['sale.order']
                    sale_search = sale_obj.search([('name', '=', self.origin)])
                    for product_line in sale_search.order_line:
                        if product_line.product_id.prod_comm_per and product_line.product_id.prod_comm_per > 0:
                            product_commission = (invoice_line_id.price_subtotal * product_line.product_id.prod_comm_per) / 100
                            total_invoice_commission += product_commission
                reference = '[' + self.name + '] ' + self.number
                self.commission_count_entries(total_invoice_commission, self.user_id, reference)

            if commission_type == '1':
                total_invoice_commission = 0
                for invoice_line_id in self.invoice_line_ids:
                    sale_obj = self.env['sale.order']
                    sale_search = sale_obj.search([('name','=',self.origin)])
                    for product_line in sale_search.order_line:
                        if product_line.product_id.categ_id.prod_categ_comm_per and product_line.product_id.categ_id.prod_categ_comm_per > 0:
                            product_categ_commission = (invoice_line_id.price_subtotal * product_line.product_id.categ_id.prod_categ_comm_per) / 100
                            total_invoice_commission += product_categ_commission
                reference = '[' + self.name + '] ' + self.number
                self.commission_count_entries(total_invoice_commission, self.user_id, reference)

            if commission_type == '2':
                if self.team_id.sale_manage_comm and self.team_id.sale_manage_comm > 0:
                    manager_commission = (self.amount_untaxed * self.team_id.sale_manage_comm) / 100
                    reference = '[' + self.name + '] ' + self.number
                    self.commission_count_entries(manager_commission, self.team_id.user_id, reference)

                if self.team_id.sale_member_comm and self.team_id.sale_member_comm > 0:
                    member_commission = (self.amount_untaxed * self.team_id.sale_member_comm) / 100
                    reference = '[' + self.name + '] ' + self.number
                    self.commission_count_entries(member_commission, self.user_id, reference)
        return res

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
    ], "When Paid")

    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref):
        commission_journal = int(self.env['ir.config_parameter'].sudo().get_param('account.commission_journal'))
        journal_obj = self.env['account.journal']
        journal_id = journal_obj.browse(commission_journal)
        commission_vals = {
            'name': journal_id.default_debit_account_id.name,
            'debit': commission_amt,
            'credit': 0.0,
            'account_id': journal_id.default_debit_account_id.id,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id,
            'commission_on': self.order_pay_state,
        }
        sale_person_vals = {
            'name': sale_person.name,
            'debit': 0.0,
            'credit': commission_amt,
            'account_id': sale_person.property_account_payable_id.id,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id,
            'commission_on': self.order_pay_state,
        }
        now = datetime.now()
        vals = {
            'journal_id': journal_id.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': journal_ref,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id
        }
        move = self.env['account.move'].create(vals)
        move.post()
        return move.id

    @api.multi
    def post(self):
        commission_type = self.env['ir.config_parameter'].sudo().get_param('sale.commission_calculation')
        res = super(AccountPayment, self).post()
        return res
        #### 27.02.2021..No need of this flow now for Income commission
        invoice_obj = self.env['account.invoice']
        sale_obj = self.env['sale.order']
        invoice_id = invoice_obj.browse(self._context.get('active_id'))
        self.write({'order_pay_state': invoice_id.order_pay_state})
        if self.order_pay_state == '2':
            if commission_type == '0':
                sale_id = sale_obj.search([('name', '=', invoice_id.origin)])
                for product_line in sale_id.order_line:
                    if product_line.product_id.prod_comm_per and product_line.product_id.prod_comm_per > 0:
                        total_commission = (self.amount * product_line.product_id.prod_comm_per) / 100
                reference = '[' + sale_id.name + '] ' + invoice_id.number + ' Payment'
                self.commission_count_entries(total_commission, sale_id.user_id, reference)

            if commission_type == '1':
                sale_id = sale_obj.search([('name', '=', invoice_id.origin)])
                for product_line in sale_id.order_line:
                    if product_line.product_id.categ_id.prod_categ_comm_per and product_line.product_id.categ_id.prod_categ_comm_per > 0:
                        total_commission = (self.amount * product_line.product_id.categ_id.prod_categ_comm_per) / 100
                reference = '[' + sale_id.name + '] ' + invoice_id.number + ' Payment'
                self.commission_count_entries(total_commission, sale_id.user_id, reference)

            if commission_type == '2':
                sale_id = sale_obj.search([('name', '=', invoice_id.origin)])
                manage_total_commission = (self.amount * sale_id.team_id.sale_manage_comm) / 100
                reference = '[' + sale_id.name + '] ' + invoice_id.number + ' Payment'
                self.commission_count_entries(manage_total_commission, sale_id.team_id.user_id, reference)
                member_total_commission = (self.amount * sale_id.team_id.sale_member_comm) / 100
                reference = '[' + sale_id.name + '] ' + invoice_id.number + ' Payment'
                self.commission_count_entries(member_total_commission, sale_id.team_id.user_id, reference)
        return res


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: