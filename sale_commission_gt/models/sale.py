# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    #378401
##############################################################################

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
 
    when_to_pay = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
        ], "When To Pay")
 
    commission_calculation = fields.Selection([
        ('0', 'Product'),
        ('1', 'Product Category'),
        ('2', 'Sales Team'),
        ], "Calculation Based On")
    apply_commission_changes = fields.Boolean(string='Apply Commission Changes')
 
    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        if self.apply_commission_changes:
            ICPSudo = self.env['ir.config_parameter'].sudo()
            when_to_pay = ICPSudo.set_param("sale_commission_gt.sale.when_to_pay", self.when_to_pay)
            commission_calculation = ICPSudo.set_param("sale_commission_gt.sale.commission_calculation", self.commission_calculation)
            product_obj = self.env['product.template']
            product_categ_obj = self.env['product.category']
            crm_team_obj = self.env['crm.team']
            # Setting the values in the vies of commission data.
            if self.commission_calculation == '0':
                # print "Product : "
                product_search = product_obj.search([]).ids
                product_browse = product_obj.browse(product_search)
                for product_id in product_browse:
                    product_id.write({'is_prod_comm': True})
                # print "Product Category : "
                product_categ_search = product_categ_obj.search([]).ids
                product_categ_browse = product_categ_obj.browse(product_categ_search)
                for product_categ_id in product_categ_browse:
                    product_categ_id.write({'is_prod_categ_comm': False})
                # print "CRM Team : "
                crm_team_search = crm_team_obj.search([]).ids
                crm_team_browse = crm_team_obj.browse(crm_team_search)
                for crm_team_id in crm_team_browse:
                    crm_team_id.write({'is_sale_team_comm': False})

            if self.commission_calculation == '1':
                # print "Product : "
                product_search = product_obj.search([]).ids
                product_browse = product_obj.browse(product_search)
                for product_id in product_browse:
                    product_id.write({'is_prod_comm': False})
                # print "Product Category : "
                product_categ_search = product_categ_obj.search([]).ids
                product_categ_browse = product_categ_obj.browse(product_categ_search)
                for product_categ_id in product_categ_browse:
                    product_categ_id.write({'is_prod_categ_comm': True})
                # print "CRM Team : "
                crm_team_search = crm_team_obj.search([]).ids
                crm_team_browse = crm_team_obj.browse(crm_team_search)
                for crm_team_id in crm_team_browse:
                    crm_team_id.write({'is_sale_team_comm': False})

            if self.commission_calculation == '2':
                # print "Product : "
                product_search = product_obj.search([]).ids
                product_browse = product_obj.browse(product_search)
                for product_id in product_browse:
                    product_id.write({'is_prod_comm': False})
                # print "Product Category : "
                product_categ_search = product_categ_obj.search([]).ids
                product_categ_browse = product_categ_obj.browse(product_categ_search)
                for product_categ_id in product_categ_browse:
                    product_categ_id.write({'is_prod_categ_comm': False})
                # print "CRM Team : "
                crm_team_search = crm_team_obj.search([]).ids
                crm_team_browse = crm_team_obj.browse(crm_team_search)
                for crm_team_id in crm_team_browse:
                    crm_team_id.write({'is_sale_team_comm': True})
                 
                 
    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        if self.apply_commission_changes:
            when_to_pay = ICPSudo.get_param('sale_commission_gt.when_to_pay')
            res.update(
                when_to_pay=ICPSudo.get_param('sale_commission_gt.when_to_pay'),
                commission_calculation=ICPSudo.get_param('sale_commission_gt.commission_calculation'),
            )
        return res

class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        res = super(SaleAdvancePaymentInv,self)._create_invoice(order, so_line, amount)
        res.write({'order_pay_state':order.order_pay_state})
        return res


class SaleTeam(models.Model):
    _inherit = 'crm.team'

    sale_manage_comm = fields.Float("Manager Sale Commission(%)")
    sale_member_comm = fields.Float("Member Sale Commission(%)")
    is_sale_team_comm = fields.Boolean("Is Sale Team Commission ?")


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    order_pay_state = fields.Selection([
        ('0', 'Sale Validation'),
        ('1', 'Invoice Validation'),
        ('2', 'Payment Validation'),
    ], "When Paid")
    commission_count_manage = fields.Float('Sale Manager Commission', default=0.0)
    commission_count_member = fields.Float('Sale Person Commission', default=0.0)

    @api.model
    def create(self, vals):
        ICPSudo = self.env['ir.config_parameter'].sudo()
        res = super(SaleOrder, self).create(vals)
        # Because of override this configuration setting fields can not accessing using sale_commission_gt module name.
        # payment_setting_state = str(self.env['ir.config_parameter'].search([('key','=','sale_commission_gt.sale.when_to_pay')]).value)
        payment_setting_state = str(self.env['ir.config_parameter'].search([('key', '=', 'gt_sale_commission_extension.sale.when_to_pay')]).value)
        if payment_setting_state:
            res.write({'order_pay_state': payment_setting_state})
        return res

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        res = super(SaleOrder, self).action_invoice_create(grouped=grouped, final=final)
        invoice_obj = self.env['account.invoice']
        invoice_search = invoice_obj.search([('id', '=', res[0])])
        invoice_browse = invoice_obj.browse(invoice_search.id)
        invoice_browse.write({'order_pay_state': self.order_pay_state})
        return res

    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref):
        commission_journal = int(self.env['ir.config_parameter'].search([('key','=','account.commission_journal')]).value)
        journal_obj = self.env['account.journal']
        journal_id = journal_obj.browse(commission_journal)
        commission_vals = {
            'name': journal_id.default_debit_account_id.name,
            'debit': commission_amt,
            'credit': 0.0,
            'account_id': journal_id.default_debit_account_id.id,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id,
            'commission_on': self.order_pay_state,
        }
        sale_person_vals = {
            'name': sale_person.name,
            'debit': 0.0,
            'credit': commission_amt,
            'account_id': sale_person.property_account_payable_id.id,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id,
            'commission_on': self.order_pay_state,
        }
        now = datetime.now()
        vals = {
            'journal_id': journal_id.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': journal_ref,
            'partner_id': sale_person.partner_id.id,
            'commission_user': sale_person.id,
        }
        move = self.env['account.move'].create(vals)
        move.post()
        return move.id

    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        for order in self:
            commission_type = self.env['ir.config_parameter'].search([('key', '=', 'sale.commission_calculation')]).value
            if order.order_pay_state == '0':
                if commission_type == '0':
                    total_order_commission = 0
                    for line_id in self.order_line:
                        if line_id.product_id.prod_comm_per and line_id.product_id.prod_comm_per > 0:
                            product_commission = (
                                                 line_id.price_subtotal * line_id.product_id.prod_comm_per) / 100
                            total_order_commission += product_commission
                    self.commission_count_entries(total_order_commission, order.user_id, order.name)

                if commission_type == '1':
                    total_order_commission = 0
                    for line_id in self.order_line:
                        if line_id.product_id.categ_id.prod_categ_comm_per and line_id.product_id.categ_id.prod_categ_comm_per > 0:
                            product_categ_commission = (
                                                       line_id.price_subtotal * line_id.product_id.categ_id.prod_categ_comm_per) / 100
                            total_order_commission += product_categ_commission
                    self.commission_count_entries(total_order_commission, order.user_id, order.name)

                if commission_type == '2':
                    if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
                        manager_commission = (order.amount_untaxed * order.team_id.sale_manage_comm) / 100
                        self.commission_count_entries(manager_commission, order.team_id.user_id, order.name)
                    if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
                        member_commission = (order.amount_untaxed * order.team_id.sale_member_comm) / 100
                        self.commission_count_entries(member_commission, order.user_id, order.name)

            if order.order_pay_state == '2':
                if commission_type == '0':
                    total_order_commission = 0
                    for line_id in self.order_line:
                        if line_id.product_id.prod_comm_per and line_id.product_id.prod_comm_per > 0:
                            product_commission = (line_id.price_subtotal * line_id.product_id.prod_comm_per) / 100
                            total_order_commission += product_commission
                    order.write({'commission_count_member': total_order_commission})

                if commission_type == '1':
                    total_order_commission = 0
                    for line_id in self.order_line:
                        if line_id.product_id.categ_id.prod_categ_comm_per and line_id.product_id.categ_id.prod_categ_comm_per > 0:
                            product_categ_commission = (line_id.price_subtotal * line_id.product_id.categ_id.prod_categ_comm_per) / 100
                            total_order_commission += product_categ_commission
                    order.write({'commission_count_member': total_order_commission})

                if commission_type == '2':
                    if order.team_id.sale_manage_comm and order.team_id.sale_manage_comm > 0:
                        manager_commission = (order.amount_untaxed * order.team_id.sale_manage_comm) / 100
                        order.write({'commission_count_manage': manager_commission})

                    if order.team_id.sale_member_comm and order.team_id.sale_member_comm > 0:
                        member_commission = (order.amount_untaxed * order.team_id.sale_member_comm) / 100
                        order.write({'commission_count_member': member_commission})
        return res

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
