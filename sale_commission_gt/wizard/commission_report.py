# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz Pvt Ltd
#    Copyright (C) 2013-Today(www.globalteckz.com).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    #378401
##############################################################################

from odoo import fields, models, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime
# from odoo.exceptions import ValidationError

import logging

_logger = logging.getLogger(__name__)

class SaleCommissionReport(models.TransientModel):
    _name = 'sale.commission.report'
    _description = 'Sale Commission Report'

    sale_person = fields.Many2one('res.users', 'Sale Person')
    start_date = fields.Datetime('Start Date', default=lambda *a:datetime.today() + relativedelta(day=1))
    end_date = fields.Datetime('End Date', default=lambda *a:datetime.today())
    type = fields.Selection([('payable','Payable'),('paid','Paid')], 'Payable/Paid', default='payable')

    @api.multi
    def action_commission_report(self):
        if self.type == 'payable':
            return self.env.ref('sale_commission_gt.action_payable_report_commission').report_action(self)
        if self.type == 'paid':
            return self.env.ref('sale_commission_gt.action_paid_report_commission').report_action(self)
    
    def get_paid_movelines(self, commission_report_id):
        wizard_obj = self.env['sale.commission.report']
        wizard_search = wizard_obj.search([('id', '=', self.id)])
        salesman = wizard_search.sale_person
        move_line_obj = self.env['account.move.line']
        move_line_search = move_line_obj.search([('commission_user','=',salesman.id), ('paid','=',True),('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
        return move_line_search
    
    def get_payable_movelines(self, commission_report_id):
        wizard_obj = self.env['sale.commission.report']
        wizard_search = wizard_obj.search([('id', '=', self.id)])
        salesman = wizard_search.sale_person
        move_line_obj = self.env['account.move.line']
        move_line_search = move_line_obj.search([('commission_user','=',salesman.id), ('paid','=',False),('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
        _logger.debug("Move line Search :  " +  move_line_search)
        return move_line_search


class AllCommissionReport(models.TransientModel):
    _name = 'all.commission.report'
    _description = 'All Commission Report'

    start_date = fields.Datetime('Start Date', default=lambda *a: datetime.today() + relativedelta(day=1))
    end_date = fields.Datetime('End Date', default=lambda *a: datetime.today())
    type = fields.Selection([('payable', 'Payable'), ('paid', 'Paid'), ('all', 'All')], 'Payable/Paid/All', default='all')

    @api.multi
    def all_commission_report(self):
        return self.env.ref('sale_commission_gt.action_all_report_commission').report_action(self)
    
    
    def get_all_movelines(self, commission_report_id):
        wizard_obj = self.env['all.commission.report']
        wizard_search = wizard_obj.search([('id', '=', self.id)])
        move_line_obj = self.env['account.move.line']
        if wizard_search.type == 'payable':
            move_line_search = move_line_obj.search([('paid','=',False), ('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
        if wizard_search.type == 'paid':
            move_line_search = move_line_obj.search([('paid','=',True), ('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
        if wizard_search.type == 'all':
            move_line_search = move_line_obj.search([('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
        return move_line_search
    
    
    
    
    
    
    
    
    
    
    
    

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
