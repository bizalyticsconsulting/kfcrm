# -*- coding: utf-8 -*-
##############################################################################
#
#    Globalteckz
#    Copyright (C) 2013-Today Globalteckz (http://www.globalteckz.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Mssql Log Management',
    'version': '10.0.0.1',
    'category': 'Generic Modules',
    'sequence': 1,
    'author': 'globalteckz',
    'website': 'http://www.globalteckz.com',
    'summary': '',
    'description': """

Mssql Log Management Import data functionality
=========================

    """,
    'depends': [
                'sale',
                'stock',
                'base',
                'delivery',
                'account',
                'purchase'
            ],
    'data': [
            'security/ir.model.access.csv',
             'views/log_file_view.xml',
            
            ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
