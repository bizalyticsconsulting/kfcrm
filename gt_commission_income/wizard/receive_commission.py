# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools.misc import xlwt
from datetime import datetime
import logging
import io
import base64
from pprint import pprint


_logger = logging.getLogger(__name__)


class receiveCommissionIncome(models.TransientModel):
	_name = "receive.commission.income"
	description = "Receivable Commission(Income)"

	partner_id = fields.Many2one('res.partner', string='Vendor', domain=[('is_company', '=', True), ('supplier', '=', True), ('parent_id', '=', False), ('active', '=', True)], required=True)
	start_date = fields.Date('Start Date')
	end_date = fields.Date('End Date')
	commission_journal = fields.Many2one('account.journal', 'Commission Journal', readonly="1",default=lambda self: int(self.env['ir.config_parameter'].get_param('commission_income_journal')))
	payment_amt = fields.Float('Receivable Amount')
	payment_journal = fields.Many2one('account.journal', 'Payment Journal', domain="[('type', 'in', ['bank','cash'])]", required=True)
	pay_reference = fields.Char('Reference')
	receive_commission_line_ids = fields.One2many("receive.commission.income.line", 'wizard_id', 'Account Move Line', help="Journal Items for the Sales Commissions")
	branch_id = fields.Many2one("res.branch", "Showroom", default=lambda self: self.env.user.branch_id)
	sale_id = fields.Many2one("sale.order", "Sale Order", domain="[('branch_id', '=', branch_id), ('x_supplier', '=', partner_id), ('state', 'in', ['sale', 'done'])]")
	
	
	@api.onchange('branch_id', 'partner_id')
	def onchange_branch_id(self):
		return_domain = {'sale_id': []}
		self.update({'sale_id': False})
		sale_domain = [('state', 'in', ['sale', 'done'])]
		if self.branch_id:
			sale_domain.append(('branch_id', '=', self.branch_id.id))
		if self.partner_id:
			sale_domain.append(('x_supplier', '=', self.partner_id.id))
		return_domain['sale_id'] = sale_domain
		return {'domain': return_domain}


	# @api.onchange('sale_id')
	# def onchange_sale_id(self):
	# 	# return_domain = {'partner_id': []}
	# 	return_domain = {}
	# 	self.update({'partner_id': False})
	# 	if self.sale_id:
	# 		return_domain['partner_id'] = [('id', 'in', self.sale_id.order_line.mapped('x_vendor').mapped('id'))]
	# 		return {'domain': return_domain}
	# 	# else:
	# 	# 	return {'domain': {'partner_id': [('is_company', '=', True), ('supplier', '=', True), ('parent_id', '=', False), ('active', '=', True)]}}


	@api.onchange('branch_id', 'sale_id', 'partner_id','payment_journal','start_date','end_date')
	def onchange_partner_id(self):
		receive_commission_list = []
		# ('received_income','=',False) use if required
		
		search_domain = ['|',
							('account_id.user_type_id.name', '=', 'Receivable'),
							('account_id.internal_type', '=', 'receivable'),
						('journal_id.name','=','Commission Income'),
						('paid', '=', False),
						('branch_id', '=', self.branch_id.id)]
						
		if self.partner_id:
			search_domain.append(('partner_id', '=', self.partner_id.id))
		if self.sale_id:
			search_domain.append(('sale_order_id', '=', self.sale_id.id))
		if self.start_date:
			search_domain.append(('date', '>=', self.start_date))
		if self.end_date:
			search_domain.append(('date', '<=', self.end_date))
		acc_mov_line_ids = self.env['account.move.line'].search(search_domain)
		
		if self.branch_id and self.partner_id and self.payment_journal:
			# if self.receive_commission_line_ids:
			self.receive_commission_line_ids = False
			for i in acc_mov_line_ids:
				receive_commission_list.append((0,0,{'name': i.partner_id.name,
													'date': i.date,
													'journal_id': i.journal_id.id,
													'commissionable_amount': i.commissionable_amount,
													'commission_rate': i.commission_rate,
													'receive_comm_amt': (i.receive_comm_amt > 0) and i.receive_comm_amt or i.total_commission,
													'receive_comm_rate': (i.receive_comm_rate > 0) and i.receive_comm_rate or i.commission_rate,
													'debit': i.debit,
													'total_commission': i.total_commission,

													# 'wizard_id': wizard_id.id,
													'og_acc_mov_line_id': i.id,
													'sale_line_id': i.sale_line_id.id,
												}))
			self.update({'receive_commission_line_ids': receive_commission_list})
			

	@api.multi
	def do_payment_receive_commission_income(self):
		"""
			This method will do payment journal entries against the commission and maintain data on that linke, sda_id, layered_id, sale_line_id, parent_id etc.
		"""
		for comm_line in self.receive_commission_line_ids:
			comm_line_id = self.env['account.move.line'].browse(int(comm_line.og_acc_mov_line_id))
			if not comm_line_id:
				continue
			if comm_line_id.move_id.state == 'draft':
				comm_line_id.move_id.action_post()
	
			now = datetime.now()
			# total_amount = comm_line.receive_comm_amt
			total_amount = (comm_line_id.receive_comm_amt > 0) and comm_line_id.receive_comm_amt or comm_line_id.total_commission
			if total_amount <= 0:
				continue
	
			acc_receivable_vals = {
				'name': comm_line_id.sale_order_id.name, # self.partner_id.name,
				'credit': total_amount,
				'debit': 0.0,
				'account_id': self.partner_id.property_account_receivable_id.id,
				'partner_id': self.partner_id.id,
				'pay_reference': self.pay_reference,
	
				'sale_line_id': comm_line_id.sale_line_id.id,
				'parent_commission_id': comm_line_id.id,

				'branch_id': comm_line_id.branch_id.id,
				'ref': comm_line_id.ref,
				'commission_partner': comm_line_id.commission_partner.id,
				'product_id': comm_line_id.sale_line_id.product_id.id,
				# 'sale_order_id': comm_line_id.sale_order_id.id,
				# 'commission_user': commission_user,
			}
			bank_cash_vals = {
				'name': comm_line_id.sale_order_id.name, # self.partner_id.name,
				'credit': 0.0,
				'debit': total_amount,
				'account_id': self.payment_journal.default_debit_account_id.id,
				'partner_id': self.partner_id.id,
				'pay_reference': self.pay_reference,
	
				'sale_line_id': comm_line_id.sale_line_id.id,
				'parent_commission_id': comm_line_id.id,

				'branch_id': comm_line_id.branch_id.id,
				'ref': comm_line_id.ref,
				'commission_partner': comm_line_id.commission_partner.id,
				'product_id': comm_line_id.sale_line_id.product_id.id,
				# 'sale_order_id': comm_line_id.sale_order_id.id,
				# 'commission_user': commission_user,
			}
			vals = {
				'journal_id': self.payment_journal.id,
				'date': now.strftime('%Y-%m-%d'),
				'state': 'draft',
				'line_ids': [(0, 0, acc_receivable_vals), (0, 0, bank_cash_vals)],
				'ref': self.pay_reference,
				'commission_partner': comm_line_id.commission_partner.id,
				'partner_id': comm_line_id.commission_partner.id,
				# 'commission_user': commission_user,
			}
			move = self.env['account.move'].create(vals)
			_logger.info(f"Commission payment is created i.e. : {move} for the commission line i.e. {comm_line_id}.")
			move.action_post()
			move.line_ids.write({'paid': True})
	
			comm_line_id.move_id.line_ids.sudo().write({'paid': True, 'received_income': True})
			comm_line_id.sale_order_id.message_post(body=_(
				f"Income Commission payment({move.name}) is created for the Product commission: {comm_line_id.sale_line_id.product_id.display_name}."))

		return True


class SaleCommissionAccountLine(models.TransientModel):
    _name = "receive.commission.income.line"
    description = "Receivable Commission Line(Income)"

    wizard_id = fields.Many2one("receive.commission.income", 'Wizard ID')
    name = fields.Char('Vendor')
    date = fields.Date("Date")
    journal_id = fields.Many2one('account.journal', 'Journal')
    commissionable_amount = fields.Float(string="Commissionable Amount", help="This will carry the Product total amount without Tax")
    commission_rate = fields.Float("Commission Rate")
    receive_comm_amt = fields.Float(string='Receivable Commission Amount')
    receive_comm_rate = fields.Float(string='Receive Commission Rate')
    debit = fields.Float(string='Receive Commission Amount')
    total_commission = fields.Float("Commissison Amount")
    og_acc_mov_line_id = fields.Char("Original Account Move line ID")
    sale_line_id = fields.Many2one('sale.order.line', "Sale Line")

    
class IncomeCommissionReport(models.TransientModel):
	_name = "income.comm.report"
	description = "Income Commission Report"


	partner_id = fields.Many2one('res.partner', 'Vendor', domain="[('supplier', '=', True), ('active', '=', True), ('is_company', '=', True)]", required=True)
	comm_type = fields.Selection([('receivable', 'Receivable'), ('received', 'Received')], string='Type', default='receivable')
	start_date = fields.Date('Start Date')
	end_date = fields.Date('End Date')
	inc_amount = fields.Float('Income Amount')
	report_type = fields.Selection([('pdf', 'PDF'), ('excel_sheet', 'Excel Sheet')], string="Report Type", default='pdf')
	company_id = fields.Many2one('res.company', string="Company")
	currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
	branch_id = fields.Many2one("res.branch", "Showroom", default=lambda self: self.env.user.branch_id)
	sale_id = fields.Many2one("sale.order", "Sale Order")
	excel_file_data = fields.Binary('Click to Download Excel Sheet')
	file_name = fields.Char('Excel File', size=64)


	@api.onchange('branch_id')
	def onchange_branch_id(self):
		return_domain = {'sale_id': []}
		self.update({'sale_id': False, 'partner_id': False})
		if self.branch_id:
			return_domain['sale_id'] = [('branch_id', '=', self.branch_id.id), ('state', 'in', ['sale', 'done'])]
		return {'domain': return_domain}


	@api.onchange('sale_id')
	def onchange_sale_id(self):
		return_domain = {'partner_id': [('supplier', '=', True), ('active', '=', True), ('is_company', '=', True)]}
		self.update({'partner_id': False})
		if self.sale_id:
			return_domain['partner_id'] = [('id', 'in', self.sale_id.order_line.mapped('x_vendor').mapped('id'))]
		return {'domain': return_domain}


	def commissions_journal_lines_amount(self):
		"""
		This method will get all the receivable/received commissions line/payment journal items and return their amount and lines.
		:return: amount and journal items
		"""
		if self.comm_type == 'receivable':
			search_domain = ['|',
								 ('account_id.user_type_id.name', '=', 'Receivable'),
								 ('account_id.internal_type', '=', 'receivable'),
							 ('journal_id.name', '=', 'Commission Income'),
							 ('branch_id', '=', self.branch_id.id),
							 ('sale_order_id','!=', False),
							 ('partner_id', '=', self.partner_id.id),
							 ('paid', '=', False),
							 # ('comm_type', '=', 'income'),
							 ]
			if self.sale_id:
				search_domain.append(('sale_order_id', '=', self.sale_id.id))
			if self.start_date:
				search_domain.append(('date', '>=', self.start_date))
			if self.end_date:
				search_domain.append(('date', '<=', self.end_date))

			acc_mov_line_ids = self.env['account.move.line'].search(search_domain)
			_logger.info(f"Receivable Account Move Lines i.e commission journal items : {acc_mov_line_ids}")
			amount = sum([i.debit for i in acc_mov_line_ids])
			return amount, acc_mov_line_ids

		if self.comm_type == 'received':
			search_domain = ['|',
								 ('account_id.user_type_id.name', '=', 'Bank and Cash'),
								 ('account_id.internal_type', '=', 'liquidity'),
							 ('paid', '=', True),
							 ('parent_commission_id', '!=', False),
							 ('sale_order_id', '=', False),
							 ]
			acc_mov_line_ids = self.env['account.move.line'].search(search_domain)
			_logger.info(f"Received Account Move Lines i.e payment journal items against the commissions lines: {acc_mov_line_ids}")
			amount = sum([i.debit for i in acc_mov_line_ids])

			# Find any reerese entries and calculate the amount of that...Then substract the amount in main amount
			if acc_mov_line_ids:
				move_ids = acc_mov_line_ids.mapped("move_id")
				reverse_move_entries_ids = move_ids.mapped("reverse_entry_ids")
				reverse_move_line_ids = reverse_move_entries_ids.mapped("line_ids").filtered(lambda x: x.account_id.internal_type == 'liquidity')
				if len(reverse_move_line_ids) > 0:
					_logger.info(f"Reversed entries for received Account Move : {reverse_move_line_ids}")
					acc_mov_line_ids |= reverse_move_line_ids
					layered_discount_amount = sum([j.credit for j in reverse_move_line_ids])
					amount = amount - layered_discount_amount

			return amount, acc_mov_line_ids


	def getIncomeAmount(self):
		amount, acc_mov_line_ids = self.commissions_journal_lines_amount()
		return amount


	def getReceivableLines(self):
		amount, acc_mov_line_ids = self.commissions_journal_lines_amount()
		return acc_mov_line_ids


	def print_report(self):
		if self.report_type == 'pdf':
			return self.env.ref('gt_commission_income.action_report_income_commission').report_action(self)
		elif self.report_type == 'excel_sheet':
			report_gen = self.print_report_xls()
			return {
					'name': _('Income Commission Excel Report'),
					'type': 'ir.actions.act_window',
					'res_model': 'income.comm.report',
					'view_type': 'form',
					'view_mode': 'form',
					'views': [(False, 'form')],
					# 'view_id': view.id,
					'res_id': self.id,
					'target': 'new',
			}


	def print_report_xls(self):
		"""
			This method will generate the Excel sheet report.
		"""
		amount, acc_mov_line_ids = self.commissions_journal_lines_amount()
		currency = self.currency_id.symbol
		filename = "income_commission_report.xls"
		workbook = xlwt.Workbook()
		worksheet = workbook.add_sheet('Sheet1')

		# Heading, Subheading, Table heading and data Format
		heading_format = xlwt.easyxf('font: bold on, height 250; alignment: horiz centre; border: left thin, right thin, top thin, bottom thin')
		sub_heading_format = xlwt.easyxf('alignment: horiz left; font: bold on, height 210;')
		table_heading_format = xlwt.easyxf('alignment: horiz centre; font: bold on, height 210; borders: left thin, top thin, bottom thin, right thin')
		data_format = xlwt.easyxf('alignment: horiz centre')
		number_data_format =  xlwt.easyxf('alignment: horiz right;')
		char_data_format = xlwt.easyxf('alignment: horiz left;')
		date_format = xlwt.XFStyle()
		date_format.num_format_str = 'dd-mm-yyyy'

		# Increase Row & Columns width
		worksheet.row(0).height = 256 * 2  	# Increase height of row-1
		worksheet.col(0).width = 256 * 25  	# Increase height of col-A
		worksheet.col(1).width = 256 * 15  	# Increase height of col-B
		worksheet.col(2).width = 256 * 25  	# Increase height of col-C
		worksheet.col(3).width = 256 * 40  	# Increase height of col-D
		worksheet.col(4).width = 256 * 20 	# Increase height of col-E
		worksheet.col(5).width = 256 * 20  	# Increase height of col-F
		worksheet.col(6).width = 256 * 20  # Increase height of col-G

		# Subheadings String Data
		worksheet.write_merge(0, 0, 0, 5, 'Receivable Sale Commission Detail', heading_format)
		worksheet.write_merge(3, 3, 0, 0, 'Vendor/Manufacturer', sub_heading_format)
		worksheet.write_merge(3, 3, 1, 1, self.partner_id.display_name, data_format)
		worksheet.write_merge(3, 3, 3, 3, 'Receivable Amount', sub_heading_format)
		worksheet.write_merge(3, 3, 4, 4, amount, number_data_format)
		worksheet.write_merge(2, 2, 3, 3, 'Date', sub_heading_format)
		worksheet.write_merge(2, 2, 4, 4, datetime.now().date(), date_format)

		# Table Heading Data
		row = 6
		worksheet.write_merge(row, row, 0, 0, 'Sr.No.', table_heading_format)
		worksheet.write_merge(row, row, 1, 1, 'Date', table_heading_format)
		worksheet.write_merge(row, row, 2, 2, 'Sale Order', table_heading_format)
		worksheet.write_merge(row, row, 3, 3, 'Product', table_heading_format)
		worksheet.write_merge(row, row, 4, 4, 'Reference', table_heading_format)
		worksheet.write_merge(row, row, 5, 5, 'Amount', table_heading_format)
		if self.comm_type == 'received':
			worksheet.write_merge(row, row, 6, 6, 'Layered Discount', table_heading_format)

		row += 1
		srno = 1
		# Actual Data
		for mv_line in acc_mov_line_ids:
			ref = mv_line.pay_reference or ''
			worksheet.write_merge(row, row, 0, 0, srno, number_data_format)
			worksheet.write_merge(row, row, 1, 1, mv_line.date, date_format)
			worksheet.write_merge(row, row, 2, 2, mv_line.sale_line_id.order_id.name, char_data_format)
			worksheet.write_merge(row, row, 3, 3, mv_line.sale_line_id.product_id.display_name, char_data_format)
			worksheet.write_merge(row, row, 4, 4, ref, char_data_format)
			worksheet.write_merge(row, row, 5, 5, currency+' '+str(mv_line.debit), number_data_format)
			if self.comm_type == 'received':
				worksheet.write_merge(row, row, 6, 6, currency+' '+str(mv_line.credit), number_data_format)
			row += 1
			srno += 1

		stream = io.BytesIO()
		workbook.save(stream)
		out = base64.encodestring(stream.getvalue())
		self.write({'excel_file_data': out, 'file_name': filename})
		stream.close()
		return True