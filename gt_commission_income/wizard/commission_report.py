# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime
from odoo.exceptions import UserError
# from odoo.exceptions import ValidationError

class SaleCommissionReport(models.TransientModel):
    _inherit = 'sale.commission.report'

    comm_type = fields.Selection([('expense','Expense'), ('income','Income')], string="Commission Type", default='expense')
    type = fields.Selection([('receivable','Receivable'), ('payable','Payable'),('paid','Paid')], 'Receivable/Payable/Paid', default='payable')
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id.id)
    
    @api.onchange('commission_pay')
    def _get_commission_pay(self):
        for rec in self:
            rec.partner_id = False
            rec.sale_person = False

    @api.onchange('type')
    def _get_commission_type_paid(self):
        for rec in self:
            if rec.type == 'paid':
                rec.comm_type = 'expense'
            else:
                rec.comm_type = False

    @api.multi
    def action_commission_report(self):
        if self.type == 'payable':
            return self.env.ref('sale_commission_gt.action_payable_report_commission').report_action(self)
        if self.type == 'paid':
            return self.env.ref('sale_commission_gt.action_paid_report_commission').report_action(self)
        if self.type == 'receivable':
            if not self.partner_id:
                raise UserError(_("Please select Showroom Partner."))
            return self.env.ref('gt_commission_income.action_receivable_report_commission').report_action(self)
        
    def get_payable_movelines(self):
        """ This mehod will get the Payable move lines """
        wizard_obj = self.env['sale.commission.report']
        # wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        wizard_search = self
        salesman = wizard_search.sale_person
        move_line_obj = self.env['account.move.line']
        domain_search = [('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0), ('journal_id.type','=','purchase'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date), ('commission_on','!=',False)]
        if wizard_search.commission_pay == 'salesperson':
            domain_search.append(('commission_user','=',salesman.id))
            # move_line_search = move_line_obj.search([('commission_user','=',salesman.id), ('paid','=',False),('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
            move_line_search = move_line_obj.search(domain_search)
        else:
            domain_search.append(('commission_partner', '=', wizard_search.partner_id.id))
            # move_line_search = move_line_obj.search([('commission_partner', '=', wizard_search.partner_id.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)])
            move_line_search = move_line_obj.search(domain_search)
        return move_line_search
    
    def get_receivable_movelines(self):
        ''' This method will give Receivable Moves lines as per the wizard fields condition '''
        wizard_obj = self.env['sale.commission.report']
        # wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        # salesman = wizard_search.sale_person
        move_line_obj = self.env['account.move.line']
        domain_search = [('commission_partner', '=', self.partner_id.id), ('account_id.user_type_id.name', '=', 'Receivable'), ('paid', '=', False), ('debit', '!=', 0), ('journal_id.type','=','sale')]
        move_line_search = move_line_obj.search(domain_search)
        return move_line_search 
        
    def get_paid_movelines(self):
        '''This method will give Paid Moves lines as per the wizard fields condition '''
        wizard_obj = self.env['sale.commission.report']
        # wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        wizard_search = self
        salesman = wizard_search.sale_person
        move_line_obj = self.env['account.move.line']
        domain_search = [('paid','=',True), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)]
        if wizard_search and wizard_search.comm_type == 'expense':
            domain_search.append(('account_id.user_type_id.name', '=', 'Payable'))
            domain_search.append(('journal_id.type','=','purchase'))
        elif wizard_search and wizard_search.comm_type == 'income':
            domain_search.append(('account_id.user_type_id.name', '=', 'Receivable'))
            domain_search.append(('journal_id.type','=','sale'))
        if wizard_search.commission_pay == 'salesperson':
            domain_search.append(('commission_user','=',salesman.id))
            # move_line_search = move_line_obj.search([('commission_user','=',salesman.id), ('paid','=',True),('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
            move_line_search = move_line_obj.search(domain_search)
        else:
            domain_search.append(('commission_partner','=',wizard_search.partner_id.id))
            # move_line_search = move_line_obj.search([('commission_partner','=',wizard_search.partner_id.id), ('paid','=',True),('account_id.user_type_id.name', '=', 'Payable'), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date)])
            move_line_search = move_line_obj.search(domain_search)
        return move_line_search      
        
class AllCommissionReport(models.TransientModel):
    _inherit = 'all.commission.report'
    
    comm_type = fields.Selection([('expense','Expense'), ('income','Income')], string="Commission Type", default='expense')
    type = fields.Selection([('receivable','Receivable'), ('payable', 'Payable'), ('paid', 'Paid'), ('all', 'All')], 'Receivable/Payable/Paid/All', default='all')
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id.id)
    
    @api.onchange('type')
    def onchange_paid_unpaid(self):
        self.paid_unpaid_group = False
 
    @api.onchange('type')
    def _get_commission_type_paid(self):
        for rec in self:
            if rec.type == 'paid':
                rec.comm_type = 'expense'
            else:
                rec.comm_type = False
                
    def get_all_movelines(self):
        """ This method get all the paid/unpaid expense/income records """
        wizard_obj = self.env['all.commission.report']
        # wizard_search = wizard_obj.search([('id', '=', commission_report_id)])
        wizard_search = self
        move_line_obj = self.env['account.move.line']
        unpaid_domain = [('paid', '=', False), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date), ('commission_on','!=',False)]
        paid_domain = [('paid', '=',True), ('date', '>=', wizard_search.start_date), ('date', '<=', wizard_search.end_date), ('commission_on','!=',False)] 
        if wizard_search.type == 'payable' or wizard_search.comm_type == 'expense':
            unpaid_domain.append(('account_id.user_type_id.name', '=', 'Payable'))
            unpaid_domain.append(('credit', '!=', 0))
            unpaid_domain.append(('journal_id.type','=','purchase'))
            paid_domain.append(('account_id.user_type_id.name', '=', 'Payable'))
            paid_domain.append(('credit', '!=', 0))
            paid_domain.append(('journal_id.type','=','purchase'))
        elif wizard_search.type == 'receivable' or wizard_search.comm_type == 'income':
            unpaid_domain.append(('account_id.user_type_id.name', '=', 'Receivable'))
            unpaid_domain.append(('debit', '!=', 0))
            unpaid_domain.append(('journal_id.type','=','sale'))
            paid_domain.append(('account_id.user_type_id.name', '=', 'Receivable'))
            paid_domain.append(('debit', '!=', 0))
            paid_domain.append(('journal_id.type','=','sale'))
        if wizard_search.type == 'payable':
            move_line_search = move_line_obj.search(unpaid_domain)
        elif wizard_search.type == 'receivable':
            move_line_search = move_line_obj.search(unpaid_domain)
        elif wizard_search.type == 'paid':
            move_line_search = move_line_obj.search(paid_domain)
        elif wizard_search.type == 'all':
            move_line_search = move_line_obj.search(unpaid_domain)
            move_line_search |= move_line_obj.search(paid_domain)
        if wizard_search.paid_unpaid_group:
            unpaid_move_line_search  = move_line_obj.search(unpaid_domain)
            paid_move_line_search = move_line_obj.search(paid_domain)
            return [paid_move_line_search,unpaid_move_line_search]
        return move_line_search