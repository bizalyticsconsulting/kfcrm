# -*- coding: utf-8 -*-

from odoo import fields, models, api, _

class CommissionPopupWizard(models.TransientModel):
    _name = 'commission.popup.wizard'

    sale_order_id = fields.Many2one('sale.order', 'SaleOrder')

    @api.multi
    def process_showroom_commission_update(self):
        order = self.sale_order_id
        comm_line_not_created = False
        comm_so_lines = self.env.context.get('comm_so_lines')
        if len(order.account_move_line_ids) > 0:
            for so_line in comm_so_lines:
                so_comm_line = order.account_move_line_ids.filtered(lambda x: x.sale_line_id.id == so_line.id)
                if len(so_comm_line) > 0:
                    so_line.sale_line_supplier_commission_update(so_comm_line)
                else:
                    comm_line_not_created = True
        else:
            comm_line_not_created = True

        # If any of the sale orde line commission line did not created ..we can create using below.
        if comm_line_not_created:
            order.action_create_order_income_commission()

        return True
