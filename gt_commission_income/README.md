Step to configure Income commission

Step 1:-create a Chart of Account
		code:- 400001
		name:- Commission Income
		type:- Income

Step 2:-create a journal
		name:- Commission Income
		type:- sale
		default debit account :- select the account "400001" created in step 1.
		default credit account:- select the account "400001" created in step 1.

Step 3:- select the journal
		Accounting App --> setting -->Accounting
		Journals
			select the journal created in step 2 for the field "Commission Journal(Income)"

Step 4:-check the Commission(Income) field
		Accounting App --> setting --> CRM
		Sale Commission
			check the "Commission(Income)" field