 # -*- coding: utf-8 -*-

{
    'name': 'GT Sales Commission Income',
    'version': '1.0',
    'category': 'Sale',
    'sequence': 2,
    'summary': 'GT Sales Commission Receivable',
    'description': """ Odoo 12 Sales Commission/Incentive Calculations Based on Sales, Payments Received, Invoicing and Fully invoice payment & Shipped and showroom partners, this is for the Reveivable Commission goes to Vendor/Manufacturer . Based on the Sale commission extension """,
    'author': 'Globalteckz',
    'website': 'https://www.globalteckz.com',
    'depends': ['sale_commission_gt', 'gt_sale_commission_extension','branch'],
    'data': [
              'security/ir.model.access.csv',
              'views/sale_commission_view.xml',
              'views/sale_view.xml',
              'views/res_partner_view.xml',
              'views/product_view.xml',
              'wizard/commission_report_views.xml',
              'wizard/receive_commission.xml',
              'report/income_comm_report.xml',
              'report/receivable_commission_report.xml',
              'report/payable_commission_report.xml',
              'report/report_menu.xml',
              'report/paid_commission_report.xml',
              'report/all_commission_report.xml',

              'views/sale_scheduler_view.xml',
              'wizard/commission_popup_wizard_view.xml',
              'views/scheduler_cron.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
