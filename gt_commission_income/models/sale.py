# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import logging
from pprint import pprint

_logger = logging.getLogger(__name__)

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    commission_income = fields.Boolean("Commission(Income)", help="Income commission for Vendor/Manufacturer")
    commission_income_journal = fields.Many2one('account.journal', 'Commission Journal(Income)')
    
    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        res.update(
            commission_income_journal = int(ICPSudo.get_param('commission_income_journal')),
            commission_income = ICPSudo.get_param('commission_income')
        )
        return res

    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        ICPSudo.set_param("commission_income_journal", self.commission_income_journal.id)
        ICPSudo.set_param("commission_income", self.commission_income)
        
        
class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    # account_move_line_ids = fields.One2many('account.move.line','sale_order_id',
    #     'Account Move Line', help="Journal Items for the Sales Commissions", 
    #     domain=['|', '&','&',('account_id.user_type_id.name', '=', 'Payable'), 
    #     ('credit','!=',0), ('journal_id.type','=','purchase'),'&', '&',
    #     ('account_id.user_type_id.name', '=', 'Receivable'), 
    #     ('debit','!=',0), ('journal_id.type','=','sale')], copy=False)

    # account_move_line_ids = fields.One2many('account.move.line','sale_order_id',
    #     'Account Move Line', help="Journal Items for the Sales Commissions", 
    #     domain=[('account_id.user_type_id.name', '=', 'Income'),
    #             ('journal_id.name','=','Commission Income'),
    #             ('paid','=',False)], copy=False)

    account_move_line_ids = fields.One2many('account.move.line','sale_order_id',
        'Account Move Line', help="Journal Items for the Sales Commissions", 
        copy=False,
        domain= ["|",
                    "&",
                            ["account_id.user_type_id.name", "=", "Income"],
                            ["journal_id.name", "=", "Commission Income"],
                    "&", "&", "&",
                            ["account_id.user_type_id.name", "=", "Receivable"],
                            ["journal_id.name", "=", "Commission Income"],
                            ["commissionable_amount", "<", 0],
                            ["debit", ">", 0]
                 ])

    # domain = [('account_id.user_type_id.name', '=', 'Income'),
              # ('journal_id.name', '=', 'Commission Income')],


    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        if self._context.get('check_commission'):
            args.append(('account_move_line_ids', '!=', False))

        ### for sale order line search....sale order..10.03.2021
        if self.env.context.get('sale_search_so'):
            args += [('name', 'ilike', name)]

        sale_ids = self._search(args, limit=limit, access_rights_uid=name_get_uid)
        return self.browse(sale_ids).name_get()


    @api.multi
    def write(self, vals):
        """ If we change showroom, then method will check for commission line already created or not.
        1. If not created then will check the commission aginst the showroom and supplier..Create new commission lines for each sale order line if any
        2.  If already created then check for the new commission calculations against the supplier/showroom...update the existing commission lines for sale order line if any
        """
        ctx = self.env.context.copy()

        res = super(SaleOrder, self).write(vals)

        for order in self:
            # If we change showroom in sale order. Then check for the Commission Lines.
            # If commission not created then create commission against the showroom supplier commission rate
            # If already created and state is 'pending' then change the commission amount.
            if vals.get('branch_id') and len(order.order_line) > 0 and any(order.order_line.filtered(lambda x: x.product_id.flag)):
                comm_so_lines = order.order_line.filtered(lambda x: x.product_id.flag)
                comm_line_not_created = False
                ctx.update({'default_sale_order_id': order.id, 'comm_so_lines': comm_so_lines})

                # ### If any supplier commission with 0 percent or no percentage will raise wizard pop.
                # for sale_line in order.order_line.filtered(lambda x: x.product_id.flag):
                #     sale_line._compute_income_commission_amount()  # Call method to recalculate the commission based on the new showroom
                #     # if change showroom and there is either no supplier commission line for same or  percentage(%) is Zero.
                #     if sale_line.income_commission_cal_amount == 0 or sale_line.income_comm_percent == 0:
                #         ## raise pop wizard...If ok then process for the below method call...else normal process
                #         return {
                #             'type': 'ir.actions.act_window',
                #             'name': _('Showroom Commission Confirmation Wizard'),
                #             'res_model': 'commission.popup.wizard',
                #             'view_type': 'form',
                #             'view_mode': 'form',
                #             'target': 'new',
                #             'context': ctx,
                #         }
                #         # raise UserError(_(
                #         #     "You can not process to change 'Showroom'.\n Selected 'Showroom' supplier commission line either not available or percentage is Zero(0) !"))

                if len(order.account_move_line_ids) > 0:
                    for so_line in comm_so_lines:
                        so_comm_line = order.account_move_line_ids.filtered(lambda x: x.sale_line_id.id == so_line.id)
                        if len(so_comm_line) > 0:
                            so_line.sale_line_supplier_commission_update(so_comm_line)
                        else:
                            comm_line_not_created = True
                else:
                    comm_line_not_created = True
                # If any of the sale orde line commission line did not created ..we can create using below.
                if comm_line_not_created:
                    order.action_create_order_income_commission()

        return res


    @api.multi
    def process_income_commission_line(self, account_id):
        """ This method will Create Income Commission Lines,
         Case-1: If sale order line not associated already created commission lines.
         """
        order = self
        now = datetime.now()
        reference = '[' + order.name + '] Income Commission'
        if len(order.order_line.filtered(lambda x: x.product_id.flag)) == 0:
            _logger.warning("In Sale Order Line, No product is not Commissionable.")
        for sale_line in order.order_line.filtered(lambda x: x.product_id.flag):
            try:
                if sale_line.id in order.account_move_line_ids.mapped('sale_line_id').ids:
                    continue
                    # we can also write code here to update the commission also if showroom changd.

                sale_line._compute_income_commission_amount()  # Call method to recalculate the commission based on the new showroom

                # if change showroom and there iseither no supplier commission line for same or  percentage(%) is Zero.
                if sale_line.income_commission_cal_amount == 0 or sale_line.income_comm_percent == 0:
                    _logger.warning(f'The Sale Line Income Commission amount: {sale_line.income_commission_cal_amount} and Percentage: {sale_line.income_comm_percent}. \n'
                                 f'So that will not create Commission for this Sale Order Lines.')
                    continue
                    # raise UserError(_("You can not process to change 'Showroom'.\n Selected 'Showroom' supplier commission line either not available or percentage is Zero(0) !"))

                if not sale_line.product_id.flag:
                    continue

                # Validate SaleOrderLine Before create commission with respect to SDA
                if sale_line.discount > 0 or sale_line.discount_show > 0 or sale_line.discount_amount > 0:
                    if len(sale_line.sda_line_ids) == 0:
                        raise ValidationError(_(
                            f"Sale Order Line is having discount but Supplier Discount Approval Lines(SDA) is not entered for the same."
                            f"\n You can not process for the Income Commission ! \n\n Sale Order Line: {sale_line.product_id.display_name}"))

                # New Code to check SDA KF Commission is Zero
                if len(sale_line.sda_line_ids) > 0 and round(sale_line.sda_line_ids[0].ac_adjust_kf_commission, 2) == 0:
                    _logger.warning("SDA Line 'Adjusted KF Commission' field is 0. So that not to proceed for 'Income Commission' creation Process.")
                    continue

                layered_sda_id = False
                vendor = sale_line.x_vendor
                _logger.warning(
                    f"Income commission is calculated as per the given priority. Commission percentage: {sale_line.income_comm_percent} and amount is {sale_line.income_commission_cal_amount}")

                if vendor:
                    # and sale_line.income_commission_cal_amount:
                    comm_amount = sale_line.income_commission_cal_amount

                    # If commission amount in negative values..Then post commission journal entry with respect to same-reverse order credit/debit... else normal entry
                    create_commission = False
                    if sale_line.income_commission_cal_amount > 0:
                        create_commission = True
                        comm_amount_credit = abs(sale_line.income_commission_cal_amount)
                        comm_amount_debit = 0.0
                    elif sale_line.income_commission_cal_amount < 0:
                        create_commission = True
                        comm_amount_debit = abs(sale_line.income_commission_cal_amount)
                        comm_amount_credit = 0.0

                    if create_commission:
                        commission_vals = {
                            # 'name': account_id.default_credit_account_id.name,
                            'name': vendor.display_name,
                            'debit': comm_amount_debit or 0.0,
                            'credit': comm_amount_credit or 0.0,
                            # 'debit': 0.0,
                            # 'credit': comm_amount,
                            'account_id': account_id.default_credit_account_id.id,
                            'partner_id': vendor.id,
                            'commission_user': False,
                            'commission_on': order.order_pay_state,
                            'commission_partner': vendor.id,
                            'commission_pay': "showroom_partner",
                            'commissionable_amount': sale_line.price_subtotal,
                            'commission_rate': sale_line.income_comm_percent,
                            'sales_commission_rate': sale_line.income_comm_percent,
                            'total_commission': comm_amount,
                            'comm_type': 'income',
                            'sale_order_id': order.id,
                            'sale_line_id': sale_line.id,
                            'branch_id': order.branch_id.id,
                            'sda_id': len(sale_line.sda_line_ids) > 0 and sale_line.sda_line_ids.id or False,
                            'layered_sda_id': layered_sda_id,
                            'ref': sale_line.product_id.display_name,
                            'product_id': sale_line.product_id.id,
                        }

                        sale_person_vals = {
                            # 'name': order.branch_id.name,
                            'name': vendor.display_name,  # sale_line.product_id.display_name,
                            'debit': comm_amount_credit or 0.0,
                            'credit': comm_amount_debit or 0.0,
                            # 'debit': comm_amount,
                            # 'credit': 0.0,
                            'account_id': vendor.property_account_receivable_id.id,
                            'partner_id': vendor.id,
                            'commission_user': False,
                            'commission_on': order.order_pay_state,
                            'commission_partner': vendor.id,
                            'commission_pay': "showroom_partner",
                            'commissionable_amount': sale_line.price_subtotal,
                            'commission_rate': sale_line.income_comm_percent,
                            'sales_commission_rate': sale_line.income_comm_percent,
                            'total_commission': comm_amount,
                            'comm_type': 'income',
                            'sale_order_id': order.id,
                            'sale_line_id': sale_line.id,
                            'branch_id': order.branch_id.id,
                            'sda_id': len(sale_line.sda_line_ids) > 0 and sale_line.sda_line_ids.id or False,
                            'layered_sda_id': layered_sda_id,
                            'ref': sale_line.product_id.display_name,
                            'product_id': sale_line.product_id.id,
                        }

                        vals = {
                            'journal_id': account_id.id,
                            'date': now.strftime('%Y-%m-%d'),
                            'state': 'draft',
                            'ref': reference,  # journal_ref,
                            'partner_id': vendor.id,
                            'commission_user': False,
                            'commission_partner': vendor.id,
                            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                            'branch_id': order.branch_id.id,
                        }
                        move = self.env['account.move'].create(vals)
                        _logger.warning(f"Income commission entires generated i.e. account.move entries : {move}")
                else:
                    _logger.warning(
                        "Vendor does not have selected in Sale Order Line or no commission percentage setup as per the priority.")

                # Update sda Line data if any
                if len(sale_line.sda_line_ids) > 0:
                        _logger.warning(
                            "Sale Order Line Linked SDA: {sale_line.sda_line_ids}, Update commission status and created date.")
                        sale_line.sda_line_ids.write({'commission_status': 'created', 'commission_created_date': now})

            except Exception as e:
                _logger.warning(f"Error on sale order line while commission process: {e}")

        return True


    @api.multi
    def action_create_order_income_commission(self):
        """ This method will create commission against the sale order line if,
        1. Check for the Invoice Fully Paid
        2. Check for Delivery orders fully done
        3. Sale order line is not associated with any already created commission lines
        """
        commission_income = self.env['ir.config_parameter'].sudo().get_param('commission_income')
        commission_income_journal = int(self.env['ir.config_parameter'].sudo().get_param('commission_income_journal'))
        account_id = self.env['account.journal'].browse(commission_income_journal)

        context = dict(self.env.context or {})
        context.update({'biz_creating_comms_lines': True})

        if commission_income and commission_income_journal:
            for order in self.filtered(lambda x: x.branch_id):
                # if len(order.account_move_line_ids) > 0:
                #     continue
                if order.state in ['draft', 'sent', 'cancel']:
                    continue

                #### 22.04.2021: Comment Code: Not to check & Validate For Payments/Stock Validate
                # ### First Validate Invoice as Paid and Stock fully Delivered.
                # _logger.warning("Before Income Commission processing, checking for the 'Delivery Order Fully Validated' and 'Invoice Fully Paid'.")
                # ### 1. Process to Validate Stock fully Delivered.
                # ordered_product_qty = sum([i.product_uom_qty for i in order.order_line if i.product_id.type != 'service'])
                # delivered_done_product_qty = sum(
                #     [j.quantity_done for i in order.picking_ids.filtered(lambda x: x.state == 'done') for j
                #      in i.move_ids_without_package])
                # fully_delivered_qty = ordered_product_qty <= delivered_done_product_qty
                # line_prod_not_full_delvd = any([i for i in order.order_line if i.product_id.type != 'service' if i.product_uom_qty > i.qty_delivered])
                # _logger.warning("Sale Order fully delivered product quantity: %s | %s ", fully_delivered_qty, line_prod_not_full_delvd)
                # if line_prod_not_full_delvd:
                #     continue
                # not_done_state = any([i for i in order.picking_ids if i.state != 'done'])
                # if not_done_state:
                #     continue

                # ### 2. First Validate Invoice as fully Paid
                # invoice_payments = sum([k.amount for j in order.invoice_ids for k in j.payment_ids if k.state in ['posted', 'reconciled']])

                # if not order.account_move_line_ids and (order.picking_ids and not line_prod_not_full_delvd and fully_delivered_qty) \
                #         and (order.amount_total <= invoice_payments):

                # 11.07.2021 if commission line is alrady created then also  new commission line can be created for the new sale order line
                # if not order.account_move_line_ids:

                if not order.x_skip_commission:  # Skip Commission Process if True
                    _logger.warning("Income Commission processing for change showroom : %s | %s", order, order.branch_id.name)
                    order.with_context(context).process_income_commission_line(account_id)

        return True


    @api.model
    def action_confirm(self):
        """
        This method will create the Sale Income Commission if state is in 'sale', 'done' and commission is not created already
        """
        res = super(SaleOrder, self).action_confirm()

        # This will create Sale Income Commission If 'sale' or 'done' And Commission is not created...22.04.2021
        if self.state in ['sale', 'done'] and not self.account_move_line_ids:
            _logger.info(_("Process to create the Income Commission....."))
            self.action_create_order_income_commission()

        return res


    @api.model
    def action_scheduler_create_income_commission(self):
        """
        This method will create Income Commission for which sale order is not already created, state is in 'sale', 'done'
        """
        sale_order_ids = self.env['sale.order'].search([('state', 'in', ['sale', 'done']),
                                                        ('account_move_line_ids', '=', False)],
                                                        order="id desc")
        _logger.info(_(f"Sale Order Counts: {len(sale_order_ids)}"))
        count = 1
        for sale_rec in sale_order_ids:
            try:
                _logger.info(f'Sale Order Income Commission Process Count: {count}')
                # if sale_rec.state not in ['sale', 'done']:
                #     continue
                # if len(sale_rec.account_move_line_ids) > 0:
                #     continue

                # This will create Sale Income Commission for each Sale Order which is not created already
                if sale_rec.state in ['sale', 'done'] and not sale_rec.account_move_line_ids:
                    sale_rec.sudo().action_create_order_income_commission()
                    self.env.cr.commit()
                count += 1
            except Exception as e:
                _logger.info(f"Error in scheduler: {e}")

        return True


    def action_mark_paid_zero_commission(self):
        """
        This method will Mark To 'Paid' for,
            - Zero Commission Amount Line
        """
        for order in self.filtered(lambda x: x.account_move_line_ids):
            for comm_line in order.account_move_line_ids.filtered(lambda x: x.commission_pay and not x.paid):
                # Update the Counter Entry with the same Debit, Credit, Balance and Residual Amount with 0.
                counter_comm_lines = comm_line.move_id.line_ids.filtered(lambda x: x.sale_line_id and (
                        x.debit > 0 or x.credit > 0 or x.amount_residual > 0 or x.balance > 0))
                if len(counter_comm_lines) > 0:
                    if comm_line.move_id.state == 'posted':
                        comm_line.move_id.button_cancel()
                    for line in counter_comm_lines:
                        line.write({
                            'debit': 0,
                            'credit': 0,
                            'balance': 0,
                            'amount_residual': 0
                        })
                    if comm_line.move_id.state == 'draft':
                        comm_line.move_id.action_post()

                if (comm_line.credit == 0 and comm_line.debit == 0):
                    comm_line.move_id.line_ids.write({
                        'paid': True,
                        'com_pay_status': 'fully_paid'
                    })

        return True


    @api.model
    def action_scheduler_mark_zero_commission_paid(self):
        """
        This method will search for the Sale Order Commission Line and Mark them Fully Paid in case of Zero Amount.
            - Mark Zero Commission Line 'Paid'
            - If Counter Commission Line is not 0 Amount then update with 0 amount.
        """
        commission_move_line_ids = self.env['account.move.line'].search([
            ('sale_order_id', '!=', False),
            ('commission_pay', '!=', False),
            ('paid', '=', False),
            ('credit', '=', 0),
            ('debit', '=', 0),
        ], order="id")

        _logger.warning("Total Commission Lines with Zero Amount: %s", len(commission_move_line_ids))

        for comm_line in commission_move_line_ids.filtered(lambda x: x.sale_order_id):
            # Call Sale Order Method to Mark Zero 'Paid'
            comm_line.sale_order_id.action_mark_paid_zero_commission()


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"


    income_commission_cal_amount = fields.Float("Income Commission", compute="_compute_income_commission_amount", store=True, copy=False, digits=(12,4), help="This is income commission based on the commission priority and SDA discount calculations amount.")
    income_comm_percent = fields.Float(string="Commission(%)", store=True, compute='_compute_income_commission_amount', copy=False, digits=(12, 4), help="This is income commission (%) based on the commission priority and SDA discount calculations (%).")


    @api.depends('price_unit', 'price_subtotal', 'x_vendor', 'product_id')
    def _compute_income_commission_amount(self):
        for rec in self:
            income_commission_amount = 0.0
            income_comm_percent = 0.0
            vendor_commission_id = rec.x_vendor.supplier_commission_lines.filtered(
                lambda x: x.branch_id.id == rec.order_id.branch_id.id and x.company_id.id == rec.company_id.id)

            vendor_commission_rate = vendor_commission_id and vendor_commission_id.commission_rate or 0.0
            category_commission_id = rec.product_id.categ_id.category_commission_lines.filtered(
                lambda x: x.branch_id.id == rec.order_id.branch_id.id and x.company_id.id == rec.company_id.id)
            category_commission_rate = category_commission_id and category_commission_id.commission_rate or 0.0
            if rec.product_id and rec.product_id.flag:
                if rec.product_id.prod_comm_per > 0:
                    income_comm_percent = rec.product_id.prod_comm_per
                    income_commission_amount = rec.price_subtotal * rec.product_id.prod_comm_per / 100
                elif category_commission_rate > 0:
                    income_comm_percent = category_commission_rate
                    income_commission_amount = rec.price_subtotal * category_commission_rate / 100
                elif vendor_commission_rate > 0:
                    #If Supplier Commission Line is not added or Commission Percentage is 0..Then raise an alert message.
                    income_comm_percent = vendor_commission_rate
                    income_commission_amount = rec.price_subtotal * vendor_commission_rate / 100
                # rec.income_comm_cal_amount = income_commission_amount
            rec.income_commission_cal_amount = income_commission_amount
            rec.income_comm_percent = income_comm_percent


    # Temporary Method for calculate the Sale Order Line Commission amount using compute method.
    @api.model
    def action_scheduler_update_commission_sol(self):
        """ This method will calculate all the sale order line which have sale order not confirmed and commission is 0 """
        sale_order_line_ids = self.search([('order_id.state', 'in', ['draft', 'sent']), ('income_comm_percent', '=', 0)])
        _logger.info("Sale Order Line count for commission Zero: %s", len(sale_order_line_ids))
        for order_line in sale_order_line_ids:
            order_line._compute_income_commission_amount()


    @api.multi
    def write(self, vals):
        """
        This method will change commission lines ('pending') amount if sale order line qty/amount.
        """
        _logger.info("Context .... %s", self.env.context)

        res = super(SaleOrderLine, self).write(vals)
        sale_line_commission_ids = self.order_id.account_move_line_ids.filtered(lambda x: x.sale_line_id.id == self.id and x.com_pay_status == 'pending')
        if len(sale_line_commission_ids) > 0 and self.order_id.state in ['sale', 'done'] and ('product_uom_qty' in vals or 'price_unit' in vals):
            # How to handle the Negative value too in the case of update...Try to resolve the same issue
            if len(self.sda_line_ids) == 0:
                sale_line_comm_debit = self.env['account.move.line'].search([('sale_line_id', '=', self.id), ('comm_type', '=', 'income'), ('debit', '>', 0), ('com_pay_status', '=', 'pending')], limit=1)
                for comm_line in sale_line_commission_ids:
                    if comm_line.credit > 0:
                        comm_line.write({'commissionable_amount': self.price_subtotal, 'credit': self.income_commission_cal_amount, 'total_commission': self.income_commission_cal_amount})
                if len(sale_line_comm_debit) > 0:
                    sale_line_comm_debit.write({'commissionable_amount': self.price_subtotal, 'total_commission': self.income_commission_cal_amount})
        return res


    def create(self, vals):
        """
        This method will create sale order line and also create commission line if,
        - Commission is already created
        - Invoices is created and 'paid'
        - commission amount is -= 0
        - SaleOrderLine 'Product' is commissionable
        - Sale Order state is in 'sale'/'done'
        - Sale Order not is x_skip_commission
        """
        res = super(SaleOrderLine, self).create(vals)

        for sol_rec in res:
            if sol_rec.product_id.flag and sol_rec.income_commission_cal_amount != 0 \
                    and sol_rec.order_id and not sol_rec.order_id.x_skip_commission \
                    and sol_rec.order_id.state in ['sale', 'done'] and len(sol_rec.order_id.invoice_ids) > 0 \
                    and (len(sol_rec.order_id.invoice_ids) == 1 and sol_rec.order_id.invoice_ids.state == 'paid') \
                    and len(sol_rec.order_id.account_move_line_ids) > 0:
                sol_rec.normal_revers_commission_income_entries()
        return res


    @api.multi
    def normal_revers_commission_income_entries(self):
        """
        This method will create income commission in reverse order i.e. if negative value then create it.
        If positive value, then need to ask what should we do?
        """
        _logger.info(f"Start Commission Entries for new sale order line: {self}")
        AccountJournal = self.env['account.journal']
        commission_income_journal = int(self.env['ir.config_parameter'].sudo().get_param('commission_income_journal'))
        account_id = AccountJournal.browse(commission_income_journal)
        if not account_id:
            return True
        if not self.order_id.branch_id:
            return True
        if not self.product_id.flag:
            return True
        if self.income_commission_cal_amount == 0:
            return True
        now = datetime.now()

        if self.order_id.branch_id:
            sale_line = self
            reference = '[' + sale_line.order_id.name + '] ' + 'Commission'

            # Validate SaleOrderLine Before create commission with respect to SDA
            sale_line_sda_id = sale_line.sda_line_ids #.filtered(lambda x: x.sale_line_id.id == sale_line.id)
            if sale_line.discount > 0 or sale_line.discount_show > 0 or sale_line.discount_amount > 0:
                if len(sale_line_sda_id) == 0:
                    raise ValidationError(_(
                        f"Sale Order Line is having discount but Supplier Discount Approval Lines(SDA) is not entered for the same.\n You can not process for the Income Commission ! \n\n Sale Order Line: {sale_line.product_id.display_name}"))
            layered_sda_id = False
            vendor = sale_line.x_vendor
            _logger.info(
                f"Income commission is calculated as per the given priority. Commission percentage: {sale_line.income_comm_percent} and amount is {sale_line.income_commission_cal_amount}")

            if vendor:
                # If commission amount in negative values..Then post commission journal entry with respect to same-reverse order credit/debit... else normal entry
                create_commission = False
                if sale_line.income_commission_cal_amount > 0:
                    create_commission = True
                    comm_amount_credit = sale_line.income_commission_cal_amount
                    comm_amount_debit = 0.0
                elif sale_line.income_commission_cal_amount < 0:
                    create_commission = True
                    comm_amount_debit = abs(sale_line.income_commission_cal_amount)
                    comm_amount_credit = 0.0
                if create_commission:
                    commission_vals = {
                        # 'name': account_id.default_credit_account_id.name,
                        'name': vendor.display_name,

                        'debit': comm_amount_debit or 0.0,
                        'credit': comm_amount_credit or 0.0,

                        'account_id': account_id.default_credit_account_id.id,
                        'partner_id': vendor.id,
                        'commission_user': False,
                        'commission_on': sale_line.order_id.order_pay_state,
                        'commission_partner': vendor.id,
                        'commission_pay': "showroom_partner",
                        'commissionable_amount': sale_line.price_subtotal,
                        'commission_rate': sale_line.income_comm_percent,
                        'sales_commission_rate': sale_line.income_comm_percent,
                        'total_commission': sale_line.income_commission_cal_amount,
                        'comm_type': 'income',
                        'sale_order_id': sale_line.order_id.id,
                        'sale_line_id': sale_line.id,
                        'branch_id': sale_line.order_id.branch_id.id,
                        'sda_id': len(sale_line_sda_id) > 0 and sale_line_sda_id.id or False,
                        'layered_sda_id': layered_sda_id,
                        'ref': sale_line.product_id.display_name,
                        'product_id': sale_line.product_id.id,
                    }

                    sale_person_vals = {
                        # 'name': sale_search.branch_id.name,
                        'name': vendor.display_name, # sale_line.product_id.display_name,

                        'debit': comm_amount_credit or 0.0,
                        'credit': comm_amount_debit or 0.0,

                        'account_id': vendor.property_account_receivable_id.id,
                        'partner_id': vendor.id,
                        'commission_user': False,
                        'commission_on': sale_line.order_id.order_pay_state,
                        'commission_partner': vendor.id,
                        'commission_pay': "showroom_partner",
                        'commissionable_amount': sale_line.price_subtotal,
                        'commission_rate': sale_line.income_comm_percent,
                        'sales_commission_rate': sale_line.income_comm_percent,
                        'total_commission': sale_line.income_commission_cal_amount,
                        'comm_type': 'income',
                        'sale_order_id': sale_line.order_id.id,
                        'sale_line_id': sale_line.id,
                        'branch_id': sale_line.order_id.branch_id.id,
                        'sda_id': len(sale_line_sda_id) > 0 and sale_line_sda_id.id or False,
                        'layered_sda_id': layered_sda_id,
                        'ref': sale_line.product_id.display_name,
                        'product_id': sale_line.product_id.id,
                    }

                    vals = {
                        'journal_id': account_id.id,
                        'date': now.strftime('%Y-%m-%d'),
                        'state': 'draft',
                        'ref': reference, #journal_ref,
                        'partner_id': vendor.id,
                        'commission_user': False,
                        'commission_partner': vendor.id,
                        'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                        'branch_id': sale_line.order_id.branch_id.id,
                    }
                    move = self.env['account.move'].create(vals)
                    _logger.info(f"Income commission entries generated i.e. account.move entries : {move}")
            else:
                _logger.warning(
                    "Vendor does not have selected in Sale Order Line or no commission percentage setup as per the priority.")

            # Update sda Line data if any
            if len(sale_line.sda_line_ids) > 0:
                _logger.info("Sale Order Line Linked SDA: {sale_line.sda_line_ids}, Update commission status and created date.")
                sale_line.sda_line_ids.write({'commission_status': 'created', 'commission_created_date': now})

            return True


    @api.multi
    def sale_line_supplier_commission_update(self, commission_line):
        """
        This method will update the supplier commission details if showroom get changed and commission in pending state.
        """
        self._compute_income_commission_amount() # Calculate commission part for new showroom
        if commission_line.paid or commission_line.com_pay_status != 'pending':
            raise UserError(_("You can not change showroom because commission already paid for current sale order !"))
            # return True

        # if change showroom and there iseither no supplier commission line for same or  percentage(%) is Zero.
        if self.income_commission_cal_amount == 0 or self.income_comm_percent == 0:
            raise UserError(_("You can not process to change 'Showroom'.\n Selected 'Showroom' supplier commission line either not available or percentage is Zero(0) !"))

        for comm_line in commission_line.filtered(lambda x: not x.paid or x.com_pay_status == 'pending'):
            # if comm_line.credit > 0:
            comm_line.move_id.branch_id = self.order_id.branch_id.id
            comm_line.write({'credit': self.income_commission_cal_amount,
                             'total_commission': self.income_commission_cal_amount,
                             'commission_rate': self.income_comm_percent,
                             'sales_commission_rate': self.income_comm_percent,
                             'branch_id': self.order_id.branch_id.id,
                             # 'commissionable_amount': self.price_subtotal,
                            })

            # Search commisison line for debit amount
            sale_line_comm_debit = self.env['account.move.line'].search(
                                                [('sale_line_id', '=', self.id), ('comm_type', '=', 'income'),
                                                 ('com_pay_status', '=', 'pending')], limit=1) #('debit', '>', 0),
            if len(sale_line_comm_debit) > 0:
                sale_line_comm_debit.write({'total_commission': self.income_commission_cal_amount,
                                            'commission_rate': self.income_comm_percent,
                                            'sales_commission_rate': self.income_comm_percent,
                                            'branch_id': self.order_id.branch_id.id,
                                            # 'commissionable_amount': self.price_subtotal,
                                            })
        return True


class CommissionRateAmount(models.TransientModel):
    _name = "commission.rate.amount"

    receive_comm_rate = fields.Float(string='Receive Commission Rate')
    receive_comm_amt = fields.Float(string='Receive Commission Amount')
    commissionable_amount = fields.Float(string='Receivable Commissionable Amount')

    @api.model
    def default_get(self, fields):
        rec = super(CommissionRateAmount, self).default_get(fields)
        acc_move_line_id = self.env['account.move.line'].browse(self._context.get('active_id'))
        rec.update({'receive_comm_amt': acc_move_line_id.credit,
                    'receive_comm_rate': acc_move_line_id.commission_rate,
                    'commissionable_amount': acc_move_line_id.commissionable_amount})
        return rec

    @api.onchange('receive_comm_rate')
    def onchange_rate(self):
        if self.receive_comm_rate:
            self.receive_comm_amt = ((self.receive_comm_rate / 100) * self.commissionable_amount)


    @api.onchange('receive_comm_amt')
    def onchange_amount(self):
        if self.receive_comm_amt:
            self.receive_comm_rate = ((self.receive_comm_amt * 100) / self.commissionable_amount)

    def process(self):
        acc_move_line_id = self.env['account.move.line'].browse(self._context.get('active_id'))
        acc_move_line_id.update({'credit': self.receive_comm_amt})


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    receive_comm_rate = fields.Float(string='Receive Commission Rate',readonly=False)
    receive_comm_amt = fields.Float(string='Receivable Commission Amount',readonly=False)

    @api.multi
    def change_comm_rate_amt(self):
        return {
        #'name': self.order_id,
        'res_model': 'commission.rate.amount',
        'type': 'ir.actions.act_window',
        'context': {},
        'view_mode': 'form',
        'view_type': 'form',
        'view_id': self.env.ref("gt_commission_income.commission_rate_amount_form_view").id,
        'target': 'new'
    }
