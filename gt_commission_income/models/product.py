# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ProductCategory(models.Model):
   _inherit = "product.category"


   category_commission_lines = fields.One2many("category.commission.line", 'category_id', 'Category Commission Lines')


class CategoryCommissionLine(models.Model):
   _name = "category.commission.line"


   category_id = fields.Many2one("product.category", "Product Category", ondelete="cascade", index=True)
   branch_id = fields.Many2one("res.branch", "Showroom", default=lambda self: self.env.user.branch_id)
   commission_rate = fields.Float("Commission Rate (%)", digits=(12,4))
   company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company']._company_default_get(
                                   'category.commission.line'))


   _sql_constraints = [
      ('category_commis_uniq', 'UNIQUE (branch_id, category_id, company_id)', 'You can not have two commission lines with the same category and branch !')
   ]


