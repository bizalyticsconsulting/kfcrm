# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

class SaleCommission(models.TransientModel):
    _inherit = 'sale.commission'
                
    comm_type = fields.Selection([('expense','Expense'), ('income','Income')], default='expense', string="Type")
    
    @api.onchange('comm_type')
    def onchange_commission_type(self):
        ''' Get Commission Journal based on the commission type '''
        if self.comm_type:
            if self.comm_type == 'expense':
                commission_journal = int(self.env['ir.config_parameter'].sudo().get_param('account.commission_journal'))
                # commission_journal = self.env['ir.values'].get_default('account.config.settings','commission_journal')
                if commission_journal != self.commission_journal.id:
                    self.commission_journal = commission_journal
            elif self.comm_type == 'income':
                commission_journal = int(self.env['ir.config_parameter'].sudo().get_param('commission_income_journal'))
                # commission_journal = self.env['ir.values'].get_default('account.config.settings','commission_income_journal')
                if commission_journal != self.commission_journal.id:
                    self.commission_journal = commission_journal
        
    @api.onchange('partner_id','start_date','end_date','comm_type')
    def _get_partner_move_lines(self):
        move_line_obj = self.env['account.move.line']
        search_domain = [('commission_partner','=',self.partner_id.id), ('paid','=',False), ('commission_on','!=',False)]
        if self.comm_type and self.comm_type == 'expense':
            # [('account_id.user_type_id.name', '=', 'Payable'), ('credit','!=',0), ('journal_id.type','=','purchase'), ('paid','=',False), ('commission_on','!=',False)]
            # search_domain.append(('comm_type','=',str(self.comm_type)))
            search_domain.append(('credit', '!=', 0))
            search_domain.append(('account_id.user_type_id.name', '=', 'Payable'))
            search_domain.append(('journal_id.type','=','purchase'))
        elif self.comm_type and self.comm_type == 'income':
            # [('account_id.user_type_id.name', '=', 'Receivable'), ('debit','!=',0), ('journal_id.type','=','sale'), ('paid','=',False), ('commission_on','!=',False)]
            # search_domain.append(('comm_type','=',str(self.comm_type)))
            search_domain.append(('debit', '!=', 0))
            search_domain.append(('account_id.user_type_id.name', '=', 'Receivable'))
            search_domain.append(('journal_id.type','=','sale'))
        if self.start_date:
            search_domain.append(('date','>=',self.start_date))
        if self.end_date:
            search_domain.append(('date','<=',self.end_date))
        move_line_search = move_line_obj.search(search_domain)
        # total_amount = 0
        commission_lines = []
        if self.partner_id:
            for mid in move_line_search:
                # total_amount += mid.credit
                if self.comm_type == 'expense':
                    comm_amount = mid.credit
                elif self.comm_type == 'income':
                    comm_amount = mid.debit
                else:
                    comm_amount = 0
                commission_lines.append((0,0,{'date':mid.date, 'reference':mid.ref, 'move_ref': mid.move_id.name, 'amount': comm_amount, 'account_move_line_id':mid.id, 'sale_commission_id': self.id, 'commissionable_amount': mid.commissionable_amount, 'commission_rate': mid.commission_rate, 'sales_commission_rate': mid.sales_commission_rate, 'total_commission': mid.total_commission, 'journal_id': mid.journal_id.id}))
                # self.p_move_lines = move_line_search.ids
                # self.payment_amt = total_amount
        self.update({'commission_account_lines': commission_lines})

    @api.onchange('sale_person','start_date','end_date')
    def _get_move_lines(self):
        move_line_obj = self.env['account.move.line']
        search_domain = [('commission_user','=',self.sale_person.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)]
        if self.comm_type and self.comm_type == 'expense':
            # search_domain.append(('comm_type','=',str(self.comm_type)))
            search_domain.append(('journal_id.type','=','purchase'))
        if self.start_date:
            search_domain.append(('date','>=',self.start_date))
        if self.end_date:
            search_domain.append(('date','<=',self.end_date))
        move_line_search = move_line_obj.search(search_domain)
        total_amount = 0
        commission_lines = []
        if self.sale_person:
            for mid in move_line_search:
                # total_amount += mid.credit
                # commission_lines.append((0,0,{'date':mid.date, 'reference':mid.ref, 'move_ref': mid.move_id.name, 'amount': mid.credit, 'account_move_line_id':mid.id, 'sale_commission_id': self.id}))
                commission_lines.append((0,0,{'date':mid.date, 'reference':mid.ref, 'move_ref': mid.move_id.name, 'amount':  mid.credit, 'account_move_line_id':mid.id, 'sale_commission_id': self.id, 'commissionable_amount': mid.commissionable_amount, 'commission_rate': mid.commission_rate, 'sales_commission_rate': mid.sales_commission_rate, 'total_commission': mid.total_commission, 'journal_id': mid.journal_id.id}))
                
                # self.move_lines = move_line_search.ids
                # self.payment_amt = total_amount
        self.update({'commission_account_lines': commission_lines})
                
    @api.onchange('commission_pay')
    def _get_commission_pay(self):
        for rec in self:
            rec.partner_id = False
            rec.sale_person = False
            rec.commission_lines = False
            if self.commission_pay == 'salesperson':
                rec.comm_type = 'expense'

    @api.onchange('commission_account_lines')
    def onchange_payment_amt(self):
        self.payment_amt = sum([i.amount for i in self.commission_account_lines])
        
    @api.multi
    def do_payment(self):
        """ This method will do the commission payments for both expense/income """
        move_line_obj = self.env['account.move.line']
        paid_commission_obj = self.env['search.commission.paid.check']
        if self.commission_pay == 'salesperson':
            # search_domain = [('commission_user', '=', self.sale_person.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)]
            # if self.start_date:
            #     search_domain.append(('date','>=',self.start_date))
            # if self.end_date:
            #     search_domain.append(('date','<=',self.end_date))
            # move_line_search = move_line_obj.search(search_domain)
            commission_partner = self.sale_person.partner_id
            commission_user = self.sale_person.id
            comm_partner = False
        elif self.commission_pay == 'showroom_partner':
            # search_domain = [('commission_partner', '=', self.partner_id.id), ('account_id.user_type_id.name', '=', 'Payable'), ('paid', '=', False), ('credit', '!=', 0)]
            # if self.start_date:
            #     search_domain.append(('date','>=',self.start_date))
            # if self.end_date:
            #     search_domain.append(('date','<=',self.end_date))
            # move_line_search = move_line_obj.search(search_domain)
            commission_partner = self.partner_id
            commission_user = False
            comm_partner = commission_partner.id
        else:
            pass
            return False
        
        # Post The Journal Entry
        post_move = [i.account_move_line_id.move_id for i in self.commission_account_lines if i.account_move_line_id and i.account_move_line_id.move_id.state == 'draft']
        _logger.debug("post move---------------------- ")
        for jmove in post_move:
            jmove.post()
        # total_amount = 0
        # for id in move_line_search:
            # total_amount += id.credit
        total_amount = sum([i.amount for i in self.commission_account_lines])
        now = datetime.now()
        if self.comm_type == 'expense':  
            _logger.debug ("expense--------------------")
            commission_vals = {
                'name': self.payment_journal.default_credit_account_id.name,
                'credit': total_amount,
                'debit': 0.0,
                'account_id': self.payment_journal.default_credit_account_id.id,
                'partner_id': commission_partner.id,
                'commission_user': commission_user,
                'commission_partner': comm_partner,
            }
            sale_person_vals = {
                'name': commission_partner.name,
                'credit': 0.0,
                'debit': total_amount,
                'account_id': commission_partner.property_account_payable_id.id,
                'partner_id': commission_partner.id,
                'commission_user': commission_user,
                'commission_partner': comm_partner,
            }
            vals = {
                'journal_id': self.payment_journal.id,
                'date': now.strftime('%Y-%m-%d'),
                'state': 'draft',
                'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                'ref': self.pay_reference,
                'partner_id': commission_partner.id,
                'commission_user': commission_user,
                'commission_partner': comm_partner,
                'check_no': self.check_number,
                
            }
            move = self.env['account.move'].create(vals)
            _logger.debug ("move create----------------- ")
        elif self.comm_type == 'income':
            _logger.debug ("income-------------------")
            move = self.commission_pay_income_entries(total_amount, commission_partner, commission_user, comm_partner)
            _logger.debug ("move =================",move)
        _logger.debug ("move----------------------sssssssssss-- ")
        move.post()
        for mlid in self.commission_account_lines:
            mlid.account_move_line_id.write({'paid': True, 'check_no': self.check_number, 'paid_move_id': move.id, 'pay_reference': self.pay_reference})
        #Create Check Payment record
        paid_commission_id = paid_commission_obj.create({'partner_id': commission_partner.id, 'commission_pay':self.commission_pay, 'check_number': self.check_number, 'sale_person': self.sale_person.id, 'pay_reference': self.pay_reference, 'pay_amount': total_amount, 'paid_move_id': move.id, 'date':now.strftime('%Y-%m-%d'), 'comm_type': str(self.comm_type)})
        return move.id

    @api.multi
    def commission_pay_income_entries(self, total_amount, commission_partner, commission_user, comm_partner):
        """ Income Commission  """
        
        commission_vals = {
            'name': self.payment_journal.default_debit_account_id.name,
            'credit': 0.0,
            'debit': total_amount,
            'account_id': self.payment_journal.default_debit_account_id.id,
            'partner_id': commission_partner.id,
            'commission_user': commission_user,
            'commission_partner': comm_partner,
        }
        sale_person_vals = {
            'name': commission_partner.name,
            'credit': total_amount,
            'debit': 0.0,
            'account_id': commission_partner.property_account_receivable_id.id,
            'partner_id': commission_partner.id,
            'commission_user': commission_user,
            'commission_partner': comm_partner,
        }
        now = datetime.now()
        vals = {
            'journal_id': self.payment_journal.id,
            'date': now.strftime('%Y-%m-%d'),
            'state': 'draft',
            'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
            'ref': self.pay_reference,
            'partner_id': commission_partner.id,
            'commission_user': commission_user,
            'commission_partner': comm_partner,
            'check_no': self.check_number,
            }
        move = self.env['account.move'].create(vals)
        return move
    
class SearchCommissionPaidCheck(models.Model):
    _inherit = "search.commission.paid.check"
          
    comm_type = fields.Selection([('expense','Expense'), ('income','Income')], default='expense', string="Type")
     