# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

class StockPicking(models.Model):
    _inherit = "stock.picking"

    @api.multi
    def action_done(self):
        res = super(StockPicking, self).action_done()
        ctx = self.env.context.copy()
        sale_obj = self.env['sale.order']

        # commission_income = self.env['ir.config_parameter'].search([('key', '=', 'commission_income')])
        commission_income = self.env['ir.config_parameter'].sudo().get_param('commission_income')
        commission_type = self.env['ir.config_parameter'].search([('key', '=', 'gt_sale_commission_extension.sale.commission_calculation')]).value
        #_logger.debug("Commission Income is ..... %s", commission_income)
        #_logger.debug("commission_journal -- " + str(commission_type) + " - " + str(self.order_pay_state))
        sale_id = sale_obj.search([('name', '=', self.origin)], order="id desc", limit=1)
        commission_income_journal = int(self.env['ir.config_parameter'].search([('key','=','commission_income_journal')]).value)
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_income_journal)

        if self.picking_type_id and self.picking_type_id.code == 'outgoing':
            _logger.debug("Checking for picking type is outgoing .... passed")
            # Check for the FUll PAYMENT against the SaleOrder and Full DeliveryOrder FUlly Shipped
            invoice_payments = 0
            _logger.debug("Invoice IDs are .... %s", sale_id.invoice_ids)
            for j in sale_id.invoice_ids:
                _logger.debug("Payment IDs are .... %s", j.payment_ids)
                for k in j.payment_ids:
                    _logger.debug("Payment status is ......%s", k.state)
                    if k.state in ['posted', 'reconciled']:
                        invoice_payments += k.amount
                        _logger.debug("Payment amount is .... %s", k.amount)
            #invoice_payments = sum(
            #    [k.amount for j in sale_id.invoice_ids for k in j.payment_ids if k.state in ['posted', 'reconciled']])

            _logger.debug("Amount total .... %s", sale_id.amount_total)
            _logger.debug("Invoice Payments .... %s", invoice_payments)

            if sale_id.amount_total <= invoice_payments:
                # Check for the Fully Shipped
                not_done_state = any([i for i in sale_id.picking_ids if i.state != 'done'])

                _logger.debug("NOT_DONE_STATE is ..... %s", not_done_state)
                if not_done_state:
                    return res
                line_prod_not_full_delvd = any([i for i in sale_id.order_line if i.product_id.type != 'service' if
                                                i.product_uom_qty > i.qty_delivered])

                _logger.debug("LINE FULLY DELIVERED .... %s", line_prod_not_full_delvd)
                if line_prod_not_full_delvd:
                    return res


                ### 22.04.2021: Comment code.... create income commission from Sale Order Confirm Process.

                # # Below is origin code
                # # if sale_id.picking_ids and not line_prod_not_full_delvd and not sale_id.account_move_line_ids:
                # #     self.with_context(ctx).commission_count_entries("Commissionable Income [" + sale_id.name + "]")
                # if sale_id.picking_ids and not line_prod_not_full_delvd and not sale_id.account_move_line_ids:
                #     if commission_income:
                #         _logger.debug("CALLING COMMISSION INCOME ENTRIES NOW .... ")
                #         if not sale_id.x_skip_commission:  # Skip Commission Process if True ... 19dec2020
                #             self.commission_count_income_entries(sale_id, account_id)

        return res
   

    # Commented by indra
    @api.multi
    # def commission_count_entries(self, journal_ref):
    def commission_count_entries(self, commission_amt, sale_person, journal_ref):
        # commission_journal = self.env['ir.values'].get_default('account.config.settings', 'commission_journal')
        commission_journal = int(self.env['ir.config_parameter'].search([('key','=','account.commission_journal')]).value)
        account_obj = self.env['account.journal']
        invoice_obj = self.env['account.invoice']
        sale_obj = self.env['sale.order']
        account_id = account_obj.browse(commission_journal)
        
        sale_search = sale_obj.search([('name', '=', self.origin)], order="id desc", limit=1)
        prod_commissinable_amt = sum([i.price_subtotal for i in sale_search.order_line if i.product_tmpl_id.flag])
        if self.env.context.get('delivery_order_contxt'):
            commission_pay = 'showroom_partner'
            comm_partner_id = sale_search.user_id
            sales_user = False
            commission_partner = comm_partner_id
        else:
            commission_pay = 'salesperson'
            comm_partner_id = sale_search.partner_id.id
            sales_user = sale_search.user_id
            commission_partner = False
    
        for sale_line in sale_search.order_line:
            if sale_line.product_id.flag:
                vendor = sale_line.x_vendor
                if vendor and vendor.commissions_percentage:
                    comm_amount = vendor.commissions_percentage * sale_line.price_subtotal / 100
                    commission_vals = {
                        'name': account_id.default_debit_account_id.name,
                        'debit': comm_amount,
                        'credit': 0.0,
                        'account_id': account_id.default_debit_account_id.id,
                        #'partner_id': vendor.id,
                        #'commission_user': sale_search.user_id,
                        'commission_on': self.order_pay_state,
                        'commission_partner': vendor.id,
                        'sale_order_id': sale_search.id,
                        'commission_pay': commission_pay,
                        'commissionable_amount': sale_line.price_subtotal,
                        'commission_rate': vendor.commissions_percentage,
                        'total_commission': comm_amount,
                        'sales_commission_rate': vendor.commissions_percentage,
                    }
                    sale_person_vals = {
                        'name': sale_search.user_id.name,
                        'debit': 0.0,
                        'credit': comm_amount,
                        'account_id': sale_search.partner_id.property_account_payable_id.id,
                        #'partner_id': vendor.id,
                        #'commission_user': sale_search.user_id,
                        'commission_on': self.order_pay_state,
                        'commission_partner': vendor.id,
                        'sale_order_id': sale_search.id,
                        'commission_pay': commission_pay,
                        'commissionable_amount': sale_line.price_subtotal,
                        'commission_rate': vendor.commissions_percentage,
                        'total_commission': comm_amount,
                        'sales_commission_rate': vendor.commissions_percentage,
                    }
                    now = datetime.now()
                    vals = {
                        'journal_id': account_id.id,
                        'date': now.strftime('%Y-%m-%d'),
                        'state': 'draft',
                        'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                        'ref': journal_ref,
                        'partner_id': vendor.id,
                        #'commission_user': sale_search.user_id,
                        'commission_partner': vendor.id,
                    }
                    move = self.env['account.move'].create(vals)
        # move.post()
        #Call income commission account entries
        income_commission_config = self.env['ir.config_parameter'].sudo().get_param('commission_income')
        _logger.info(f"Income commission commission configuration: {income_commission_config}")
        if income_commission_config:
            if not sale_search.x_skip_commission:  # Skip Commission Process if True ... 19dec2020
                income_move_id = self.commission_count_income_entries(sale_search, journal_ref)
        return True


    @api.multi
    def commission_count_income_entries(self, sale_search, journal_ref):
        """ Income Commission entries on the basis of sale order line
        """
        _logger.info(f"gt_commission_income: Stock Picking income commission process: {self}, {sale_search}, {journal_ref}")

        commission_income_journal = int(self.env['ir.config_parameter'].sudo().get_param('commission_income_journal'))
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_income_journal)
        # account_id = journal_ref
        now = datetime.now()

        if sale_search.branch_id:
            reference = '[' + sale_search.name + '] ' + self.name + ' Picking'
            if len(sale_search.order_line.filtered(lambda x: x.product_id.flag)) == 0:
                _logger.info("In Sale Order Line, No product is not commissionable.")
            for sale_line in sale_search.order_line.filtered(lambda x: x.product_id.flag):
                if not sale_line.product_id.flag:
                    continue
                vendor = sale_line.x_vendor
                _logger.info(f"Income commission is calculated as per the given priority. Commission percentage: {sale_line.income_comm_percent} and amount is {sale_line.income_commission_cal_amount}")
                if vendor and sale_line.income_commission_cal_amount:
                    comm_amount = sale_line.income_commission_cal_amount
                    commission_vals = {
                            # 'name': account_id.default_credit_account_id.name,
                            'name': vendor.display_name,
                            'debit': 0.0,
                            'credit': comm_amount,
                            'account_id': account_id.default_credit_account_id.id,
                            'partner_id': vendor.id,
                            'commission_user': False,
                            'commission_on': self.order_pay_state,
                            'commission_partner': vendor.id,
                            'commission_pay': "showroom_partner",
                            'commissionable_amount': sale_line.price_subtotal,
                            'commission_rate': sale_line.income_comm_percent,
                            'sales_commission_rate': sale_line.income_comm_percent,
                            'total_commission': comm_amount,
                            'comm_type': 'income',
                            'sale_order_id': sale_search.id,
                            'sale_line_id': sale_line.id,
                            'branch_id': sale_search.branch_id.id,
                            'ref': sale_line.product_id.display_name,
                            'product_id': sale_line.product_id.id,
                    }

                    sale_person_vals = {
                            'name': vendor.display_name, # sale_line.product_id.display_name,
                            'debit': comm_amount,
                            'credit': 0.0,
                            'account_id': vendor.property_account_receivable_id.id,
                            'partner_id': vendor.id,
                            'commission_user': False,
                            'commission_on': self.order_pay_state,
                            'commission_partner': vendor.id,
                            'commission_pay': "showroom_partner",
                            'commissionable_amount': sale_line.price_subtotal,
                            'commission_rate': sale_line.income_comm_percent,
                            'sales_commission_rate': sale_line.income_comm_percent,
                            'total_commission': comm_amount,
                            'comm_type': 'income',
                            'sale_order_id': sale_search.id,
                            'sale_line_id': sale_line.id,
                            'branch_id': sale_search.branch_id.id,
                            'ref': sale_line.product_id.display_name,
                            'product_id': sale_line.product_id.id,
                    }

                    vals = {
                        'journal_id': account_id.id,
                        'date': now.strftime('%Y-%m-%d'),
                        'state': 'draft',
                        'ref': reference, #journal_ref,
                        'partner_id': vendor.id,
                        'commission_user': False,
                        'commission_partner': vendor.id,
                        'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                        'branch_id': sale_search.branch_id.id,
                    }
                    move = self.env['account.move'].create(vals)
                    _logger.info(f"Income commission entires generated i.e. account.move entries : {move}")
                else:
                    _logger.warning("Vendor does not have selected in Sale Order Line or no commission percentage setup as per the priority.")

            return True