# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class Partner(models.Model):
   _inherit = "res.partner"


   supplier_commission_lines = fields.One2many("supplier.commission.line", 'partner_id', 'Supplier Commission Lines')

   # @api.model
   # def name_search(self, name, args=None, operator='ilike', limit=100):
   #    """
   #    Not getting args i.e. domain in parameter, so that applying domain based on condition manually
   #    """
   #    print ("argsssssssssssssssssssss", args)
   #    args = args or []
   #    if self.env.context.get('commission_receive_wizard') and len(args) == 0:
   #       args = [('is_company', '=', True), ('supplier', '=', True), ('parent_id', '=', False), ('active', '=', True)]
   #    return super(Partner, self).name_search(name=name, args=args, operator=operator, limit=limit)


class SupplierCommissionLine(models.Model):
   _name = "supplier.commission.line"


   partner_id = fields.Many2one("res.partner", "Supplier/Customer", ondelete="cascade", index=True)
   branch_id = fields.Many2one("res.branch", "Showroom", default=lambda self: self.env.user.branch_id)
   commission_rate = fields.Float("Commission Rate (%)", digits=(12,4))
   company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company']._company_default_get(
                                   'supplier.commission.line'))

   _sql_constraints = [
      ('supplier_commis_uniq', 'UNIQUE (branch_id, partner_id, company_id)', 'You can not have two commission lines with the same supplier and branch !')
   ]



