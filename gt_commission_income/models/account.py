# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"
    
    @api.one
    def _compute_commission_type(self):
        for rec in self:
            if rec.account_id.user_type_id.name == 'Payable' and rec.credit != 0 and rec.journal_id.type == 'purchase' and rec.commission_on != False:
                rec.comm_type = 'expense'
            elif rec.account_id.user_type_id.name == 'Receivable' and rec.debit != 0 and rec.journal_id.type == 'sale' and rec.commission_on != False:
                rec.comm_type = 'income'

        _logger.debug("Commission Type is set to --- ")
                
    comm_type = fields.Selection([('expense', 'Expense'), ('income','Income')], string="Commission Type", compute="_compute_commission_type", store=True)
    received_income = fields.Boolean("Received Income", copy=False)
    sale_line_id = fields.Many2one('sale.order.line', 'Sale Order Line', index=1, copy=False)
    sale_customer = fields.Many2one(related="sale_line_id.order_id.partner_id", string="Sale Customer")
    sale_sidemark = fields.Char(related="sale_line_id.order_id.x_sidemark", string="Sidemark")
    supplier_order_number = fields.Char(related="sale_line_id.order_id.x_mfg_order", string="Supp Order No.") # Supplier Order No
    supplier_invoice_number = fields.Char(related="sale_line_id.order_id.mfg_invoice", string="Supp Inv No.") # Supplier Invoice No
    x_order_shipdate = fields.Date(related="sale_line_id.x_order_shipdate", string="Ship Date", store=True)


    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = args or []
        # When search from the Sale Search Report then below code
        if self.env.context.get('receivable_sale_line_id'):
            order_args = [i for i in args if isinstance(i, list) and 'sale_order_id' in i and 'ilike' in i]
            if len(order_args) > 0:
                order_args = order_args[0]
                if len(order_args) > 0:
                    order_search_name = order_args[2]
                    sale_search_id = self.env['sale.order'].search([('name', 'ilike', order_search_name)])
                    args.remove(order_args)
                    args.append(['sale_order_id', 'in', sale_search_id.ids])
        return super(AccountMoveLine, self).search(args=args, offset=offset, limit=limit, order=order, count=count)


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    # Old code already commented in file
    # def post(self):
    #     res = super(AccountPayment, self).post()
    #     done_pick_list = []
    #     commission_journal = int(self.env['ir.config_parameter'].search([('key','=','account.commission_journal')]).value)
    #     if commission_journal:
    #         journal_ref = self.env['account.journal'].browse(commission_journal)
    #     inv_id = self.env['account.invoice'].browse(self._context.get('active_id'))
    #     if inv_id:
    #         sale_search = self.env['sale.order'].search([('name', '=', inv_id.origin)])
    #     if inv_id.state == 'paid':
    #         picking_ids = self.env['stock.picking'].search([('origin', '=', inv_id.origin)])
    #         [done_pick_list.append(i) for i in picking_ids.filtered(lambda x: x.state=='done')]
    #         if len(picking_ids) == len(done_pick_list):
    #             self.commission_count_income_entries(self,sale_search, journal_ref)
    #     return res


    def post(self):
        """ This method will create Account Payment entry and if full payment done against the Invoices and
            also sale order fully delivered then create commission entry based o n the Income Commission configurations.
        """
        res = super(AccountPayment, self).post()
        commission_income = self.env['ir.config_parameter'].sudo().get_param('commission_income')
        commission_type = self.env['ir.config_parameter'].search([('key', '=', 'gt_sale_commission_extension.sale.commission_calculation')]).value
        commission_income_journal = int(self.env['ir.config_parameter'].search([('key', '=', 'commission_income_journal')]).value)
        _logger.warning("Commission Income Configuration: %s | Commission Type %s: | Income Journal: %s  ", commission_income,
                        commission_type, commission_income_journal)

        if commission_income and commission_income_journal:
            journal_ref = self.env['account.journal'].browse(commission_income_journal)

            ### This process wil process fine with the Account.Invoice Modekl Register Payment But Because some we are doing payment from the sale order Add Payment Button
            ### We should pass invoice Id and Context from there and take here into the consideration
            if (self.env.context.get('active_model') == 'account.invoice' and self.env.context.get('active_id')) or \
               (self.env.context.get('ks_oneclick_add_payment') and self.env.context.get('ks_onclick_invoice_id')):
                invoice_res_id = self._context.get('active_id') or self.env.context.get('ks_onclick_invoice_id')
                invoice_id = self.env['account.invoice'].browse(invoice_res_id)
                if invoice_id and invoice_id.state == 'paid':
                    sale_search_id = self.env['sale.order'].search([('name', '=', invoice_id.origin)], limit=1)
                    if sale_search_id:
                        ordered_product_qty = sum([i.product_uom_qty for i in sale_search_id.order_line if i.product_id.type != 'service'])
                        delivered_done_product_qty = sum([j.quantity_done for i in sale_search_id.picking_ids.filtered(lambda x: x.state == 'done') for j in i.move_ids_without_package])
                        fully_delivered_qty = ordered_product_qty <= delivered_done_product_qty
                        line_prod_not_full_delvd = any([i for i in sale_search_id.order_line if i.product_id.type != 'service' if
                                                        i.product_uom_qty > i.qty_delivered])
                        _logger.warning("Sale Order fully delivered product quantity: %s | %s ",fully_delivered_qty, line_prod_not_full_delvd)
                        if line_prod_not_full_delvd:
                            return res


                        ### 22.04.2021: Comment code.... create income commission from Sale Order Confirm Process.
                        # if sale_search_id.picking_ids and not line_prod_not_full_delvd and fully_delivered_qty:
                        #     if not sale_search_id.x_skip_commission:  # Skip Commission Process if True ... 19dec2020
                        #         _logger.warning("Income Commission processing: %s | %s", sale_search_id, journal_ref)
                        #         self.commission_count_income_entries(sale_search_id, journal_ref)

        return res


    @api.multi
    def commission_count_entries(self, commission_amt, sale_person, journal_ref):
        # commission_journal = self.env['ir.values'].get_default('account.config.settings', 'commission_journal')
        commission_journal = int(self.env['ir.config_parameter'].search([('key','=','account.commission_journal')]).value)
        account_obj = self.env['account.journal']
        invoice_obj = self.env['account.invoice']
        sale_obj = self.env['sale.order']
        account_id = account_obj.browse(commission_journal)
        invoice_id = invoice_obj.browse(self._context.get('active_id'))
        if not invoice_id:
            invoice_id = self.invoice_ids[0]
            sale_search = sale_obj.search([('name', '=', invoice_id.origin)])
        if not sale_search:
            sale_search = sale_obj.search([('name', '=', self.communication)], order="id desc", limit=1)
            self.write({'order_pay_state': sale_search.order_pay_state})
        prod_commissinable_amt = sum([i.price_subtotal for i in sale_search.order_line if i.product_id.flag])

        if self.env.context.get('payment_partner_contxt'):
            commission_pay = 'showroom_partner'
            comm_partner_id = sale_person.id
            sales_user = False
            commission_partner = comm_partner_id
        else:
            commission_pay = 'salesperson'
            comm_partner_id = sale_person.partner_id.id
            sales_user = sale_person.id
            commission_partner = False

        for sale_line in sale_search.order_line:
            if sale_line.product_id.flag:
                vendor = sale_line.x_vendor
                if vendor and vendor.commissions_percentage:
                    comm_amount = vendor.commissions_percentage * sale_line.price_subtotal / 100

                    commission_vals = {
                        'name': account_id.default_debit_account_id.name,
                        'debit': comm_amount,
                        'credit': 0.0,
                        'account_id': account_id.default_debit_account_id.id,
                        'partner_id': comm_partner_id,
                        'commission_user': sales_user,
                        'commission_on': self.order_pay_state,
                        'commission_partner': commission_partner,
                        'sale_order_id': sale_search.id,
                        'commission_pay': commission_pay,
                        'commissionable_amount': sale_line.price_subtotal,
                        'commission_rate': vendor.commissions_percentage,
                        'total_commission': comm_amount,
                        'sales_commission_rate': vendor.commissions_percentage,
                        # 'comm_type': 'expense',
                    }
                    sale_person_vals = {
                        'name': sale_person.name,
                        'debit': 0.0,
                        'credit': comm_amount,
                        'account_id': sale_person.property_account_payable_id.id,
                        'partner_id': comm_partner_id,
                        'commission_user': sales_user,
                        'commission_on': self.order_pay_state,
                        'commission_partner': commission_partner,
                        'sale_order_id': sale_search.id,
                        'commission_pay': commission_pay,
                        'commissionable_amount': sale_line.price_subtotal,
                        'commission_rate': vendor.commissions_percentage,
                        'total_commission': comm_amount,
                        'sales_commission_rate': vendor.commissions_percentage,
                        # 'comm_type': 'expense',
                    }
                    now = datetime.now()
                    vals = {
                        'journal_id': account_id.id,
                        'date': now.strftime('%Y-%m-%d'),
                        'state': 'draft',
                        'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                        'ref': journal_ref,
                        'partner_id': comm_partner_id,
                        'commission_user': sales_user,
                        'commission_partner': commission_partner,
                    }
                    move = self.env['account.move'].create(vals)

        # Call income commission account entries
        income_commission_config = self.env['ir.config_parameter'].sudo().get_param('commission_income')
        if income_commission_config:
            if not sale_search.x_skip_commission:  # Skip Commission Process if True ... 19dec2020
                # income_move_id = self.commission_count_income_entries(commission_amt, prod_commissinable_amt, sale_search, journal_ref)
                income_move_id = self.commission_count_income_entries(sale_search, journal_ref)
        return move.id


    @api.multi
    def commission_count_income_entries(self, sale_search, journal_ref):
        """ Income Commission entries on the basis of sale order line
        """
        _logger.info(f"gt_commission_income: Account Payment income commission process: {self}, {sale_search}, {journal_ref}")

        commission_income_journal = int(self.env['ir.config_parameter'].sudo().get_param('commission_income_journal'))
        account_obj = self.env['account.journal']
        account_id = account_obj.browse(commission_income_journal)
        # account_id = journal_ref
        now = datetime.now()

        if sale_search.branch_id:
            reference = '[' + sale_search.name + '] ' + self.name + ' Payment'
            if len(sale_search.order_line.filtered(lambda x: x.product_id.flag)) == 0:
                _logger.info("In Sale Order Line, No product is not commissionable.")
            for sale_line in sale_search.order_line.filtered(lambda x: x.product_id.flag):
                if not sale_line.product_id.flag:
                    continue
                vendor = sale_line.x_vendor
                _logger.info(f"Income commission is calculated as per the given priority. Commission percentage: {sale_line.income_comm_percent} and amount is {sale_line.income_commission_cal_amount}")
                if vendor and sale_line.income_commission_cal_amount:
                    comm_amount = sale_line.income_commission_cal_amount
                    commission_vals = {
                            # 'name': account_id.default_credit_account_id.name,
                            'name': vendor.display_name,
                            'debit': 0.0,
                            'credit': comm_amount,
                            'account_id': account_id.default_credit_account_id.id,
                            'partner_id': vendor.id,
                            'commission_user': False,
                            'commission_on': self.order_pay_state,
                            'commission_partner': vendor.id,
                            'commission_pay': "showroom_partner",
                            'commissionable_amount': sale_line.price_subtotal,
                            'commission_rate': sale_line.income_comm_percent,
                            'sales_commission_rate': sale_line.income_comm_percent,
                            'total_commission': comm_amount,
                            'comm_type': 'income',
                            'sale_order_id': sale_search.id,
                            'sale_line_id': sale_line.id,
                            'branch_id': sale_search.branch_id.id,
                            'ref': sale_line.product_id.display_name,
                            'product_id': sale_line.product_id.id,
                    }

                    sale_person_vals = {
                            # 'name': sale_search.branch_id.name,
                            'name': vendor.display_name,    # sale_line.product_id.display_name,
                            'debit': comm_amount,
                            'credit': 0.0,
                            'account_id': vendor.property_account_receivable_id.id,
                            'partner_id': vendor.id,
                            'commission_user': False,
                            'commission_on': self.order_pay_state,
                            'commission_partner': vendor.id,
                            'commission_pay': "showroom_partner",
                            'commissionable_amount': sale_line.price_subtotal,
                            'commission_rate': sale_line.income_comm_percent,
                            'sales_commission_rate': sale_line.income_comm_percent,
                            'total_commission': comm_amount,
                            'comm_type': 'income',
                            'sale_order_id': sale_search.id,
                            'sale_line_id': sale_line.id,
                            'branch_id': sale_search.branch_id.id,
                            'ref': sale_line.product_id.display_name,
                            'product_id': sale_line.product_id.id,
                    }

                    vals = {
                        'journal_id': account_id.id,
                        'date': now.strftime('%Y-%m-%d'),
                        'state': 'draft',
                        'ref': reference, #journal_ref,
                        'partner_id': vendor.id,
                        'commission_user': False,
                        'commission_partner': vendor.id,
                        'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
                        'branch_id': sale_search.branch_id.id,
                    }
                    move = self.env['account.move'].create(vals)
                    _logger.info(f"Income commission entires generated i.e. account.move entries : {move}")
                else:
                    _logger.warning("Vendor does not have selected in Sale Order Line or no commission percentage setup as per the priority.")

            return True

    # @api.multi
    # def commission_count_income_entries(self,commission_amt, prod_commissinable_amt, sale_search, journal_ref):
    #     """ Income Commission """
    #     commission_income_journal = int(self.env['ir.config_parameter'].sudo().get_param('commission_income_journal'))
    #     account_obj = self.env['account.journal']
    #     account_id = account_obj.browse(commission_income_journal)
    #
    #     if sale_search.branch_id:
    #         if (len(sale_search.order_line) > 0):
    #             _logger.info("Operating on Sales Order Lines to get Vendor information....")
    #             for sale_line in sale_search.order_line:
    #                 # check if the product sold is commissionable
    #                 if sale_line.product_id.flag:
    #                     vendor = sale_line.x_vendor
    #                     if vendor and vendor.commissions_percentage:
    #                         _logger.info("Vendor has commissions percentage that we need to use for this commission calculation " + vendor.commissions_percentage)
    #                         comm_amount = vendor.commissions_percentage * sale_line.price_subtotal / 100
    #                         commission_vals = {
    #                             'name': account_id.default_credit_account_id.name,
    #                             'debit': 0.0,
    #                             'credit': comm_amount,
    #                             'account_id': account_id.default_credit_account_id.id,
    #                             'partner_id': vendor.id,
    #                             'commission_user': False,
    #                             'commission_on': self.order_pay_state,
    #                             'commission_partner': vendor.id,
    #                             'sale_order_id': sale_search.id,
    #                             'commission_pay': "showroom_partner",
    #                             'commissionable_amount': sale_line.price_subtotal,
    #                             'commission_rate': vendor.commissions_percentage,
    #                             'total_commission': comm_amount,
    #                             'sales_commission_rate': vendor.commissions_percentage,
    #                             # 'comm_type': 'income',
    #                         }
    #                         sale_person_vals = {
    #                             'name': sale_search.branch_id.name,
    #                             'debit': comm_amount,
    #                             'credit': 0.0,
    #                             'account_id': vendor.property_account_receivable_id.id,
    #                             'partner_id': vendor.id,
    #                             'commission_user': False,
    #                             'commission_on': self.order_pay_state,
    #                             'commission_partner': vendor.id,
    #                             'sale_order_id': sale_search.id,
    #                             'commission_pay': "showroom_partner",
    #                             'commissionable_amount': sale_line.price_subtotal,
    #                             'commission_rate': vendor.commissions_percentage,
    #                             'total_commission': comm_amount,
    #                             'sales_commission_rate': vendor.commissions_percentage,
    #                             # 'comm_type': 'income',
    #                         }
    #                         now = datetime.now()
    #                         vals = {
    #                             'journal_id': account_id.id,
    #                             'date': now.strftime('%Y-%m-%d'),
    #                             'state': 'draft',
    #                             'line_ids': [(0, 0, commission_vals), (0, 0, sale_person_vals)],
    #                             'ref': journal_ref,
    #                             'partner_id': vendor.id,
    #                             'commission_user': False,
    #                             'commission_partner': vendor.id,
    #                         }
    #                         move = self.env['account.move'].create(vals)
    #                         _logger.info("Created account.move entries for income generated from commissions .... ")
    #                     else:
    #                         _logger.warning("Vendor does not have any commission percentage setup in Vendor Record")
    #                 else:
    #                     _logger.info("Product is not commissionable..... ")
    #                         # move.post()
    #         return move.id
