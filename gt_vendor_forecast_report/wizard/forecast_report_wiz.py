from odoo.addons.web.controllers.main import serialize_exception,content_disposition
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime
from odoo.http import request
from odoo import http
import base64


class Binary(http.Controller):

    @http.route('/download', type='http', auth="public")
    @serialize_exception
    def download_document(self,model,field,id,filename=None, **kw):
        Model = request.env[model]
        res = Model.browse(int(id))
        filecontent = base64.b64decode(res.data or '')
        if not filecontent:
            return request.not_found()
        else:
            if not filename:
                filename = '%s_%s' % (model.replace('.', '_'), id)
            return request.make_response(filecontent, [('Content-Type', 'application/octet-stream'), ('Content-Disposition', content_disposition(filename))])
                        
                        
class SaleForecastReport(models.TransientModel):
    _name = 'sale.forecast.report'

    branch_id = fields.Many2one("res.branch", string="Showroom", default=lambda self: self.env.user.branch_id)
    month = fields.Date(string="Month", default=datetime.now().date())
    data = fields.Binary('File')
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.user.company_id)

    def get_sda_layered_calculation(self, order_line_ids):
        '''
            With Discount SaleOrderLine SDA Amount Sum....If SDA is available with same month then take sda adjusted_book_sale amount instead of the SaleOrderLine
            price_subtotal amount..And if any Layered data is available for the same month then take the amount and subtract from the total amount.
            Take saleorderline with discounted amount and then if sda is available for the same month then substract from amount same like the layered.
        '''
        discounted_sale_line_amount = 0.0
        for sale_line in order_line_ids:
            discounted_sale_line_amount += (sale_line.price_subtotal - sale_line.discount_amount)
        return discounted_sale_line_amount


    def get_month_calculation_amount(self, cur_month_sale_order_lines=False, prv_month_sale_order_lines=False):
        """
            This method will give calculated amount for the current and previous year same month amount data
            :return: Current and Pervious Yeas month amount
        """
        cur_year_sale = cur_month_sale_order_lines
        prv_year_sale = prv_month_sale_order_lines

        ''' Based On Current Year Month calculation '''
        cur_year_month_ids = cur_year_sale #cur_year_sale.filtered(lambda sol: sol.x_vendor.id == vendor_id.id)
        cur_year_month_with_discount_ids = cur_year_month_ids.filtered(lambda sol: sol.discount_show > 0 or sol.discount_amount > 0)
        cur_year_month_without_discount_ids = cur_year_month_ids.filtered(lambda sol: sol.discount_show == 0 or sol.discount_amount == 0)
        # Call method to calculate SDA and Layered Amount if any and return sum of the amount
        sum_with_discount_amount = self.get_sda_layered_calculation(cur_year_month_with_discount_ids)

        # Without Discount SaleOrderLine Amount Sum
        sum_cur_month_without_discount = sum([i.price_subtotal for i in cur_year_month_without_discount_ids])
        sum_cur_month = sum_cur_month_without_discount + sum_with_discount_amount                       # Addition of SaleOrderLine with and without discount

        ''' Based on Previous Year Month Calculation '''
        prv_year_month_ids = prv_year_sale #prv_year_sale.filtered(lambda sol: sol.x_vendor.id == vendor_id.id)
        prv_year_month_with_discount_ids = prv_year_month_ids.filtered(lambda sol: sol.discount_show > 0 or sol.discount_amount > 0)
        prv_year_month_without_discount_ids = prv_year_month_ids.filtered(lambda sol: sol.discount_show == 0 or sol.discount_amount == 0)
        # Call method to calculate SDA and Layered Amount if any
        sum_prv_with_discount_amount = self.get_sda_layered_calculation(prv_year_month_with_discount_ids)

        # Without Discount SaleOrderLine Amount Sum
        sum_prv_month_without_discount = sum([i.price_subtotal for i in prv_year_month_without_discount_ids])
        sum_prv_month = sum_prv_month_without_discount + sum_prv_with_discount_amount                   # Addition of SaleOrderLine with and without discount

        return sum_cur_month, sum_prv_month


    def get_year_calculation_amount(self, cur_year_sale_order_lines=False, prv_year_sale_order_lines=False):
        """
            This method will give calculated amount for the current and previous year to date amount data
            :return: Current and Pervious Yeas to date amount
        """
        cur_sale_y_to_date_ids = cur_year_sale_order_lines
        prv_sale_y_to_date_ids = prv_year_sale_order_lines

        ''' Based On Current Year To Date Calculation '''
        cur_year_to_date_ids = cur_sale_y_to_date_ids # cur_sale_y_to_date_ids.filtered(lambda sol: sol.x_vendor.id == vendor_id.id)
        cur_ytd_with_discount_ids = cur_year_to_date_ids.filtered(lambda sol: sol.discount_show > 0 or sol.discount_amount > 0)
        cur_ytd_date_without_discount_ids = cur_year_to_date_ids.filtered(lambda sol: sol.discount_show == 0 or sol.discount_amount == 0)
        # Call method to calculate SDA and Layered Amount if any
        sum_cur_ytd_with_discount_amount = self.get_sda_layered_calculation(cur_ytd_with_discount_ids)

        # Without Discount SaleOrderLine Amount Sum
        sum_cur_ytd_without_discount = sum([i.price_subtotal for i in cur_ytd_date_without_discount_ids])
        sum_cur_ytodate = sum_cur_ytd_without_discount + sum_cur_ytd_with_discount_amount                  # Addition of SaleOrderLine with and without discount

        ''' Based On Previous Year To Date Calculation '''
        prv_year_to_date_ids = prv_sale_y_to_date_ids # prv_sale_y_to_date_ids.filtered(lambda sol: sol.x_vendor.id == vendor_id.id)
        prv_ytd_with_discount_ids = prv_year_to_date_ids.filtered(lambda sol: sol.discount_show > 0 or sol.discount_amount > 0)
        prv_ytd_date_without_discount_ids = prv_year_to_date_ids.filtered(lambda sol: sol.discount_show == 0 or sol.discount_amount == 0)
        # Call method to calculate SDA and Layered Amount if any
        sum_prv_ytd_with_discount_amount = self.get_sda_layered_calculation(prv_ytd_with_discount_ids)

        # Without Discount SaleOrderLine Amount Sum
        sum_prv_ytd_without_discount = sum([i.price_subtotal for i in prv_ytd_date_without_discount_ids])
        sum_prv_ytodate = sum_prv_ytd_without_discount + sum_prv_ytd_with_discount_amount                  # Addition of SaleOrderLine with and without discount

        return sum_cur_ytodate, sum_prv_ytodate


    @api.multi
    def action_download_forecast_report(self):
        """
            This method will search and match the given field data i.e. Showroom, Month and Company.
            If Condition get matched with the existing report data..then get stored report file and download the same file.
        """
        if not self.branch_id:
            raise UserError(_("Please select Showroom !"))
        if not self.month:
            raise UserError(_("Please select Month !"))

        domain = [('company_id', '=', self.company_id.id)] # self.env.user.company_id.id
        if self.branch_id:
            domain += [('branch_id', '=', self.branch_id.id)]
        if self.month:
            current_month = str(self.month.month)
            current_year = str(self.month.year)
            domain += [('cy_month', '=', current_month), ('current_year', '=', current_year)]
        vendor_forecast_reprot_id = self.env['showroom.vendor.forecast.report'].search(domain, order='id desc', limit=1)
        if len(vendor_forecast_reprot_id) > 0:
            return {
                    'type': 'ir.actions.act_url',
                    'url': '/download?model=showroom.vendor.forecast.report&field=report_data_file&id=%s&filename=%s' % (vendor_forecast_reprot_id.id, vendor_forecast_reprot_id.filename),
                    'target': 'new',
                    }


