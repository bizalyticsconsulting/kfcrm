from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo import models, fields, api, _
from operator import itemgetter
from calendar import monthrange
import itertools
import calendar
import base64
import xlwt
import io


class ShowroomVendorForecastReport(models.Model):
    _name = 'showroom.vendor.forecast.report'
    _description = 'Showroom Vendor Forecast Report'
    _order = 'id desc'

    seq_number = fields.Char("Sequence", copy=False, index=True, default=lambda self: _('New'))
    name = fields.Char("Name", index=True, copy=False)                                                              # Vendor - Forecast Report : Branch_Company_Month_Year
    branch_id = fields.Many2one("res.branch", string="Showroom", default=lambda self: self.env.user.branch_id)
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.user.company_id.currency_id)
    current_year = fields.Char(string="Current Year")
    cy_month = fields.Selection([('1', 'Jan'), ('2', 'Feb'), ('3', 'Mar'), ('4', 'Apr'), ('5', 'May'), ('6', 'Jun'), ('7', 'Jul'), ('8', 'Aug'), ('9', 'Sep'), ('10', 'Oct'), ('11', 'Nov'), ('12', 'Dec')], string="Current Year Month")
    py_month = fields.Selection([('1', 'Jan'), ('2', 'Feb'), ('3', 'Mar'), ('4', 'Apr'), ('5', 'May'), ('6', 'Jun'), ('7', 'Jul'), ('8', 'Aug'), ('9', 'Sep'), ('10', 'Oct'), ('11', 'Nov'), ('12', 'Dec')], string="Previous Year Month")
    previous_year = fields.Char(string="Previous Year")

    sum_current_year = fields.Monetary(string='Current Year Total', currency_field='currency_id')                   # Current Month Sales Total
    sum_previous_year = fields.Monetary(string='Previous Year Total', currency_field='currency_id')                 # Previous Month Sales Total
    sum_diff_month = fields.Monetary(string='Difference Sales Total', currency_field='currency_id')                 # Difference Month Sales Total
    sum_goals = fields.Monetary(string='Goal Total', currency_field='currency_id')                                  # Goal Total

    sum_diff_goal = fields.Monetary(string='Year Difference Total', currency_field='currency_id')                   # Year diff Total
    sum_cur_year2date = fields.Monetary(string='Current YearToDate Total', currency_field='currency_id')            # Current Year To Date Total
    sum_prv_year2date = fields.Monetary(string='Previous YearToDate Total', currency_field='currency_id')           # Previous Year To Date Total
    sum_diff_year2date = fields.Monetary(string='Difference YearToDate Total', currency_field='currency_id')        # Difference Year to date Total
    sum_y2d_goals = fields.Monetary(string='Goal YearToDate Total', currency_field='currency_id')                   # Goal year to date Total
    sum_y2d_diff_goal = fields.Monetary(string='Goal Difference YearToDate Total', currency_field='currency_id')    # Goal Difference Year to date Total

    meet_goals = fields.Monetary(string='To Meet Goal', currency_field='currency_id')                               # To Meet Goal

    forecast_report_lines = fields.One2many("showroom.vendor.forecast.line", "forecast_report_id", "Vendor Forecast Report")
    forecast_vendor_report_lines = fields.One2many("vendor.forecast.data", "forecast_report_id", "Vendor Forecast Line Report")
    report_data_file = fields.Binary('Report File', attachment=True)
    filename = fields.Char("Filename")
    active = fields.Boolean('Active', default=True)

    @api.model
    def create(self, vals):
        if vals.get('seq_number', _('New')) == _('New'):
            vals['seq_number'] = self.env['ir.sequence'].next_by_code('showroom.vendor.forecast.report') or _('New')
        res = super(ShowroomVendorForecastReport, self).create(vals)
        return res

    @api.model
    def action_scheduler_vendor_forecast_report(self, months=33):
        """
            This method will create Vendor Forecast Report in the current model.
            Args: Month=18: If 18 Month then generate the report from current month to previous 18 month i.e. each month till 18 month.
                  Month=3: Default-If 3 Month then generate the report from current month to previous 3 month i.e. each month till 3 month.
            Imp: Check for record for the same month if any, the update the record with the data.
        """
        SaleForecastReport = self.env['sale.forecast.report']
        SaleForecastData = self.env['sale.forecast.report']
        previous_month_lists = []
        branch_showroom_ids = self.env['res.branch'].search([])
        now_date = datetime.now().date()
        previous_month_lists.append(now_date)
        # Logic for add 18month or 3 months date in list
        for cnt in range(1, months):
            previous_month_date = now_date - relativedelta(months=cnt)
            previous_month_lists.append(previous_month_date)
        if len(previous_month_lists) > 0:
            for month_data in previous_month_lists:
                for branch in branch_showroom_ids:
                    SaleForecastData |= SaleForecastReport.create({'month': month_data, 'branch_id': branch.id})

            if len(SaleForecastData) > 0:
                forecast_report_create_ids = self.get_vendor_saleforecast_report(SaleForecastData)
                self.env.cr.commit()
        return True

    @api.multi
    def get_vendor_saleforecast_report(self, SaleForecastData):
        """
            This method will create vendor forecast report data in Model and store the Excel Sheet in the database.
            Later we can download the Excel Report using domain condition.
        """
        ResPartner = self.env['res.partner']
        SupplierReportCategory = self.env['supplier.report.category']
        ProductReportCategory = self.env['product.report.category']
        SaleOrderLine = self.env['sale.order.line']
        VendorGoals = self.env['vendor.goals']
        SalesTerritory = self.env['sales.territory']
        forecast_report_create_ids = self
        if not SaleForecastData:
            return False

        for rec_month in SaleForecastData:
            vendor_forecast_data_dict = {}
            total_day = monthrange(rec_month.month.year, rec_month.month.month)
            # Year Dates
            cur_end_y_to_date = rec_month.month.replace(day=total_day[1])
            cur_start_y_to_date = cur_end_y_to_date - relativedelta(months=rec_month.month.month - 1)   # Get Same Year First Month and then change month date as first day
            cur_start_y_to_date = cur_start_y_to_date.replace(day=1)                                    # So will get same year  first day-first month of the year 01.01.2021
            prv_end_y_to_date = cur_end_y_to_date - relativedelta(years=1)
            prv_start_y_to_date = prv_end_y_to_date - relativedelta(months=prv_end_y_to_date.month - 1)
            prv_start_y_to_date = prv_start_y_to_date.replace(day=1)

            # Month Dates
            cur_end_date = rec_month.month.replace(day=total_day[1])
            cur_start_date = cur_end_date - timedelta(days=total_day[1] - 1)
            cur_year = str(cur_end_date.year)
            prv_start_date = cur_start_date - relativedelta(years=1)
            prv = monthrange(prv_start_date.year, prv_start_date.month)
            prv_end_date = prv_start_date + timedelta(days=prv[1] - 1)
            previous_endyear = str(prv_end_date.year)

            # Current & Previous Year
            current_year = str(cur_start_date.year)
            current_year_month = str(cur_start_date.month)
            previous_year = str(prv_start_date.year)
            previous_year_month = str(prv_start_date.month)

            ### Search for the Vendor Forecast Report, If available then Update with new records else Create...Same for the vendor forecast line and forecast lines too
            search_vendor_forecast_ids = self.search([('company_id', '=', self.env.user.company_id.id),
                                                      ('branch_id', '=', rec_month.branch_id.id),
                                                      ('current_year', '=', current_year),
                                                      ('cy_month', '=', current_year_month),
                                                      ('previous_year', '=', previous_year),
                                                      ('py_month', '=', previous_year_month)], order='id desc')

            # Excel Sheet, Workbook and formatting
            workbook = xlwt.Workbook()
            sheet = workbook.add_sheet('forecast_report')
            Hedaer_Text = str(rec_month.branch_id.name) + ' BOOKED SALES REPORT \n' + str(rec_month.month.strftime("%B %d, %Y"))

            bold1 = xlwt.easyxf('alignment: horiz centre;font: bold on, height 200; borders: top 5, bottom 5')
            bold2 = xlwt.easyxf('alignment: horiz centre;font: bold on, height 200; borders: left 5, top 5, bottom 5')
            bold3 = xlwt.easyxf('alignment: horiz centre;font: bold on, height 200; borders: right 5, top 5, bottom 5')

            style1 = xlwt.XFStyle()
            style1.num_format_str = 'MM-DD-YY'

            date_style1 = xlwt.easyxf('alignment: horiz centre; font: bold on, height 200; border: top 5,bottom 5', num_format_str='$#,##0.00')
            date_style2 = xlwt.easyxf('alignment: horiz centre; font: bold on, height 200; border: left 5,right 5,top 5,bottom 5', num_format_str='$#,##0.00')
            date_style3 = xlwt.easyxf('alignment: horiz centre; font: bold on, height 200; border: right 5,top 5,bottom 5', num_format_str='$#,##0.00')

            vendor_style1 = xlwt.easyxf('alignment: horiz left; font: bold on, height 200; border: left 5,right 5,top 5,bottom 5')
            vendor_style2 = xlwt.easyxf('alignment: horiz left; font: bold on, height 200; border: left 5,top 5,bottom 5')
            vendor_style3 = xlwt.easyxf('alignment: horiz left; font: bold on, height 200; border: right 5,top 5,bottom 5')

            top_border = xlwt.easyxf('border: top 5')

            font = xlwt.XFStyle()
            font.num_format_str = '$#,##0.00'

            font2 = xlwt.XFStyle()
            font2.num_format_str = '$#,##0.00'

            borders = xlwt.Borders()
            borders.right = 5
            font2.borders = borders

            # Increase Columns width
            total_column = 13
            sheet.col(0).width = 256 * 32  # Increase width of col-A
            for col_count in range(1, total_column + 1):
                sheet.col(col_count).width = 256 * 15  # Increase width of col-B

            sheet.write_merge(0, 1, 0, 5, Hedaer_Text, bold1, )
            sheet.write_merge(5, 5, 0, 0, 'SALES BY SUPPLIER', bold2, )
            sheet.write_merge(5, 5, 1, 5, 'Current Month Sales', bold3, )
            sheet.write_merge(5, 5, 6, 10, 'Year To Date', bold3, )

            row_index = 6
            sheet.write(row_index, 0, '', bold2)
            sheet.write(row_index, 1, cur_year, bold1)
            sheet.write(row_index, 2, previous_endyear, bold1)
            sheet.write(row_index, 3, '$ DIFF', bold1)
            sheet.write(row_index, 4, 'GOAL', bold1)
            sheet.write(row_index, 5, '$ Year DIFF', bold3)
            sheet.write(row_index, 6, cur_year, bold1)
            sheet.write(row_index, 7, previous_endyear, bold1)
            sheet.write(row_index, 8, '$ DIFF', bold1)
            sheet.write(row_index, 9, 'GOAL', bold1)
            sheet.write(row_index, 10, '$ DIFF', bold3)
            row_index = 7

            sum_current_year = 0.0
            sum_previous_year = 0.0
            sum_cur_year2date = 0.0
            sum_prv_year2date = 0.0
            sum_diff_month = 0.0
            sum_goals = 0.0
            sum_diff_goal = 0.0
            sum_diff_year2date = 0.0
            sum_y2d_goals = 0.0
            sum_y2d_diff_goal = 0.0

            # General Data Preparation
            forecast_report_name = rec_month.branch_id.name + ' [' + self.env.user.company_id.name + ']' + ' : Forecast Report [Month: ' + str(calendar.month_name[rec_month.month.month]) + ', Year: ' + str(rec_month.month.year) + ']'
            vendor_forecast_data_dict.update({'name': forecast_report_name,
                                              'branch_id': rec_month.branch_id.id,
                                              'current_year': current_year,
                                              'cy_month': current_year_month,
                                              'py_month': previous_year_month,
                                              'previous_year': previous_year})

            vendor_ids = ResPartner.search([("supplier", "=", True), ("active", "=", True), ("vendor_goal_ids", "!=", False)])
            vendor_ids = [vnd for vnd in vendor_ids for gl in vnd.vendor_goal_ids if gl.showroom.id == rec_month.branch_id.id and gl.annual_goal.year == str(rec_month.month.year)]
            forecast_vendor_lines_list = []

            cur_month_sale_order_lines = SaleOrderLine.search([('x_order_confirmation_date', '>=', cur_start_date),
                                                               ('x_order_confirmation_date', '<=', cur_end_date),
                                                               ('order_id.branch_id', '=', rec_month.branch_id.id),
                                                               ('order_id.state', '=', 'sale')])
            prv_month_sale_order_lines = SaleOrderLine.search([('x_order_confirmation_date', '>=', prv_start_date),
                                                               ('x_order_confirmation_date', '<=', prv_end_date),
                                                               ('order_id.branch_id', '=', rec_month.branch_id.id),
                                                               ('order_id.state', '=', 'sale')])
            cur_year_sale_order_lines = SaleOrderLine.search([('x_order_confirmation_date', '>=', cur_start_y_to_date),
                                                              ('x_order_confirmation_date', '<=', cur_end_y_to_date),
                                                              ('order_id.branch_id', '=', rec_month.branch_id.id),
                                                              ('order_id.state', '=', 'sale')])
            prv_year_sale_order_lines = SaleOrderLine.search([('x_order_confirmation_date', '>=', prv_start_y_to_date),
                                                              ('x_order_confirmation_date', '<=', prv_end_y_to_date),
                                                              ('order_id.branch_id', '=', rec_month.branch_id.id),
                                                              ('order_id.state', '=', 'sale')])

            cur_vendor_amount = VendorGoals.search([('annual_goal.year', '=', cur_year), ('showroom', '=', self.branch_id.id)])
            prv_vendor_amount = VendorGoals.search([('annual_goal.year', '=', previous_endyear), ('showroom', '=', self.branch_id.id)])

            top_ten_customer_list = []
            for vendor in vendor_ids:
                # call Method to calculate Current/Previous Year Month SaleOrder Amount
                cur_month_vendor_sale_order_lines = cur_month_sale_order_lines.filtered(lambda sol: sol.x_vendor.id == vendor.id)
                prv_month_vendor_sale_order_lines = prv_month_sale_order_lines.filtered(lambda sol: sol.x_vendor.id == vendor.id)
                sum_cur_month, sum_prv_month = rec_month.get_month_calculation_amount(cur_month_sale_order_lines=cur_month_vendor_sale_order_lines, prv_month_sale_order_lines=prv_month_vendor_sale_order_lines)
                sum_current_year += sum_cur_month
                sum_previous_year += sum_prv_month

                # Difference of Current Year Month and Previous Year Month
                diff_month = sum_cur_month - sum_prv_month
                sum_diff_month += diff_month

                # vendor Goal for Current and Previous year
                cur_vendor_amount = cur_vendor_amount.filtered(lambda gol: gol.goals_partner_id.id == vendor.id)
                prv_vendor_amount = prv_vendor_amount.filtered(lambda gol: gol.goals_partner_id.id == vendor.id)
                goals = cur_vendor_amount.an_amount / 12
                sum_goals += goals
                diff_goal = sum_cur_month - goals
                sum_diff_goal += diff_goal

                # call Method to calculate Current/Previous Year To Date SaleOrder Amount
                cur_year_vendor_sale_order_lines = cur_year_sale_order_lines.filtered(lambda sol: sol.x_vendor.id == vendor.id)
                prv_year_vendor_sale_order_lines = prv_year_sale_order_lines.filtered(lambda sol: sol.x_vendor.id == vendor.id)
                sum_cur_ytodate, sum_prv_ytodate = rec_month.get_year_calculation_amount(cur_year_sale_order_lines=cur_year_vendor_sale_order_lines, prv_year_sale_order_lines=prv_year_vendor_sale_order_lines)

                # Sum of yeartodate
                sum_cur_year2date += sum_cur_ytodate
                sum_prv_year2date += sum_prv_ytodate

                # Difference of Current and Previous Year
                diff_year2date = sum_cur_ytodate - sum_prv_ytodate
                sum_diff_year2date += diff_year2date
                y2d_goals = (cur_vendor_amount.an_amount / 12) * cur_start_date.month
                sum_y2d_goals += y2d_goals
                y2d_diff_goal = sum_cur_ytodate - y2d_goals
                sum_y2d_diff_goal += y2d_diff_goal

                # Add Data on Sheet row & columns
                sheet.write(row_index, 0, vendor.name, vendor_style3)
                sheet.write(row_index, 1, sum_cur_month, font)
                sheet.write(row_index, 2, sum_prv_month, font)
                sheet.write(row_index, 3, diff_month, font)
                sheet.write(row_index, 4, goals, font)
                sheet.write(row_index, 5, diff_goal, font2)

                sheet.write(row_index, 6, sum_cur_ytodate, font)
                sheet.write(row_index, 7, sum_prv_ytodate, font)
                sheet.write(row_index, 8, diff_year2date, font)
                sheet.write(row_index, 9, y2d_goals, font)
                sheet.write(row_index, 10, y2d_diff_goal, font2)
                row_index += 1

                vendor_forecast_name = rec_month.branch_id.name +' [' + self.env.user.company_id.name + ']' + ' : ' + vendor.name + ' - ' + ' Forecast Report [Month: ' + str(calendar.month_name[rec_month.month.month]) + ', Year: ' + str(rec_month.month.year) + ']'
                vendor_data_dict = {
                    'name': vendor_forecast_name,
                    'branch_id': rec_month.branch_id.id,
                    'vendor_id': vendor.id,
                    'current_year': current_year,
                    'cy_month': current_year_month,
                    'py_month': previous_year_month,
                    'previous_year': previous_year,

                    'cur_vendor_amount': cur_vendor_amount.an_amount,
                    'prv_vendor_amount': prv_vendor_amount.an_amount,
                    'sum_cur_month': sum_cur_month,
                    'sum_prv_month': sum_prv_month,
                    'diff_month': diff_month,
                    'goals': goals,

                    'diff_goal': diff_goal,
                    'sum_cur_ytodate': sum_cur_ytodate,
                    'sum_prv_ytodate': sum_prv_ytodate,
                    'diff_year2date': diff_year2date,
                    'y2d_goals': y2d_goals,
                    'y2d_diff_goal': y2d_diff_goal,
                    # 'forecast_report_id': ''
                }
                forecast_vendor_lines_list.append((0, 0, vendor_data_dict))
                top_ten_customer_list.append(vendor_data_dict)

            sheet.write(row_index, 0, 'TOTAL', vendor_style2)
            sheet.write(row_index, 1, sum_current_year, date_style1)
            sheet.write(row_index, 2, sum_previous_year, date_style1)
            sheet.write(row_index, 3, sum_diff_month, date_style1)
            sheet.write(row_index, 4, sum_goals, date_style1)
            sheet.write(row_index, 5, sum_diff_goal, date_style3)
            sheet.write(row_index, 6, sum_cur_year2date, date_style1)
            sheet.write(row_index, 7, sum_prv_year2date, date_style1)
            sheet.write(row_index, 8, sum_diff_year2date, date_style1)
            sheet.write(row_index, 9, sum_y2d_goals, date_style1)
            sheet.write(row_index, 10, sum_y2d_diff_goal, date_style3)

            row_index += 2
            sheet.write(row_index, 0, 'CURRENT TOTAL', vendor_style1)
            sheet.write(row_index, 1, sum_current_year, date_style2)
            row_index += 1

            monthly_goal = self.env['showroom.monthly.goals'].search([('year_id.year','=',current_year),('branch_id','=',rec_month.branch_id.id)])
            sheet.write(row_index, 0, 'GOAL', vendor_style1)
            sheet.write(row_index, 1, monthly_goal.goal_amount if monthly_goal else 0, date_style2)

            row_index += 1
            meet_goals = (monthly_goal.goal_amount if monthly_goal else 0) - sum_current_year
            sheet.write(row_index, 0, 'TO MEET GOAL', vendor_style1)
            sheet.write(row_index, 1, meet_goals, date_style2)

            # Prepare Vendor Forecast dict values
            vendor_forecast_data_dict.update({'sum_current_year': sum_current_year,
                                              'sum_previous_year': sum_previous_year,
                                              'sum_diff_month': sum_diff_month,
                                              'sum_goals': sum_goals,
                                              'sum_diff_goal': sum_diff_goal,
                                              'sum_cur_year2date': sum_cur_year2date,
                                              'sum_prv_year2date': sum_prv_year2date,
                                              'sum_diff_year2date': sum_diff_year2date,
                                              'sum_y2d_goals': sum_y2d_goals,
                                              'sum_y2d_diff_goal': sum_y2d_diff_goal,
                                              'meet_goals': meet_goals,
                                              'forecast_vendor_report_lines': forecast_vendor_lines_list,
                                              })

            forecast_report_lines_list = []
            increment_month = 0
            increment_month1 = 1
            row_index += 1
            for tags in rec_month.branch_id.showroom_goal_ids:
                sale_forecast_lines = {}
                sum_projection = 0.0
                cur_end_date = cur_end_date + relativedelta(months=increment_month)
                increment_month = 1
                increment_end_date = cur_end_date + relativedelta(months=increment_month1)
                month_inword = increment_end_date.strftime("%b %Y")

                order_line_ids = self.env['sale.order.line'].search([('order_id.date_order', '>=', cur_end_date),
                                                                     ('order_id.date_order', '<=', increment_end_date),
                                                                     ('order_id.state', 'in', ['draft', 'sent']),
                                                                     ('order_id.branch_id', '=', rec_month.branch_id.id)])
                month_incremented_amount = [i.price_subtotal for i in order_line_ids]
                month_incremented_amount = sum(month_incremented_amount)
                for tag in tags.tag_ids:
                    # self.env.cr.execute("SELECT distinct sotr.order_id from sale_order_tag_rel sotr, sale_order so where sotr.tag_id= %s and so.state in ('draft','sent')" % tag.id)
                    self.env.cr.execute(
                        "SELECT distinct sotr.order_id from sale_order_tag_rel sotr, sale_order so where sotr.tag_id=%s and so.id = sotr.order_id and so.state in ('draft','sent') and so.branch_id=%s" % (
                        tag.id, rec_month.branch_id.id))
                    order_ids = self.env.cr.fetchall()
                    order_list = list(itertools.chain(*order_ids))
                    orders = self.env['sale.order'].browse(order_list)
                    for order in orders:
                        order_line_list = self.env['sale.order.line'].search([('order_id', '=', order.id)])
                        amount = [i.price_subtotal for i in order_line_list]
                        amounts = sum(amount)
                        sum_projection += amounts

                if sum_projection > 0.0:
                    row_index += 1
                    sheet.write(row_index, 0, tags.goal_text, vendor_style1)
                    sheet.write(row_index, 1, sum_projection, date_style2)

                    ##Comment: According to the Formattin...Not to show the Goal Text and amount
                    # Prepare dict values
                    sale_forecast_lines.update({'branch_id': rec_month.branch_id.id,
                                                'current_year': current_year,
                                                'cy_month': current_year_month,
                                                'tags_goal_text': tags.goal_text,
                                                'sum_projection': sum_projection,
                                                'month_inward_date': month_inword.upper() + ' GOAL',
                                                'month_incremented_amount': month_incremented_amount,
                                                'diff_sum_proj_increm_amount': sum_projection - month_incremented_amount,
                                                })
                    forecast_report_lines_list.append((0, 0, sale_forecast_lines))

            ################################################################################### SALES BY SPECIAL GROUPS ###################################################################################

            row_index += 2
            sheet.write_merge(row_index, row_index, 0, 0, 'SALES BY SPECIAL GROUPS', bold2, )
            sheet.write_merge(row_index, row_index, 1, 3, 'Current Month Sales', bold3, )
            sheet.write_merge(row_index, row_index, 4, 6, 'Year To Date', bold3, )

            row_index += 1
            sheet.write(row_index, 0, '', bold2)
            sheet.write(row_index, 1, cur_year, bold1)
            sheet.write(row_index, 2, previous_endyear, bold1)
            sheet.write(row_index, 3, '$ DIFF', bold3)
            sheet.write(row_index, 4, cur_year, bold1)
            sheet.write(row_index, 5, previous_endyear, bold1)
            sheet.write(row_index, 6, '$ DIFF', bold3)
            row_index += 1

            supplier_report_category_ids = SupplierReportCategory.search([], order='name')
            for sup_rep_cat in supplier_report_category_ids:
                # call Method to calculate Current/Previous Year Month SaleOrder Amount
                cur_month_sup_rep_cat_sale_order_lines = cur_month_sale_order_lines.filtered(lambda sol: sol.x_vendor.report_category_id.id == sup_rep_cat.id)
                prv_month_sup_rep_cat_sale_order_lines = prv_month_sale_order_lines.filtered(lambda sol: sol.x_vendor.report_category_id.id == sup_rep_cat.id)
                g_sum_cur_month, g_sum_prv_month = rec_month.get_month_calculation_amount(cur_month_sale_order_lines=cur_month_sup_rep_cat_sale_order_lines, prv_month_sale_order_lines=prv_month_sup_rep_cat_sale_order_lines)

                # Difference of Current Year Month and Previous Year Month
                g_diff_month = g_sum_cur_month - g_sum_prv_month

                # call Method to calculate Current/Previous Year To Date SaleOrder Amount
                cur_year_sup_rep_cat_sale_order_lines = cur_year_sale_order_lines.filtered(lambda sol: sol.x_vendor.report_category_id.id == sup_rep_cat.id)
                prv_year_sup_rep_cat_sale_order_lines = prv_year_sale_order_lines.filtered(lambda sol: sol.x_vendor.report_category_id.id == sup_rep_cat.id)
                g_sum_cur_ytodate, g_sum_prv_ytodate = rec_month.get_year_calculation_amount(cur_year_sale_order_lines=cur_year_sup_rep_cat_sale_order_lines, prv_year_sale_order_lines=prv_year_sup_rep_cat_sale_order_lines)

                # Difference of Current and Previous Year
                g_diff_year2date = g_sum_cur_ytodate - g_sum_prv_ytodate

                # Add Data on Sheet row & columns
                sheet.write(row_index, 0, sup_rep_cat.name, vendor_style3)
                sheet.write(row_index, 1, g_sum_cur_month, font)
                sheet.write(row_index, 2, g_sum_prv_month, font)
                sheet.write(row_index, 3, g_diff_month, font2)

                sheet.write(row_index, 4, g_sum_cur_ytodate, font)
                sheet.write(row_index, 5, g_sum_prv_ytodate, font)
                sheet.write(row_index, 6, g_diff_year2date, font2)
                row_index += 1

            product_report_category_ids = ProductReportCategory.search([], order='name')
            for pro_rep_cat in product_report_category_ids:
                # call Method to calculate Current/Previous Year Month SaleOrder Amount
                cur_month_pro_rep_cat_sale_order_lines = cur_month_sale_order_lines.filtered(lambda sol: sol.product_id.categ_id.report_category_id.id == pro_rep_cat.id)
                prv_month_pro_rep_cat_sale_order_lines = prv_month_sale_order_lines.filtered(lambda sol: sol.product_id.categ_id.report_category_id.id == pro_rep_cat.id)
                g_sum_cur_month, g_sum_prv_month = rec_month.get_month_calculation_amount(cur_month_sale_order_lines=cur_month_pro_rep_cat_sale_order_lines, prv_month_sale_order_lines=prv_month_pro_rep_cat_sale_order_lines)

                # Difference of Current Year Month and Previous Year Month
                g_diff_month = g_sum_cur_month - g_sum_prv_month

                # call Method to calculate Current/Previous Year To Date SaleOrder Amount
                cur_year_pro_rep_cat_sale_order_lines = cur_year_sale_order_lines.filtered(lambda sol: sol.product_id.categ_id.report_category_id.id == pro_rep_cat.id)
                prv_year_pro_rep_cat_sale_order_lines = prv_year_sale_order_lines.filtered(lambda sol: sol.product_id.categ_id.report_category_id.id == pro_rep_cat.id)
                g_sum_cur_ytodate, g_sum_prv_ytodate = rec_month.get_year_calculation_amount(cur_year_sale_order_lines=cur_year_pro_rep_cat_sale_order_lines, prv_year_sale_order_lines=prv_year_pro_rep_cat_sale_order_lines)

                # Difference of Current and Previous Year
                g_diff_year2date = g_sum_cur_ytodate - g_sum_prv_ytodate

                # Add Data on Sheet row & columns
                sheet.write(row_index, 0, pro_rep_cat.name, vendor_style3)
                sheet.write(row_index, 1, g_sum_cur_month, font)
                sheet.write(row_index, 2, g_sum_prv_month, font)
                sheet.write(row_index, 3, g_diff_month, font2)

                sheet.write(row_index, 4, g_sum_cur_ytodate, font)
                sheet.write(row_index, 5, g_sum_prv_ytodate, font)
                sheet.write(row_index, 6, g_diff_year2date, font2)
                row_index += 1

            sheet.write(row_index, 1, '', top_border)
            sheet.write(row_index, 2, '', top_border)
            sheet.write(row_index, 3, '', top_border)

            sheet.write(row_index, 4, '', top_border)
            sheet.write(row_index, 5, '', top_border)
            sheet.write(row_index, 6, '', top_border)

            ################################################################################### SALES BY SPECIAL GROUPS Ends ###################################################################################


            ################################################################################### TOP 10 CUSTOMERS ###################################################################################

            row_index += 2
            sheet.write_merge(row_index, row_index, 0, 0, 'TOP 10 CUSTOMERS', bold2, )
            sheet.write_merge(row_index, row_index, 1, 3, 'Current Month Sales', bold3, )
            sheet.write_merge(row_index, row_index, 4, 6, 'Year To Date', bold3, )

            row_index += 1
            sheet.write(row_index, 0, '', bold2)
            sheet.write(row_index, 1, cur_year, bold1)
            sheet.write(row_index, 2, previous_endyear, bold1)
            sheet.write(row_index, 3, '$ DIFF', bold3)
            sheet.write(row_index, 4, cur_year, bold1)
            sheet.write(row_index, 5, previous_endyear, bold1)
            sheet.write(row_index, 6, '$ DIFF', bold3)
            row_index += 1

            sorted_top_ten_customer_list = sorted(top_ten_customer_list, key=itemgetter('sum_cur_ytodate'), reverse=True)
            for element in sorted_top_ten_customer_list[:10]:
                vendor_name = ResPartner.browse(element.get('vendor_id', False)).name
                sheet.write(row_index, 0, vendor_name, vendor_style3)
                sheet.write(row_index, 1, element.get('sum_cur_month'), font)
                sheet.write(row_index, 2, element.get('sum_prv_month'), font)
                sheet.write(row_index, 3, element.get('diff_month'), font2)

                sheet.write(row_index, 4, element.get('sum_cur_ytodate'), font)
                sheet.write(row_index, 5, element.get('sum_prv_ytodate'), font)
                sheet.write(row_index, 6, element.get('diff_year2date'), font2)
                row_index += 1

            sheet.write(row_index, 1, '', top_border)
            sheet.write(row_index, 2, '', top_border)
            sheet.write(row_index, 3, '', top_border)

            sheet.write(row_index, 4, '', top_border)
            sheet.write(row_index, 5, '', top_border)
            sheet.write(row_index, 6, '', top_border)

            ################################################################################### TOP 10 CUSTOMERS Ends ###################################################################################


            ################################################################################### SALES BY SALES TERRITORY ###################################################################################

            row_index += 2
            sheet.write_merge(row_index, row_index, 0, 0, 'SALES BY SALES TERRITORY', bold2, )
            sheet.write_merge(row_index, row_index, 1, 3, 'Current Month Sales', bold3, )
            sheet.write_merge(row_index, row_index, 4, 6, 'Year To Date', bold3, )

            row_index += 1
            sheet.write(row_index, 0, '', bold2)
            sheet.write(row_index, 1, cur_year, bold1)
            sheet.write(row_index, 2, previous_endyear, bold1)
            sheet.write(row_index, 3, '$ DIFF', bold3)
            sheet.write(row_index, 4, cur_year, bold1)
            sheet.write(row_index, 5, previous_endyear, bold1)
            sheet.write(row_index, 6, '$ DIFF', bold3)
            row_index += 1

            territory_ids = SalesTerritory.search([], order='name')
            for territory_id in territory_ids:
                # call Method to calculate Current/Previous Year Month SaleOrder
                cur_month_territory_sale_order_lines = cur_month_sale_order_lines.filtered(lambda sol: sol.x_sales_territory_id.id == territory_id.id)
                prv_month_territory_sale_order_lines = prv_month_sale_order_lines.filtered(lambda sol: sol.x_sales_territory_id.id == territory_id.id)
                t_sum_cur_month, t_sum_prv_month = rec_month.get_month_calculation_amount(cur_month_sale_order_lines=cur_month_territory_sale_order_lines, prv_month_sale_order_lines=prv_month_territory_sale_order_lines)

                # Difference of Current Year Month and Previous Year Month
                t_diff_month = t_sum_cur_month - t_sum_prv_month

                # call Method to calculate Current/Previous Year To Date SaleOrder Amount
                cur_year_territory_sale_order_lines = cur_year_sale_order_lines.filtered(lambda sol: sol.x_sales_territory_id.id == territory_id.id)
                prv_year_territory_sale_order_lines = prv_year_sale_order_lines.filtered(lambda sol: sol.x_sales_territory_id.id == territory_id.id)
                t_sum_cur_ytodate, t_sum_prv_ytodate = rec_month.get_year_calculation_amount(cur_year_sale_order_lines=cur_year_territory_sale_order_lines, prv_year_sale_order_lines=prv_year_territory_sale_order_lines)

                # Difference of Current and Previous Year
                t_diff_year2date = t_sum_cur_ytodate - t_sum_prv_ytodate

                # Add Data on Sheet row & columns
                sheet.write(row_index, 0, territory_id.name, vendor_style3)
                sheet.write(row_index, 1, t_sum_cur_month, font)
                sheet.write(row_index, 2, t_sum_prv_month, font)
                sheet.write(row_index, 3, t_diff_month, font2)

                sheet.write(row_index, 4, t_sum_cur_ytodate, font)
                sheet.write(row_index, 5, t_sum_prv_ytodate, font)
                sheet.write(row_index, 6, t_diff_year2date, font2)
                row_index += 1

            sheet.write(row_index, 1, '', top_border)
            sheet.write(row_index, 2, '', top_border)
            sheet.write(row_index, 3, '', top_border)

            sheet.write(row_index, 4, '', top_border)
            sheet.write(row_index, 5, '', top_border)
            sheet.write(row_index, 6, '', top_border)

            ################################################################################### SALES BY SALES TERRITORY Ends ###################################################################################

            stream = io.BytesIO()
            workbook.save(stream)
            out = base64.encodebytes(stream.getvalue())

            vendor_forecast_data_dict.update({'report_data_file': out, 'filename': 'vendor_forecast_report.xls', 'forecast_report_lines': forecast_report_lines_list})

            if len(search_vendor_forecast_ids) > 0:
                # First Update Empty One2many List..Then Write the data into the O2m fields
                search_vendor_forecast_ids.forecast_vendor_report_lines = [(6, 0, [])]
                search_vendor_forecast_ids.forecast_report_lines = [(6, 0, [])]
                search_vendor_forecast_ids.write(vendor_forecast_data_dict)
                forecast_report_create_ids |= search_vendor_forecast_ids
            else:
                forecast_report_create_ids |= self.create(vendor_forecast_data_dict)

        return forecast_report_create_ids


class VendorForecastData(models.Model):
    _name = 'vendor.forecast.data'
    _description = 'Vendor Forecast Data'

    name = fields.Char("Name", copy=False, index=True)
    branch_id = fields.Many2one("res.branch", string="Showroom", default=lambda self: self.env.user.branch_id)
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.user.company_id)
    vendor_id = fields.Many2one("res.partner", string="Supplier", domain="[('supplier', '=', True), ('active', '=', True)]", copy=False, index=True)
    forecast_report_id = fields.Many2one("showroom.vendor.forecast.report", string="Vendor Forecast Report", ondelete='cascade', index=True, copy=False)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.user.company_id.currency_id)

    cur_vendor_amount = fields.Monetary(string='Vendor Current Year Goal', currency_field='currency_id')           # Current Year Vendor Goal
    prv_vendor_amount = fields.Monetary(string='Vendor Previous Year Goal', currency_field='currency_id')          # Previous Year Vendor Goal

    # Current/Previous Year Month SaleOrder Amount
    sum_cur_month = fields.Monetary(string='Current Month Sales', currency_field='currency_id')                     # Current Month Sales
    sum_prv_month = fields.Monetary(string='Previous Month Sales', currency_field='currency_id')                    # Previous Month Sales
    diff_month = fields.Monetary(string='Difference Sales', currency_field='currency_id')                           # Difference Month Sales
    goals = fields.Monetary(string='Vendor Current Year Goal', currency_field='currency_id')                        # Current Month Sales Goal

    # Year to Date Columns
    diff_goal = fields.Monetary(string='Year Difference', currency_field='currency_id')                             # Year diff
    sum_cur_ytodate = fields.Monetary(string='Current YearToDate', currency_field='currency_id')                    # current year to date
    sum_prv_ytodate = fields.Monetary(string='Previous YearToDate', currency_field='currency_id')                   # previous year to date
    diff_year2date = fields.Monetary(string='Difference YearToDate', currency_field='currency_id')                  # Difference year to date
    y2d_goals = fields.Monetary(string='Goal YearToDate', currency_field='currency_id')                             # Goal year to date
    y2d_diff_goal = fields.Monetary(string='Goal Difference YearToDate', currency_field='currency_id')              # Goal Difference Year to date

    current_year = fields.Char(string="Current Year")
    cy_month = fields.Selection([('1', 'Jan'), ('2', 'Feb'), ('3', 'Mar'), ('4', 'Apr'), ('5', 'May'), ('6', 'Jun'), ('7', 'Jul'), ('8', 'Aug'), ('9', 'Sep'), ('10', 'Oct'), ('11', 'Nov'), ('12', 'Dec')], string="Current Year Month")
    py_month = fields.Selection([('1', 'Jan'), ('2', 'Feb'), ('3', 'Mar'), ('4', 'Apr'), ('5', 'May'), ('6', 'Jun'), ('7', 'Jul'), ('8', 'Aug'), ('9', 'Sep'), ('10', 'Oct'), ('11', 'Nov'), ('12', 'Dec')], string="Previous Year Month")
    previous_year = fields.Char(string="Previous Year")


class ShowroomVendorForecastLine(models.Model):
    _name = 'showroom.vendor.forecast.line'
    _description = 'Showroom Vendor Forecast Line'


    branch_id = fields.Many2one("res.branch", string="Showroom", default=lambda self: self.env.user.branch_id)
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.user.company_id)
    forecast_report_id = fields.Many2one("showroom.vendor.forecast.report", string="Vendor Forecast Report", ondelete='cascade', index=True, copy=False)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.user.company_id.currency_id)

    tags_goal_text = fields.Char("Tags Goal Text")                                                                  # Tags Goal Text
    sum_projection = fields.Monetary(string='Projected Sales Sum', currency_field='currency_id')                    # Sum Projection
    month_inward_date = fields.Char('Month Inward Date')                                                            # Month Inward Goal
    month_incremented_amount = fields.Monetary(string="Month Incremented Amount", currency_field='currency_id')     # Month Incremental Amount
    diff_sum_proj_increm_amount = fields.Monetary(string="Difference Sum", currency_field='currency_id')            # Difference AMount

    current_year = fields.Char(string="Current Year")
    cy_month = fields.Selection([('1', 'Jan'), ('2', 'Feb'), ('3', 'Mar'), ('4', 'Apr'), ('5', 'May'), ('6', 'Jun'), ('7', 'Jul'), ('8', 'Aug'), ('9', 'Sep'), ('10', 'Oct'), ('11', 'Nov'), ('12', 'Dec')], string="Current Year Month")


