from odoo import models, fields, api

   
class Partner(models.Model):
    _inherit = 'res.partner'
    
    vendor_goal_ids = fields.One2many('vendor.goals', 'goals_partner_id', string='Vendor Goals')
    report_category_id = fields.Many2one('supplier.report.category', string='Report Category')

    @api.model
    def action_update_sale_territory_in_partner(self):
        SalesTerritory = self.env['sales.territory']

        false_territory_partner_ids = self.search([('x_sales_territory', '=', False)])
        false_territory_partner_ids.write({'x_sales_territory': ''})

        territory_partner_ids = self.search([('x_sales_territory', 'not in', ['', ' '])], order='x_sales_territory')
        x_sales_territories = sorted(set(territory_partner_ids.mapped('x_sales_territory')))

        for x_sales_territory in x_sales_territories:
            try:
                if not SalesTerritory.search([('name', '=', x_sales_territory)]):
                    SalesTerritory.create({'name':x_sales_territory})
            except Exception as e:
                print ("Erorr: ", e)
                pass
        for territory_partner in territory_partner_ids:
            territory_id = SalesTerritory.search([('name','=',territory_partner.x_sales_territory)])
            self._cr.execute("""update res_partner set x_sales_territory_id=%s where id=%s""", (territory_id.id, territory_partner.id))

class VendorGoals(models.Model):
   _name = 'vendor.goals'
   _description = 'Vendor Goals'
   _rec_name = 'showroom'

   annual_goal = fields.Many2one('annual.goals', string="Annual Goal",)
   showroom = fields.Many2one('res.branch', string='Showroom', required=True)
   an_amount = fields.Float(string='Amount')
   goals_partner_id = fields.Many2one('res.partner', string="Goals Partner",)