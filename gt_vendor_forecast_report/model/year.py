from odoo import fields, models


class AnnualGoals(models.Model):
    _name = 'annual.goals'
    _rec_name = 'year'
    _order = 'year, id'

    year = fields.Char(string='Year', required=True)

    _sql_constraints = [('year_unique_type', 'UNIQUE(year)', 'This year is already exist.')]

