from odoo import fields, models


class ProductReportCategory(models.Model):
    _name = 'product.report.category'
    _description = 'Product Report Category'

    name = fields.Char('Report Category', required=True)

    _sql_constraints = [('name_unique_type', 'UNIQUE(name)', 'This category is already exist.')]