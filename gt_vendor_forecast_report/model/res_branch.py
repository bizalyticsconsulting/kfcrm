from odoo import fields, models


class Branch(models.Model):
   _inherit = 'res.branch'

   showroom_goal_ids = fields.One2many('showroom.goals', 'showroom_partner_id', string='Vendor Goals Report')
   monthly_goal_ids = fields.One2many('showroom.monthly.goals', 'branch_id', string='Annual Goals')

class ShowroomGoals(models.Model):
   _name = 'showroom.goals'
   _description = 'Showroom Goals Report'
   _rec_name = 'sequence'

   sequence = fields.Char(string="Sequence", required=True)
   goal_text = fields.Char(string='Goal Text')
   tag_ids = fields.Many2many('crm.lead.tag', 'showroom_tag_rel','showrooms_tag_id','showroom_id', string="Tags")
   showroom_partner_id = fields.Many2one('res.branch', string='Showroom Goals')

class ShowroomMonthlyGoals(models.Model):
   _name = 'showroom.monthly.goals'
   _description = 'Showroom Monthly Goals'
   _rec_name = 'branch_id'

   year_id = fields.Many2one('annual.goals', string='Year')
   goal_amount = fields.Float(string='Goal Amount')
   branch_id = fields.Many2one('res.branch', string='Showroom')