from odoo import fields, models


class SupplierReportCategory(models.Model):
    _name = 'supplier.report.category'
    _description = 'Supplier Report Category'

    name = fields.Char('Report Category', required=True)

    _sql_constraints = [('name_unique_type', 'UNIQUE(name)', 'This category is already exist.')]