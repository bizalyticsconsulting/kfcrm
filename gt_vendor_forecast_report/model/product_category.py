from odoo import fields, models


class ProductCategory(models.Model):
    _inherit = 'product.category'

    report_category_id = fields.Many2one('product.report.category', string='Report Category')