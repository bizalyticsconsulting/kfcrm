{
    'name': 'Sale Forecast Report',
    'summary': """Module adds custom fields to vendor level and Branch as Showrooms.""",
    'description': """The fields are 
    - Annual Goal
    - Current Month Sales
    - Current Month Diff
    - Current Month Goal
    - Current Year Goal
    - Year To Date
    - Year To Date Diff
    - Year To Date Goal
    - Year To Date Annual Goal""",
    'author': 'BizAlytics Consulting LLC',
    'website': 'http://www.bizalytics.com',
    'category': 'Sales',
    'version': '0.1',
    'depends': ['base', 'sale', 'branch', 'crm', 'showroom_fields', 'gt_studio_it_import_fields'],
    'data': [
        'security/ir.model.access.csv',
        'data/report_sequence.xml',
        'data/forecast_scheduler.xml',
        'wizard/forecast_report_wiz_view.xml',
        'views/product_report_category_view.xml',
        'views/product_category_view.xml',
        'views/res_branch_view.xml',
        'views/res_partner_view.xml',
        'views/supplier_report_category_view.xml',
        'views/vendor_forecast_report_view.xml',
        'views/year.xml',
        'views/menuitems_view.xml',
    ],
}
