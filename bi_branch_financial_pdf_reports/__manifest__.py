# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Multiple Branch/Operation Accounting PDF Reports',
    'version': '12.0.0.0',
    'category': 'Accounting',
    'summary': 'This plugin helps you to print Branch Accouting Reports for Balance Sheet, Profit and Loss, Trail Balance, General Ledger',
    'description': """ 


Odoo Branch Financial Reports
Multiple Branch wise Financial PDF Reports
Multiple Branch wise Financial Reports
Branch Financial Reports
Financial Reports
ranch accounting reports
        print Branch accounting report odoo12
        Odoo12 Branch accounting tax reports
        Odoo12 Branch accounting reports
        Branch accounting report odoo12
        Branch excel report odoo12
        Multiple Branch/operation accounting reports
        multibranch accounting reports
        multibranch financial accounting reports
        multibranch financial reports
        multiple branch accounting reports
        multiple branch financial accounting reports
        multiple branch financial reports

        multi Branch pdf report odoo12
        print branch accounting pdf report odoo12
        Odoo12 Branch accounting pdf reports
        Branch accounting report pdf odoo12


        print Branch accounting report odoo12
        Odoo12 Branch accounting reports
        Odoo12 Branch accounting reports
        Branch accounting report odoo12

        Branch financial pdf report odoo12
        print Branch accounting report pdf odoo12
        Odoo12 Branch accounting pdf reports
        Odoo12 Branch accounting pdf reports
        Branch accounting report pdf odoo12
        financial branch reports

        print operating unit accounting report odoo12
        Odoo12 operating unit accounting reports
        Odoo12 operating unit accounting reports
        operating unit accounting report odoo12

        operating unit financial pdf report odoo12
        print operating unit accounting report pdf odoo12
        Odoo12 operating unit accounting pdf reports
        Odoo12 operating unit accounting pdf reports
        operating unit accounting report pdf odoo12

 
        print accounting branch report odoo12
        Odoo12 accounting operating unit reports
        Odoo12 branch accounting reports
        opeation unit accounting report odoo12

        opetrating unit accounting pdf report odoo12
        print branch accounting report pdf odoo12
        
        Print branch Accounting Reports
        With the help of this module you can print report for Accounting
        print branch General Ledger Report
        print branch Trial Balance Report
        print branch Profit & Loss Report
        print branch Balance Sheet Report
        print branch General Ledger Excel Report
        print branch Trial Balance Excel Report
        print branch Profit & Loss Excel Report
        print branch Balance Sheet Excel Report
        print branch General Ledger Report
        print branch Trial Balance Report
        print branch Profit & Loss Report
        print branch Balance Sheet Report
        All branch Accounting reports
        branch Accounting XLS report
        branch Accounting NIIF reports
        branch Account balance sheet reports
        branch Account General Ledger Reports
        branch Account Trial balance reports
        branch Account profit and loss reports

        print Operating unit General Ledger Report
        print Operating unit Trial Balance Report
        print Operating unit Profit & Loss Report
        print Operating unit Balance Sheet Report
        print Operating unit General Ledger Excel Report
        print Operating unit Trial Balance Excel Report
        print Operating unit Profit & Loss Excel Report
        print Operating unit Balance Sheet Excel Report
        print Operating unit General Ledger Report
        print Operating unit Trial Balance Report
        print Operating unit Profit & Loss Report
        print Operating unit Balance Sheet Report
        Operating unit Account profit and loss reports
        Operating unit Accounting XLS report
        Operating unit Accounting NIIF reports

    """,
    'author': 'BrowseInfo',
    'website': 'www.browseinfo.in',
    'price': 11,
    'currency': 'EUR',
    'depends': ['account','branch', 'bi_financial_pdf_reports'],
    'data': [
        'wizard/inherited_balancesheet_view.xml',
        'wizard/inherited_generalledger_view.xml',
        'wizard/inherited_trialbalance_view.xml',
        'reports/inherited_report_balancesheet.xml',
        'reports/inherited_report_trailbalance.xml',
        'reports/inherited_report_generalledger.xml',
    ],
    'installable': True,
    'auto_install': False,
    'images':['static/description/Banner.png'],
    'live_test_url':'https://youtu.be/eyRAayx88oI',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
