{
    'name' : 'Avoid Create Followers',
    'version' : '1.0',
    'author' : 'BizAlytics',
    'category' : 'Extra Tools',
    'description' : """ """,
    "summary":"This module can be used to avoid creating followers record when any model record is created in the odoo.",
    'website': 'https://www.bizalytics.com',
    'depends' : ['sale'],
    'data': [],
}