from odoo import models, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def _write(self, values):
        return super(SaleOrder, self.with_context(mail_activity_automation_skip=True))._write(values)
