from odoo import api, models


class MailThread(models.AbstractModel):
    _inherit = "mail.thread"


    def _message_auto_subscribe_followers(self, updated_values, default_subtype_ids):
        """
        This method is override the default Mail.Thread Model method to restrict the Assigned Mail on sale.order and account.invoice
        Models assigned to users.
        """

        fnames = []
        for name, field in self._fields.items():
            if name == 'user_id' and updated_values.get(name) and getattr(field, 'track_visibility', False):
                if field.comodel_name == 'res.users':
                    fnames.append(name)
        new_subscriptions = []
        return new_subscriptions


    @api.multi
    def message_subscribe(self, partner_ids=None, channel_ids=None, subtype_ids=None):
        """
        This method will stop subscribe the Followers etc if context contains 'mail_create_nosubscribe' as True Values
        """
        if self.env.context.get('alias_model_name') == 'mail.channel' or self.env.context.get('alias_parent_model_name') == 'mail.channel':
            return super(MailThread, self).message_subscribe(partner_ids, channel_ids, subtype_ids)

        if self.env.context.get('mail_create_nosubscribe') or self.env.context.get('mail_post_autofollow'):
            return True
        else:
            return super(MailThread, self).message_subscribe(partner_ids, channel_ids, subtype_ids)


    @api.multi
    def _message_auto_subscribe(self, updated_values, followers_existing_policy='skip'):
        """
            This method will stop subscribe the Followers etc if context contains 'mail_create_nosubscribe' as True Values
        """
        followers_existing_policy = 'skip'
        if self.env.context.get('alias_model_name') == 'mail.channel' or self.env.context.get('alias_parent_model_name') == 'mail.channel':
            return super(MailThread, self)._message_auto_subscribe(updated_values, followers_existing_policy)

        if self.env.context.get('mail_create_nosubscribe') or self.env.context.get('mail_post_autofollow'):
            return True
        else:
            return super(MailThread, self)._message_auto_subscribe(updated_values, followers_existing_policy)

    def _message_subscribe(self, partner_ids=None, channel_ids=None, subtype_ids=None, customer_ids=None):
        if self.env.context.get('mail_create_nosubscribe') or self.env.context.get('mail_post_autofollow'):
            return True
        else:
            return super(MailThread, self)._message_subscribe(partner_ids, channel_ids, subtype_ids, customer_ids)
