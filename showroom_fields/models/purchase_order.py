from odoo import models, api


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.one
    def get_com_cot(self):
        sale_line = self.env['sale.order.line'].search([('purchase_line_id', '=', self.order_line[0].id)])
        if sale_line:
            return sale_line.order_id.x_com_cot
        return ''

    @api.one
    def get_balance(self):
        payment = self.get_payment()[0]
        balance = self.amount_total - payment
        return balance
