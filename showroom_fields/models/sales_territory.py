from odoo.exceptions import ValidationError
from odoo import fields, models, api, _


class SalesTerritory(models.Model):
    _name = 'sales.territory'
    _description = 'Sales Territory'

    name = fields.Char('Sales Territory', required=True)

    _sql_constraints = [('name_unique_type', 'UNIQUE(name)', 'This Territory is already exist.')]

    @api.constrains('name')
    def _check_unique_territory(self):
        territory_ids = self.search([]) - self

        value = [x.name.lower() for x in territory_ids]

        if self.name and self.name.lower() in value:
            raise ValidationError(_('The Sales Territory is already Exist'))

        return True
