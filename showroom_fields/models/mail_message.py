from odoo import api, models


class Message(models.Model):
    _inherit = 'mail.message'


    @api.multi
    def _notify_compute_recipients(self, record, msg_vals):
        """
            This method is overridden,
                - Only modified code for: Do not skip Email for the same Author From : To
                - Send to yourself also

        Compute recipients to notify based on subtype and followers. This
        method returns data structured as expected for ``_notify_recipients``. """
        msg_sudo = self.sudo()

        pids = [x[1] for x in msg_vals.get('partner_ids')] if 'partner_ids' in msg_vals else msg_sudo.partner_ids.ids
        cids = [x[1] for x in msg_vals.get('channel_ids')] if 'channel_ids' in msg_vals else msg_sudo.channel_ids.ids
        subtype_id = msg_vals.get('subtype_id') if 'subtype_id' in msg_vals else msg_sudo.subtype_id.id

        recipient_data = {
            'partners': [],
            'channels': [],
        }
        res = self.env['mail.followers']._get_recipient_data(record, subtype_id, pids, cids)
        author_id = msg_vals.get('author_id') or self.author_id.id if res else False
        for pid, cid, active, pshare, ctype, notif, groups in res:
            if pid and pid == author_id and not self.env.context.get('mail_notify_author'):  # do not notify the author of its own messages
                # continue

                # Custom Code: 26.10.2023 Send Email to user himself only for the Note case
                if self.subtype_id.name == 'Note' and pid == author_id:
                    pass
                else:
                    continue
                #--------------------------------------------------------------------------

            if pid:
                if active is False:
                    # avoid to notify inactive partner by email (odoobot)
                    continue
                pdata = {'id': pid, 'active': active, 'share': pshare, 'groups': groups}
                if notif == 'inbox':
                    recipient_data['partners'].append(dict(pdata, notif=notif, type='user'))
                else:
                    if not pshare and notif:  # has an user and is not shared, is therefore user
                        recipient_data['partners'].append(dict(pdata, notif='email', type='user'))
                    elif pshare and notif:  # has an user but is shared, is therefore portal
                        recipient_data['partners'].append(dict(pdata, notif='email', type='portal'))
                    else:  # has no user, is therefore customer
                        recipient_data['partners'].append(dict(pdata, notif='email', type='customer'))
            elif cid:
                recipient_data['channels'].append({'id': cid, 'notif': notif, 'type': ctype})
        return recipient_data
