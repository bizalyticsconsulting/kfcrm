from odoo.addons.sale.models.sale import SaleOrderLine as ORG_SOL
from odoo.exceptions import UserError, AccessError
from odoo.exceptions import ValidationError
from odoo import models, fields, api, _
from datetime import datetime
import werkzeug.urls
import logging
import json
import pytz
import copy
_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    partner_id = fields.Many2one('res.partner', string='Customer', readonly=True, required=True, change_default=True,
                                 states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, index=True,
                                 track_visibility='always', track_sequence=1,
                                 help="You can find a customer by its Name, TIN, Email or Internal Reference.",
                                 context="{'so_customer_ctx': True}")
    partner_invoice_id = fields.Many2one('res.partner', string='Bill To', readonly=True, required=True,
                                         domain=[('active', '=', True), ('company_type', '=', 'company')],
                                         states={'draft': [('readonly', False)], 'sent': [
                                             ('readonly', False)], 'sale': [('readonly', False)]},
                                         help="Invoice address for current sales order.",
                                         context="{'so_partner_invoice_ctx': True}")
    partner_shipping_id = fields.Many2one('res.partner', string='Ship To Address', readonly=True, required=True,
                                          domain=['&', '|',('partner_type', '=', 'ship to'), ('type', '=', 'delivery'), ('active','=', True)],
                                          states={'draft': [('readonly', False)], 'sent': [
                                              ('readonly', False)], 'sale': [('readonly', False)]},
                                          help="Delivery address for current sales order.",
                                          context="{'so_partner_shipping_ctx': True}")
    x_showroom_terms = fields.Many2one('account.payment.term', 'Terms')
    x_freight_terms = fields.Many2one('account.payment.term', 'Freight Terms')
    x_fob = fields.Char(string="FOB", related="x_supplier.fob",
                        size=100, help="Free On Board Terms")
    # x_attention_to = fields.Char(
    #     string="Attention", index=True, help="Customer Contact")
    x_attention_to = fields.Many2one('res.partner',
        string="Attention", index=True, help="Customer Contact")

    x_ship_to = fields.Many2one(
        'res.partner', string="Ship To", index=True, help="Shipping Contact")
    x_non_residential = fields.Boolean(
        string="Non Residential", help="Non Residential")
    x_attention_ship_to = fields.Char(
        string="Attention Ship To", index=True, help="Shipping Contact")
    x_client_po = fields.Char(string="Customer PO", copy=False,
                              size=100, help="Customer Purchase Order")
    x_shipping_com = fields.Char(
        string="Delivery Notes", size=1000, help="Delivery Notes")
    x_showroom_po = fields.Char(
        string="Showroom PO", size=100, help="Showroom Purchase Order")
    x_client_account = fields.Char(
        string="Account No.", size=100, help="Account No")
    x_sidemark = fields.Char(
        string="Sidemark", size=150, help="Order Sidemark", index=1)
    x_mfg_order = fields.Char(
        string="Supplier Order No.", copy=False, size=100, help="Supplier Order Reference")
    x_mfg_order_date = fields.Date(
        string="Mfg Order Date", help="Manufacturing Order Date")
    x_vend_inv_num = fields.Char(
        string="Vendor Invoice Number", size=100, help="Vendor Invoice Number")
    x_vend_inv_date = fields.Date(
        string="Vendor Invoice Date", help="Vendor Invoice Date")
    x_special_inst = fields.Html(
        string="Special Instructions", help="Special Instructions for the Sales Order / Quote")
    x_order_cfa = fields.Selection([('A_CFA Requested','CFA Requested'),
                                    ('B_CFA Approved', 'CFA Approved'),
                                    ('C_CFA Waived', 'CFA Waived'),
                                    ('E_CFA Waived', 'CFA Not Requested'),
                                    ('D_CFA Not Available', 'CFA Not Available')], string='CFA', track_visibility='onchange')
    x_salesperson1 = fields.Many2one('res.partner', string="Salesperson 1", index=True, domain=[(
        'is_company', '=', False), ('customer', '=', 'True'), ('type', '=', 'contact')], help="Showroom Contact")
    x_salesperson2 = fields.Many2one('res.partner', string="Salesperson 2", index=True, domain=[(
        'is_company', '=', False), ('customer', '=', 'True'), ('type', '=', 'contact')], help="Showroom Alternate Contact")
    sit_salesperson2 = fields.Many2one('res.users', string="Salesperson 2", index=True, help="Salesperson 2")

    x_order_shipdate = fields.Date(
        string="Ship Date", copy=False, help="Sales Order Ship Date")
    x_tracking_num = fields.Char(
        string="Tracking Number", copy=False, size=100, help="Shipping Tracking Number")
    x_supplier = fields.Many2one('res.partner', string='Supplier', store=True, context="{'so_supplier_ctx': True}",
                                 help="Seller Information", domain=[('supplier','=',True), ('parent_id', '=', False), ('active', '=', True)])
    x_showroom = fields.Many2one(
        'res.partner', ondelete='set null', string="xShowroom", index=True)
    x_resale_num = fields.Char(
        string="Resale Number", related="partner_id.x_resale_num", size=100, help="ReSale Number", index=1)
    x_dimensions = fields.Char(
        string="Dimensions", size=200, help="Dimensions Product or Package")
    x_com_cot = fields.Char(string="COM/COT", size=100,
                            help="Client Owned Material / Trim")
    x_ship_via = fields.Many2one('delivery.carrier', 'Ship Via')
    x_npd_order = fields.Boolean(
        string="NPD Order", help="Select for NPD Order")
    x_order_changed = fields.Boolean(
        string="Order Edited Message", help="Show customer order edited message in red bold", default=False)
    x_specifier = fields.Many2one('res.partner', string="Specifier", index=True, domain=[
                                  ('is_company', '=', 'True')], help="Specifier for the Sale")
    x_override_ship = fields.Boolean(
        string="Override Shipping Address", help="Override Shipping Address", default=False)
    dropship_id = fields.Many2one('res.partner')
    x_est_comp_date = fields.Date(
        string="Est. Completion Date", help="Est. Completion Date")
    x_com_ship_to = fields.Many2one('res.partner', string="COM ShipTo Address", index=True, domain=[
        ('supplier', '=', 'True'), ('type', '=', 'delivery')], help="COM ShipTo Address")
    x_quote_created = fields.Boolean(compute='get_quote_created_flag')
    x_prod_category = fields.Many2one(
        'product.category', string="Product Category", index=True, help="Product Category")
    x_cust_tags = fields.Many2many(
        string="Customer Tags", related='partner_id.category_id', readonly=True, required=False, store=False)
    mfg_invoice = fields.Char(string='Supplier Invoice No.', copy=False)
    lead_time = fields.Char(string="Lead Time")
    rdd = fields.Date(string='RDD', copy=False)
    ecd = fields.Date(string='ORIGINAL ECD', track_visibility='onchange', copy=False, track_sequence=10)
    final_destination = fields.Char(string='Final Destination')
    x_quot_prefix = fields.Char(string="Quote Prefix", size=100)
    x_product_names = fields.Text(string="Products", help="Product Names in Order", copy=True, index=True)
    x_product_code = fields.Char(string="Products #", help="Product Default Code in Order Lines", copy=True, index=True)
    x_cust_phone = fields.Char(
        string="Phone", related='partner_id.phone', readonly=True, store=False)
    x_ship_phone = fields.Char(
        string="Phone", related='partner_shipping_id.phone', readonly=True, store=False)
    x_converted_by = fields.Many2one(
        'res.users', string="Converted by", help="KF Employee who converted the quote to an order", track_visibility='onchange')

    x_non_res_bill_to_contact = fields.Many2one("res.partner", "Bill to Contact")
    x_non_res_model_room_date = fields.Date("Model Room date")
    x_logged_in_user = fields.Char("LoggedIn User", default=lambda self: self.env.user.name)

    new_sol_updated = fields.Boolean("New SOL Updated ?")
    x_reserve_exp_date = fields.Date(
        string="Reserve Expiration Date")
    x_skip_commission = fields.Boolean(string="Skip Commission Calculations", copy=False,
                                       help="Will skip Commissions Calculations if this is set to True for Sales Order")
    x_alt_ship_address = fields.Text(string="SHIP TO ADDRESS", help="Ship to Addresses tied to this Sale")
    x_alt_cfa_shipto = fields.Text("Alternate CFA Ship To Address")

    x_alt_com_ship_to = fields.Many2one('res.partner', string="Alternate COM SHIP Address", index=True,
                                        domain=[('supplier', '=', 'True'), ('type', '=', 'delivery')], help="Alternate COM ShipTo Address")

    x_supp_production_status = fields.Char("STATUS", copy=False, index=True)
    x_supp_completion_date = fields.Date(string="COMPLETE DATE", copy=False, index=True)
    current_ecd = fields.Date(string='CURRENT ECD', copy=False, track_visibility='onchange', track_sequence=11)

    # prod_status = fields.Char(string="PRODUCTION STATUS", copy=False)
    # complete_date = fields.Date(string='COMPLETE DATE', copy=False)
    x_partner_address_text = fields.Text(string="Partner Address", readonly=True, store=True,
                                          compute='_compute_partner_address_text')

    x_partner_invoice_text = fields.Text(string="Invoice Address", readonly=True, store=True,
                                         compute='_compute_invoice_address_text')

    x_partner_shipping_text = fields.Text(string="Shipping Address", readonly=True, store=True,
                                         compute='_compute_shipping_address_text')


    @api.onchange('x_client_po')
    def _onchange_is_x_client_po_duplicated(self):
        sale_order_obj = self.env['sale.order']
        for order in self.filtered(lambda so: so.x_client_po not in ['',' ', False]):
            x_client_po_duplicate_record = sale_order_obj.search([('x_client_po','=',order.x_client_po), ('id','not in',order.ids)])

            if x_client_po_duplicate_record:
                return {'warning': {'title': "Customer PO No Duplication", 'message': "This customer P.O. number is already in the system. Please double check this is not a duplicate. Are you sure to create a new one?"}}

    @api.onchange('x_mfg_order')
    def _onchange_is_x_mfg_order_duplicated(self):
        sale_order_obj = self.env['sale.order']
        for order in self.filtered(lambda so: so.x_mfg_order not in ['',' ', False]):
            x_mfg_order_duplicate_record = sale_order_obj.search([('x_mfg_order','=',order.x_mfg_order), ('id','not in',order.ids)])

            if x_mfg_order_duplicate_record:
                return {'warning': {'title': "Supplier Order No Duplication", 'message': "This supplier order number is already in the system. Please double check this is not a duplicate. Are you sure to create a new one?"}}

    @api.one
    @api.depends('partner_id')
    def _compute_partner_address_text(self):
        if self.partner_id:
            addr_str = ''
            addr_str = self.partner_id.with_context({'show_address': 1})._get_address_str()
            self.x_partner_address_text = addr_str

    @api.one
    @api.depends('partner_invoice_id')
    def _compute_invoice_address_text(self):
        if self.partner_invoice_id:
            addr_str = ''
            addr_str = self.partner_invoice_id.with_context({'show_address': 1})._get_address_str()
            self.x_partner_invoice_text = addr_str

    @api.one
    @api.depends('partner_shipping_id')
    def _compute_shipping_address_text(self):
        if self.partner_shipping_id:
            addr_str = ''
            addr_str = self.partner_shipping_id.with_context({'show_address': 1})._get_address_str()
            self.x_partner_shipping_text = addr_str

    # @api.onchange('x_client_po')
    # def onchange_x_client_po(self):
    #     _logger.debug("Checking for Sales Order and Customer PO")
    #     if self.x_client_po and self.partner_id:
    #         # Check if another sales order exists with this combination
    #         _logger.debug("%s %s", self.partner_id.id, self.x_client_po)
    #         x_sales_order = self.env['sale.order'].search([('partner_id', '=', self.partner_id.id), ('x_client_po', '=', self.x_client_po)])
    #         if x_sales_order:
    #             return {
    #                 'warning': {
    #                     'title': "Customer PO Exists",
    #                     'message': f"Customer PO exists in the system tied to Sales Order: {x_sales_order.name}"
    #                 },
    #             }

    @api.onchange('x_non_residential')
    def onchange_x_non_residential(self):
        if self.x_non_residential and self.partner_id:
            partner = self.partner_id.with_context({'show_address': 1}).name_get()
            self.x_specifier = partner[0]

    @api.multi
    def action_confirm(self):
        ctx = self.env.context.copy()
        for order in self:
            # using this code we can avoid the followers in the sale order while confirmed...19.04.2021
            ctx.update({'mail_notify_force_send': True, 'mail_create_nosubscribe': True})
            res = super(SaleOrder, order.with_context(ctx)).action_confirm()
            order.x_converted_by = self.env.uid
            for line in order.order_line:
                line.x_order_confirmation_date = order.confirmation_date

    @api.onchange('confirmation_date')
    def onchange_confirmation_date(self):
        for line in self.order_line:
            line.x_order_confirmation_date = self.confirmation_date

    # @api.onchange('x_supplier')
    # def onchange_x_supplier(self):
    #     if self.partner_id.dropship_ids:
    #         dropship_id = self.partner_id.dropship_ids.filtered(lambda ds: ds.vendor == self.x_supplier)
    #         if dropship_id:
    #             self.x_client_account = dropship_id[0].account_no
    #             self.x_ship_via = dropship_id[0].ship_via.id
    #             self.payment_term_id = dropship_id[0].terms.id
    #             self.x_freight_terms = dropship_id[0].freight_terms.id
    #         else:
    #             self.x_client_account = ''
    #             self.x_ship_via = False
    #             self.payment_term_id = False
    #             self.x_freight_terms = self.partner_id.dropship_ids[0].freight_terms.id if self.partner_id.dropship_ids else False


    @api.onchange('partner_id')
    def onchange_partner_id_domain(self):
        res = {'domain': {'x_attention_to': []}}
        if self.partner_id:
            res_partner_ids = self.partner_id.ids
            if self.partner_id.child_ids:
                res_partner_ids.extend(self.partner_id.child_ids.ids)
            res['domain']['x_attention_to'] = [('id', 'in', res_partner_ids)]
            self.user_id = self.partner_id.user_id.id
        return res

    @api.multi
    def discount_amount(self):
        for sale in self:
            discount = 0.0
            for line in sale.order_line:
                discount += (line.price_unit *
                             line.product_uom_qty) * line.discount/100.00
            return discount


    @api.multi
    def get_company_terms(self):
        return self.company_id.sale_note


    @api.multi
    def get_ecd_text(self):
        ret_text = ""
        if self.current_ecd:
            day_in_month = self.current_ecd.day
            month_name = self.current_ecd.strftime('%B')
            year_num = self.current_ecd.strftime('%Y')
            if day_in_month <= 9:
                ret_text = "EARLY"
            elif day_in_month >= 10 and day_in_month <= 19:
                ret_text = "MID"
            else:
                ret_text = "LATE"

            ret_text = ret_text + ' ' + month_name + ' ' + year_num
        elif self.ecd:
            day_in_month = self.ecd.day
            month_name = self.ecd.strftime('%B')
            year_num = self.ecd.strftime('%Y')
            if day_in_month <= 9:
                ret_text = "EARLY"
            elif day_in_month >= 10 and day_in_month <= 19:
                ret_text = "MID"
            else:
                ret_text = "LATE"

            ret_text = ret_text + ' ' + month_name + ' ' + year_num
        return ret_text


    @api.multi
    def get_quote_created_flag(self):
        for ord in self:
            if ord.name:
                ord.x_quote_created = True
            else:
                ord.x_quote_created = False
            return ord.x_quote_created


    @api.one
    def get_deposit_req(self):
        # Get the payment terms associated with this order
        # Get the payment term Lines
        # If 100% is the value, return amount_total
        # Else, get the amount without the freight and freight tax and return calculated value....
        if (self.payment_term_id):
            payment_term_lines = self.payment_term_id.line_ids.sorted(
                key=lambda r: r.sequence)
            if (len(payment_term_lines) > 0):
                balance_term = payment_term_lines[0]
                perc = balance_term.value_amount / 100.0
            if (perc >= 0):
                return self.amount_total * perc
            # else:
            #     self.x_client_account = ''
            #     self.x_ship_via = False
            #     self.payment_term_id = False
            #     self.x_freight_terms = False


    @api.multi
    def discount_amount(self):
        for sale in self:
            discount = 0.0
            for line in sale.order_line:
                discount += (line.price_unit *
                             line.product_uom_qty) * line.discount/100.00
            return discount


    @api.multi
    def get_company_terms(self):
        return self.company_id.sale_note

    @api.multi
    def get_quote_created_flag(self):
        for ord in self:
            if ord.name:
                ord.x_quote_created = True
            else:
                ord.x_quote_created = False
            return ord.x_quote_created


    @api.one
    def get_deposit_req(self):
        # Get the payment terms associated with this order
        # Get the payment term Lines
        # If 100% is the value, return amount_total
        # Else, get the amount without the freight and freight tax and return calculated value....
        if (self.payment_term_id):
            payment_term_lines = self.payment_term_id.line_ids.sorted(
                key=lambda r: r.sequence)
            if (len(payment_term_lines) > 0):
                balance_term = payment_term_lines[0]
                perc = balance_term.value_amount / 100.0
                if (perc >= 0):
                    return self.amount_total * perc
                else:
                    return self.amount_total
            else:
                return self.amount_total
        else:
            return self.amount_total / 2


    @api.multi
    def update_client_terms_freight(self, vals):
        """ This method return client_terms, client_freight_terms, x_freight_terms, x_client_account
            Priority: Customer/Product Category/Supplier
        """
        x_supplier = self.x_supplier
        x_prod_category = self.x_prod_category
        res_partner_id = self.partner_id
        if self.env.context.get('client_terms_write'):
            if vals.get('x_supplier'):
                x_supplier = self.env['res.partner'].browse(vals.get('x_supplier'))
            if vals.get('x_prod_category'):
                x_prod_category = self.env['product.category'].browse(vals.get('x_prod_category'))
            if vals.get('partner_id'):
                res_partner_id = self.env['res.partner'].browse(vals.get('partner_id'))
        client_terms_id = x_freight_terms = client_freight_terms = x_client_account = x_ship_via = False
        # Get the vendor records from the dropship module related to itself
        vendor_dropship_id = x_supplier.dropship_ids.filtered(lambda ds: ds.vendor.id == x_supplier.id)
        # Get the client records from the dropship module related to the customer
        partner_dropship_id = res_partner_id.dropship_ids.filtered(lambda ds: ds.vendor.id == x_supplier.id)
        _logger.debug("In update_client_terms_freight ...........")
        _logger.debug("Vendor dropship is ..... %s", vendor_dropship_id)
        _logger.debug("Partner dropship is ..... %s", partner_dropship_id)
        _logger.debug("Category is ..... %s", x_prod_category)

        check_ship_via = False
        if partner_dropship_id:
            x_client_account = partner_dropship_id[0].account_no
            client_terms_id = partner_dropship_id[0].terms.id
            x_freight_terms = partner_dropship_id[0].freight_terms.id
            client_freight_terms = partner_dropship_id[0].freight_terms.id
            x_client_account = partner_dropship_id[0].account_no
            x_ship_via = partner_dropship_id[0].ship_via.id

            if not x_ship_via:
                check_ship_via = True

        if x_prod_category and x_prod_category.x_payment_terms and x_prod_category.x_freight_terms:
            if x_prod_category.x_payment_terms and not client_terms_id:
                client_terms_id = x_prod_category.x_payment_terms.id
            if x_prod_category.x_freight_terms and not x_freight_terms:
                x_freight_terms = x_prod_category.x_freight_terms.id
            if x_prod_category.x_freight_terms and not client_freight_terms:
                client_freight_terms = x_prod_category.x_freight_terms.id
            if x_prod_category.x_ship_via and not x_ship_via:
                x_ship_via = x_prod_category.x_ship_via.id

        if vendor_dropship_id:
            if not client_terms_id:
                client_terms_id = vendor_dropship_id[0].terms.id
            if not x_freight_terms:
                x_freight_terms = vendor_dropship_id[0].freight_terms.id
            if not client_freight_terms:
                client_freight_terms = vendor_dropship_id[0].freight_terms.id
            if not x_ship_via:
                x_ship_via = vendor_dropship_id[0].ship_via.id

        if check_ship_via:
            if vendor_dropship_id:
                x_ship_via = vendor_dropship_id[0].ship_via.id

        _logger.debug("IN update_client_terms_freight ...........")
        _logger.debug("client_terms_id ... %s", client_terms_id)
        _logger.debug("client_freight_terms ... %s", client_freight_terms)
        _logger.debug("x_freight_terms ... %s", x_freight_terms)
        _logger.debug("x_client_account ... %s", x_client_account)
        _logger.debug("x_ship_via ... %s", x_ship_via)
        _logger.debug("LEAVING update_client_terms_freight ...........")

        return client_terms_id, client_freight_terms, x_freight_terms, x_client_account, x_ship_via


    @api.model
    def create(self, vals):
        ctx = self.env.context.copy()
        vals['x_quot_prefix'] = 'Quote '

        try:
            # Incase if date_order field does not contains the time: 14:00:00 Then add explicitly
            if vals.get('date_order'):
                date_order_dtf = self.set_default_date_order_time(vals.get('date_order'))
                if date_order_dtf:
                    vals.update({'date_order': date_order_dtf})
        except Exception as e:
            _logger.debug(_("Error Exception in create(): %s"), e)

        # While Create update tax_id same to x_tax_id
        # if 'order_line' in vals:
        #     for i in vals.get('order_line'):
        #         if i[0] in [0] and i[2] and 'x_tax_ids' in i[2] and len(i[2]['x_tax_ids'][0][2]) > 0:  # check for order_line fields get added
        #             i[2]['taxes_assigned_bool'] = True

        # Using this code we can not add the followers of user/partner in the sale order...19.04.2021
        ctx.update({'mail_notify_force_send': True, 'mail_create_nosubscribe': True})
        res = super(SaleOrder, self.with_context(ctx)).create(vals)

        pnames = ''
        pnid = ''
        if res.order_line:
            for line in res.order_line:
                if pnames:
                    pnames = pnames + ', ' + line.product_id.product_tmpl_id.name
                    if line.product_id.product_tmpl_id.product_default_code:
                        pnid = pnid + ', ' + line.product_id.product_tmpl_id.product_default_code
                    elif line.product_id.product_tmpl_id.default_code:
                        pnid = pnid + ', ' + line.product_id.product_tmpl_id.default_code
                else:
                    pnames = line.product_id.product_tmpl_id.name
                    if line.product_id.product_tmpl_id.product_default_code:
                        pnid = line.product_id.product_tmpl_id.product_default_code
                    elif line.product_id.product_tmpl_id.default_code:
                        pnid = line.product_id.product_tmpl_id.default_code

            res.x_product_names = pnames
            res.x_product_code = pnid

        if (len(res.order_line) > 0):
            sale_line = res.order_line[0]
            product_id = sale_line.product_id
            if (len(product_id.seller_ids) > 0):
                seller = product_id.seller_ids[0]
                res.x_supplier = seller.name.id
            res.x_prod_category = product_id.categ_id.id

        if res.x_prod_category or res.x_supplier:
            # Priority: Customer/Product Category/Supplier
            ctx.update({'client_terms_create': True})
            _logger.debug("IN CREATE SALES ORDER ..... SETTING TERMS ......")
            client_terms_id, client_freight_terms, x_freight_terms, x_client_account, x_ship_via = res.update_client_terms_freight(vals)
            _logger.debug("client_terms_id ... %s", client_terms_id)
            _logger.debug("client_freight_terms ... %s", client_freight_terms)
            _logger.debug("x_freight_terms ... %s", x_freight_terms)
            _logger.debug("x_client_account ... %s", x_client_account)
            _logger.debug("x_ship_via ... %s", x_ship_via)
            if 'client_terms' not in vals or not vals.get('client_terms'):
                if not res.client_terms:
                    res.with_context(ctx).write({'client_terms': client_terms_id})

            if 'payment_term_id' not in vals or not vals.get('payment_term_id'):
                if not res.payment_term_id:
                    res.with_context(ctx).write({'payment_term_id': client_terms_id})

            if 'x_freight_terms' not in vals or not vals.get('x_freight_terms'):
                if not res.x_freight_terms:
                    res.with_context(ctx).write({'x_freight_terms': x_freight_terms})
                    # res.client_freight_terms = client_freight_terms

            if 'client_freight_terms' not in vals or not vals.get('client_freight_terms'):
                if not res.client_freight_terms:
                    res.with_context(ctx).write({'client_freight_terms': client_freight_terms})
                    # res.x_freight_terms = x_freight_terms

            if 'x_client_account' not in vals or not vals.get('x_client_account'):
                if not res.x_client_account:
                    res.with_context(ctx).write({'x_client_account': x_client_account})

            if 'x_ship_via' not in vals or not vals.get('x_ship_via'):
                if not res.x_ship_via:
                    res.with_context(ctx).write({'x_ship_via': x_ship_via})

        return res


    @api.multi
    def write(self, vals):
        ctx = self.env.context.copy()
        # check changes for the sale order state as 'sale'
        # By using the new tax field in sale order line...we can handle tax_id many2many field values
        # In case of, sale order in ['sale', 'done'] state and invoice is created
        if vals.get('order_line') and any(['yes' for i in vals.get('order_line') if i[0] in [1]]): # Edit existing Sale Order Line:
            if not self.env.context.get('sale_mass_edit_ctx'):
                for order_rec in self:
                    if len(order_rec.invoice_ids) > 0 and order_rec.state in ['sale', 'done']:
                        new_taxes_vals_dict = {i[1]: i[2]['x_tax_ids'] for i in vals.get('order_line') if i[0] in [1] and i[2] and 'x_tax_ids' in i[2]}
                        for i in vals.get('order_line'):
                            if i[0] in [1] and i[2] and 'x_tax_ids' in i[2]: # check for order_line fields get updated
                                for olrec in order_rec.order_line.filtered(lambda x: x.id == i[1]):
                                    # if olrec.taxes_assigned_bool:
                                    new_assigned_tax_ids = [v[0][2] for k,v in new_taxes_vals_dict.items() if k == olrec.id]
                                    updated_tax_ids = new_assigned_tax_ids[0]
                                    # else:
                                    #     new_assigned_tax_ids = [v[0][2] for k,v in new_taxes_vals_dict.items() if k == olrec.id]
                                    #     updated_tax_ids = olrec.tax_id.ids + new_assigned_tax_ids[0]
                                    #     # i[2]['taxes_assigned_bool'] = True
                                    #     i[2]['x_tax_ids'] = [(6, False, updated_tax_ids)]
                                    i[2]['tax_id'] = [(6, False, updated_tax_ids)]
                    # else:
                        # updating taxes from original field to new taxes field
                        # for i in vals.get('order_line'):
                        #     if i[0] in [1] and i[2] and 'x_tax_ids' in i[2] and len(i[2]['x_tax_ids'][0][2]) > 0: # check for order_line fields get updated
                        #         for olrec in self.order_line.filtered(lambda x: x.id == i[1]):
                        #             if not olrec.taxes_assigned_bool:
                        #                 i[2]['taxes_assigned_bool'] = True
        #---------------------------------------------------------------------------------------------------------------
        ctx = self.env.context.copy()

        # When Mass edit for Multiple Sale Order then dont process below code.
        if not self.env.context.get('sale_mass_edit_ctx') and not self.env.context.get('mass_editing_object'):
            for order_rec in self:
                if (len(order_rec.order_line) > 0):
                    sale_line = order_rec.order_line[0]
                    product_id = sale_line.product_id

                    if (len(product_id.seller_ids) > 0):
                        seller = product_id.seller_ids[0]
                        vals.update({'x_supplier': seller.name.id})
                        _logger.debug(vals)

                    vals.update({'x_prod_category': product_id.categ_id.id})

                _logger.debug("IN WRITE METHOD ....... %s", vals.get('state'))
                if vals.get('state') or order_rec.state:
                    curr_state = vals.get('state') or order_rec.state
                    if (curr_state == 'sale' or curr_state == 'done'):
                        curr_state = 'Sales Order '
                    elif (curr_state == 'draft' or curr_state == 'sent'):
                        curr_state = 'Quote '
                    elif (curr_state == 'cancel'):
                        curr_state = 'Cancelled '
                    else:
                        curr_state = ''

                    _logger.debug("IN WRITE METHOD ....... %s", curr_state)
                    vals.update({'x_quot_prefix': curr_state})

                new_order_line_data = []
                updated_order_line_data = []
                new_sol_updated = False

                if 'order_line' in vals and order_rec.state == 'sale' and len(order_rec.invoice_ids) > 0:
                    # Check for new Sale Order Line ...Based on that new Invoices needs to add
                    new_order_line_data = [i[2] for i in vals['order_line'] if i[0] == 0]

                    # Check for update in Sale Order Line ...Based on that existing Invoice lines needs to update
                    SaleOrderLine = self.env['sale.order.line']
                    updated_order_line_data = {SaleOrderLine.browse(i[1]): i[2] for i in vals['order_line'] if i[0] == 1}

                    if len(new_order_line_data) > 0 or len(updated_order_line_data) > 0:
                        vals.update({"new_sol_updated": True})

                # Update client_terms, client_freight_terms and x_freight_terms
                if not self.env.context.get('client_terms_create'):
                    ctx.update({'client_terms_write': True})
                    # Update these in-case blank..Follow the Priority for update
                    client_terms_id, client_freight_terms, x_freight_terms, x_client_account, x_ship_via = order_rec.with_context(ctx).update_client_terms_freight(vals)
                    if 'client_terms' not in vals or not vals.get('client_terms'):
                        if not order_rec.client_terms or self.env.context.get('order_line_unlink'):
                            vals.update({'client_terms': client_terms_id})
                    if 'payment_term_id' not in vals or not vals.get('payment_term_id'):
                        if not order_rec.payment_term_id or self.env.context.get('order_line_unlink'):
                            vals.update({'payment_term_id': client_terms_id})
                    if 'x_freight_terms' not in vals or not vals.get('x_freight_terms'):
                        if not order_rec.x_freight_terms or self.env.context.get('order_line_unlink'):
                            vals.update({'x_freight_terms': x_freight_terms})
                    if 'client_freight_terms' not in vals or not vals.get('client_freight_terms'):
                        if not order_rec.client_freight_terms or self.env.context.get('order_line_unlink'):
                            vals.update({'client_freight_terms': client_freight_terms})
                    if 'x_client_account' not in vals or not vals.get('x_client_account'):
                        if not order_rec.x_client_account or self.env.context.get('order_line_unlink'):
                            vals.update({'x_client_account': x_client_account})
                    if 'x_ship_via' not in vals or not vals.get('x_ship_via'):
                        if not order_rec.x_ship_via or self.env.context.get('order_line_unlink'):
                            vals.update({'x_ship_via': x_ship_via})

                try:
                    # Incase if date_order field does not contains the time: 14:00:00 Then add explicitly
                    if vals.get('date_order'):
                        date_order_dtf = order_rec.set_default_date_order_time(vals.get('date_order'))
                        if date_order_dtf:
                            vals.update({'date_order': date_order_dtf})

                    # For confirmation_date...No Need for now.
                    if vals.get('confirmation_date'):
                        confirmation_date_dtf = order_rec.set_default_date_order_time(vals.get('confirmation_date'))
                        if confirmation_date_dtf:
                            vals.update({'confirmation_date': confirmation_date_dtf})
                except Exception as e:
                    _logger.debug(_("Error Exception in write(): %s"), e)


        res = super(SaleOrder, self).write(vals)

        # When Mass edit for Multiple Sale Order then dont process below code.
        if not self.env.context.get('sale_mass_edit_ctx') and not self.env.context.get('mass_editing_object'):
            for order_rec in self:
                # Create invoice Line based on the new OrderLine if SaleOrder is in 'sale' state and Invoice is created
                if order_rec.new_sol_updated and len(new_order_line_data) > 0:
                    order_rec.create_invoice_lines_order_lines(new_order_line_data)
                    order_rec._ks_sale_payment_amount_update()
                    order_rec.new_sol_updated = False

                # Update invoice Line based on the updated OrderLine if SaleOrder is in 'sale' state and Invoice is created
                if order_rec.new_sol_updated and len(updated_order_line_data) > 0:
                    check_invoice_product = any([k for k, v in updated_order_line_data.items() if k.product_id.id in order_rec.invoice_ids.mapped('invoice_line_ids').mapped('product_id').mapped('id')])
                    if check_invoice_product:
                        order_rec.update_account_invoice_line(updated_order_line_data)
                        # self._ks_sale_payment_amount_update()
                        order_rec.new_sol_updated = False

                if vals.get('order_line'):
                    # Add Product Name after any changes in the Order Lines Products
                    for so_rec in order_rec:
                        pnames = ''
                        pnid = ''
                        for line in order_rec.order_line:
                            if pnames:
                                pnames = pnames + ', ' + line.product_id.product_tmpl_id.name
                                if line.product_id.product_tmpl_id.product_default_code:
                                    pnid = pnid + ', ' + line.product_id.product_tmpl_id.product_default_code
                                elif line.product_id.product_tmpl_id.default_code:
                                    pnid = pnid + ', ' + line.product_id.product_tmpl_id.default_code
                            else:
                                pnames = line.product_id.product_tmpl_id.name
                                if line.product_id.product_tmpl_id.product_default_code:
                                    pnid = line.product_id.product_tmpl_id.product_default_code
                                elif line.product_id.product_tmpl_id.default_code:
                                    pnid = line.product_id.product_tmpl_id.default_code

                        so_rec.x_product_names = pnames
                        so_rec.x_product_code = pnid
        return res

    @api.onchange('partner_id', 'x_sidemark')
    def caps_sidemark(self):
        if self.x_sidemark:
            self.x_sidemark = str(self.x_sidemark).upper()
        elif self.partner_id.x_cust_sidemark:
            self.x_sidemark = str(self.partner_id.x_cust_sidemark).upper()
        else:
            self.x_sidemark = self.partner_id.name


    @api.multi
    def get_soname(self):
        return self.name[2:]


    @api.multi
    def get_paybill_to(self):
        ret_string = ""
        if self.x_supplier.showroom_type:
            if self.x_supplier.showroom_type == 'showroom_bill':
                ret_string = "Kneedler Fauchère"
            elif self.x_supplier.showroom_type == 'vendor_bill':
                if self.x_supplier.ref:
                    ret_string = self.x_supplier.ref
                else:
                    ret_string = self.x_supplier.name
        else:
            if self.x_supplier.ref:
                ret_string = self.x_supplier.ref
            else:
                ret_string = self.x_supplier.name
        return ret_string


    @api.multi
    def get_payment_details(self):
        payments = {}
        inv_lines = self.invoice_ids.filtered(lambda d: d.state != 'draft')
        if (len(inv_lines) > 0):
            for inv in inv_lines:
                p_lines = inv.payment_ids.filtered(lambda p: (
                    p.state == 'posted' or p.state == 'open'))
                if (len(p_lines) > 0):
                    for p in p_lines:
                        payments.update(p)
        return payments


    @api.multi
    def get_vendor_name(self):
        vendor_list = self.get_vendor()
        if vendor_list:
            vendor = vendor_list[0]
            return vendor.name
        return ""


    @api.multi
    def get_vendor(self):
        if (len(self.order_line) > 0):
            sale_line = self.order_line[0]
            product_id = sale_line.product_id
            if (len(product_id.seller_ids) > 0):
                seller = product_id.seller_ids[0]
                return seller.name
        return ""


    @api.multi
    def get_ship_via(self):
        vendor_list = self.get_vendor()
        if vendor_list:
            vendor = vendor_list[0]

        dropship_lines = self.partner_id.dropship_ids
        if (len(dropship_lines) == 0):
            dropship_lines = self.partner_id.parent_id.dropship_ids

        d_lines = ""
        if vendor_list and vendor:
            d_lines = dropship_lines.filtered(
                lambda d: d.vendor.id == vendor.id)
        if (d_lines and len(d_lines) > 0):
            return d_lines[0].ship_via.id
        else:
            return ""


    @api.multi
    def get_terms(self):
        vendor_list = self.get_vendor()
        if vendor_list:
            vendor = vendor_list[0]

        dropship_lines = self.partner_id.dropship_ids
        if (len(dropship_lines) == 0):
            dropship_lines = self.partner_id.parent_id.dropship_ids

        d_lines = ""
        if vendor_list and vendor:
            d_lines = dropship_lines.filtered(
                lambda d: d.vendor.id == vendor.id)
        if (d_lines and len(d_lines) > 0):
            return d_lines[0].terms
        else:
            return ""


    @api.multi
    def get_freight_terms(self):
        vendor_list = self.get_vendor()
        if vendor_list:
            vendor = vendor_list[0]

        dropship_lines = self.partner_id.dropship_ids
        if (len(dropship_lines) == 0):
            dropship_lines = self.partner_id.parent_id.dropship_ids

        d_lines = ""
        if vendor_list and vendor:
            d_lines = dropship_lines.filtered(
                lambda d: d.vendor.id == vendor.id)
        if (d_lines and len(d_lines) > 0):
            return d_lines[0].freight_terms
        else:
            return ""


    @api.multi
    def get_account_num(self):
        vendor_list = self.get_vendor()
        if vendor_list:
            vendor = vendor_list[0]

        dropship_lines = self.partner_id.dropship_ids
        if (len(dropship_lines) == 0):
            dropship_lines = self.partner_id.parent_id.dropship_ids

        d_lines = ""
        if vendor_list and vendor:
            d_lines = dropship_lines.filtered(
                lambda d: d.vendor.id == vendor.id)
        if (d_lines and len(d_lines) > 0):
            return d_lines[0].account_no
        else:
            return ""


    @api.multi
    def _get_tax_amount_by_group(self):
        self.ensure_one()
        res = {}
        currency = self.currency_id or self.company_id.currency_id
        for line in self.order_line:
            base_tax = 0
            for tax in line.tax_id:
                group = tax.tax_group_id
                res.setdefault(group, 0.0)
                amount = tax.compute_all(
                    line.price_reduce + base_tax, quantity=line.product_uom_qty)['taxes'][0]['amount']
                res[group] += amount
                if tax.include_base_amount:
                    base_tax += amount
        res = sorted(res.items(), key=lambda l: l[0].sequence)
        res = map(lambda l: (l[0].name, l[1]), res)
        return res


    @api.one
    def get_commission_amount(self):
        if (len(self.commission_ids) > 0):
            amount = 0
            for comm in self.commission_ids:
                amount += comm.amount_total
            return amount
        else:
            return 0


    @api.one
    def get_amount_post_commission(self):
        comm_amount = self.get_commission_amount()[0]
        return self.amount_total - comm_amount


    @api.one
    def get_deposit_req(self):
        return self.amount_total / 2


    @api.one
    def get_payment(self):
        payment = 0
        for invoice in self.invoice_ids:
            if invoice.state != 'draft':
                payment += invoice.balance_due
        return payment

    @api.multi
    def get_invoice_payments(self):
        installments = []
        move_obj = self.env['account.move']
        payment_obj = self.env['account.payment']
        invoices = self.invoice_ids.filtered(
            lambda d: d.state != 'draft' and d.type == 'out_invoice')
        for inv in invoices:
            if inv.payments_widget:
                _logger.info(" inv.payments_widget --- " + inv.payments_widget)
                dic_obj = json.loads(inv.payments_widget)
                if dic_obj and isinstance(dic_obj, dict):
                    for line in dic_obj.get('content'):
                        # Changes for Account Payment Details...27.07.2021
                        if line.get('account_payment_id'):
                            account_payment_id = payment_obj.sudo().browse(line.get('account_payment_id'))
                            line.update({'p_amount': account_payment_id.amount,
                                         'memo': line.get('ref') or account_payment_id.communication,
                                         'date': account_payment_id.payment_date.strftime("%m/%d/%Y"),
                                         'p_date': account_payment_id.payment_date.strftime("%m/%d/%Y")
                            })
                        else:
                            # move_id = move_obj.sudo().search([('id', '=', line.get('move_id'))])
                            move_id = move_obj.sudo().browse(line.get('move_id'))
                            for move_line in move_id.line_ids:
                                date = move_line.date
                                formate_date = date.strftime("%m/%d/%Y")
                                if move_line.debit > 0:
                                    line.update({'date': formate_date, 'p_amount': move_line.debit,
                                                 'memo': move_line.ref, 'p_date': move_line.date})

                        installments.append(line)
                        _logger.info(line)
        return installments

    @api.one
    def get_balance(self):
        payment = self.get_payment()[0]
        balance = self.amount_total - payment
        return balance


    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        res = super(SaleOrder, self).onchange_partner_id()

        """
        Update the following fields when the partner is changed:
        - Pricelist
        - Payment terms
        - Invoice address
        - Delivery address
        """
        if not self.partner_id:
            self.update({
                'partner_invoice_id': False,
                'partner_shipping_id': False,
                'payment_term_id': False,
                'fiscal_position_id': False,
            })
            return
        addr = self.partner_id.address_get(['delivery', 'invoice'])
        will_advise_default = self.env['res.partner'].search([('partner_code','=','WILLADVISE')], limit=1, order='id').id
        values = {
            'pricelist_id': self.partner_id.property_product_pricelist and self.partner_id.property_product_pricelist.id or False,
            'payment_term_id': self.partner_id.property_payment_term_id and self.partner_id.property_payment_term_id.id or False,
            'partner_invoice_id': addr['invoice'],
            # 'partner_shipping_id': addr['delivery'],
            # 'partner_shipping_id': None,
            'user_id': self.partner_id.user_id.id or self.partner_id.commercial_partner_id.user_id.id or self.env.uid
        }
        if will_advise_default:
            values['partner_shipping_id'] = will_advise_default

        if self.env['ir.config_parameter'].sudo().get_param('sale.use_sale_note') and self.env.user.company_id.sale_note:
            values['note'] = self.with_context(
                lang=self.partner_id.lang).env.user.company_id.sale_note

        if self.partner_id.team_id:
            values['team_id'] = self.partner_id.team_id.id
        self.update(values)
        return res


    @api.multi
    def create_line_taxes_values(self, invoice_id, invoice_line_id):
        """
        This method will create tax Invoice Lines for new Invoice Lines added from New SaleOrdeLines.
        """
        tax_grouped = {}
        account_invoice_tax = self.env['account.invoice.tax']
        AccountInvoiceTax = self.env['account.invoice.tax']
        round_curr = invoice_id.currency_id.round
        for line in invoice_line_id:
            if not line.account_id or line.display_type:
                continue
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.invoice_line_tax_ids.compute_all(price_unit, invoice_id.currency_id, line.quantity, line.product_id, invoice_id.partner_id)['taxes']
            for tax in taxes:
                val = invoice_id._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)

                if key not in tax_grouped:
                    tax_grouped[key] = val
                    tax_grouped[key]['base'] = round_curr(val['base'])
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += round_curr(val['base'])

        # Create new tax lines
        for tax in tax_grouped.values():
            AccountInvoiceTax |= account_invoice_tax.create(tax)

        return AccountInvoiceTax


    def _prepare_account_move_line(self, order_line_dict, account_invoice_id):
        """
        This method will prepare account.move.line values for new added sale.order.line
        """
        move_line_list = []
        tax_lines_ids_list = []
        # line_taxes_dict = {}
        company_currency = account_invoice_id.company_id.currency_id
        diff_currency = account_invoice_id.currency_id != company_currency

        invoice_line_ids = account_invoice_id.invoice_line_ids.filtered(lambda x: any([k for k, v in order_line_dict.items() if k.id in x.sale_line_ids.ids]))
        _logger.info(f"New account.invoice.line get created for the new sale.order.line update. Account Invoice Line: {invoice_line_ids}")
        for line in invoice_line_ids:
            ### Create Taxes lines in Invoice....
            tax_lines_ids = self.create_line_taxes_values(line.invoice_id, line)
            if tax_lines_ids:
                tax_lines_ids_list.extend(tax_lines_ids.ids)

            if not line.account_id:
                continue
            if line.quantity == 0:
                continue
            tax_ids = []
            # line_taxes_dict[line] = line.invoice_line_tax_ids.ids
            for tax in line.invoice_line_tax_ids:
                tax_ids.append((4, tax.id, None))
                for child in tax.children_tax_ids:
                    if child.type_tax_use != 'none':
                        tax_ids.append((4, child.id, None))
            analytic_tag_ids = [(4, analytic_tag.id, None) for analytic_tag in line.analytic_tag_ids]

            move_line_dict = {
                    'invl_id': line.id,
                    'type': 'src',
                    'name': line.name,
                    'price_unit': line.price_unit,
                    'quantity': line.quantity,
                    'price': line.price_subtotal,
                    'account_id': line.account_id.id,
                    'product_id': line.product_id.id,
                    'uom_id': line.uom_id.id,
                    'account_analytic_id': line.account_analytic_id.id,
                    'analytic_tag_ids': analytic_tag_ids,
                    'tax_ids': tax_ids,
                    'invoice_id': account_invoice_id.id,
                    'branch_id': account_invoice_id.branch_id.id,
            }
            move_line_list.append(move_line_dict)

        # Get only current invoice line taxes
        new_line_tax_list = account_invoice_id.tax_line_move_line_get()   # Handle for only current invoice line tax
        new_line_tax_list = [v for v in new_line_tax_list if v.get('invoice_id') == line.invoice_id.id and v.get('invoice_tax_line_id') in tax_lines_ids_list]
        move_line_list += new_line_tax_list
        total, total_currency, move_line_list = account_invoice_id.compute_invoice_totals(company_currency, move_line_list)
        name = account_invoice_id.name or ''

        iml = []
        if account_invoice_id.payment_term_id:
            totlines = account_invoice_id.payment_term_id.with_context(currency_id=company_currency.id).compute(total, account_invoice_id.date_invoice)[0]
            res_amount_currency = total_currency
            for i, t in enumerate(totlines):
                if account_invoice_id.currency_id != company_currency:
                    amount_currency = company_currency._convert(t[1], account_invoice_id.currency_id, inv.company_id,
                                                                account_invoice_id._get_currency_rate_date() or fields.Date.today())
                else:
                    amount_currency = False

                # last line: add the diff
                res_amount_currency -= amount_currency or 0
                if i + 1 == len(totlines):
                    amount_currency += res_amount_currency

                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': t[1],
                    'account_id': account_invoice_id.account_id.id,
                    'date_maturity': t[0],
                    'amount_currency': diff_currency and amount_currency,
                    'currency_id': diff_currency and account_invoice_id.currency_id.id,
                    'invoice_id': account_invoice_id.id
                })
        else:
            iml.append({
                'type': 'dest',
                'name': name,
                'price': total,
                'account_id': account_invoice_id.account_id.id,
                'date_maturity': account_invoice_id.date_due,
                'amount_currency': diff_currency and total_currency,
                'currency_id': diff_currency and account_invoice_id.currency_id.id,
                'invoice_id': account_invoice_id.id
            })

        partner_id = self.env['res.partner']._find_accounting_partner(account_invoice_id.partner_id)
        move_line_data = [(0, 0, account_invoice_id.line_get_convert(ml, partner_id.id)) for ml in move_line_list]
        move_line_data = account_invoice_id.group_lines(move_line_list, move_line_data)

        return move_line_data, iml


    def create_new_account_move_line(self, order_line_dict, account_invoice_id):
        """
        This method will create new account.move.line based on the new sale.order.line and do calculations accordingly
        """
        ctx = self.env.context.copy()
        account_move_id = account_invoice_id.move_id
        move_line_data, iml_data = self._prepare_account_move_line(order_line_dict, account_invoice_id)

        ## update other account line amount
        other_move_line_id = account_move_id.line_ids.filtered(lambda
                    x: not x.product_id and x.account_id.id == account_invoice_id.account_id.id and x.user_type_id.type == 'receivable' and x.debit > 0 and not x.tax_line_id)
        _logger.info(f"Other account.move.line: {other_move_line_id}")

        if other_move_line_id:
            other_move_line_id = other_move_line_id.sorted(key=lambda x: x.id, reverse=True)[0]
            difference_iml_amount = sum([i['price'] for i in iml_data])
            # other_move_line_amount = sum([i.debit for i in  other_move_line_id])
            # other_line_update_amount = other_move_line_amount + difference_iml_amount
            other_line_update_amount = other_move_line_id.debit + difference_iml_amount
            ctx.update({'showroom_move_line_update': True})
            account_move_id.with_context(ctx).write({
                'line_ids': move_line_data
            })
            account_move_id.with_context(ctx).write({
                'line_ids': [[1, other_move_line_id.id, {'debit': other_line_update_amount}]]
            })
            account_move_id.action_post()
            _logger.info(_("Account Move Line is get 'posted' after update the entries."))

        return True


    def create_invoice_lines_order_lines(self, order_line_data):
        """
        This method will create new Invoice Lines based on the new sale order lines after sale 'confirmed' and invoice is created
        :param order_line_data: new sale order line data in dict
        """
        _logger.info(_('Create Invoice Lines Order Lines In-process: %s'), self)
        ctx = self.env.context.copy()
        sale_order_id = self
        AccountInvoice =  self.env['account.invoice']
        IrProperty = self.env['ir.property']
        ProductProduct = self.env['product.product']
        account_id = False
        order_line_id = False

        # In-case SaleOrderLine creating from the Freight/Pack Wizard
        if self.env.context.get('freight_pack_data'):
            order_line_id = order_line_data
            order_line_data = [{
                'name': order_line_id.name,
                'price_unit': order_line_id.price_unit,
                'product_uom_qty': order_line_id.product_uom_qty,
                'discount': order_line_id.discount,
                'discount_amount': order_line_id.discount_amount,
                'discount_show': order_line_id.discount_show,
                'product_uom': order_line_id.product_uom.id,
                'product_id': order_line_id.product_id.id,
            }]
        if sale_order_id.state != 'sale':
            return False
        if len(sale_order_id.invoice_ids) == 0:
            return False

        # get associated saleorder -->Invoice Id
        account_invoice_id = sale_order_id.invoice_ids.filtered(lambda x: x.state in ['draft', 'open', 'paid', 'cancel']) #not in ['cancel']) # 07.11.2021
        if not account_invoice_id:
            account_invoice_id = AccountInvoice.search(
                [('origin', '=', sale_order_id.name), ('state', 'in', ['draft', 'open', 'paid', 'cancel'])], order="id desc", limit=1)  # ['draft', 'open', 'paid']
        if not account_invoice_id:
            return False
        order_line_dict = {}
        CurrentOrderLine = self.env['sale.order.line']   # If already used put into this
        for order_line in order_line_data:
            product_id = ProductProduct.browse(order_line.get('product_id'))
            if not product_id:
                continue

            # Get Current Order Line ID
            if order_line_id:
                current_order_line_id = order_line_id
            else:
                sort_order_lines = sale_order_id.order_line.sorted(key=lambda x: x.id, reverse=True)
                current_order_lines = sort_order_lines[:len(order_line_data)]
                _logger.info(_(f"Newly added sale order line: {current_order_lines}"))
                current_order_line_id = current_order_lines.filtered(
                    lambda x: x.product_id.id == order_line.get('product_id') and \
                              round(x.price_unit, 3) == round(order_line.get('price_unit'), 3) and \
                              x.product_uom_qty == order_line.get('product_uom_qty'))
                _logger.info(_(f"Filtered from the Sale Order Line for Current added SOL: {current_order_line_id}"))
                current_order_line_id = current_order_line_id - CurrentOrderLine
                if len(current_order_line_id) > 0:
                    current_order_line_id = current_order_line_id[0]
                CurrentOrderLine |= current_order_line_id

            account_id = sale_order_id.fiscal_position_id.map_account(
                product_id.property_account_income_id or product_id.categ_id.property_account_income_categ_id).id
            if not account_id:
                inc_acc = IrProperty.get('property_account_income_categ_id', 'product.category')
                account_id = sale_order_id.fiscal_position_id.map_account(inc_acc).id if inc_acc else False
            if not account_id:
                raise UserError(
                    _(
                        'There is no income account defined for this product: "%s". You may have to install a chart of account from Accounting app, settings menu.') %
                    (product_id.name,))

            taxes = current_order_line_id.tax_id
            # taxes = product_id.taxes_id.filtered(lambda r: not sale_order_id.company_id or r.company_id == sale_order_id.company_id)

            if sale_order_id.fiscal_position_id and taxes:
                tax_ids = sale_order_id.fiscal_position_id.map_tax(taxes, self.product_id, sale_order_id.partner_shipping_id).ids
            else:
                tax_ids = taxes.ids

            if account_invoice_id:
                # If any Invoice is in 'Cancel' state Then first take it in draft state
                if account_invoice_id.state == 'cancel':
                    account_invoice_id.action_invoice_draft()
                _logger.info(_(f"Newly added Sale Order Line ID: {current_order_line_id}"))
                invoice_line_vals = {
                        'origin': sale_order_id.name,
                        'account_id': account_id,
                        'name': order_line.get('name'),
                        'price_unit': order_line.get('price_unit'),
                        'quantity': order_line.get('product_uom_qty'),
                        'discount': order_line.get('discount'),
                        'discount_amount': order_line.get('discount_amount'),
                        'discount_show': order_line.get('discount_show'),
                        'uom_id': order_line.get('product_uom'),
                        'product_id': order_line.get('product_id'),
                        'invoice_line_tax_ids': [(6, 0, tax_ids)],
                        'account_analytic_id': sale_order_id.analytic_account_id.id or False,
                        'sale_line_ids': [(6, 0, [current_order_line_id.id])],
                        'analytic_tag_ids': [(6, 0, current_order_line_id.analytic_tag_ids.ids)],
                }
                _logger.info(_(f"New Invoice Line: {invoice_line_vals}"))
                ### We can do cancel invoice, set to draft, validate invoice, add payments if any....05.02.2021
                if account_invoice_id.state == 'draft':
                    account_invoice_id.write({
                        'invoice_line_ids': [(0, 0, invoice_line_vals)]
                    })
                    account_invoice_id._onchange_invoice_line_ids()
                    account_invoice_id._compute_amount()

                elif account_invoice_id.state in ['open', 'paid']:
                    move_line_list = []
                    ctx.update({'invoice_id': account_invoice_id.id})
                    _logger.info(_("Invoice is in Open State and associated payments: %s."),
                                 account_invoice_id.payment_ids)
                    if account_invoice_id.payment_ids:
                        # First unreconcile and then update invoice lines then add payments
                        for inv_pay in account_invoice_id.payment_ids:
                            inv_move_id = inv_pay.move_line_ids.filtered(lambda x: x.credit > 0)
                            _logger.info(_("Account Payment & Journal Entry: %s - %s"), inv_pay, inv_move_id)
                            move_line_list.append(inv_move_id)
                            inv_move_id.with_context(ctx).remove_move_reconcile()
                            for inv_mv_line in inv_move_id:     # Added June 2, 2022
                                inv_mv_line.reconciled = False  # Added 24.01.2022
                            # inv_pay.move_line_ids.filtered(lambda x: x.credit > 0).with_context(ctx).remove_move_reconcile()

                    # Cancel invoice & Again Reset to Draft...Make update into account.invoice.line and validate
                    account_invoice_id.action_invoice_cancel()
                    account_invoice_id.action_invoice_draft()

                    account_invoice_id.write({
                        'invoice_line_ids': [(0, 0, invoice_line_vals)]
                    })

                    account_invoice_id._onchange_invoice_line_ids()
                    account_invoice_id._compute_amount()
                    account_invoice_id.action_invoice_open()

                    if len(move_line_list) > 0:
                        _logger.info(_("The payments associated account.move.line: %s"), move_line_list)
                        for mv_line in move_line_list:
                            account_invoice_id.assign_outstanding_credit(mv_line.id)

                order_line_dict[current_order_line_id] = invoice_line_vals

        # if account_invoice_id:
            ### comment out code on 05.02.2021...Instead of that we care following cancel invoice, reset fraft, validate and payment if any
            # # If any Journals has been created then cancel the entry for the same and insert new line in journal entry and click on post button
            # if account_invoice_id.move_id:
            #     # Set into 'draft' state and insert new lines
            #     account_invoice_id.move_id.button_cancel()
            #     _logger.info(_(f"Account Move Line is set in 'draft' state to update the new sale order line products entry. Account Move: {account_invoice_id.move_id} "))
            #     move_line_id = sale_order_id.create_new_account_move_line(order_line_dict, account_invoice_id)
            #
            #     # Again Post...24.01.2021
            #     # account_invoice_id.move_id.action_post()

        return True


    @api.multi
    @api.constrains('ks_amount')
    def ks_check_amount_total(self):
        for rec in self:
            if rec.ks_residual:
                if rec.new_sol_updated:
                    pass
                else:
                    if rec.ks_amount > rec.ks_residual:
                        raise ValidationError(
                            _("Reselect your Amount. You have entered amount greater than residual amount"))

            # applying constraint for total amount > input
            if not rec.ks_payment_repeat and rec.ks_amount > rec.amount_total and len(rec.order_line):
                raise ValidationError(
                    _("Reselect your Amount. You have enter amount higher than required amount"))
            if rec.ks_amount < 0:
                raise UserError(_('The payment amount cannot be negative.'))

    @api.onchange('order_line')
    def onchange_order_line_fields(self):
        # client_terms # client_freight_terms # client_terms: payment_terms_id, client_freight_terms = x_client_freight_terms
        for rec in self:
            for line in rec:
                line.name = line.name.encode('utf-8')

            if (len(rec.order_line) > 0):
                sale_line = rec.order_line[0]
                product_id = sale_line.product_id
                if (len(product_id.seller_ids) > 0):
                    seller = product_id.seller_ids[0]
                    rec.x_supplier = seller.name.id
                    rec.x_prod_category = product_id.categ_id.id

                pnames = ''
                pnid = ''
                for line in rec.order_line:
                    _logger.debug(line)
                    if pnames:
                        pnames = pnames + ', ' + line.product_id.product_tmpl_id.name
                        pnid = pnid + ', ' + str(line.product_id.id)
                    else:
                        pnames = line.product_id.product_tmpl_id.name
                        pnid = str(line.product_id.id)

                rec.x_product_names = pnames
                rec.x_product_id = pnid
                _logger.debug(pnames)
            else:
                rec.x_supplier = False
                rec.x_prod_category = False


    @api.multi
    def action_quotation_send(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        but changed only template custom to load.
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('showroom_fields', 'email_template_kf_order')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        lang = self.env.context.get('lang')
        template = template_id and self.env['mail.template'].browse(template_id)
        if template and template.lang:
            lang = template._render_template(template.lang, 'sale.order', self.ids[0])
        ctx = {
            'default_model': 'sale.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'model_description': self.with_context(lang=lang).type_name,
            'custom_layout': "mail.mail_notification_paynow",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


    @api.multi
    def prepare_update_invoice_line(self, invoice_id, invoice_line_id, updated_vals):
        """
        This method will prepare and update invoice lines based on the sale orde lines.
        Cancel Invoice-->Reset To Draft-->Update Invoice Lines-->Validate Invoices
        """
        invoice_line_id_list = []
        sol_invoice_qty_dict = {}
        for sol_id, sol_vals in updated_vals.items():
            try:
                if sol_id.product_id.id == invoice_line_id.product_id.id and invoice_line_id.id in sol_id.invoice_lines.ids:
                    write_vals = {}
                    if 'price_unit' in sol_vals:
                        write_vals['price_unit'] = sol_vals['price_unit']
                    if 'product_uom_qty' in sol_vals:
                        write_vals['quantity'] = sol_vals['product_uom_qty']
                        sol_invoice_qty_dict.update({sol_id: sol_vals['product_uom_qty']})
                    if 'name' in sol_vals:
                        write_vals['name'] = sol_vals['name']
                    if 'tax_id' in sol_vals:
                        if sol_id.taxes_assigned_bool:
                            tax_id_list = [(6, 0, sol_vals['tax_id'][0][2])]
                        # else:
                        #     tax_id_list = [(4, tid) for tid in sol_vals['tax_id'][0][2]]
                        write_vals['invoice_line_tax_ids'] = tax_id_list #[(4, sol_vals['tax_id'][0])]

                    if 'discount' in sol_vals:
                        write_vals['discount'] = sol_vals['discount']
                    if 'discount_amount' in sol_vals:
                        write_vals['discount_amount'] = sol_vals['discount_amount']
                    if 'discount_show' in sol_vals:
                        write_vals['discount_show'] = sol_vals['discount_show']

                    if len(write_vals) > 0:
                        invoice_line_id_list.append([1, invoice_line_id.id, write_vals])
            except Exception as e:
                print("Error: ",e)
                _logger.info(_("Error while process invoices update: %s"), e)
        return invoice_line_id_list, sol_invoice_qty_dict


    @api.multi
    def update_account_invoice_line(self, updated_vals):
        """
        This method will update the account.invoice.line based on the sale.order.line values.
        case-1: Invoice in 'Open' state-->Direct change in invoice lines and Journal Entry
        case-2: Invoice in 'Partial Paid' state-->Unreconcile -->changes in invoice lines and Journal Entry-->Add Payment
        case-3: Invoice in 'paid' state-->Unreconcile -->changes in invoice lines and Journal Entry-->Add Payment
        """
        _logger.info(_("Update Account Invoice Line In-process.....: %s"), self)
        AccountInvoice = self.env['account.invoice']
        AccountInvoiceLine = self.env['account.invoice.line']
        ctx = self.env.context.copy()
        sale_line_invoice_qty = {}
        invoice_ids = self.invoice_ids.filtered(lambda x: x.state in ['draft', 'open', 'paid', 'cancel']) # ['draft', 'open', 'paid'] # 07.11.2021
        if not invoice_ids:
            invoice_ids = AccountInvoice.search(
                [('origin', '=', self.name), ('state', 'in', ['draft', 'open', 'paid', 'cancel'])], order="id desc", limit=1) # ['draft', 'open', 'paid']
        _logger.info(_("Sale's associated Account Invoices : %s"), invoice_ids)
        if not invoice_ids:
            return False
        for inv_rec in invoice_ids:
            # If Sale's Invoice is in 'Cancel' state then take it in the 'Draft' state first then process for the next step...07.11.2021
            if len(inv_rec) > 0 and inv_rec.state == 'cancel':
                inv_rec.action_invoice_draft()

            ctx.update({'invoice_id': inv_rec.id})
            _logger.info(_('Process Invoice Line update:  %s'), inv_rec)
            invoice_line_value_lists = []
            move_line_list = []
            for inv_line in inv_rec.invoice_line_ids:
                if inv_line.product_id.id in [i.product_id.id for i, j in updated_vals.items()]:
                    line_vals_list, sol_invoice_qty_dict = self.prepare_update_invoice_line(inv_rec, inv_line, updated_vals)
                    if len(line_vals_list) > 0:
                        invoice_line_value_lists.extend(line_vals_list)
                    if len(sol_invoice_qty_dict) > 0:
                        if list(sol_invoice_qty_dict.keys())[0] not in list(sale_line_invoice_qty.keys()):
                            sale_line_invoice_qty[list(sol_invoice_qty_dict.keys())[0]] = list(sol_invoice_qty_dict.values())[0]
            if len(invoice_line_value_lists) > 0:
                _logger.info(_("Update invoice values lists: %s"), invoice_line_value_lists)

                # Check if only invoice lines changed with 'Name' i.e. 'description' field in Sale Order Lines
                # Then only update the Invoice Lines too using direct Query..instead of process..Cancel Invoices/Draft/Open/Paid Payment etc.
                # That could be easy for process
                invoice_line_value_data = list(filter(lambda i: i[0] == 1 and len(i[2]) == 1 and 'name' in i[2], invoice_line_value_lists))
                invoice_line_value_lists = list(filter(lambda x: x not in invoice_line_value_data, invoice_line_value_lists))

                if len(invoice_line_value_lists) > 0:
                    if inv_rec.state == 'draft':
                        inv_rec.write({'invoice_line_ids': invoice_line_value_lists})
                        inv_rec._onchange_invoice_line_ids()
                        inv_rec._compute_amount()

                        ### Update sale order line qty_invoiced field too
                        if len(sale_line_invoice_qty) > 0:
                            for k,v in sale_line_invoice_qty.items():
                                k.qty_invoiced = v
                        # Call method to visible the Add Payment button conditionally
                        self._ks_sale_payment_amount_update()

                    elif inv_rec.state in ['open', 'paid']:
                        inv_payment_ids = inv_rec.payment_ids.filtered(lambda x: x.has_invoices)
                        _logger.warn("Payments associated with invoice .... %s", inv_payment_ids)
                        if len(inv_payment_ids) > 0:
                            # First unreconcile and then update invoice lines then add payments
                            for inv_pay in inv_payment_ids:
                                inv_move_id = inv_pay.move_line_ids.filtered(lambda x: x.credit > 0)
                                _logger.info(_("Account Payment & Journal Entry: %s - %s"), inv_pay, inv_move_id)
                                move_line_list.append(inv_move_id)
                                inv_pay.move_line_ids.filtered(lambda x: x.credit > 0).with_context(ctx).remove_move_reconcile()
                                inv_move_id.reconciled = False  # Added 24.01.2022

                        _logger.warn("Payment account move line is .... %s", move_line_list)
                        # Cancel invoice & Again Reset to Draft...Make update into account.invoice.line and validate
                        inv_rec.action_invoice_cancel()
                        inv_rec.action_invoice_draft()
                        _logger.warn("Starting to change invoice .... %s", invoice_line_value_lists)
                        inv_rec.write({'invoice_line_ids': invoice_line_value_lists})
                        _logger.warn("Starting to compute total for invoice .... %s", inv_rec)
                        inv_rec._onchange_invoice_line_ids()
                        inv_rec._compute_amount()
                        _logger.warn("Starting to mark invoice open.... %s", inv_rec)
                        inv_rec.action_invoice_open()

                        if len(move_line_list) > 0:
                            for mv_line in move_line_list:
                                _logger.warn("Starting to assign payments again .... %s", mv_line)
                                inv_rec.assign_outstanding_credit(mv_line.id)

                        ### Update sale order line qty_invoiced field too
                        if len(sale_line_invoice_qty) > 0:
                            for k, v in sale_line_invoice_qty.items():
                                k.qty_invoiced = v

                        # Call method to visible the Add Payment button conditionally
                        self._ks_sale_payment_amount_update()

                elif len(invoice_line_value_data) > 0:
                    #_logger.info(_("Update Account Invoice Line 'Name' i.e. 'Description' using Query."))
                    for inv_line_rec in invoice_line_value_data:
                        acc_inv_res = AccountInvoiceLine.browse(inv_line_rec[1])
                        if acc_inv_res:
                            acc_inv_res.name = inv_line_rec[2].get('name')
                            self.env.cr.commit()
                        #line_name = "'"+str(inv_line_rec[2].get('name'))+"'"
                        #update_query = "update account_invoice_line set name=" + line_name + " where id=" + str(inv_line_rec[1])
                        #_logger.info(_("Update Query:  %s"), update_query)
                        #self.env.cr.execute(update_query)

        return True


    def action_open_sale_payment_wizard(self):
        """
        This method will open a Sale Payment Wizard with the associated sale order payments list.
        User can able to check and cancel the payments.
        """
        if self.balance_due >= 0:
            return True
        ctx = self.env.context.copy()
        # SalePaymentWizard = self.env['sale.payment.cancel.wizard.line']
        # create_id = False
        invoice_ids = self.env['account.invoice'].search([('origin', '=', self.name)], limit=1)
        if len(invoice_ids) > 0:
            sale_payment_wizard_line = []
            for inv in invoice_ids:
                payment_ids = inv.mapped('payment_ids').filtered(lambda x: x.state == 'posted')
                for pay_line in payment_ids:
                    move_ids = pay_line.move_line_ids.mapped('move_id')
                    pay_info_vals = {
                        'payment_date': pay_line.payment_date,
                        'journal_id': pay_line.journal_id.id,
                        'memo': pay_line.communication,
                        'account_move_id': move_ids and move_ids[0].id or False,
                        'amount_paid':  pay_line.amount,
                        'invoice_id': inv.id,
                        'partner_id': pay_line.partner_id.id,
                        'payment_id': pay_line.id,
                    }
                    sale_payment_wizard_line.append((0, 0, pay_info_vals))

            if len(sale_payment_wizard_line) > 0:
                ctx.update({'default_sale_order_id': self.id, 'default_payment_line_ids': sale_payment_wizard_line})
                # create_id = SalePaymentWizard.create({'sale_order_id': self.id, 'payment_line_ids': sale_payment_wizard_line})

        return {
            'name': _('Cancel Payment Wizard'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sale.payment.cancel.wizard',
            # 'res_id': create_id and create_id.id or False,
            'target': 'new',
            'context': ctx,
        }


    @api.model
    def default_get(self, fields_values):
        res = super(SaleOrder, self).default_get(fields_values)
        try:
            # Set default timestamp as 14:00
            if res.get('date_order'):
                _logger.info(_("Sale Order 'date_order' field value : %s"), res.get('date_order'))
                date_order_dtf = self.set_default_date_order_time(res.get('date_order'))
                if date_order_dtf:
                    res.update({'date_order': date_order_dtf})
        except Exception as e:
            _logger.debug(_("Error Exception in default_get(): %s"), e)
        return res


    @api.multi
    def set_default_date_order_time(self, so_date_order):
        """
            This method will set time as 14:00:00 to 'date_order'.)
        """
        now_dt = fields.Datetime.context_timestamp(self, datetime.now())
        _logger.info(_("Current user default date field value : %s"), now_dt)
        if so_date_order:
            if isinstance(so_date_order, str):
                so_date_order_datetime = datetime.strptime(so_date_order, '%Y-%m-%d %H:%M:%S')
            else:
                so_date_order_datetime = so_date_order

            if str(so_date_order_datetime.time()) != "14:00:00":
                _logger.info(_("Sale Order default date field value : %s"), so_date_order)
                so_date_order_str = now_dt.date().strftime('%Y-%m-%d') + " 14:00:00"
                so_date_order_dtf = datetime.strptime(so_date_order_str, '%Y-%m-%d %H:%M:%S')
                _logger.info(_("Updated datetime with 14:00:00 Time: %s"), so_date_order_dtf)
                return so_date_order_dtf
        return False


    @api.multi
    def _get_sale_url(self):
        """
        This method will open a Many2one field i.e. sale order in new tab
        """
        # ctx = self.env.context.copy()
        record_sale_url = ''
        if self:
            # ctx.update({'create': 0, 'edit': 0, 'delete': 0})
            view_type = 'form'
            model = 'sale.order'
            menu_id = self.env.ref("sale.menu_sale_order")
            action_id = self.env.ref("sale.action_orders")
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            fragment = dict()
            fragment['id'] = self.id
            if action_id:
                fragment['action'] = action_id.id
            if model:
                fragment['model'] = model
            if view_type:
                fragment['view_type'] = view_type
            if menu_id:
                fragment['menu_id'] = menu_id.id

            record_sale_url = "%s/web#%s" % (base_url, werkzeug.urls.url_encode(fragment))
        return record_sale_url


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.one
    def _compute_previous_confirmation_month(self):
        user_exists = self.env.user.has_group('showroom_fields.group_kf_administrator')

        today_date = datetime.now()
        for rec in self:
            if user_exists:
                rec.x_previous_confirmation_month = False
            elif rec.x_order_confirmation_date:
                x_previous_month = False
                if rec.x_order_confirmation_date.month < today_date.month:
                    x_previous_month = True
                rec.x_previous_confirmation_month = x_previous_month

    @api.depends('order_id.partner_id', 'order_id.partner_id.x_sales_territory_id')
    def _compute_x_sales_territory_id(self):
        SalesTerritory = self.env['sales.territory']
        for line in self:
            if line.x_state_territory not in ['',' ', False]:
                x_sales_territory_id = SalesTerritory.search([('name', '=', line.x_state_territory)])
                line.x_sales_territory_id = x_sales_territory_id.id
            else:
                line.x_sales_territory_id = line.order_id.partner_id.x_sales_territory_id.id

    x_showroom = fields.Many2one("res.partner", related='order_id.x_showroom', string="Dep Showroom", readonly=True, required=False, store=True)
    x_branch = fields.Many2one("res.branch", related='order_id.branch_id', string="Showroom", readonly=True, required=False, store=True)
    x_order_shipdate = fields.Date(string="Ship Date", help="Sales Order Ship Date", related='order_id.x_order_shipdate', readonly=True, required=False, store=True)
    x_customer = fields.Many2one("res.partner", related='order_id.partner_id', string="Customer", readonly=True, required=False, store=True)
    x_mfg_order = fields.Char(string="Mfg Order#", related='order_id.x_mfg_order', help="Mfg Order#", readonly=True, required=False, store=True)
    x_vend_inv_num = fields.Char(string="Mfg Inv#", related='order_id.x_vend_inv_num', help="Mfg Inv#", readonly=True, required=False, store=True)
    x_order_total = fields.Monetary(string="Order Total", related='order_id.amount_total', help="Sales Order Total", readonly=True, required=False, store=True)
    x_sales_team = fields.Many2one("crm.team", related='order_id.team_id', string="Sales Team", readonly=True, required=False, store=True)
    x_order_city = fields.Char(string="City", related='order_partner_id.city', size=200, help="City", readonly=True, required=False, store=True)
    x_order_state_id = fields.Many2one("res.country.state", related='order_partner_id.state_id', string="State", readonly=True, required=False, store=True)
    x_order_date = fields.Datetime(string="Order Date", related='order_id.date_order', help="Sales Order Date", readonly=True, required=False, store=True)
    x_sidemark = fields.Char(string="Sidemark", related='order_id.x_sidemark', readonly=True, required=False, store=True)
    x_order_confirmation_date = fields.Datetime(string="Order Conversion Date", help="Sales Order Confirmation Date", readonly=False, required=False, store=True, copy=False)

    x_internal_cat1 = fields.Many2one("product.category", string="Category", required=False, store=True)
    product_tmpl_id = fields.Many2one('product.template', 'Product Template', auto_join=True, index=True, ondelete="cascade")
    x_status = fields.Selection("sale.order.state", related='order_id.state', readonly=True, required=False, store=True)
    x_client_po = fields.Char(string="Customer PO", related='order_id.x_client_po', copy=False, readonly=True, required=False, store=True, help="Customer Purchase Order")
    layout_category_id = fields.Many2one('sale.layout_category', string='Section')
    x_vendor = fields.Many2one('res.partner', string='Supplier',  index=True, context="{'so_supplier_ctx': True}", domain=[('supplier', '=', 'True')], help="Supplier Information", copy=True, store=True)

    x_tax_ids = fields.Many2many("account.tax", "xtax_sale_line_rel", "sol_id", "tax_id", string="Taxes", help="You can assign Taxes from here in case of invoice is created.")
    invoice_ids = fields.Many2many("account.invoice", related="order_id.invoice_ids", string='Invoices')
    taxes_assigned_bool = fields.Boolean("All Taxes Assigned ?", default=True, copy=False)

    discounted_price_subtotal = fields.Monetary(readonly=True, store=True, string='Discounted SubTotal', compute='_compute_amount', help="Discounted SubTotal amount without taxes")

    x_sales_territory_id = fields.Many2one('sales.territory', string="Sales Territory", compute='_compute_x_sales_territory_id', store=True, compute_sudo=True)
    x_state_territory = fields.Char(string="Territory", related="order_id.partner_id.x_state_territory", store=True)
    x_zip_county_id = fields.Many2one('state.county', string='County', related="order_id.partner_id.zip_county_id", store=True)
    x_previous_confirmation_month = fields.Boolean(string='Previous Confirmation Month?', compute='_compute_previous_confirmation_month')
    x_converted_by = fields.Many2one("res.users", related='order_id.x_converted_by', string="Converted By", readonly=True, required=False, store=True)
    x_created_by = fields.Many2one("res.users", related='order_id.create_uid', string="Order Created By", readonly=True, required=False, store=True)

    x_supplier_id = fields.Many2one('res.partner', string='Sale Line - Supplier', context="{'so_supplier_ctx': True}", domain="[('supplier', '=', True), ('active', '=', True), ('type', '=', 'contact'), ('parent_id', '=', False)]")
    x_salesperson = fields.Many2one("res.users", related='order_id.user_id', string="Sale Line - Salesperson", readonly=True, required=False, store=True)


    # @api.multi
    # @api.onchange('product_id')
    # def onchange_product_id(self):
    #    res = super(SaleOrderLine, self).product_id_change()
    #    name = self.product_id.name
    #    desc_name = self.product_id.name
    #    if self.product_id.description_sale:
    #       name += '\n' + self.product_id.description_sale
    #    res.update(name=name)
    #    res.write({name=desc_name)
    #    return res

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        if not self.product_id:
            return {'domain': {'product_uom': []}}

        # remove the is_custom values that don't belong to this template
        for pacv in self.product_custom_attribute_value_ids:
            if pacv.attribute_value_id not in self.product_id.product_tmpl_id._get_valid_product_attribute_values():
                self.product_custom_attribute_value_ids -= pacv

        # remove the no_variant attributes that don't belong to this template
        for ptav in self.product_no_variant_attribute_value_ids:
            if ptav.product_attribute_value_id not in self.product_id.product_tmpl_id._get_valid_product_attribute_values():
                self.product_no_variant_attribute_value_ids -= ptav

        vals = {}
        domain = {'product_uom': [
            ('category_id', '=', self.product_id.uom_id.category_id.id)]}
        if not self.product_uom or (self.product_id.uom_id.id != self.product_uom.id):
            vals['product_uom'] = self.product_id.uom_id
            vals['product_uom_qty'] = self.product_uom_qty or 1.0

        product = self.product_id.with_context(
            lang=self.order_id.partner_id.lang,
            partner=self.order_id.partner_id,
            quantity=vals.get('product_uom_qty') or self.product_uom_qty,
            date=self.order_id.date_order,
            pricelist=self.order_id.pricelist_id.id,
            uom=self.product_uom.id
        )

        result = {'domain': domain}

        name = self.product_id.name
        if self.product_id.description_sale:
            name += '<br/>\n' + self.product_id.description_sale
        # name = self.get_sale_order_line_multiline_description_sale(product)

        vals.update(name=name)
        self._compute_tax_id()

        if self.order_id.pricelist_id and self.order_id.partner_id:
            vals['price_unit'] = self.env['account.tax']._fix_tax_included_price_company(self._get_display_price(product),
                                                                                         product.taxes_id, self.tax_id,
                                                                                         self.company_id)
        self.update(vals)

        title = False
        message = False
        warning = {}
        if product.sale_line_warn != 'no-message':
            title = _("Warning for %s") % product.name
            message = product.sale_line_warn_msg
            warning['title'] = title
            warning['message'] = message
            result = {'warning': warning}
            if product.sale_line_warn == 'block':
                self.product_id = False

        return result

    @api.multi
    def sale_line_stock_return_update(self, updated_product_qty):
        """ This method will update/delete/return stock.picking-->stock.move for the same product accordingly if any sale order line product update/delete etc. """
        ctx = self.env.context.copy()
        # Incase of mulitple pickings then check for picking..which is not in done/cancel and move having the equal or more qty for decreased
        sol_picking_id_list = [i.id for i in self.order_id.picking_ids for j in i.move_ids_without_package if i.picking_type_id.code in ['outgoing'] \
                               and i.state not in ['done', 'cancel'] and j.sale_line_id.id == self.id and j.product_uom_qty >= abs(updated_product_qty)]
        if len(sol_picking_id_list) == 0:
            sol_picking_id_list = [i.id for i in self.order_id.picking_ids for j in i.move_ids_without_package if i.picking_type_id.code in ['outgoing'] \
                                   and i.state not in ['cancel'] and j.sale_line_id.id == self.id and j.product_uom_qty >= abs(updated_product_qty)]
        if len(sol_picking_id_list) > 0:
            sol_picking_ids = self.env['stock.picking'].browse(sol_picking_id_list)
            total_done_qty = sum(
                [j.quantity_done for i in self.order_id.picking_ids for j in i.move_ids_without_package if
                 i.picking_type_id.code in ['outgoing'] and i.state in ['done']])
            for pick in sol_picking_ids.sorted()[0]:
                sol_move_id = pick.move_ids_without_package.filtered(lambda x: x.sale_line_id.id == self.id)
                if pick.state in ['draft', 'waiting', 'confirmed', 'assigned']:
                    # Unreserve and change product qty and again do reserve
                    pick.do_unreserve()
                    # for mv_line in pick.move_ids_without_package.filtered(lambda x: x.sale_line_id.id == self.id):

                    # Delete stock.move if deleted sale_order_line
                    if self.env.context.get('delete_sale_line'):
                        # sol_move_id.unlink()  # Can only delete in 'draft', 'cancel' state
                        for mv_line in sol_move_id:
                            mv_line.product_uom_qty = 0  # mv_line.product_uom_qty - abs(updated_product_qty)
                    else:
                        for mv_line in sol_move_id:
                            mv_line.product_uom_qty = self.product_uom_qty  # mv_line.product_uom_qty - abs(updated_product_qty)

                    pick.action_assign()

                elif pick.state in ['done']:
                    StockReturnPicking = self.env['stock.return.picking']
                    ctx.update({'active_model': 'stock.picking',
                                'active_id': pick.id,
                                'active_ids': pick.ids,
                                'contact_display': 'partner_address',
                                })
                    stk_return_fields = ['move_dest_exists', 'product_return_moves', 'parent_location_id',
                                         'original_location_id', 'location_id']
                    stk_return_values = StockReturnPicking.with_context(ctx).default_get(stk_return_fields)

                    if 'product_return_moves' in stk_return_values:
                        product_return_move_lines = stk_return_values['product_return_moves']
                        product_return_move_lines = [i for i in product_return_move_lines if 'move_id' in i[2] and i[2]['move_id'] in sol_move_id.ids]
                        if product_return_move_lines:
                            product_return_move_lines[0][2]['quantity'] = abs(updated_product_qty)
                        stk_return_values['product_return_moves'] = product_return_move_lines

                    ### comment 28.02.2021..Above code added
                    # change quantity in the return stock..only present the sale order line product_id..other remove
                    # product_return_moves = [v[0][2] for k, v in stk_return_values.items() if
                    #                         k == 'product_return_moves' and v[0][2][
                    #                             'product_id'] == self.product_id.id and v[0][2][
                    #                             'move_id'] == sol_move_id.id]
                    # if len(product_return_moves) > 0:
                    #     product_return_moves_set = product_return_moves[0]
                    #     product_return_moves_set['quantity'] = abs(updated_product_qty)
                    #     stk_return_values['product_return_moves'] = [(0, 0, product_return_moves_set)]

                    stk_return_picking_id = StockReturnPicking.with_context(ctx).create(stk_return_values)
                    if len(stk_return_picking_id) > 0:
                        new_return_picking_values = stk_return_picking_id.with_context(ctx).create_returns()
                        if new_return_picking_values and new_return_picking_values.get('res_id'):
                            new_return_picking_id = self.env['stock.picking'].browse(
                                new_return_picking_values.get('res_id'))
                            if new_return_picking_id:
                                new_return_picking_id.auto_fill_product_qty()
                                new_return_picking_id.button_validate()

                                # update reverse picking in current picking(So that we can get reverse entry against that)
                                pick.return_picking_id = new_return_picking_id.id
                                new_return_picking_id.return_picking_id = pick.id

                                # Update qty delivered
                                # self.qty_delivered = total_done_qty - abs(updated_product_qty)
        return True

    @api.multi
    def write(self, vals):
        """ Related to the update Sale Order Line if not in 'quotation'.
            1. group_kf_administrator can make any changes
            2. Quotes can be edited anytime.
            3. For Sales Order Lines
               a. All can make changes to Sales Order Line when the conversion date (x_order_confirmation_date) is in the current month and year
               b. All can make changes to product description and other NON-Money related fields in existing Sales Order Line which is not in the same conversion month....
                  - product description
                  - CFA i.e.     x_order_cfa
                  - CFA Boolean i.e. x_alt_address
               c. They can add a new Sales Order Line anytime
        """
        ctx = self.env.context.copy()
        vals_copy = copy.deepcopy(vals)
        product_id = False
        if 'product_id' in vals:
            product_id = self.env['product.product'].browse(vals.get('product_id'))
        else:
            if self.product_id:
                product_id = self.product_id

        if product_id and len(product_id.seller_ids) > 0:
            seller = product_id.seller_ids[0]
            vals.update({'x_vendor': seller.name.id})

        if product_id:
            vals.update({'product_tmpl_id': product_id.product_tmpl_id.id})

        ### 07.11.2021: merge Code from Other write Method (In this Sale Order Line class 2 - write method written...comment out other one)
        try:
            # Incase if x_order_confirmation_date field does not contains the time: 14:00:00 Then add explicitly
            if vals.get('x_order_confirmation_date'):
                sol_date_order_dtf = self.set_default_date_orderline_time(vals.get('x_order_confirmation_date'))
                if sol_date_order_dtf:
                    vals.update({'x_order_confirmation_date': sol_date_order_dtf})
        except Exception as e:
            _logger.debug(_("Error Exception in SaleOrderLine write(): %s"), e)
        #-----------------------------------------------------------------------
        # Check the context
        is_create_comms = False
        if self.env.context.get('biz_creating_comms_lines'):
            is_create_comms = True
            _logger.info("Creating commission lines.....%s", self.env.context.get('biz_creating_comms_lines'))

        is_create_sda = False
        if self.env.context.get('biz_creating_sda'):
            is_create_sda = True
            _logger.info("Creating SDA LINE.....%s", self.env.context.get('biz_creating_sda'))

        # SaleOrderLine update fields except 3 and state is in 'sale' then check for restriction/access right.11.01.2022
        is_kompass_admin = self.env.user.has_group('showroom_fields.group_kf_administrator')
        _logger.info(
            _(f"check for current logged-in user with 'KF_KOMPASS_ADMINISTRATOR' group permission: {is_kompass_admin}"))
        if (not is_kompass_admin) and (not is_create_comms) and (not is_create_sda):
            if vals.get('state') in ['sale', 'done'] or 'sale' in self.mapped('state') or 'done' in self.mapped('state'):
                # Remove these 3 field from vals_copy & no field is remaining then proceed
                # otherwise if any field is remaining that means user is trying to change other field too then restriction apply.
                # Also added income_commission_cal_amount in the field list. When an Accountant tries to re-create commissions in the
                # future months on the order, we should allow them to do so.....
                if vals_copy.get('x_order_cfa'):
                    vals_copy.pop('x_order_cfa')
                if vals_copy.get('x_alt_address'):
                    vals_copy.pop('x_alt_address')
                if vals_copy.get('name'):
                    vals_copy.pop('name')
                if vals_copy.get('income_commission_cal_amount'):
                    vals_copy.pop('income_commission_cal_amount')
                if vals_copy.get('income_comm_percent'):
                    vals_copy.pop('income_comm_percent')
                # If any other fields is trying to change then apply restrictions.
                if len(vals_copy) > 0:
                    # Check for the Conversion date-->Month, Year : If not in the current month then restriction apply
                    current_date = datetime.now()
                    _logger.debug("Current Date is .... %s", current_date)
                    user_tz    = pytz.timezone(self.env.user.tz or self.env.context.get('tz'))
                    _logger.debug("User TimeZone is .... %s", user_tz)
                    local_time = pytz.utc.localize(current_date).astimezone(user_tz)
                    _logger.debug("Local TimeZone is .... %s", local_time)
                    for sale_line_rec in self.filtered(lambda x: x.product_id.flag):
                        sale_conversion_date = vals.get('x_order_confirmation_date') and vals.get('x_order_confirmation_date') or sale_line_rec.x_order_confirmation_date
                        if sale_conversion_date:
                            if isinstance(sale_conversion_date, str):
                                sale_conversion_date = datetime.strptime(sale_conversion_date, "%Y-%m-%d %H:%M:%S")
                            if sale_conversion_date.year == local_time.year and sale_conversion_date.month == local_time.month:
                                pass
                            else:
                                error_msg = f"PLEASE NOTE!  This item line was converted in a prior month.  We cannot change historical sales figures for accounting purposes.  " \
                                            f"Pricing and Quantity changes need to be done by adding a new line.\n\n" \
                                            f"You have not permission to change the 'Sale Order Line' fields if conversion month is not current month of the same year.\n\n" \
                                            f"Fields - {list(vals_copy.keys())}\n\n"\
                                            f"(Document type: Sale Order Line, Operation: write) - (Records: [{sale_line_rec.id}], User: {self.env.user.id})"
                                raise AccessError(_(error_msg))

        res = super(SaleOrderLine, self).write(vals)

        ### Sale Order Line changes in quantity...Decrease...Same change in Stock.Picking-->Stock.Move product_qty or return picking for that qty
        if self.order_id.state in ['sale', 'done']:
            if self.order_id.picking_ids and 'product_uom_qty' in vals:
                qty = self._get_qty_procurement()
                updated_product_qty = self.product_uom_qty - qty
                if updated_product_qty < 0:
                    self.sale_line_stock_return_update(updated_product_qty)

            # Update associated invoice lines too if prce/qty get changed from SHIP/PACK/Other wizard.
            if self.env.context.get('freight_pack_data_update') and len(self.order_id.invoice_ids) > 0:
                sol_line_update = {self: vals}
                self.order_id.new_sol_updated = True
                self.order_id.update_account_invoice_line(sol_line_update)
                self.order_id.new_sol_updated = False

            return res
        return res

    @api.onchange('product_uom_qty', 'product_uom', 'route_id')
    def _onchange_product_id_check_availability(self):
        if not self.product_id or not self.product_uom_qty or not self.product_uom:
            self.product_packaging = False
            return {}
        return {}

    @api.onchange('product_id')
    def onchange_product_id_supplier(self):
        if self.product_id:
            product_id = self.product_id
            if (len(product_id.seller_ids) > 0):
                seller = product_id.seller_ids[0]
                self.x_vendor =  seller.name.id
                self.x_internal_cat1 = product_id.categ_id.id

    def create(self, vals):
        res = super(SaleOrderLine, self).create(vals)
        # Create Invoice Line if sale order is in 'confirmed' state and invoice -->account.move.line is created
        # For the created sale order line, the confirmation date should be the write date of the record and not
        # the confirmation date of the sale order. This way the adjusted sale order line can be in the future and
        # will be accounted for correctly in the sales reports, etc.
        if res.order_id.state == 'sale':
            _logger.debug("TRYING TO OVERWRITE x_order_confirmation_date to write_date ....... %s", res.write_date)
            user_tz    = pytz.timezone(self.env.context.get('tz') or self.env.user.tz)
            local_time = pytz.utc.localize(res.write_date).astimezone(user_tz)
            _logger.debug("Local time is ... %s", local_time)
            local_time_wo_tz = local_time.replace(tzinfo=None)
            _logger.debug("Local time is ... %s", local_time_wo_tz)
            res.x_order_confirmation_date = local_time_wo_tz

        if self.env.context.get('freight_pack_data') and res.order_id.state == 'sale' and len(res.order_id.invoice_ids) > 0:
            res.order_id.new_sol_updated = True
            res.order_id.create_invoice_lines_order_lines(res)
            res.order_id._ks_sale_payment_amount_update()
            res.order_id.new_sol_updated = False

        try:
            # Incase if x_order_confirmation_date field does not contains the time: 14:00:00 Then add explicitly
            if vals.get('x_order_confirmation_date'):
                sol_date_order_dtf = self.set_default_date_orderline_time(vals.get('x_order_confirmation_date'))
                if sol_date_order_dtf:
                    vals.update({'x_order_confirmation_date': sol_date_order_dtf})
        except Exception as e:
            _logger.debug(_("Error Exception in SaleOrderLine create(): %s"), e)
        return res

    def unlink(self):
        ctx = self.env.context.copy()
        order_id = self.order_id

        ### Unlink Sale order line which product is not commissionable after...First need to cancel the process
        # Unlink sale order line which commission is not created or commission is not paid.26.02.2021
        non_commissionable_sol_ids = self.filtered(lambda x: x.order_id.state in ['sale', 'done']) #and not x.product_id.flag)
        invoice_line_ids = False
        invoice_ids = False
        sale_commission_id = False
        sol_invoice_lines_ids = []
        if len(non_commissionable_sol_ids) > 0:
            invoice_line_ids = non_commissionable_sol_ids.mapped('invoice_lines')
            invoice_ids = non_commissionable_sol_ids.mapped('order_id').mapped('invoice_ids')
            return_sale_line_ids = []
            for sol_id in non_commissionable_sol_ids:
                sale_commission_id = sol_id.order_id.account_move_line_ids.filtered(lambda x: x.sale_line_id.id == sol_id.id) #x.com_pay_status == 'pending')
                delete_sale_line = False
                if len(sale_commission_id) > 0:
                    for sale_comm in sale_commission_id:
                        if not sale_comm.paid:
                            delete_sale_line = True
                        else:
                            raise UserError(_("You can not delete Sale Order Line !\n Commission line is 'Created' and 'Paid' for same Sale Order Line !"))
                else:
                    delete_sale_line = True
                if delete_sale_line:
                    sol_invoice_lines_ids.extend(sol_id.mapped('invoice_lines').mapped('id'))
                    sol_id.state = 'cancel'
                    return_sale_line_ids.append(sol_id)

            if len(return_sale_line_ids) > 0:
                ctx.update({'delete_sale_line': True})
                # stock delivery order return for that product qty if validated or remove stock.move if not validated after unreserve
                for sale_line_id in return_sale_line_ids:
                    sale_line_id.with_context(ctx).sale_line_stock_return_update(sale_line_id.product_uom_qty)

        res = super(SaleOrderLine, self).unlink()

        if len(non_commissionable_sol_ids) > 0:
            if invoice_ids and invoice_line_ids:
                self.invoice_cancel_validate_reconcile(invoice_ids, invoice_line_ids)

            # Delete commission line if not paid and posted
            if sale_commission_id:
                comm_move_id = sale_commission_id.mapped('move_id')
                if len(comm_move_id) > 0 : # and comm_move_id.state == 'draft':
                    comm_move_id.unlink()   # Delete Commission Journal Entry

        if order_id:
            ctx.update({'order_line_unlink': True})
            order_id.with_context(ctx).write({})
            order_id.onchange_order_line_fields()

        return res

    @api.onchange('x_tax_ids')
    def onchange_taxes_id(self):
        for rec in self:
            rec.tax_id = False
            # if rec.taxes_assigned_bool:
            rec.tax_id = rec.x_tax_ids.ids
            # else:
            #     rec.tax_id = rec.tax_id.ids + rec.x_tax_ids.ids

    @api.onchange('tax_id')
    def onchange_original_taxes_id(self):
        for rec in self:
            rec.x_tax_ids = False
            if len(rec.tax_id) > 0:
                rec.x_tax_ids = rec.tax_id.ids


    @api.multi
    def invoice_cancel_validate_reconcile(self, invoice_ids, invoice_line_ids):
        """
        Invoice Cancel, Validate and Reconcile Payment
        """
        ctx = self.env.context.copy()
        for inv_rec in invoice_ids:
            move_line_list = []
            ctx.update({'invoice_id': inv_rec.id, 'only_set_draft_state': True})
            if inv_rec.state in ['draft', 'cancel']:
                if inv_rec.state == 'cancel':
                    inv_rec.with_context(ctx).action_invoice_draft()
                if len(invoice_line_ids) > 0:
                    invoice_line_ids.unlink()
                    inv_rec._onchange_invoice_line_ids()
                    inv_rec._compute_amount()

            elif inv_rec.state in ['open', 'paid']:
                if inv_rec.payment_ids:
                    # First unreconcile and then update invoice lines then add payments
                    for inv_pay in inv_rec.payment_ids:
                        move_line_list.append(inv_pay.move_line_ids.filtered(lambda x: x.credit > 0))
                        inv_pay.move_line_ids.filtered(lambda x: x.credit > 0).with_context(ctx).remove_move_reconcile()

                # Cancel invoice & Again Reset to Draft...Make update into account.invoice.line and validate
                inv_rec.action_invoice_cancel()

                inv_rec.with_context(ctx).action_invoice_draft()

                ### Delete associated Invoice Lines with sale orde lines
                if len(invoice_line_ids) > 0:
                    invoice_line_ids.unlink()

                inv_rec._onchange_invoice_line_ids()
                inv_rec._compute_amount()
                inv_rec.action_invoice_open()
                if len(move_line_list) > 0:
                    for mv_line in move_line_list:
                        inv_rec.assign_outstanding_credit(mv_line.id)
        return True

    @api.onchange('product_uom', 'product_uom_qty')
    def product_uom_change(self):
        """
        This method is overriden from original method and changes on for the price.
        If price is aleady set, dont changes it otherwise take price from the product.
        """
        if not self.product_uom or not self.product_id:
            self.price_unit = 0.0
            return
        if self.order_id.pricelist_id and self.order_id.partner_id:
            product = self.product_id.with_context(
                lang=self.order_id.partner_id.lang,
                partner=self.order_id.partner_id,
                quantity=self.product_uom_qty,
                date=self.order_id.date_order,
                pricelist=self.order_id.pricelist_id.id,
                uom=self.product_uom.id,
                fiscal_position=self.env.context.get('fiscal_position')
            )
            if not self.price_unit:
                self.price_unit = self.env['account.tax']._fix_tax_included_price_company(self._get_display_price(product), product.taxes_id, self.tax_id, self.company_id)

    def _update_line_quantity(self, values):
        """
        Override this method from original only for avoid alert message
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        line_products = self.filtered(lambda l: l.product_id.type in ['product', 'consu'])
        # commented code
        # if line_products.mapped('qty_delivered') and float_compare(values['product_uom_qty'], max(line_products.mapped('qty_delivered')), precision_digits=precision) == -1:
        #     raise UserError(_('You cannot decrease the ordered quantity below the delivered quantity.\n'
        #                       'Create a return first.'))
        ORG_SOL._update_line_quantity(self, values) # Skipped method in sale_stock-->sale_order.py-->line: 477


    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        _logger.debug("Working on .... %s", self)
        res = super(SaleOrderLine, self)._compute_amount()
        for line in self:
            ###price = line.price_unit # * (1 - (line.discount or 0.0) / 100.0)
            ### Calculate Tax after discount amount...31.01.2021====================>>
            price = line.price_unit * (1 - (line.discount_show or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)

            discounted_amount = 0.0
            if not line.order_id.x_skip_commission:
                discounted_amount = taxes['total_excluded']

            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': line.price_unit * line.product_uom_qty, # taxes['total_excluded'], # 31.01.2021
                'discounted_price_subtotal': discounted_amount,   # Amount after discount
            })


    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = args or []

        # When search from the Sale Search Report then below code
        if self.env.context.get('sale_search_so'):
            order_args = [i for i in args if isinstance(i, list) and 'order_id' in i and 'ilike' in i]
            if len(order_args) > 0:
                order_args = order_args[0]
                if len(order_args) > 0:
                    order_search_name = order_args[2]
                    sale_search_id = self.env['sale.order'].search([('name', 'ilike', order_search_name)])
                    args.remove(order_args)
                    args.append(['order_id', 'in', sale_search_id.ids])

        return super(SaleOrderLine, self).search(args=args, offset=offset, limit=limit, order=order, count=count)


    @api.onchange('x_supplier_id')
    def onchange_x_supplier_id(self):
        for rec in self:
            rec.product_id = False


    @api.model
    def default_get(self, fields_values):
        res = super(SaleOrderLine, self).default_get(fields_values)
        try:
            # Set default to 'x_order_confirmation_date' timestamp as 14:00
            if res.get('x_order_confirmation_date'):
                sol_date_order_dtf = self.set_default_date_orderline_time(res.get('x_order_confirmation_date'))
                if sol_date_order_dtf:
                    res.update({'x_order_confirmation_date': sol_date_order_dtf})
        except Exception as e:
            _logger.debug(_("Error Exception in SaleOrderLine default_get(): %s"), e)
        return res


    # Comment out on 07.11.2021...Actually one write method was already available in Sale Order Line Class above.
    # @api.multi
    # def write(self, vals):
    #     try:
    #         # Incase if x_order_confirmation_date field does not contains the time: 14:00:00 Then add explicitly
    #         if vals.get('x_order_confirmation_date'):
    #             sol_date_order_dtf = self.set_default_date_orderline_time(vals.get('x_order_confirmation_date'))
    #             if sol_date_order_dtf:
    #                 vals.update({'x_order_confirmation_date': sol_date_order_dtf})
    #     except Exception as e:
    #         _logger.debug(_("Error Exception in SaleOrderLine write(): %s"), e)
    #     return super(SaleOrderLine, self).write(vals)


    @api.multi
    def set_default_date_orderline_time(self, sol_date_order):
        """
            This method will set time as 14:00:00 to 'x_order_confirmation_date'.)
        """
        now_dt = fields.Datetime.context_timestamp(self, datetime.now())
        if isinstance(sol_date_order, str):
            sol_date_order_datetime = datetime.strptime(sol_date_order, '%Y-%m-%d %H:%M:%S')
        else:
            sol_date_order_datetime = sol_date_order
        if str(sol_date_order_datetime.time()) != "14:00:00":
            _logger.info(_("Sale Order Line default date field value : %s"), sol_date_order)
            sol_date_order_str = now_dt.date().strftime('%Y-%m-%d') + " 14:00:00"
            sol_date_order_dtf = datetime.strptime(sol_date_order_str, '%Y-%m-%d %H:%M:%S')
            _logger.info(_("Updated datetime with 14:0:00 Time: %s"), sol_date_order_dtf)
            return sol_date_order_dtf
        return False
