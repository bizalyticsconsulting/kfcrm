from odoo import tools
from odoo import api, fields, models
import werkzeug.urls


class SaleOrderFixedView(models.Model):
    _name = "sale.order.fixed.view"
    _description = "Sale Order Fixed View"
    _auto = False
    _order = 'quote_confirm_date desc'

    partner_id          = fields.Many2one('res.partner', 'Customer', readonly=True)
    x_supplier          = fields.Many2one('res.partner', 'Supplier', readonly=True)
    name                = fields.Char('Quote/Order Number', readonly=True)
    x_product_names     = fields.Text('Products', readonly=True)
    # x_product_ids       = fields.Char('Products #', readonly=True)
    x_client_po         = fields.Char('Customer PO', readonly=True)
    x_mfg_order         = fields.Char('Supplier Order No.', readonly=True)
    quote_confirm_date  = fields.Datetime('Date')
    estmate_close_ecd   = fields.Char('Status')
    state               = fields.Selection([
                                ('draft', 'Quotation'),
                                ('sent', 'Quotation Sent'),
                                ('sale', 'Sales Order'),
                                ('done', 'Locked'),
                                ('cancel', 'Cancelled'),
                            ], string='Status', readonly=True)
    amount_total        = fields.Float('Total')
    user_id             = fields.Many2one('res.users', 'Salesperson', readonly=True)
    sale_order_id       = fields.Many2one('sale.order', 'Sale Order', readonly=True)
    branch_id           = fields.Many2one('res.branch', 'Showroom', readonly=True)
    company_id          = fields.Many2one('res.company', 'Company', readonly=True)


    def _query(self):
        ''' This method contains views query which will execute '''

        view_query = """
                         SELECT row_number() over (order by so.id) as id,
                                so.partner_id as partner_id,
                                so.x_supplier as x_supplier,
                                so.name as name,
                                so.x_client_po as x_client_po,
                                so.x_mfg_order as x_mfg_order,
                                so.state as state,
                                so.amount_total as amount_total,
                                so.user_id as user_id,
                                so.id as sale_order_id,
                                so.branch_id as branch_id,
                                so.company_id as company_id,
                                so.x_product_names as x_product_names,
                                
                                CASE When so.state in ('sale', 'done') Then so.confirmation_date
                                     Else so.date_order
                                     END as quote_confirm_date,
                            
                                CASE When so.state in ('sale', 'done') Then to_char(so.ecd, 'mm/dd/yyyy')
                                     Else sotr.tag_names
                                     END as estmate_close_ecd
                                
                         FROM   sale_order so
                                Join sale_order_tags_relation sotr on so.id = sotr.order_id
                     """
        return view_query


    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        view_query = """CREATE or REPLACE VIEW %s as (%s)""" % (self._table, self._query())
        self.env.cr.execute(view_query)


    @api.multi
    def action_open_sale_order(self):
        ctx = self.env.context.copy()
        action = {'type': 'ir.actions.act_window_close'}
        if self.sale_order_id:
            ctx.update({'create': 0, 'edit': 0, 'delete': 0})
            view_type = 'form'
            model = 'sale.order'
            menu_id = self.env.ref("sale.sale_menu_root")
            if self.sale_order_id.state in ['sale', 'done']:
                action_id = self.env.ref("sale.action_orders")
            else:
                action_id = self.env.ref("sale.action_quotations_with_onboarding")
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            record_url = base_url

            for rec in self:
                fragment = dict()
                if rec.sale_order_id:
                    fragment['id'] = rec.sale_order_id.id
                if action_id:
                    fragment['action'] = action_id.id
                if model:
                    fragment['model'] = model
                if view_type:
                    fragment['view_type'] = view_type
                if menu_id:
                    fragment['menu_id'] = menu_id.id

                record_url = "%s/web#%s" % (base_url, werkzeug.urls.url_encode(fragment))

            action = {
                'type': 'ir.actions.act_url',
                'target': 'new',
                'url': record_url,
                'context': ctx,
            }
        return action


    """ # View for Order and Tag relation
    CREATE or Replace VIEW sale_order_tags_relation AS select sot.order_id, string_agg(ct.name, ', ') as tag_names from sale_order_tag_rel sot
    inner join crm_lead_tag ct on sot.tag_id = ct.id
    group by sot.order_id
        
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO DATBASE_USER;
    
    # ref: https: // stackoverflow.com / questions / 2560946 / postgresql - group - concat - equivalent
    """
