from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def action_invoice_draft(self):
        if self.filtered(lambda inv: inv.state != 'cancel'):
            raise UserError(_("Invoice must be cancelled in order to reset it to draft."))
        # go from canceled state to draft state
        self.write({'state': 'draft', 'date': False})
        # Delete former printed invoice
        try:
            report_invoice = self.env['ir.actions.report']._get_report_from_name('account.report_invoice')
        except IndexError:
            report_invoice = False

        if report_invoice and report_invoice.attachment:
            for invoice in self:
                with invoice.env.do_in_draft():
                    # this will only call if process the sale order line(order -->state in 'sale'/'done')...delete
                    if not self.env.context.get('only_set_draft_state'):
                        invoice.number, invoice.state = invoice.move_name, 'open'
                    attachment = self.env.ref('account.account_invoices').retrieve_attachment(invoice)
                if attachment:
                    attachment.unlink()
        return True


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    name = fields.Html(string='Description', required=True)
    discounted_price_subtotal = fields.Monetary(string='Discounted Subtotal (without Taxes)',
                                                store=True, readonly=True,
                                                compute='_compute_price',
                                                help="Discounted Subtotal amount without taxes")

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity',
                 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
                 'invoice_id.date_invoice', 'invoice_id.date')
    def _compute_price(self):
        sale_ids = self.env['sale.order'].search([('name','=',self.invoice_id.origin)])
        if sale_ids:
            for line in sale_ids[0].order_line:
                self.total_charges =  line.total_charges if line.total_charges else 0.0
        currency = self.invoice_id and self.invoice_id.currency_id or None
        price = self.price_unit
        discounted_unit_price = self.price_unit  * (1 - (self.discount or 0.0) / 100.0)

        taxes = False
        if self.invoice_line_tax_ids:
            taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id,
                                                          partner=self.invoice_id.partner_id)

            # Use for store discounted subtotal
            new_taxes_cal = self.invoice_line_tax_ids.compute_all(discounted_unit_price, currency, self.quantity, product=self.product_id,
                                                          partner=self.invoice_id.partner_id)

        self.price_subtotal = price_subtotal_signed = taxes['total_excluded'] if taxes else self.quantity * price

        # Use for store discounted subtotal
        if sale_ids and not sale_ids.x_skip_commission:
            # pass
            self.discounted_price_subtotal = new_taxes_cal['total_excluded'] if taxes else self.quantity * discounted_unit_price # New field to store the discounted subtotal without the Tax

        if self.invoice_id.currency_id and self.invoice_id.company_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
            #In odoo-10 _get_currency_rate_date() method is not found so that, changes this code as date_invoice or date 20.02.2019
            # price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id._get_currency_rate_date()).compute(price_subtotal_signed,self.invoice_id.company_id.currency_id)
            price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id.date_invoice or self.invoice_id.date).compute(price_subtotal_signed,self.invoice_id.company_id.currency_id)
        sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
        self.price_subtotal_signed = price_subtotal_signed * sign

