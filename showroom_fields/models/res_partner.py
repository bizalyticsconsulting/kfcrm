from odoo import models, fields, api, tools


class ResPartner(models.Model):
    _inherit = 'res.partner'

    x_cust_sidemark = fields.Char(string="Sidemark", size=150, help="Customer Sidemark", index=1)
    sale_line_ids = fields.One2many('sale.order.line', 'order_partner_id', 'Sales Order Lines')
    total_sale_line_amount = fields.Monetary(compute='_compute_sale_report_amount', string="Total Sale Line Amount")
    x_resale_num = fields.Char(string="Resale Number", size=100, help="ReSale Number", index=1)
    x_resale_date = fields.Date(string="Resale Number Date", help="Resale Number Date")
    x_com_ship_to = fields.Many2one("res.partner", string="Com ship to", index=1)
    x_sales_territory_id = fields.Many2one('sales.territory', string="Sales Territory", help="Sales Territory")
    x_sales_territory = fields.Char(string="Sales Territory", size=100, help="Sales Territory", index=1)
    zip = fields.Char(change_default=True, index=1)
    city = fields.Char(index=1)
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=?', country_id)]", index=1)
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', index=1)
    email = fields.Char(index=1)
    customer = fields.Boolean(string='Is a Customer', default=True, index=1, help="Check this box if this contact is a customer. It can be selected in sales orders.")
    supplier = fields.Boolean(string='Is a Vendor', index=1, help="Check this box if this contact is a vendor. It can be selected in purchase orders.")
    company_name = fields.Char('Company Name', index=1)
    type = fields.Selection([('contact', 'Contact'), ('invoice', 'Invoice address'), ('delivery', 'Shipping address'), ('other', 'Other address'), ("private", "Private Address"),], string='Address Type', default='contact', index=1, help="Used by Sales and Purchase Apps to select the relevant address depending on the context.")
    x_outside_sales = fields.Many2one('res.users', string="Outside Sales (Salesperson2)", index=True, help="Outside Sales (Salesperson2)")
    x_com_shipto_bool = fields.Boolean(string='COM Ship To ?')
    x_instagram_handle = fields.Char(string="IG", size=100, help="Instagram handle", index=1)
    x_instagram_handle_url = fields.Char(string="IG URL", size=500, help="Instagram URL")


    @api.onchange('x_instagram_handle')
    def instagram_handle_change(self):
        for rec in self:
            rec.x_instagram_handle_url = False
            if rec.x_instagram_handle:
                rec.x_instagram_handle_url = "https://www.instagram.com/" + rec.x_instagram_handle

    # dropship_ids = fields.One2many('sale.order','dropship_id','Dropship')
    # @api.multi
    # def name_get(self):
    #    res = []
    #    for partner in self:
    #       name = partner.name or ''
    #       name = partner._display_address(without_company=True)
    #       name = name.replace('\n\n', '\n')
    #       if partner.name and partner.street:
    #          name = partner.name + ' - ' + partner.street
    #       if partner.name and partner.commissions_percentage:
    #          name = name + " (" + str(partner.commissions_percentage) + "%)"
    #       res.append((partner.id, name))
    #    return res
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        if self._context.get('order_line') or self._context.get('x_prod_category'):
            '''
            x_com_ship_to = False
            x_prod_category_id = self._context.get('x_prod_category')
            if not x_prod_category_id:
                if self._context.get('order_line')[0][2]:
                    x_product_id = self.env['product.product'].browse(self._context.get('order_line')[0][2].get('product_id'))
                    x_prod_category_id = x_product_id.categ_id.id
                else:
                    sale_line_id = self.env['sale.order.line'].browse(self._context.get('order_line')[0][1])
                    x_prod_category_id = sale_line_id.product_id.categ_id.id
            x_prod_category_res = self.env['product.category'].browse(x_prod_category_id)
            if len(x_prod_category_res) > 0:
                x_com_ship_to = x_prod_category_res and x_prod_category_res.x_com_ship_to or False

            # If Product Category contains x_xom_ship_to.Take same in SaleOrder Com Ship To Drop-down..Else Take From Suppliers
            if x_com_ship_to:
                args = [['id', '=', x_com_ship_to.id]]
            else:
            ''' # comment on 26.06.2021.

            # If Sale.order 'Supplier' Contacts have 'x_com_shipto_bool' is True then take same in the 'Com SHip To address' drop-down
            if self.env.context.get('order_supplier_id'):
                order_supplier_id = self.browse(self.env.context.get('order_supplier_id'))
                partner_comship_ids = order_supplier_id.child_ids.filtered(lambda x: x.type == 'delivery' and x.x_com_shipto_bool) #(lambda x: x.x_com_ship_to)
                if len(partner_comship_ids) > 0:
                    args = [['id', 'in', partner_comship_ids.ids]]
                else:
                    args = [['id', 'in', [0]]]
                    '''
                    partner_delivery_ids = order_supplier_id.child_ids.filtered(lambda x: x.type == 'delivery')
                    if len(partner_delivery_ids) > 0:
                        args = [['id', 'in', partner_delivery_ids.ids]]
                    '''

            # # Old Code...## This code is commented for #736 ticket ...14.06.2021
            # if self._context.get('order_line')[0][2]:
            #     product_id = self.env['product.product'].browse(self._context.get('order_line')[0][2].get('product_id'))
            #     com_ship_to = product_id.product_tmpl_id.categ_id.x_com_ship_to
            #     if com_ship_to:
            #         args = [['id', '=', com_ship_to.id]]
            #     else:
            #         res_partner_id = [rec.x_com_ship_to.id for rec in self.env['res.partner'].search([('x_com_ship_to','!=',False)]) if rec.x_com_ship_to]
            #         args = [['id', 'in', res_partner_id]]

        return super(ResPartner, self).name_search(name=name, args=args, operator=operator, limit=limit)

    @api.model
    def default_get(self, fields):
        defaults = super(ResPartner, self).default_get(fields)
        res_partner = self.env['res.partner'].browse(self.env.context.get('sale_order_partner_id'))
        if res_partner and res_partner.company_type == 'company':
            defaults['parent_id'] = self.env.context.get('sale_order_partner_id')
        return defaults



    @api.model
    def create(self, vals):
        if self.env.context.get('sale_order_partner_id'):
            res_partner = self.env['res.partner'].browse(self.env.context.get('sale_order_partner_id'))
            if res_partner and res_partner.company_type == 'company':
                vals.update({'parent_id':self.env.context.get('sale_order_partner_id')})
        elif self.env.context.get('sale_order_shipping_parent_id'):
            vals.update({'partner_type':'ship to', 'company_type':'company', 'type':'delivery'})

        partners = super(ResPartner, self).create(vals)
        return partners

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        if self.env.context.get('partner_contact_domain') and self.env.context.get('sale_order_partner_id'):
            so_partner_id = self.browse(self.env.context.get('sale_order_partner_id'))
            if so_partner_id:
                res_partner_ids = so_partner_id.ids
                if so_partner_id.child_ids:
                    res_partner_ids.extend(so_partner_id.child_ids.ids)
                    args += [('id', 'in', res_partner_ids)]

        # Code for Filter the Customer & Supplier in Sale Order, Quotation and Product Search/Filter View some fields
        # if self.env.context.get('so_customer_search_ctx'):
        if self.env.context.get('so_supplier_ctx') or self.env.context.get('product_supplier_ctx'):
            args = [('supplier', '=', True), ('parent_id', '=', False), ('is_company', '=', True)]
        if self.env.context.get('so_customer_ctx'):
            args = [('customer', '=', True), ('parent_id', '=', False), ('is_company', '=', True)]
        if self.env.context.get('so_partner_invoice_ctx'):
            args = [('active', '=', True), ('company_type', '=', 'company')]
        if self.env.context.get('so_partner_shipping_ctx'):
            args = ['&', '|', ('partner_type', '=', 'ship to'), ('type', '=', 'delivery'), ('active', '=', True)]

        # This is useful when search in sale order based on the Customer and Supplier both
        if self.env.context.get('so_supplier_ctx') and self.env.context.get('so_customer_ctx'):
            args = [('parent_id', '=', False), '|', ('supplier', '=', True), ('customer', '=', True)]

        return super(ResPartner, self).name_search(name=name, args=args, operator=operator, limit=limit)


    @api.depends('name', 'email')
    def _compute_email_formatted(self):
        for partner in self:
            if partner.email:
                try:
                    partner.email_formatted = tools.formataddr((partner.name or u"False", partner.email or u"False"))
                except Exception as e:
                    print ('Error: ',e)
                    partner.email_formatted = ''
                    # partner.email_formatted = tools.formataddr((partner.name or u"False", partner.email.encode('ascii', 'ignore').decode('ascii') or u"False"))
            else:
                partner.email_formatted = ''

    @api.model
    def _get_address_format(self):
        return self._get_default_address_format()

    def _get_addr_str(self, addr_str, address):
        if addr_str:
            addr_str = addr_str + ' | ' + address
        else:
            addr_str = address
        return addr_str

    def _get_address_str(self):
        addr_str = ''
        if self.parent_id:
            addr_str = self._get_addr_str(addr_str, self.parent_id.name)
        if self.street:
            addr_str = self._get_addr_str(addr_str, self.street)
        if self.street2:
            addr_str = self._get_addr_str(addr_str, self.street2)
        if self.city:
            addr_str = self._get_addr_str(addr_str, self.city)
        if self.state_id:
            addr_str = self._get_addr_str(addr_str, self.state_id.name)
        if self.zip:
            addr_str = self._get_addr_str(addr_str, self.zip)
        if self.branch_id:
            addr_str = self._get_addr_str(addr_str, 'SHRM:' + self.branch_id.name)

        return addr_str

    def _get_name(self):
        """ Utility method to allow name_get to be overrided without re-browse the partner """
        partner = self
        name = partner.name or ''

        if partner.company_name or partner.parent_id:
            if not name and partner.type in ['invoice', 'delivery', 'other']:
                name = dict(self.fields_get(['type'])['type']['selection'])[partner.type]
            # if not partner.is_company:
            #     name = self._get_contact_name(partner, name)
        if self._context.get('show_address_only'):
            name = partner._display_address(without_company=True)
        if self._context.get('show_address'):
            addrs_str = partner._get_address_str()
            if addrs_str:
                name = name + ' | ' + addrs_str
            #name = name + "\n" + partner._display_address(without_company=True)
        name = name.replace('\n\n', '\n')
        name = name.replace('\n\n', '\n')
        if self._context.get('address_inline'):
            name = name.replace('\n', ', ')
        if self._context.get('show_email') and partner.email:
            name = "%s %s" % (name, partner.email)
        if self._context.get('html_format'):
            name = name.replace('\n', '<br/>')
        if self._context.get('show_vat') and partner.vat:
            name = "%s ‒ %s" % (name, partner.vat)
        return name


    def _compute_sale_report_amount(self):
        for partner in self:
            sale_line_amount = sum(partner.sale_line_ids.filtered(lambda x: x.order_id.state != 'cancel').mapped('price_total'))
            partner.total_sale_line_amount = sale_line_amount
            # domain = "[('state', '!=', 'cancel'), ('x_internal_cat1', 'not ilike', 'MISC')]"


    def action_view_partner_sale_report(self):
        """
        This method will Open Sale Report Action and its Tree view
        """
        self.ensure_one()
        action = self.env.ref('showroom_fields.sale_order_line_report_action').read()[0]
        action['domain'] = [('order_partner_id', '=', self.id)]
        return action
