from odoo import fields, models


class ProductCategory(models.Model):
    _inherit = 'product.category'

    min_lead_time = fields.Float("Min lead time")
    max_lead_time = fields.Float("Max lead time")
    x_com_ship_to = fields.Many2one('res.partner', string="COM ShipTo Address", index=True, domain=[('supplier', '=', 'True'), ('type', '=', 'delivery')], help="COM ShipTo Address")
    x_payment_terms = fields.Many2one('account.payment.term', 'Payment Terms')
    x_freight_terms = fields.Many2one('account.payment.term', 'Freight Terms')
    x_ship_via = fields.Many2one('delivery.carrier', 'Ship Via')
