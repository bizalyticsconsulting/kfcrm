from odoo.exceptions import ValidationError
from odoo import models, api, _
import logging
_logger = logging.getLogger(__name__)


class ProductProduct(models.Model):
    _inherit = "product.product"

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        if self.env.context.get('x_supplier_ref_id'):
            # Get only Products which is associated with the Supplier
            ProductSupplierInfo = self.env['product.supplierinfo']
            productinfo_seller_ids = ProductSupplierInfo.search([('name', '=', self.env.context.get('x_supplier_ref_id'))])
            product_tmpl_ids = productinfo_seller_ids.mapped('product_tmpl_id')
            product_ids = product_tmpl_ids.mapped('product_variant_ids')
            args += [('id', 'in', product_ids.ids)]
        return super(ProductProduct, self).name_search(name=name, args=args, operator=operator, limit=limit)

    @api.multi
    def get_clean_sku(self):
        clean_sku = self.default_code
        if self.seller_ids:
            pref_supplier = self.seller_ids[0]
            supplier = pref_supplier.name
            if supplier.partner_code:
                ref_code = supplier.partner_code
                repl_string = ref_code + '-'
                clean_sku = clean_sku.replace(repl_string, '')
                repl_string = '-' + ref_code
                clean_sku = clean_sku.replace(repl_string, '')
        return clean_sku
    # @api.multi
    # def name_get(self):
    #    # TDE: this could be cleaned a bit I think
    #
    #    def _name_get(d):
    #       name = d.get('name', '')
    #       code = self._context.get('display_default_code', True) and d.get('default_code', False) or False
    #       if code:
    #          name = '[%s] %s' % (name,code)
    #       return (d['id'], name)
    #
    #    partner_id = self._context.get('partner_id')
    #    if partner_id:
    #       partner_ids = [partner_id, self.env['res.partner'].browse(partner_id).commercial_partner_id.id]
    #    else:
    #       partner_ids = []
    #
    #    # all user don't have access to seller and partner
    #    # check access and use superuser
    #    self.check_access_rights("read")
    #    self.check_access_rule("read")
    #
    #    result = []
    #
    #    # Prefetch the fields used by the `name_get`, so `browse` doesn't fetch other fields
    #    # Use `load=False` to not call `name_get` for the `product_tmpl_id`
    #    self.sudo().read(['name', 'default_code', 'product_tmpl_id', 'attribute_value_ids', 'attribute_line_ids'],
    #                     load=False)
    #
    #    product_template_ids = self.sudo().mapped('product_tmpl_id').ids
    #
    #    if partner_ids:
    #       supplier_info = self.env['product.supplierinfo'].sudo().search([
    #          ('product_tmpl_id', 'in', product_template_ids),
    #          ('name', 'in', partner_ids),
    #       ])
    #       # Prefetch the fields used by the `name_get`, so `browse` doesn't fetch other fields
    #       # Use `load=False` to not call `name_get` for the `product_tmpl_id` and `product_id`
    #       supplier_info.sudo().read(['product_tmpl_id', 'product_id', 'product_name', 'product_code'], load=False)
    #       supplier_info_by_template = {}
    #       for r in supplier_info:
    #          supplier_info_by_template.setdefault(r.product_tmpl_id, []).append(r)
    #    for product in self.sudo():
    #       # display only the attributes with multiple possible values on the template
    #       variable_attributes = product.attribute_line_ids.filtered(lambda l: len(l.value_ids) > 1).mapped(
    #          'attribute_id')
    #       variant = product.attribute_value_ids._variant_name(variable_attributes)
    #
    #       name = variant and "%s (%s)" % (product.name, variant) or product.name
    #       sellers = []
    #       if partner_ids:
    #          product_supplier_info = supplier_info_by_template.get(product.product_tmpl_id, [])
    #          sellers = [x for x in product_supplier_info if x.product_id and x.product_id == product]
    #          if not sellers:
    #             sellers = [x for x in product_supplier_info if not x.product_id]
    #       if sellers:
    #          for s in sellers:
    #             seller_variant = s.product_name and (
    #                     variant and "%s (%s)" % (s.product_name, variant) or s.product_name
    #             ) or False
    #             mydict = {
    #                'id': product.id,
    #                'name': seller_variant or name,
    #                'default_code': s.product_code or product.default_code,
    #             }
    #             temp = _name_get(mydict)
    #             if temp not in result:
    #                result.append(temp)
    #       else:
    #          mydict = {
    #             'id': product.id,
    #             'name': name,
    #             'default_code': product.default_code,
    #          }
    #          result.append(_name_get(mydict))
    #    return result

    @api.model
    def create(self, vals):
        # Check for the Duplication of Default Code
        _logger.debug("In product.product .......")
        _logger.debug(vals)
        if vals.get('product_tmpl_id'):
            _logger.debug("Will get the default_code just fine ......")
        else:
            if vals.get('default_code'):
                exist_default_code = self.search(
                    [('default_code', '=', vals.get('default_code'))])
                if exist_default_code:
                    raise ValidationError(_(
                        "The given 'Part Number / Internal Reference already exist.\n Please provide different 'Part Number / Internal Reference'."))
            else:
                raise ValidationError(_(
                    "'Part Number / Internal Reference cannot be blank.\n Please provide 'Part Number / Internal Reference'."))

            # Validation for empty Sellerinfo while creating from form view
            if 'seller_ids' in vals and (len(self.seller_ids) == 0 and len(vals.get('seller_ids')) == 0):
                raise ValidationError(
                    _("Create failed - Please provide Vendor details for this product (Purchase Tab)."))
            if 'seller_ids' not in vals and len(self.seller_ids) == 0:
                raise ValidationError(
                    _("Create failed - Please provide Vendor details for this product (Purchase Tab)."))

        res = super(ProductProduct, self).create(vals)
        return res
    # @api.multi
    # def name_get(self):
    #    # TDE: this could be cleaned a bit I think
    #
    #    def _name_get(d):
    #       name = d.get('name', '')
    #       code = self._context.get('display_default_code', True) and d.get('default_code', False) or False
    #       if code:
    #          name = '%s' % (name)
    #       return (d['id'], name)
    #
    #    partner_id = self._context.get('partner_id')
    #    if partner_id:
    #       partner_ids = [partner_id, self.env['res.partner'].browse(partner_id).commercial_partner_id.id]
    #    else:
    #       partner_ids = []
    #
    #    # all user don't have access to seller and partner
    #    # check access and use superuser
    #    self.check_access_rights("read")
    #    self.check_access_rule("read")
    #
    #    result = []
    #
    #    # Prefetch the fields used by the `name_get`, so `browse` doesn't fetch other fields
    #    # Use `load=False` to not call `name_get` for the `product_tmpl_id`
    #    self.sudo().read(['name', 'default_code', 'product_tmpl_id', 'attribute_value_ids', 'attribute_line_ids'],
    #                     load=False)
    #
    #    product_template_ids = self.sudo().mapped('product_tmpl_id').ids
    #
    #    if partner_ids:
    #       supplier_info = self.env['product.supplierinfo'].sudo().search([
    #          ('product_tmpl_id', 'in', product_template_ids),
    #          ('name', 'in', partner_ids),
    #       ])
    #       # Prefetch the fields used by the `name_get`, so `browse` doesn't fetch other fields
    #       # Use `load=False` to not call `name_get` for the `product_tmpl_id` and `product_id`
    #       supplier_info.sudo().read(['product_tmpl_id', 'product_id', 'product_name', 'product_code'], load=False)
    #       supplier_info_by_template = {}
    #       for r in supplier_info:
    #          supplier_info_by_template.setdefault(r.product_tmpl_id, []).append(r)
    #    for product in self.sudo():
    #       # display only the attributes with multiple possible values on the template
    #       variable_attributes = product.attribute_line_ids.filtered(lambda l: len(l.value_ids) > 1).mapped(
    #          'attribute_id')
    #       variant = product.attribute_value_ids._variant_name(variable_attributes)
    #
    #       name = variant and "%s (%s)" % (product.name, variant) or product.name
    #       sellers = []
    #       if partner_ids:
    #          product_supplier_info = supplier_info_by_template.get(product.product_tmpl_id, [])
    #          sellers = [x for x in product_supplier_info if x.product_id and x.product_id == product]
    #          if not sellers:
    #             sellers = [x for x in product_supplier_info if not x.product_id]
    #       if sellers:
    #          for s in sellers:
    #             seller_variant = s.product_name and (
    #                     variant and "%s (%s)" % (s.product_name, variant) or s.product_name
    #             ) or False
    #             mydict = {
    #                'id': product.id,
    #                'name': seller_variant or name,
    #                'default_code': s.product_code or product.default_code,
    #             }
    #             temp = _name_get(mydict)
    #             if temp not in result:
    #                result.append(temp)
    #       else:
    #          mydict = {
    #             'id': product.id,
    #             'name': name,
    #             'default_code': product.default_code,
    #          }
    #          result.append(_name_get(mydict))
    #    return result

    # @api.model
    # def create(self, vals):
    #    # Check for the Duplication of Default Code
    #    _logger.debug("In product.product .......")
    #    _logger.debug(vals)
    #    if vals.get('product_tmpl_id'):
    #       _logger.debug("Will get the default_code just fine ......")
    #    else:
    #       if vals.get('default_code'):
    #          exist_default_code = self.search([('default_code', '=', vals.get('default_code'))])
    #          if exist_default_code:
    #             raise ValidationError(_(
    #                "The given 'Part Number / Internal Reference already exist.\n Please provide different 'Part Number / Internal Reference'."))
    #       else:
    #          raise ValidationError(_(
    #             "'Part Number / Internal Reference cannot be blank.\n Please provide 'Part Number / Internal Reference'."))
    #
    #       # Validation for empty Sellerinfo while creating from form view
    #       if 'seller_ids' in vals and (len(self.seller_ids) == 0 and len(vals.get('seller_ids')) == 0):
    #          raise ValidationError(_("Create failed - Please provide Vendor details for this product (Purchase Tab)."))
    #       if 'seller_ids' not in vals and len(self.seller_ids) == 0:
    #          raise ValidationError(_("Create failed - Please provide Vendor details for this product (Purchase Tab)."))
    #
    #    res = super(ProductProduct, self).create(vals)
    #    return res