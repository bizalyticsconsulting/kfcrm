from odoo import models, fields, api
from datetime import date


class Producttemplate(models.Model):
    _inherit = 'product.template'

    first_supplier = fields.Many2one('res.partner', string='Supplier', store=True, compute='_display_first_supplier', context="{'product_supplier_ctx': True}")
    x_product_intro_date = fields.Date(string="Introduction Date", help="Product Introduction Date", required=False, index=True)
    x_product_arch_date = fields.Date(string="Archive Date", help="Product Archive Date", required=False, index=True)

    @api.multi
    def write(self, vals):
        if vals.get('active') == False:
            todays_date = date.today().strftime("%m/%d/%y")
            vals.update({'x_product_arch_date': todays_date})
        else:
            vals.update({'x_product_arch_date': None})
        return super(Producttemplate, self).write(vals)

    @api.multi
    @api.depends('seller_ids')
    def _display_first_supplier(self):
        for product in self:
            if product.seller_ids:
                product.first_supplier = product.seller_ids[0].name.id