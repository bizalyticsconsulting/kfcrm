from odoo import models, fields, api, _
from odoo.exceptions import UserError


class SalePaymentCancelWizard(models.TransientModel):
    _name = 'sale.payment.cancel.wizard'


    payment_line_ids = fields.One2many("sale.payment.cancel.wizard.line", "salepay_wizard_id")
    sale_order_id = fields.Many2one('sale.order', "Sale Order")


    @api.multi
    def action_cancel_payment(self):
        if len(self.payment_line_ids) == 0:
            raise UserError(_('There no payment associated with selected Sale Order !'))
        if not any([i for i in self.payment_line_ids if i.cancel_check]):
            raise UserError(_("Please tick to 'Cancel Payment' box in 'Payment Line' to process the cancel for selected payments !"))

        for pay_line in self.payment_line_ids.filtered(lambda x: x.cancel_check):
            if pay_line.payment_id:
                pay_line.payment_id.cancel()

        self.sale_order_id._ks_sale_payment_amount_update()

        return True


class SalePaymentCancelWizardLine(models.TransientModel):
    _name = 'sale.payment.cancel.wizard.line'


    salepay_wizard_id = fields.Many2one("sale.payment.cancel.wizard", "Salepay Wizard")
    payment_date = fields.Date(string='Payment Date')
    journal_id = fields.Many2one('account.journal', string='Journal')
    memo = fields.Char('Memo')
    amount_paid = fields.Float(string="Amount")
    account_move_id = fields.Many2one("account.move", string="Account Journal")
    invoice_id = fields.Many2one("account.invoice", string="Invoice")
    partner_id = fields.Many2one("res.partner", string="Partner")
    cancel_check = fields.Boolean(string="Cancel Payment", default=True)
    payment_id = fields.Many2one("account.payment", "Payment")

