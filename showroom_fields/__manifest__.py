{
    'name': "Showroom Fields",
    'summary': """Module adds custom fields to Quotes and Sales Orders for the Showrooms.""",
    'description': """
	The fields are 
        - Terms
        - Freight Terms
        - FOB
        - Attention —> Customer Contact
        - Client PO
        - Client Account
        - Sidemark
        - Mfg Order
        - Mfg Order Date
        - Vendor Invoice Number
        - Vendor Invoice Date
        - Special Instructions
        - CFA
        - Salesperson 1
        - Salesperson 2
        - Ship Date
        - Tracking Number
    """,
    'author': "BizAlytics Consulting LLC",
    'website': "http://www.bizalytics.com",
    'category': 'Sales',
    'version': '0.1',
    'depends': ['base','sale','crm','delivery','purchase','product','sales_team','account','branch','pragmatic_state_county',
                'ks_oneclick_sale_purchase','gt_special_instruction', 'sms', 'mail', 'float_number_discount','gt_studio_it_import_fields'],
    'data': [
        'security/ir.model.access.csv',
        'security/sale_security.xml',
        'report/report.xml',
        #'report/kf_quote.xml',
        'report/kf_reserve.xml',
        #'report/kf_back_order.xml',
        'report/kf_custom_order.xml',
        #'report/kf_custom_quote.xml',
        #'report/kf_hospitality_invoice.xml',
        #'report/kf_hospitality_order.xml',
        #'report/kf_hospitality_performa_invoice.xml',
        #'report/kf_hospitality_quote.xml',
        #'report/kf_hospitality_reserve.xml',
        'report/kf_invoice.xml',
        'report/kf_order.xml',
        #'report/kf_contract_order.xml',
        'report/kf_performa_invoice.xml',
        #'report/kf_non_residential_quote.xml',
        #'report/kf_non_residential_reserve.xml',
        #'report/kf_non_residential_custom_quote.xml',
        #'report/kf_non_residential_proforma_invoice.xml',
        #'report/kf_non_residential_order.xml',
        #'report/kf_non_residential_custom_order.xml',
        #'report/kf_non_residential_invoice.xml',
        ##'report/kf_memo_quote_order.xml',
        ##'report/kf_memo_order.xml',
        'report/email_templates.xml',
        'views/showroom_fields_view.xml',
        'views/templates.xml',
        'wizard/sale_payment_cancel_wizard_view.xml',
        'views/res_partner_view.xml',
        'views/product_view.xml',
        ##'views/sale_order_fixed_view.xml',
        'views/sale_order_view.xml',
        'views/assets.xml',
        'views/menu_changes_view.xml',
        'views/sales_territory_view.xml',
    ],
}
