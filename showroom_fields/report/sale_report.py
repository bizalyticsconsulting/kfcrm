from odoo import fields, models


class SaleReport(models.Model):
    _inherit = "sale.report"


    partner_id = fields.Many2one('res.partner', 'Customer', readonly=True, context="{'so_customer_ctx': True}")
