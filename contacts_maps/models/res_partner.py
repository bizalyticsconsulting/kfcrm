# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
import logging
_logger = logging.getLogger(__name__)

class ResPartner(models.Model):
    _inherit = 'res.partner'

    marker_color = fields.Char(
        string='Marker Color', default='red', required=True)

    @api.model
    def action_scheduler_geocode(self):
        customers = self.env['res.partner'].search(['|', ('parent_id', '=', False), ('is_company', '=', True),
                                                    ], order="name asc")
        for partner in customers:
            _logger.warning("Working on %s", partner.name)
            partner.geo_localize()